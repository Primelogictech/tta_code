<?php

use App\Http\Controllers\Admin\HomePage\BannerController;
use App\Http\Controllers\Admin\HomePage\VenueController;
use App\Http\Controllers\Admin\HomePage\MessageController;
use App\Http\Controllers\Admin\Menu\MenuController;
use App\Http\Controllers\Admin\Menu\SubMenuController;
use App\Http\Controllers\Admin\Menu\PageController;
use App\Http\Controllers\Admin\DesignationController;
use App\Http\Controllers\Admin\DonortypeController;
use App\Http\Controllers\Admin\InviteeController;
use App\Http\Controllers\Admin\VouchersController;
use App\Http\Controllers\Admin\LeadershipTypeController;
use App\Http\Controllers\Admin\SponsorCategoryTypeController;
use App\Http\Controllers\Admin\LeftLogoController;
use App\Http\Controllers\Admin\RightLogoController;
use App\Http\Controllers\Admin\HomePage\ProgramController;
use App\Http\Controllers\Admin\HomePage\EventController;
use App\Http\Controllers\Admin\HomePage\ScheduleController;
use App\Http\Controllers\Admin\HomePage\DonorController;
use App\Http\Controllers\Admin\HomePage\VideoController;
use App\Http\Controllers\Admin\HomePage\MemberController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\AdminRegistrationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\Admin\PaymenttypeController;
use App\Http\Controllers\Admin\SponsorCategoryController;
use App\Http\Controllers\Admin\PaymentController;
use App\Http\Controllers\Admin\BenefittypeController;
use App\Http\Controllers\Admin\CommitteController;
use App\Http\Controllers\Admin\ExhibitorTypeController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\SouvenirRegistrationController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'] );
Route::get('otp_send', [RegisteredUserController::class, 'send_verify_email']);
Route::post('new_registertion', [RegisteredUserController::class, 'new_registertion']);
Route::post('update_password', [RegisteredUserController::class, 'update_password']);


Route::get('convention_details', function () {
   return view('convention_details');});

Route::get('forgotpassword', function () {
   return view('auth.forgotpassword');});

 Route::get('exhibits-registration-export', [AdminRegistrationController::class, 'ExhibitRegistrationsExport']);
 Route::get('cruise-registration-export', [AdminRegistrationController::class, 'CruiseRegistrationsExport']);

Route::get('convention-registration-export', [AdminRegistrationController::class, 'ConventionRegistrationsExport']);
Route::get('satamanam-bhavati-registration-export', [AdminRegistrationController::class, 'satamanamBhavatiRegistrationsExport']);





Route::prefix('admin')->middleware(['admin', 'auth'])->group(function () {


    Route::get('/', [DashboardController::class, 'index'] );

    Route::resource('banner', BannerController::class);
    Route::put('banner-status-update', [BannerController::class ,'updateStatus']);

    Route::resource('venue', VenueController::class);

    Route::resource('message', MessageController::class);
    Route::put('message-status-update', [MessageController::class , 'updateStatus']);

    Route::resource('menu', MenuController::class);
    Route::put('menu-status-update', [MenuController::class, 'updateStatus']);

    Route::resource('submenu', SubMenuController::class);
    Route::put('submenu-status-update', [SubMenuController::class, 'updateStatus']);
    Route::get('get-submenu-with-menu/{id}/{edit?}', [SubMenuController::class, 'GetSubmenuWithMenu']);

    Route::resource('page', PageController::class);
    Route::put('page-status-update', [PageController::class, 'updateStatus']);

    Route::resource('donor', DonorController::class);
    Route::put('donor-status-update', [DonorController::class, 'updateStatus']);

    Route::post('donorImport', [RegistrationController::class, 'ImportRegesterUserStore']);
    Route::get('importMemberMail', [RegistrationController::class, 'importMemberMail']);

    Route::resource('video', VideoController::class);
    Route::put('video-status-update', [VideoController::class, 'updateStatus']);

    Route::resource('member', MemberController::class);
    Route::put('member-status-update', [MemberController::class, 'updateStatus']);
    Route::get('assigne-roles/{id}', [MemberController::class, 'showAssigneRolesPage']);
    Route::post('store-assigne-roles', [MemberController::class, 'storeAssigneRolesPage']);

    // master routes
    Route::resource('designation', DesignationController::class);
    Route::put('designation-status-update', [DesignationController::class, 'updateStatus']);

    Route::resource('donortype', DonortypeController::class);
    Route::put('donortype-status-update', [DonortypeController::class, 'updateStatus']);

    Route::resource('invitee', InviteeController::class);
    Route::put('invitee-status-update', [InviteeController::class, 'updateStatus']);

    Route::resource('vouchers', VouchersController::class);
    Route::put('vouchers-status-update', [VouchersController::class, 'updateStatus']);

    Route::resource('leadership-type', LeadershipTypeController::class);
    Route::put('leadership-type-status-update', [LeadershipTypeController::class, 'updateStatus']);

    Route::resource('sponsor-category-type', SponsorCategoryTypeController::class);
    Route::put('sponsor-category-type-status-update', [SponsorCategoryTypeController::class, 'updateStatus']);

    Route::resource('left-logo', LeftLogoController::class);
    Route::put('left-logo-status-update', [LeftLogoController::class, 'updateStatus']);
    Route::resource('right-logo', RightLogoController::class);
    Route::put('right-logo-status-update', [RightLogoController::class, 'updateStatus']);

    Route::resource('program', ProgramController::class);
    Route::put('program-status-update', [ProgramController::class, 'updateStatus']);

    Route::resource('event', EventController::class);
    Route::put('event-status-update', [EventController::class, 'updateStatus']);

    Route::resource('schedule', ScheduleController::class);

    Route::resource('sponsor-category', SponsorCategoryController::class);
    Route::put('sponsor-category-status-update', [SponsorCategoryController::class, 'updateStatus']);

    Route::resource('paymenttype', PaymenttypeController::class);
    Route::put('paymenttype-status-update', [PaymenttypeController::class, 'updateStatus']);

    Route::resource('benefittype', BenefittypeController::class);
    Route::put('benefittype-status-update', [BenefittypeController::class, 'updateStatus']);

    Route::resource('committe', CommitteController::class);
    Route::put('committe-status-update', [CommitteController::class, 'updateStatus']);

    Route::resource('exhibitor-type', ExhibitorTypeController::class);
    Route::put('exhibitor-type-status-update', [ExhibitorTypeController::class, 'updateStatus']);



    Route::get('registrations', [RegistrationController::class, 'showRegistrations']);
    Route::get('assigne-features/{id}', [RegistrationController::class, 'showAssigneFeatures']);
    Route::put('assigne-features-update', [RegistrationController::class, 'updateAssigneFeatures']);
    Route::get('exhibit-registrations', [AdminRegistrationController::class, 'exhibitRegistrations']);
    Route::get('cruise_registrations', [AdminRegistrationController::class, 'cruiseRegistrations']);

    Route::get('youth-activities-registrations', [AdminRegistrationController::class, 'youthActivitiesRegistrations']);

    Route::get('add-note/{id}', [RegistrationController::class, 'showAddNoteForm']);
    Route::post('add-note', [RegistrationController::class, 'storeAddNoteForm'])->name('registration.addnote');

    Route::get('payment-history/{user_id}', [RegistrationController::class, 'adminSidePaymentHistory']);
    Route::put('update-payment-status/{payment_id}', [PaymentController::class, 'updatePaymentStatus']);
     Route::put('update-cruisepayment-status/{id}', [PaymentController::class, 'updatecruisepaymentStatus']);
    Route::get('downlode-banner-image/{id}', [RegistrationController::class, 'download']);
    
});


 Route::get('private_convention_members', [AdminRegistrationController::class, 'private_convention_members']);
 Route::get('private_exhibits_members', [AdminRegistrationController::class, 'private_exhibits_members']);

 Route::prefix('admin')->middleware(['admin'])->group(function () {
     Route::get('add-note/{id}', [RegistrationController::class, 'showAddNoteForm']);
    Route::post('add-note', [RegistrationController::class, 'storeAddNoteForm'])->name('registration.addnote');
     Route::get('payment-history/{user_id}', [RegistrationController::class, 'adminSidePaymentHistory']);
      Route::put('update-payment-status/{payment_id}', [PaymentController::class, 'updatePaymentStatus']);
     Route::get('assigne-features/{id}', [RegistrationController::class, 'showAssigneFeatures']);
    Route::put('assigne-features-update', [RegistrationController::class, 'updateAssigneFeatures']);
    Route::get('update_notes', [RegistrationController::class, 'update_notes']);
    Route::get('update_tickets', [RegistrationController::class, 'update_tickets']);
    Route::get('auditlogs', [HomeController::class, 'auditLogs']);
 });

Route::get('reload-captcha', [HomeController::class, 'reloadCaptcha']);

Route::get('payment', [PaymentController::class, 'index']);
Route::get('payment/status', [PaymentController::class, 'status'])->name('payment.status');
Route::get('paypal/status', [PaymentController::class, 'paypalStatus'])->name('paypal.status');
Route::get('paypal/cruisestatus', [PaymentController::class, 'cruisestatus'])->name('paypal.cruisestatus');
Route::get('padingpayment/status', [PaymentController::class, 'pendingstatus'])->name('pendingpayment.status');
Route::get('print-ticket', [RegistrationController::class, 'printTicket']);


/* Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
 */

require __DIR__.'/auth.php';

Route::middleware(['auth'])->group(function () {
    Route::get('viewfeatuees', [RegistrationController::class, 'viewFeatures'])->name('viewfeatures');
    Route::get('paymenthistory', [RegistrationController::class, 'PaymentHistory'])->name('PaymentHistory');
    Route::get('pay-pending-amount', [PaymentController::class, 'payPendingAmount'])->name('pay-pending-amount');
    Route::post('pay-pending-amount', [PaymentController::class, 'storePayPendingAmount'])->name('pay-pending-amount');
    Route::get('myaccount', [RegistrationController::class, 'myaccount'])->name('myaccount');
    Route::get('upgrade', [RegistrationController::class, 'showUpgradeForm'])->name('upgrade');
    Route::post('upgrade', [RegistrationController::class, 'storeUpgradeForm'])->name('upgrade');
    
    Route::get('reg-pkg-upgrade', [RegistrationController::class, 'showRegPkgUpgradeForm'])->name('reg-pkg-upgrade');
    Route::post('reg-pkg-upgrade', [RegistrationController::class, 'storeRegPkgUpgradeForm'])->name('reg-pkg-upgrade');
   

    Route::get('youthActivities-regestration', [RegistrationController::class, 'youthActivitiesRegestration']);
    Route::post('youthActivities-regestration', [RegistrationController::class, 'storeYouthActivitiesRegestration']);
});

Route::get('/bookticket', function () {
    return redirect('/registration');
});

Route::get('registration', [RegistrationController::class, 'ShowRegistrationForm'])->name('registration');

Route::get('/exhibits-reservation', function () {
    return redirect('/vendorexhibits');
});


Route::get('vendorexhibits', [RegistrationController::class, 'exibitRegestration']);

Route::get('show-registration-details/{id}', [RegistrationController::class, 'showRegistrationDetails']);
Route::get('email_verification', [RegistrationController::class, 'email_verification'])->name('email_verification');
Route::get('voucher_verification', [RegistrationController::class, 'voucher_verification'])->name('voucher_verification');
Route::get('send-otp-for-login', [RegistrationController::class, 'sendOtpForLogin'])->name('send-otp-for-login');

Route::get('registration-success', [RegistrationController::class, 'registrationsuccess']);
Route::post('exhibits-reservation', [RegistrationController::class, 'stoteExibitRegestrationForm']);
Route::post('update_banner_image', [RegistrationController::class, 'updateBannerImage']);
Route::get('selected-packages/{id}', [AdminRegistrationController::class, 'selected_packages']);



Route::get('souvenir', [SouvenirRegistrationController::class, 'souvenirRegestration']);
Route::post('souvenir-reservation', [SouvenirRegistrationController::class, 'storesouvenirRegestrationForm']);
Route::get('souvenir-email-verification', [SouvenirRegistrationController::class, 'email_verification']);
Route::post('souvenir_new_registertion', [SouvenirRegistrationController::class, 'new_registertion']);

Route::post('bookticketstore', [RegistrationController::class, 'RegesterUserStore']);

Route::get('registration-page-content-update', [RegistrationController::class, 'RegistrationPageContentedit'])->name('registration-page-content-update');
Route::patch('registration-page-content-update', [RegistrationController::class, 'RegistrationPageContentUpdate'])->name('registration-page-content-update');





Route::get('message_content/{id}', [HomeController::class, 'messageContent']);
Route::get('programs/{id}', [HomeController::class, 'programDetailsPage']);
Route::get('event-schedule/{id}', [HomeController::class, 'eventSchedule']);
Route::get('show-all-videos', [HomeController::class, 'showAllVideos']);
Route::get('more-donor-details/{id}', [HomeController::class, 'moreDonerDetails']);
Route::get('more-leadership-details/{id}', [HomeController::class, 'moreLeadershipDetails']);
Route::get('more-invitee-details/{id}', [HomeController::class, 'moreInviteeDetails']);
Route::get('more-committe-members/{id}', [HomeController::class, 'moreCommitteMembers']);
Route::get('show-events-on-calander', [HomeController::class, 'showEventsOnCalander']);



Route::get('api/get-schedule-on-date/{id}', [ScheduleController::class, 'getScheduleDatails']);
Route::get('api/get-donor-with-typeid/{id}', [HomeController::class, 'getDonorsWithTypeId']);
Route::get('api/get-leadership-with-typeid/{id}', [HomeController::class, 'getLeadershipsWithTypeId']);
Route::get('api/get-invitee-with-typeid/{id}', [HomeController::class, 'getinviteeWithTypeId']);
Route::get('api/get-invitee-with-typeid/{id}', [HomeController::class, 'getinviteeWithTypeId']);

Route::get('api/get-events-in-date', [HomeController::class, 'geteventsIndates']);
Route::get('api/get-details', [RegistrationController::class, 'getdetailsfromNriva']);


Route::get('/contact-us', [HomeController::class, 'showContactUs']);
Route::post('/contact-us', [HomeController::class, 'ContactUsSendMail']);

Route::get('/ttastar', function () {return view('registration.showttaStarRegistrationForm'); });

Route::get('/convention_committee_list', function () {return view('committees.all_committee'); });
Route::get('/banquet', function () {return view('committees.banquet_committee'); });
Route::get('/budgetfinance', function () {return view('committees.budget_n_finance_committee'); });
Route::get('/businessforum', function () {return view('committees.business_forum_committee'); });
Route::get('/celebritiescoordination', function () {return view('committees.celebrities_coordination_committee'); });
Route::get('/cultural', function () {return view('committees.cultural_committee'); });
Route::get('/cme', function () {return view('committees.cme_committee'); });
Route::get('/corporatesponsorship', function () {return view('committees.corporate_sponsorship_committee'); });
Route::get('/decorations', function () {return view('committees.decorations_committee'); });
Route::get('/donorrelationship', function () {return view('committees.donor_relationship_committee'); });
Route::get('/food', function () {return view('committees.food_committee'); });
Route::get('/fundraising', function () {return view('committees.fund_raising_committee'); });
Route::get('/hospitality', function () {return view('committees.hospitality_committee'); });
Route::get('/immigrationforum', function () {return view('committees.immigration_forum_committee'); });
Route::get('/kidsactivities', function () {return view('committees.kids_activities_committee'); });
Route::get('/literacy', function () {return view('committees.literary_committee'); });
Route::get('/matrimonial', function () {return view('committees.matrimonial_committee'); });
Route::get('/mediacommunication', function () {return view('committees.media_n_communication_committee'); });
Route::get('/overseascoordination', function () {return view('committees.overseas_coordination_committee'); });
Route::get('/politicalforum', function () {return view('committees.political_forum_committee'); });
Route::get('/programevents', function () {return view('committees.program_n_events_committee'); });
Route::get('/reception', function () {return view('committees.reception_committee'); });
//Route::get('/registration', function () {return view('committees.registration_committee'); });
Route::get('/safetysecurity', function () {return view('committees.safety_n_security_committee'); });
Route::get('/shortfilm', function () {return view('committees.short_film_committee'); });
Route::get('/socialmedia', function () {return view('committees.social_media_committee'); });
//Route::get('/souvenir', function () {return view('committees.souvenir_committee'); });
Route::get('/spiritual', function () {return view('committees.spiritual_committee'); });
Route::get('/stage', function () {return view('committees.stage_committee'); });
Route::get('/startupcube', function () {return view('committees.startup_forum_committee'); });
Route::get('/transportation', function () {return view('committees.transportation_committee'); });
// Route::get('/ttastar', function () {return view('committees.tta_star_committee'); });
//Route::get('/vendorexhibits', function () {return view('committees.vender_n_exhibits_committee'); });
Route::get('/volunteer', function () {return view('committees.volunteer_committee'); });
Route::get('/web', function () {return view('committees.web_committee'); });
Route::get('/womensforum', function () {return view('committees.womens_forum_committee'); });
Route::get('/youthforum', function () {return view('committees.youth_forum_committee'); });
Route::get('/page-under', function () {return view('committees.page_under'); });

Route::get('/events-schedule', function () {return view('events_schedule'); });



Route::get('/clear', function() {

    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    return "Cleared!";
});
Route::get('cruise_registration', [RegistrationController::class, 'cruise_registration']);
Route::post('cruise_registration', [RegistrationController::class, 'submit_cruise_registration']);


Route::put('first_data_transactions', [PaymentController::class, 'first_data_transactions']);







Route::get('{slug}', [HomeController::class, 'dinamicPage']);




















