-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2021 at 03:21 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tta_convention`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'banner_1.jpg', 1, '2021-09-08 23:53:18', '2021-09-17 03:12:42'),
(2, 'banner_2.jpg', 1, '2021-09-08 23:53:18', '2021-09-17 03:12:43'),
(3, 'banner_3', 0, '2021-09-08 23:53:18', '2021-09-17 03:12:44');

-- --------------------------------------------------------

--
-- Table structure for table `benefit_types`
--

CREATE TABLE `benefit_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_count` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'has count->1 no count->0',
  `has_input` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'has input->1 no input->0',
  `count` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benefit_types`
--

INSERT INTO `benefit_types` (`id`, `name`, `has_count`, `has_input`, `count`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Type 1', 0, 1, NULL, 1, '2021-09-22 03:38:08', '2021-09-22 03:38:08'),
(2, 'Benefit type 2', 0, 0, NULL, 1, '2021-09-22 03:38:40', '2021-09-22 03:38:40');

-- --------------------------------------------------------

--
-- Table structure for table `benefit_user`
--

CREATE TABLE `benefit_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `benfit_id` bigint(20) UNSIGNED NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `benfit_sponsor_category`
--

CREATE TABLE `benfit_sponsor_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sponsor_category_id` bigint(20) UNSIGNED NOT NULL,
  `benfit_id` bigint(20) UNSIGNED NOT NULL,
  `count` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'President', 1, '2021-09-08 23:53:17', '2021-09-16 07:44:16'),
(2, 'President Elect', 1, '2021-09-08 23:53:17', '2021-09-16 07:44:31'),
(3, 'Convenor', 1, '2021-09-08 23:53:17', '2021-09-16 07:44:39');

-- --------------------------------------------------------

--
-- Table structure for table `donors`
--

CREATE TABLE `donors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `donortype_id` bigint(20) UNSIGNED NOT NULL,
  `donor_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donors`
--

INSERT INTO `donors` (`id`, `donortype_id`, `donor_name`, `description`, `image_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'fJPyJTs409mrnoVCXHwpeOFZpc1Cb4pyzGva5o08TFa7Vj3MJgWYRloQfkLA1Rep2rYpkdaYme91IZAFsXK0FlEptOjJYyBf16FW', 'Similique vero ut nihil dolorum fuga reprehenderit. Quibusdam quisquam nostrum ipsam. Et aut non hic nesciunt.', 'c7ykekoYF0UaoT6syqdWI8wb0NQD9X6rc10D5snXBOC3OLtHfNfYUNUXHDsKR13EvK4XLfff7ScqVjLlcoKdhBo2Lt7ePFQ0NTac', 1, '2021-09-08 23:53:15', '2021-09-08 23:53:15'),
(2, 2, 'OP98suh57fd8CG0YThqyyzkTUEtCjnSSlnuNGwFbX6SYCV8Llc9M95qkadBClXK5OPdNhNfcMQn82UziGEb8QoKmIQeyXzgjPsYN', 'Sapiente aut voluptatum ad deleniti repudiandae rerum et quis. Dolorem commodi facilis animi ut ipsa ut incidunt. Tempora ea non quas sit quidem aperiam. Sed ut eligendi et possimus.', 'NuqKsp8N6xHTMZOo2seeMkLRUFYRzxbSfnaxDP6jSCX1nqmrIrLbzMky9UMCq4Ou3sGxD75wan7CnxhQsMHYcinmyt178fFgbryy', 1, '2021-09-08 23:53:15', '2021-09-08 23:53:15'),
(3, 3, 'WdaMMOMR10BbVKvH68gkkfOQi2bZ6DQzYQWJ3YuPYtTvPjoJ0jxSp5tov2Au0LTSeQmfpXQYEhakPEPud1CaD2b3sWBvJcF8IM9v', 'Voluptatem aut in animi inventore voluptas. Totam quas est qui necessitatibus nesciunt dolore enim. Voluptas perspiciatis voluptatem libero. Id optio corrupti illum iste sed.', '0e5ofUT5B3dtWyQlxfFC23v4E495S5Bp25I9A0thwiomZYvhkSwq4UQqckwKFevMM2tPnOiX0bwXYRF9bsF872MHmKJkbeE4rPGN', 1, '2021-09-08 23:53:15', '2021-09-08 23:53:15'),
(4, 4, 'sCYNYvZcb8uIJeJz2KT6HMU4rnKrnsoLjYlFi1hnHYek9ZkA5xmYDrE4vFtnCmpdsgGkbuJWCiqmzsXG9ZbhQvpykY4qcF7YPo3M', 'Iure repellat dolore culpa voluptatem. Reprehenderit beatae natus nesciunt quo eveniet.', 'L2qGkgop40V9DIHmo0KwDJpobOniM7iUgCKz4Hxc7lAE9BpnKcrijrdxI4OlsF5utl9ahcLGJb5PcmE6zFsv2KOyjK0IjQZuDVQK', 1, '2021-09-08 23:53:15', '2021-09-08 23:53:15'),
(5, 5, 'G6BO4AoNLqmfZNdz0OYQX6HK0O3FGOA9OfZC1mr31oC5RXWfD1RZZqtVADJlzzn9mwdmjekfkHZUGKPWiBYenHol7avQiyyDeqv5', 'Blanditiis voluptatem non molestiae facilis assumenda qui voluptatum vel. Ut modi in et. Architecto dolorem consequatur ipsum magnam.', 'x9BSOCMj8dzLNJZPVf1MEKsrRxqWWbJ3oJ6D3oc92ihbvfzBhhplBpL0l0yzmNKjcqrwGw1siEJZFTQrdMdDjpP8nmc0GOxKVRu9', 1, '2021-09-08 23:53:15', '2021-09-08 23:53:15');

-- --------------------------------------------------------

--
-- Table structure for table `donortypes`
--

CREATE TABLE `donortypes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `donortypes`
--

INSERT INTO `donortypes` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Ms. Euna Medhurst', 0, '2021-09-08 23:53:14', '2021-09-08 23:53:14'),
(2, 'Prof. Frank Breitenberg PhD', 1, '2021-09-08 23:53:14', '2021-09-08 23:53:14'),
(3, 'Lupe Nolan', 0, '2021-09-08 23:53:14', '2021-09-08 23:53:14'),
(4, 'Chance Armstrong', 0, '2021-09-08 23:53:15', '2021-09-08 23:53:15'),
(5, 'Ernestine Rosenbaum', 1, '2021-09-08 23:53:15', '2021-09-08 23:53:15'),
(6, 'Miss Viva Cassin', 1, '2021-09-08 23:53:18', '2021-09-08 23:53:18'),
(7, 'Amina Abshire', 1, '2021-09-08 23:53:18', '2021-09-08 23:53:18'),
(8, 'Theodore Kreiger MD', 0, '2021-09-08 23:53:18', '2021-09-08 23:53:18');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `event_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `image_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `from_date`, `to_date`, `description`, `status`, `image_url`, `created_at`, `updated_at`) VALUES
(1, 'Breakfast and Matrimony', '2021-09-17 00:00:00', '2021-09-17 00:00:00', '<p>Breakfast and MatrimonyBreakfast and MatrimonyBreakfast and MatrimonyBreakfast and Matrimony</p>', 1, 'event_1.jpg', '2021-09-10 10:33:38', '2021-09-17 03:29:39'),
(2, 'Business conference, Women’s Con...', '2021-09-17 00:00:00', '2021-09-17 00:00:00', '<p>Business conference, Women&rsquo;s Con...Business conference, Women&rsquo;s Con...Business conference, Women&rsquo;s Con...</p>', 1, 'event_2.jpg', '2021-09-10 10:34:34', '2021-09-17 03:29:52'),
(3, 'Sri Laxmi Narasimha Swamy Kalyanam', '2021-09-17 00:00:00', '2021-09-17 00:00:00', '<p>Sri Laxmi Narasimha Swamy Kalyanam Sri Laxmi Narasimha Swamy Kalyanam Sri Laxmi Narasimha Swamy Kalyanam</p>', 1, 'event_3.jpg', '2021-09-10 10:35:33', '2021-09-17 03:29:26');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invitees`
--

CREATE TABLE `invitees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invitees`
--

INSERT INTO `invitees` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Prof. Alda Ankunding DVM', 0, '2021-09-08 23:53:16', '2021-09-08 23:53:16'),
(2, 'Elmira Schultz', 0, '2021-09-08 23:53:16', '2021-09-08 23:53:16'),
(3, 'Vanessa Olson', 1, '2021-09-08 23:53:16', '2021-09-08 23:53:16');

-- --------------------------------------------------------

--
-- Table structure for table `leadership_types`
--

CREATE TABLE `leadership_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leadership_types`
--

INSERT INTO `leadership_types` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Convention Advisory Committee', 1, '2021-09-08 23:53:17', '2021-09-16 08:01:51'),
(2, 'Convention Executive Committee', 1, '2021-09-08 23:53:17', '2021-09-16 07:58:31'),
(3, 'Convention Regional Advisors', 1, '2021-09-08 23:53:17', '2021-09-16 08:10:09');

-- --------------------------------------------------------

--
-- Table structure for table `logos`
--

CREATE TABLE `logos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'left',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logos`
--

INSERT INTO `logos` (`id`, `image_url`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'logo_1.png', 'left', 1, '2021-09-08 23:53:17', '2021-09-17 03:37:21'),
(2, 'logo_1', 'left', 1, '2021-09-08 23:53:17', '2021-09-08 23:53:17'),
(3, 'logo_3.png', 'right', 1, '2021-09-17 03:35:01', '2021-09-17 03:35:01');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `image_url`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Ewald Leffler', 'member_1.jpg', '<p>Ab mollitia ad in necessitatibus. Sunt sunt repellendus ratione ea est consequuntur numquam harum. Odit deserunt atque qui quasi repudiandae nam totam. Et est velit quae.</p>', 1, '2021-09-08 23:53:13', '2021-09-17 03:22:04'),
(2, 'Adan Kihn V', 'member_2.jpg', '<p>Consequuntur qui hic sed aut autem. Blanditiis ut minima quo autem. Et fugit sit similique dicta. Consectetur aut nam non nobis ut sunt.</p>', 1, '2021-09-08 23:53:14', '2021-09-17 03:22:32'),
(3, 'Sylvia Howe', 'member_3.jpeg', '<p>Iure repellat dolorem dignissimos voluptas consequatur quas vero quisquam. Tempore voluptas in consequatur vitae eum. Accusamus repudiandae quia enim est illum.</p>', 1, '2021-09-08 23:53:14', '2021-09-17 03:22:47'),
(4, 'Dino Schulist', 'member_4.png', '<p>Sed maxime dolore consectetur voluptatibus. Laudantium et saepe et perspiciatis nesciunt. Dolores commodi quaerat aut rerum. Et aut ut repellat.</p>', 1, '2021-09-08 23:53:14', '2021-09-17 03:23:14'),
(5, 'Mr. Kellen Stamm', 'member_5.png', '<p>Et recusandae omnis aut dolor. Sit omnis debitis nam voluptates consequatur. Qui omnis dolorum consectetur magni iure exercitationem aut. Facere esse occaecati maiores exercitationem aut.</p>', 1, '2021-09-08 23:53:14', '2021-09-17 03:23:27'),
(6, 'Santhosh Pathuri', 'member_6.jpg', '<p>Description1</p>', 1, '2021-09-16 08:00:02', '2021-09-16 08:00:03'),
(7, 'Manohar Kasagani', 'member_7.png', '<p>Manohar Kasagani,</p>', 1, '2021-09-16 08:00:43', '2021-09-17 03:24:17');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `page_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `slug`, `parent_id`, `page_content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Home', NULL, 0, NULL, 1, '2021-09-09 06:27:45', '2021-09-09 06:27:45'),
(2, 'Registration', NULL, 0, NULL, 1, '2021-09-09 06:28:20', '2021-09-09 06:28:20'),
(3, 'Team', NULL, 0, NULL, 1, '2021-09-09 06:28:32', '2021-09-09 06:28:32'),
(4, 'Speakers', NULL, 0, NULL, 1, '2021-09-09 06:28:48', '2021-09-09 06:28:48'),
(5, 'Programs', 'programs', 0, NULL, 1, '2021-09-09 06:29:04', '2021-09-11 05:18:32'),
(6, 'Exhibits', NULL, 0, NULL, 1, '2021-09-09 06:29:17', '2021-09-09 06:29:17'),
(7, 'registration 1', 'registration-1', 2, NULL, 1, '2021-09-11 05:35:26', '2021-09-11 05:35:26'),
(8, 'Venue', NULL, 0, NULL, 1, '2021-09-14 08:55:24', '2021-09-14 08:55:24');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation_id` bigint(20) UNSIGNED NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `designation_id`, `message`, `image_url`, `status`, `created_at`, `updated_at`) VALUES
(1, 'President Message', 1, '<p>With great privilege and honor, I assume the role of President of the Telangana American Telugu Association for the next two years.</p>', 'Message_1.jpg', 1, '2021-09-08 23:53:18', '2021-09-17 03:14:56'),
(4, 'president-elect message', 2, '<p>With great privilege and honor, I assume the role of President of the Telangana American Telugu Association for the next two years....</p>', 'Message_4.jpg', 1, '2021-09-16 07:47:13', '2021-09-16 07:47:13'),
(5, 'Convenor Message', 3, '<p>With great privilege and honor, I assume the role of President of the Telangana American Telugu Association for the next two years.</p>', 'Message_5.jpg', 1, '2021-09-16 07:48:01', '2021-09-16 07:48:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_08_23_183230_create_banners_table', 1),
(6, '2021_08_24_061501_create_venues_table', 1),
(7, '2021_08_24_085914_create_designations_table', 1),
(8, '2021_08_24_085918_create_messages_table', 1),
(9, '2021_08_26_073612_create_menus_table', 1),
(10, '2021_08_28_120444_create_donortypes_table', 1),
(11, '2021_08_28_124854_create_invitees_table', 1),
(12, '2021_08_30_065031_create_leadership_types_table', 1),
(13, '2021_08_30_074526_create_sponsor_category_types_table', 1),
(14, '2021_08_30_123957_create_logos_table', 1),
(15, '2021_09_01_052109_create_programs_table', 1),
(16, '2021_09_01_085813_create_events_table', 1),
(17, '2021_09_01_085814_create_schedules_table', 1),
(18, '2021_09_02_065917_create_donors_table', 1),
(19, '2021_09_02_105413_create_videos_table', 1),
(20, '2021_09_03_074401_create_members_table', 1),
(21, '2021_09_03_100522_create_roles_and_designations_table', 1),
(22, '2021_09_08_065148_create_sponsor_categories_table', 2),
(23, '2021_09_09_041230_create_registration_page_content_table', 2),
(24, '2021_09_13_061957_create_payment_types_table', 2),
(25, '2021_09_13_123623_create_payments_table', 2),
(26, '2021_09_17_071823_create_benefit_types_table', 3),
(27, '2021_09_17_091828_create_benfit_sponsor_category_table', 3),
(28, '2021_09_20_075917_create_benefit_user_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `payment_amount` double(8,2) NOT NULL,
  `payment_methord` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unique_id_for_payment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'rererence id for zelle, payment_id for paypal',
  `more_info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `chair_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `co_chair_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Location` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `page_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `chair_name`, `mobile_number`, `co_chair_name`, `program_name`, `Location`, `image_url`, `date`, `page_content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'KKoufKzk1TRehMJQnbHqlboKRBc1v2Wwhc0PjsyHLtbzwOpRhzxV5t6v3nOzNkYX93a4nWdkIdEWo7Y5L6SNZVp0HahejZKXhnHE', 'F0zEPbaxfQKJghQ2wyd4', 'VdFL5cYpmCbyWKAmhdDiFrNnPb2oQoQIpNMULQD59izY5uEBkonWjkr0rEMGbLjLtg7GCKgRECjnLTUeBbXfoYveO36EjYltu1Ne', 'GGayL1ueRTjStoCbp481', '5DyzJAQARz5uGXS6R5ibqFo4CGKdSkjC0S1sTFDmR8XIryHBNYni2KpvS7sLumooX3TjwAkI23rKIyZqjorQ9t4jruN1GswhYToh7RQaWAxUOWCLI4JX4ghSlXePYOITmLNeFagCzTgRdDsfTsqvaDUAmwEVaUhdoAEqfLDb0uW9qlK2Xf2pSH2eXjav2kzsT5Tkf2cR', NULL, '2009-06-29', 'Natus rerum recusandae quam dolore id laudantium. Consequatur quis sint praesentium omnis. Itaque inventore et hic ea eum.', 1, '2021-09-08 23:53:16', '2021-09-08 23:53:16'),
(2, 'kMe9rFlodsaBoEIp33sfuvII8f1rhCCTZLH22dQSSQ7m782lWfgpyYjLgTrloGJfB9NjwGr9nqspYpL0Ycl9JIOUSQ2NWHfGHFPB', 'jO5tFh6tHJOF8fzfMT6V', 'H2J9ly0LhQBgoEBxuu5pN9w0gge6MPkn9pyzMK5KUSJWDU8q1aRg4rDo4LN4in2YqYzxpVcEpTBfpLnRevbcXxRw6xUrGRJZgVxT', 'wtbLyBiPidhMdxLzGLTL', 'll2VU4DA3H4JiwTXQermT9rYkGQZiWWIh4kc2ldiqXXME8PJBYrBzDy2uHQsbSsAO8s0uqO0W0g8rMwK9nz6ohHfQrl6DTYW6CKfSEv6G2BKTSeJmWak6Zr2jjGiuqPHxXYnQsaEP0YMfawwEGP7FUzbPJg4BlR5Lu324TSISwVBYR4O4WEgi1JPP9tqR4Ir0JoDrrSc', NULL, '1976-12-31', 'Libero nemo adipisci placeat harum quae quam. Explicabo qui quidem assumenda aut ex aliquam. Eligendi et eum ut voluptas. Qui asperiores atque qui eos voluptas eum. Et autem est quo quia.', 1, '2021-09-08 23:53:16', '2021-09-08 23:53:16'),
(3, 'lSlZtyzdEgGONeksV59HZ2GupOFEQ95Sq41kkorXaURoYWdfV0uJTnLizSZNhmGSCpJPJ7PBJQLmJy5QEOnIAFn792ulzzJ0HYjg', 'aEyFEfL5YDIhybrbDfdp', 'PO1HE5zKX6uRj75CrfORzVlb30UelB6ogeQWpRXuqqioMsZjpZ9GvpDgXEnzQXQML3LyIrafsfuS8ZVy3xWMZDUT1wtnLRdCDgLp', 'baOXpcnsv6BhapqxOyFO', 'wsfPUq2yYiDphaGIItSJp1DY94V6ABu3bl0QS3uWIzL3CcEM56oZ17etFYYS5PZVS6OytN5Ia32hGpK0n29Y25meB6OXDNcZlzntUWCwspgtM8nHjPNr2ofGYB19XVZTHlPNsSGMgOrvM5jpk2csxS7gp2Beq24vFFLqmbMLP6fxqi2b2hClNkwRI0ayLOOGNNL28g7E', NULL, '1972-06-15', 'Ipsum dicta harum libero perspiciatis consequatur. Itaque nihil velit mollitia odit et impedit eos. Similique quod id sint et perspiciatis.', 1, '2021-09-08 23:53:16', '2021-09-08 23:53:16'),
(4, 'vNVSMiXggO2Igq9YuOYjBUCQ63pthB8Iyr8X83pc9L5wopBRhcUt8pZDhxlAMfJvheXvF11zQtns2EfENVvSDW4GYgCrB3rci9qn', 'XaxHzLpcktnwTUVYZlT2', 'FNDzd9T7iGotHKP8qJGaILr47NqLy5KJWSAX0rUNHeQeRWkzdFoX9SI8jR6j08BYKxGEEZCJYchf5S9PaZcWfvYoSkihXzZPpCa1', 'EH1oZs16VYfSMygzmg9C', 'KfktHUlEnAFGTmWOzlprwgpJl7cnixKt6mL2WOnMSpsPvXQgAFClRdKfyWHluYipWeD5GDxeKAi6rQtlCuSM0wT76m0y6dfjVxmuE7uvwoWHM2bKl1fLLdwPA6DTdZuOiIVsMUDBJ7Q61rGRtKiPjAxPThXoSY25JJzccyOuyeq14fiOcweivTs0TAfIzHbQEMl5gvCe', NULL, '1986-03-27', 'Magni accusantium sunt quidem. Doloremque quasi cum qui fugiat voluptas qui modi. Sint occaecati omnis esse ut. Officiis eos fuga laborum omnis repudiandae ea. Sit cum et et ipsa.', 0, '2021-09-08 23:53:16', '2021-09-08 23:53:16'),
(5, 'Z55NvVFrcsCSLIPXAzB3TV2Kjg1jxgPHidQxl5N2EJQEGZgH6NvhL27thwr6PZ1qBvUTB7pic6kOoJqgWmLx3fAD8y4yTwpOdKLf', '6kUpZrjjNh7sRHoPCaOr', 'Kxhkd80k3qt0CWi239pY52h0uqgdRSAFv6KdkHCh0k5azoHs8lY7QJbEUvQg92CUeFobmlC5Gb8ZxKiuESVtAS3RQVmmVli3K7Lw', 'Pb6y4w9SqzXh8gu9OvFp', 'fPz8bC1KpVF4ZN3I6otAs3jPm31ZRjLV4EvBQFaAYuGNZEWZ6y9QC75q2uDXXedw3bVEGKalPPoIGnc4nDUs5MGQpWQ2wyThucnQrEAergt5FocNDPC15Ff9kdN8Wn889k5NZeKQTlHzN1uMhZG3Oj1ZzP0vBgWQdRxCL6prrx4rOc6AcNlJ61zSacpXnD6Myis47ULY', NULL, '1982-05-12', 'Et accusantium voluptatibus ipsum molestiae id. Autem ipsum odio voluptatibus omnis. Cupiditate saepe magni alias quia quis occaecati qui.', 0, '2021-09-08 23:53:16', '2021-09-08 23:53:16');

-- --------------------------------------------------------

--
-- Table structure for table `registration_page_content`
--

CREATE TABLE `registration_page_content` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `additional_donor_benefits` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patment_types` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_payable_to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles_and_designations`
--

CREATE TABLE `roles_and_designations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `member_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_type_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles_and_designations`
--

INSERT INTO `roles_and_designations` (`id`, `member_id`, `type`, `sub_type_id`, `designation_id`, `created_at`, `updated_at`) VALUES
(1, 7, 'leadershiptype', '3', 3, '2021-09-16 08:02:44', '2021-09-16 08:02:44'),
(2, 5, 'leadershiptype', '1', 2, '2021-09-16 08:03:20', '2021-09-16 08:03:20'),
(3, 1, 'leadershiptype', '2', 3, '2021-09-17 01:41:25', '2021-09-17 01:41:25'),
(4, 3, 'leadershiptype', '1', 1, '2021-09-17 01:44:55', '2021-09-17 01:44:55');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `program_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `event_id`, `date`, `from_time`, `to_time`, `program_name`, `room_no`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-09-17', '03:51:36', '21:10:16', 'WvNDdvbHOQ', 'NdGOC7DzIc', '2021-09-08 23:53:16', '2021-09-13 23:59:45'),
(2, 7, '1975-11-30', '04:12:08', '00:26:16', '8Mg6wLcXpT', 'GtctbobvXr', '2021-09-08 23:53:16', '2021-09-08 23:53:16'),
(3, 8, '1978-02-03', '06:12:24', '08:24:18', 'T2yYHD0tpz', 'kPYLAEsoR9', '2021-09-08 23:53:16', '2021-09-08 23:53:16'),
(4, 9, '2006-09-27', '04:38:45', '02:07:53', 'IJ4UlQM9mU', '9qTuRSs33s', '2021-09-08 23:53:16', '2021-09-08 23:53:16'),
(5, 10, '2019-10-22', '18:21:16', '15:50:25', 'L4dzBEZgkk', 'yXDrcZUcQz', '2021-09-08 23:53:16', '2021-09-08 23:53:16'),
(6, 1, '2021-09-17', '09:00:00', '10:00:00', 'p1', '1', '2021-09-11 05:21:40', '2021-09-11 05:21:40'),
(7, 1, '2021-09-17', '11:00:00', '13:00:00', 'p2', '2', '2021-09-11 05:21:40', '2021-09-11 05:21:40'),
(8, 1, '2021-09-17', '11:11:00', '13:11:00', '111', '222', '2021-09-13 23:36:41', '2021-09-13 23:36:41'),
(9, 2, '2021-09-17', '11:00:00', '12:00:00', 'p1', 'r1', '2021-09-13 23:58:29', '2021-09-13 23:58:29');

-- --------------------------------------------------------

--
-- Table structure for table `sponsor_categories`
--

CREATE TABLE `sponsor_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_type_id` bigint(20) UNSIGNED NOT NULL,
  `donor_type_id` bigint(20) UNSIGNED NOT NULL COMMENT 'null if Individual',
  `amount` double(8,2) NOT NULL,
  `benefits` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sponsor_category_types`
--

CREATE TABLE `sponsor_category_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sponsor_category_types`
--

INSERT INTO `sponsor_category_types` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Donor', 1, '2021-09-08 23:53:17', '2021-09-20 05:05:37'),
(2, 'Family/Individuals', 1, '2021-09-08 23:53:17', '2021-09-20 05:06:35'),
(3, 'Mohamed Dooley', 0, '2021-09-08 23:53:17', '2021-09-20 05:06:36'),
(4, 'zxzx', 0, '2021-09-20 04:51:54', '2021-09-20 05:05:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spouse_first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spouse_last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not initiated',
  `total_amount` double(8,2) DEFAULT NULL,
  `amount_paid` double(8,2) DEFAULT NULL,
  `registration_type_id` bigint(20) UNSIGNED NOT NULL,
  `individual_registration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sponsorship_category_id` bigint(20) UNSIGNED NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `member_id`, `email`, `first_name`, `last_name`, `spouse_first_name`, `spouse_last_name`, `phone_code`, `mobile`, `country`, `state`, `city`, `zip_code`, `image_url`, `payment_status`, `total_amount`, `amount_paid`, `registration_type_id`, `individual_registration`, `sponsorship_category_id`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '', '10001', 'a@nriva.org', 'admin', 'Nriva', '', '', NULL, '(999) 999-9999', 'India', 'AL', 'Hyderabad', '000000', NULL, 'not initiated', NULL, NULL, 0, NULL, 0, NULL, '', NULL, '2021-09-20 05:02:25', '2021-09-20 05:02:25');

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE `venues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Event_date_time` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `Event_date_time`, `end_date`, `location`, `created_at`, `updated_at`) VALUES
(1, '2021-09-17 00:00:00', '2021-09-17 00:00:00', 'location', '2021-09-08 23:53:18', '2021-09-09 04:29:09');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `video_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube_video_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'active->1 inactive->0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `video_url`, `name`, `youtube_video_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'libero', 'Mario Tremblay', NULL, 1, '2021-09-08 23:53:14', '2021-09-08 23:53:14'),
(2, 'quia', 'Taurean Borer II', NULL, 1, '2021-09-08 23:53:14', '2021-09-08 23:53:14'),
(3, 'explicabo', 'Dr. Isaiah Auer PhD', NULL, 1, '2021-09-08 23:53:14', '2021-09-08 23:53:14'),
(4, 'et', 'Harold Kovacek', NULL, 1, '2021-09-08 23:53:14', '2021-09-08 23:53:14'),
(5, 'voluptas', 'Mr. Braulio Wunsch', NULL, 1, '2021-09-08 23:53:14', '2021-09-08 23:53:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benefit_types`
--
ALTER TABLE `benefit_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benefit_user`
--
ALTER TABLE `benefit_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benfit_sponsor_category`
--
ALTER TABLE `benfit_sponsor_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donors`
--
ALTER TABLE `donors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donortypes`
--
ALTER TABLE `donortypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `invitees`
--
ALTER TABLE `invitees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leadership_types`
--
ALTER TABLE `leadership_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logos`
--
ALTER TABLE `logos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_slug_unique` (`slug`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration_page_content`
--
ALTER TABLE `registration_page_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_and_designations`
--
ALTER TABLE `roles_and_designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsor_categories`
--
ALTER TABLE `sponsor_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsor_category_types`
--
ALTER TABLE `sponsor_category_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `venues`
--
ALTER TABLE `venues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `benefit_types`
--
ALTER TABLE `benefit_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `benefit_user`
--
ALTER TABLE `benefit_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `benfit_sponsor_category`
--
ALTER TABLE `benfit_sponsor_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `donors`
--
ALTER TABLE `donors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `donortypes`
--
ALTER TABLE `donortypes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invitees`
--
ALTER TABLE `invitees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leadership_types`
--
ALTER TABLE `leadership_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `logos`
--
ALTER TABLE `logos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `registration_page_content`
--
ALTER TABLE `registration_page_content`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles_and_designations`
--
ALTER TABLE `roles_and_designations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sponsor_categories`
--
ALTER TABLE `sponsor_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sponsor_category_types`
--
ALTER TABLE `sponsor_category_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `venues`
--
ALTER TABLE `venues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
