<?php
return [
    'banner_upload' =>'public/banner/',
    'banner_display' =>'public/storage/banner/',

    'message_upload' => 'public/message/',
    'message_display' => 'public/storage/message/',

    'logo_upload' => 'public/logo/',
    'logo_display' => 'public/storage/logo/',

    'event_upload' => 'public/event/',
    'event_display' => 'public/storage/event/',

    'program_upload' => 'public/program/',
    'program_display' => 'public/storage/program/',


    'donor_upload' => 'public/donor/',
    'donor_display' => 'public/storage/donor/',

    'member_upload' => 'public/member/',
    'member_display' => 'public/storage/member/',

    'user_upload' => 'public/user/',
    'user_display' => 'public/storage/user/',


    'user_upload' => 'public/user/',
    'user_display' => 'public/storage/user/',


    'committe_upload' => 'public/committe/',
    'committe_display' => 'public/storage/committe/',

    'benfit_image_upload' => 'public/benfit_image/',
    'benfit_image_display' => 'public/storage/benfit_image/',
    
    'paypal_name_db' => 'Paypal',
    'zelle_name_db' => 'Zelle',
    'check_name_db' => 'Check',
    'other_name_db' => 'Other',
    'first_data_name_db' => 'Credit / Debit Card',


    'APP_URL'=>'https://ttaconvention.org',

   /* 'client_id' => 'AUgrnC583xtC-ncKmMOg_I36TkOW1FLVyslS1xTM4VPAQe1WChOzzq1WvfYFytuiJSWiECTHcnTVv7p7',
    'secret' => 'EP2GGLP8repPMPiX4O2GfKwAVp03xH_O2RQR1lghMx8ZFKEn_DBubgTT5YsUUpHM8GWDE57owNFKS68b', 
    */
    //sandbox
  // 'PAYPAL_SANDBOX_CLIENT_ID' => 'AcvOzkmsm8jkwAj0QkxF6yYDQz34d57Pn19I2bpKRmgE1K_O-yxE-mb0UVyYhl8lQRrVyOjZNJQmpPr0',
  //  'PAYPAL_SANDBOX_CLIENT_SECRET' => 'EBK5aXthOoh7UJZYFof18Tc4ZHTpEJs9zf_B1U5lSSp-5sMZ3kots9TsTyns3uezRy4EiCMjzp8sv9f2',
  // 'PAYPAL_MODE' => 'sandbox',
    
    //live
    'PAYPAL_SANDBOX_CLIENT_ID' => 'AUgrnC583xtC-ncKmMOg_I36TkOW1FLVyslS1xTM4VPAQe1WChOzzq1WvfYFytuiJSWiECTHcnTVv7p7',
    'PAYPAL_SANDBOX_CLIENT_SECRET' => 'EP2GGLP8repPMPiX4O2GfKwAVp03xH_O2RQR1lghMx8ZFKEn_DBubgTT5YsUUpHM8GWDE57owNFKS68b',
    'PAYPAL_MODE' => 'live',
    
    //first data prod keys
    
    'FIRSTDATA_CLIENT_ID'=>'YGo8Jycn1YtvADrZQpix1drgEVTbdNRW',
    'FIRSTDATA_CLIENT_SECRET'=>'551de890a83dd4a4b96c23f9da49400e5e182e07183ee53f49ce455e4256406f',
    'FIRSTDATA_MERCHANT_TOKEN'=>'fdoa-7cf8d35776bf45a7b97a8cc84ca132165dfe23455b45c8f8',
    'FIRSTDATA_SITE_URL'=>"https://api.payeezy.com/v1/transactions",




//First data sandbox details
   // 'FIRSTDATA_CLIENT_ID'=>'RJ2rsZbi3eXEwixEKuzGHf6l75eOvWIG',
    //'FIRSTDATA_CLIENT_SECRET'=>'b8c4d8e2807a84695f708d8e9a9c3b45ef67bab80c028e591ef8fb5d0bf7f5aa',
    //'FIRSTDATA_MERCHANT_TOKEN'=>'fdoa-6e8c68cbc7078a62d52cad2cc14c04c4e6b5d92c45a4808c',
    //'FIRSTDATA_SITE_URL'=>"https://api-cert.payeezy.com/v1/transactions",

];
