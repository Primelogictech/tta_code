<?php
return array(
    // set your paypal credential

/*    'client_id' => 'AUgrnC583xtC-ncKmMOg_I36TkOW1FLVyslS1xTM4VPAQe1WChOzzq1WvfYFytuiJSWiECTHcnTVv7p7',
    'secret' => 'EP2GGLP8repPMPiX4O2GfKwAVp03xH_O2RQR1lghMx8ZFKEn_DBubgTT5YsUUpHM8GWDE57owNFKS68b',  */
    'client_id' => config('conventions.PAYPAL_SANDBOX_CLIENT_ID'),
    'secret' => config('conventions.PAYPAL_SANDBOX_CLIENT_SECRET'),


    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => config('conventions.PAYPAL_MODE'),

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);
