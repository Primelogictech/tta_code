<style type="text/css">
    .inner-page-donors-bg {
        background: #a30202;
        background-image: url(images/donors/donor-body.webp), url(images/donors/donors.webp);
        width: 100%;
        padding: 0px 0px 20px 0px;
        background-repeat: repeat-y, repeat;
        background-position: left top, left top;
        background-size: contain, auto;
        text-align: center;
    }
    .donors-nav-item a {
        cursor: pointer;
        font-family: "Eurostar";
        font-size: 20px;
        color: #fff;
    }
    @media (min-width: 1200px) {
        .container,
        .container-lg,
        .container-md,
        .container-sm,
        .container-xl {
            max-width: 1260px;
        }
    }
</style>

<section class="container mt35 inner-page-donors-bg">
    <div class="mx-auto main-heading">
        <span>Donors</span>
    </div>
    <div class="container">
        <div class="row px-4 px-md-4 px-lg-2">
            <div class="col-12 px-3 px-md-3 px-lg-5">
                <div>
                    <ul class="list-unstyled text-center donors-horizontal-scroll">
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none donors_active_tab_btn" target-id="#chakravarti-poshakulu">
                                <span>Chakravarti Poshakulu</span>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#maharaja-poshakulu">
                                <span>Maharaja Poshakulu</span>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#raja-poshakulu">
                                <span>Raja Poshakulu</span>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#mukhya-atithi">
                                <span>Mukhya Atithi</span>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#visishta-atithi">
                                <span>Visishta Atithi</span>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#pratyeka-atithi">
                                <span>Pratyeka Atithi</span>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#gourava-atithi">
                                <span>Gourava Atithi</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row justify-content-center px-0 px-lg-2 donors_active_tab" id="chakravarti-poshakulu">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img src="images/donors/malla-reddy-pailla.webp" class="img-fluid mx-auto d-block" alt="Malla Reddy Pailla" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Dr. Malla Reddy Pailla & Sadhana (NY)</h6>
                                <div class="donors-amount">$100,000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center px-0 px-lg-2" id="maharaja-poshakulu" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Premsagar Reddy" class="img-fluid mx-auto d-block" src="images/donors/premsagar-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Premsagar Reddy (CA)
                                </h6>
                                <div class="donors-amount">
                                    $50,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="VijayPal Reddy" class="img-fluid mx-auto d-block" src="images/donors/vijaypal-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Vijaypal Reddy & Shobha Reddy (IN)
                                </h6>
                                <div class="donors-amount">
                                    $50,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Haranath Policherla" class="img-fluid mx-auto d-block" src="images/donors/haranath-policherla.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Haranath Policherla (MI)
                                </h6>
                                <div class="donors-amount">
                                    $50,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Mohan Patalolla" class="img-fluid mx-auto d-block" src="images/donors/mohan-patalolla.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Mohan Patalolla (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $50,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="MSN Reddy" class="img-fluid mx-auto d-block" src="images/donors/msn-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. MSN Reddy
                                </h6>
                                <div class="donors-amount">
                                    $50,000
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center px-0 px-lg-2" id="raja-poshakulu" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vamshi Reddy" class="img-fluid mx-auto d-block" src="images/donors/vamshi-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Vamshi Reddy (WA)
                                </h6>
                                <div class="donors-amount">
                                    $25,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Srinivas Ganagoni" class="img-fluid mx-auto d-block" src="images/donors/srinivas-ganagoni.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Srinivas Ganagoni (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $25,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Bharath Madadi" class="img-fluid mx-auto d-block" src="images/donors/bharath-madadi.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Bharath Madadi (GA)
                                </h6>
                                <div class="donors-amount">
                                    $25,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Dr. Sudhakar Vidiyala &amp; Geetha" class="img-fluid mx-auto d-block" src="images/donors/sudhakar-vidyala.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Sudhakar Vidiyala &amp; Geetha (NY)
                                </h6>
                                <div class="donors-amount">
                                    $25,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ramnath Reddy" class="img-fluid mx-auto d-block" src="images/donors/ramnath-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ramnath Reddy (CT)
                                </h6>
                                <div class="donors-amount">
                                    $25,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Bhuvanesh Boojala" class="img-fluid mx-auto d-block" src="images/donors/bhuvanesh-boojala.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Bhuvanesh Boojala (DC)
                                </h6>
                                <div class="donors-amount">
                                    $25,000
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center px-0 px-lg-2" id="mukhya-atithi" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sekhar Vemparala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Sekhar Vemparala
                                </h6>
                                <div class="donors-amount">
                                    $20,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Santosh Somireddy" class="img-fluid mx-auto d-block" src="images/donors/santosh-somireddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Santosh Somireddy (VA)
                                </h6>
                                <div class="donors-amount">
                                    $20,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="M.S. Reddy" class="img-fluid mx-auto d-block" src="images/donors/m.s.reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. M.S. Reddy (Cheese Reddy)
                                </h6>
                                <div class="donors-amount">
                                    $15,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Poorna Atluri" class="img-fluid mx-auto d-block" src="images/donors/poorna-atluri.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Poorna Atluri & Dr. Shuba (NY)
                                </h6>
                                <div class="donors-amount">
                                    $15,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Lakshmi Moparthi" class="img-fluid mx-auto d-block" src="images/donors/lakshmi-moparthi.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Lakshmi Moparthi(NYLIFE) (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $15,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sharath Vemuganti" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Sharath Vemuganti (NY)
                                </h6>
                                <div class="donors-amount">
                                    $12,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Satish Vemana" class="img-fluid mx-auto d-block" src="images/donors/satish-vemana.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Satish Vemana (DC)
                                </h6>
                                <div class="donors-amount">
                                    $11,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="HanimiReddy Kakireddy Family Foundation" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. HanimiReddy Kakireddy Family Foundation (CA)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Pavan Ravva" class="img-fluid mx-auto d-block" src="images/donors/pavan-ravva.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. & Mr. Ragini & Pavan Ravva (NY)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Gangadhar Vuppala" class="img-fluid mx-auto d-block" src="images/donors/gangadhar-vuppala.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Saritha & Mr. Gangadhar Vuppala (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Shravan &amp; Shilpa" class="img-fluid mx-auto d-block" src="images/donors/shravan-shilpa.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Shravan Reddy & Dr. Shilpa Reddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Suresh Reddy Venkannagari" class="img-fluid mx-auto d-block" src="images/donors/suresh-reddy-venkannagari.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Suresh Reddy Venkannagari (PA)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Venkat Gaddam" class="img-fluid mx-auto d-block" src="images/donors/venkat-gaddam.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Venkat Gaddam (GA)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Satish Mekala" class="img-fluid mx-auto d-block" src="images/donors/satish-mekala.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Satish Mekala (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Amith Reddy" class="img-fluid mx-auto d-block" src="images/donors/amith-reddy-surakanti.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Amith Reddy Surakanti (CA)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vishnu Battula" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Vishnu Battula (NY)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Pailla Arjun Reddy" class="img-fluid mx-auto d-block" src="images/donors/arjun-reddy-pailla.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Pailla Arjun Reddy (CA)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Jithender Reddy" class="img-fluid mx-auto d-block" src="images/donors/jithender-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Jithender Reddy (TX)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Kumar Reddy" class="img-fluid mx-auto d-block" src="images/donors/kumar-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Kumar Reddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Praveen Guduru" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Praveen Guduru (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vinod Koduru" class="img-fluid mx-auto d-block" src="images/donors/vinod-koduru.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Deepa & Mr. Vinod Koduru (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sreenivasa Reddy" class="img-fluid mx-auto d-block" src="images/donors/srinivas-reddy-gade.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Gade Sreenivasa Reddy (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Bharathi Reddy" class="img-fluid mx-auto d-block" src="images/donors/bharathi-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Bharathi Reddy & Ramu Reddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Mamatha" class="img-fluid mx-auto d-block" src="images/donors/mamatha.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Mamatha & Mr. Madhava Reddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Rajender Reddy" class="img-fluid mx-auto d-block" src="images/donors/rajender-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Rajender Reddy & Jhansi Reddy (CA)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Kishore Ganji" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Kishore Ganji (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Prasad Chukkapalli" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Prasad Chukkapalli (MI)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Srinivas Anugu" class="img-fluid mx-auto d-block" src="images/donors/srinivas-anugu.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Srinivas Anugu (PA)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="PN9 Realty" class="img-fluid mx-auto d-block" src="images/donors/pn9-reality.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    PN9 Realty (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $10,000
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center px-0 px-lg-2" id="visishta-atithi" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Shiva Reddy" class="img-fluid mx-auto d-block" src="images/donors/shiva-reddy-palempalli.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Shiva Reddy Palempalli (NY)
                                </h6>
                                <div class="donors-amount">
                                    $6,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sanjeeva Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Sanjeeva Reddy (LA)
                                </h6>
                                <div class="donors-amount">
                                    $5,001
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div>
                            <img alt="Lakshmi Narasimha Reddy Donthireddy" class="img-fluid mx-auto d-block" src="images/donors/lakshmi-narasimha-reddy-donthireddy.webp" />
                        </div>
                        <div class="pt-1">
                            <h6 class="donors-name">
                                Dr. Narasimha Reddy Donthireddy & Prashanti Reddy (PA)
                            </h6>
                            <div class="donors-amount">
                                $5,000
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Divakar Jandyam" class="img-fluid mx-auto d-block" src="images/donors/divakar-jandyam.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Divakar Jandyam (MA)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Shiva Reddy Kolla" class="img-fluid mx-auto d-block" src="images/donors/shiva-reddy-kolla.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Shiva Reddy Kolla & Navya Reddy (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Kiran Duddagi" class="img-fluid mx-auto d-block" src="images/donors/kiran-duddagi.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Kiran Duddagi (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vijay Bhaskar Kalal" class="img-fluid mx-auto d-block" src="images/donors/vijay-bhaskar.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Vijay Bhaskar Kalal (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Rajeshwar Reddy Gangasani" class="img-fluid mx-auto d-block" src="images/donors/rajeshwar-reddy-gangasani.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Rajeshwar Reddy Gangasani (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Anna Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Anna Reddy (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ram Mohan Chinnala" class="img-fluid mx-auto d-block" src="images/donors/ram-mohan.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ram Mohan & Dr. Sandhya R (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="T. P. Srinivas Rao" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. T. P. Srinivas Rao (NY)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Venkateshwar Reddy" class="img-fluid mx-auto d-block" src="images/donors/venkateshwar-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Venkateshwar Reddy (OK)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Kavitha Reddy" class="img-fluid mx-auto d-block" src="images/donors/kavitha-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Kavitha Reddy (IN)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Naveen Goli" class="img-fluid mx-auto d-block" src="images/donors/naveen-goli.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Veena & Mr. Naveen Goli (WA)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sharath Vemula" class="img-fluid mx-auto d-block" src="images/donors/archana-and-sharath-chandra-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. & Mr. Archana & Sharath Chandra Reddy Vemula (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Amar Reddy" class="img-fluid mx-auto d-block" src="images/donors/amarender.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Prasanna & Amarender Reddy Paduru (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Chinnababu Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Chinnababu Reddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Manjunath Matada" class="img-fluid mx-auto d-block" src="images/donors/manjunath.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Manjunath Matada (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Nathan Godhumala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Nathan Godhumala (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Naveen Reddy" class="img-fluid mx-auto d-block" src="images/donors/naveen-reddy-vodlakonda.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Naveen Reddy Vodlakonda (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Pradeep Samala" class="img-fluid mx-auto d-block" src="images/donors/pradeepsamala.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Pradeep Samala (NY)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Rama Surya Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Rama Surya Reddy (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ravi Dhannapuneni" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ravi Dhannapuneni (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Srinivas Reddy Pinnapureddy" class="img-fluid mx-auto d-block" src="images/donors/srinivas-pinnapureddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Srinivas Reddy Pinnapureddy (TX)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Bindu Latha" class="img-fluid mx-auto d-block" src="images/donors/bindu-latha-cheedella.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Bindu Latha Cheedella (KS)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sambashiva Rao Venigalla" class="img-fluid mx-auto d-block" src="images/donors/sambashiva-rao.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Mahalakshmi & Mr. Sambashiva Rao Venigalla (NY)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Usha Mannem" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. &amp; Mrs. Usha &amp; Suneel Teepireddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Lenina Thumma" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Lenina Thumma & Mr. Rajender Prasad Vavilala (NY)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vani" class="img-fluid mx-auto d-block" src="images/donors/laxman-reddy-anugu.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Vani & Mr. Laxman Reddy Anugu (NY)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Hanumanth Reddy" class="img-fluid mx-auto d-block" src="images/donors/hanumanth-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Hanumanth Reddy (IL)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Suresh Puttagunta" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Suresh Puttagunta (MI)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Venkat Aekka" class="img-fluid mx-auto d-block" src="images/donors/venkat-aekka.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Venkat Aekka (MI)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Mallik Bolla" class="img-fluid mx-auto d-block" src="images/donors/mallik-bolla.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Mallik Bolla (PA)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Raj Anandeshi" class="img-fluid mx-auto d-block" src="images/donors/raj-anandeshi.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Raj Anandeshi (TX)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Jay Challa" class="img-fluid mx-auto d-block" src="images/donors/jay-challa.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Jay Challa (DC)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ranjita & Raja Kasukurthi" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Ranjita & Raja Kasukurthi (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Mr.Praveen Tadakamalla" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Praveen Tadakamalla (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Mr. Anjaiah Chowdary Lavu" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Anjaiah Chowdary Lavu (GA)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Mr. Ravi Dhannapuneni" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ravi Dhannapuneni (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $5,000
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center px-0 px-lg-2" id="pratyeka-atithi" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Raghuram Pannala" class="img-fluid mx-auto d-block" src="images/donors/raghuram-pannala.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Raghuram Pannala (NY)
                                </h6>
                                <div class="donors-amount">
                                    $4,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sunitha Kanumuri" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Sunitha Kanumuri (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $3,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ajay Reddy Macha" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ajay Reddy Macha (WA)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Chendra Sena Sriramoju" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Chendra Sena Sriramoju (WA)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ganesh Veramaneni" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ganesh Veramaneni (WA)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Manikyam" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Manikyam Thukkapuram (WA)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Manohar Rao Bodke" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Manohar Rao Bodke (WA)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Pradeep Mettu" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Pradeep Mettu (WA)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ravinder Veeravalli" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ravinder Veeravalli (WA)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sangeetha Borra" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Sangeetha Borra (WA)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sridhar Chaduvu" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Sridhar Chaduvu (WA)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Nagaj" class="img-fluid mx-auto d-block" src="images/donors/hanumanthu-davuluri.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Nagaj & Mr. Hanumanthu Davuluri (NY)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sirisha Donthireddy" class="img-fluid mx-auto d-block" src="images/donors/sirisha.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Sirisha Donthireddy & Mr. Chendrashekar Pylla (NY)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vasu Sabbella" class="img-fluid mx-auto d-block" src="images/donors/vasu-sabbella.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Vasu Sabbella (NY)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Raju Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Raju Reddy (CA)
                                </h6>
                                <div class="donors-amount">
                                    $3,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Arun Arkala" class="img-fluid mx-auto d-block" src="images/donors/arun-arkala.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Arun Arkala (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,600
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vidyasagara Maramareddi" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Vidyasagara Maramareddi (MI)
                                </h6>
                                <div class="donors-amount">
                                    $2,501
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Mahesh Sambu" class="img-fluid mx-auto d-block" src="images/donors/mahesh-sambu.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Mahesh Sambu (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Linga Reddy Nathala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Linga Reddy Nathala (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Dr.Silaja & Satish Kalva" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Silaja & Satish Kalva (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Arun Reddy Mekala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Arun Reddy Mekala (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sunil Koganti" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Sunil Koganti (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Satish Tummala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Satish Tummala (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ravi Potluri" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ravi Potluri (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Anjan Karnati" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Anjan Karnati (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Chandrashekara Reddy" class="img-fluid mx-auto d-block" src="images/donors/chandrashekara-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Chandrashekara Reddy (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Santosh Reddy Koram" class="img-fluid mx-auto d-block" src="images/donors/santosh-reddy-koram.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Santosh Reddy Koram (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Srinivas Manapragada" class="img-fluid mx-auto d-block" src="images/donors/srinivasa_manapragada.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Narasimha Murthy (Simha) , Yuvraj Tejase, Manihar Murthy, Srinivasa & Kavitha Manapragada (CA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Srinivas Guduru" class="img-fluid mx-auto d-block" src="images/donors/srinivas-guduru.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Srinivas Guduru (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ashok Chintakunta &amp; Madhavi Soleti" class="img-fluid mx-auto d-block" src="images/donors/ashok-chintakunta.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Abhinav, Anirudh, Madhavi &amp; Ashok Chintakunta (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Bhaskar Pinna" class="img-fluid mx-auto d-block" src="images/donors/bhaskar-pinna.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Bhaskar Pinna (DE)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Hari Pyreddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Hari Pyreddy (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Kiran Guduru" class="img-fluid mx-auto d-block" src="images/donors/kiran-guduru.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Kiran Guduru (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Mahender Narala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Mahender Narala
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ranjeeth Kyatham" class="img-fluid mx-auto d-block" src="images/donors/ranjeeth-kyatham.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ranjeeth and Nagasree (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sahodhar Peddireddy" class="img-fluid mx-auto d-block" src="images/donors/sahodhar-peddireddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Sahodhar Peddireddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Mallik Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Mallik Reddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Narasihma Peruka" class="img-fluid mx-auto d-block" src="images/donors/narasimha-peruka.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Narsimha Peruka & Vijaya Vani Peruka Family (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Prasad Kunarapu" class="img-fluid mx-auto d-block" src="images/donors/prasad-kunarapu.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Prasad Kunarapu family (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ramakrishna Sannidhi" class="img-fluid mx-auto d-block" src="images/donors/ramakrishna-sannidhi.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ramakrishna Sannidhi (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ramana Kotha" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ramana Kotha (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Roopak Kalluri" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Roopak Kalluri (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vamshi Gullapalli" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Vamshi Gullapalli (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Anjan Karnati" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Anjan Karnati (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Murtuza Mohammad" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Murtuza Mohammad (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Venkat Sunki Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Venkat Sunki Reddy (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Deepa Jalagam" class="img-fluid mx-auto d-block" src="images/donors/deepa-jalagam.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mrs. Deepa Jalagam (NJ)</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ravinder Kamtam" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mrs. & Mr. Manorama & Ravinder Kamtam (NY)</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Karthik Nimmala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Karthik Nimmala (GA)</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Srini Sunkaraneni" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Srini Sunkaraneni (NJ)</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sripal Reddy Katanguri" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Sripal Reddy Katanguri (GA)</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sudheer" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Sudheer Kotha (GA)</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Nishanth Sirikonda" class="img-fluid mx-auto d-block" src="images/donors/nishanth-sirikonda-and-shravya.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. & Mrs. Nishanth Sirikonda & Shravya (NC)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sudhakar Uppala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Sudhakar Uppala (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Srinivas Reddy Alla" class="img-fluid mx-auto d-block" src="images/donors/srinivas-reddy-alla.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Srinivas Reddy Alla (TX)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Jagga Rao Alluri" class="img-fluid mx-auto d-block" src="images/donors/jagga-rao.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Jagga Rao Alluri & Janaki (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Silaja" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Silaja & Satish Kalva (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sridevi & Sharath Bhoomi" class="img-fluid mx-auto d-block" src="images/donors/sridevi.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Sridevi & Sharath Bhoomi (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Subhadra" class="img-fluid mx-auto d-block" src="images/donors/dattatreyudu.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Subhadra & Dr. Nori Dattatreyudu (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Kalyan Ramachary" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Kalyan Ramachary (MI)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sandhya Gavva" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Sandhya Gavva (TX)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Stanley" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Stanley & Mr. Sumathy Reddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="V. Nagendra Guptha" class="img-fluid mx-auto d-block" src="images/donors/vijaya.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Vijaya & Mr. Nagendra Guptha Vellore (NY)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Arun Mekala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Arun Mekala (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Gautham Vepur" class="img-fluid mx-auto d-block" src="images/donors/goutham-vepur.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Gautham Vepur (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Venu Enugala & Shiva Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Venu Enugala & Shiva Reddy (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Narendar Yarava" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Narender Reddy Yarava & Kavitha (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Srinivas Reddy Nandi" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Srinivas Reddy Nandi (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Raju Kakkerla" class="img-fluid mx-auto d-block" src="images/donors/raju-kakkerla.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Raju Kakkerla (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Giridhar Masireddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Giridhar Masireddy (PA)
                                </h6>
                                <div class="donors-amount">
                                    $2,500
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center px-0 px-lg-2" id="gourava-atithi" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Anil Kumar Chintalapudi" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Anil Kumar Chintalapudi (MI)
                                </h6>
                                <div class="donors-amount">
                                    $2,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ramesh Thangellapalli" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Ramesh Thangellapalli (CA)
                                </h6>
                                <div class="donors-amount">
                                    $2,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Manohar Chillara" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Manohar Chillara (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,500
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Srikanth Routhu" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Srikanth Routhu (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $1,250
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Aruna Sridhar Kancharla" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Aruna Sridhar Kancharla (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $1,200
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Chandana Angitipalli" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Chandana Angitipalli & Mr. Venkat Reddy Guduru (Ny)
                                </h6>
                                <div class="donors-amount">
                                    $1,116
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Jaya Prakash" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Jaya Prakash Enjapuri (Ny)
                                </h6>
                                <div class="donors-amount">
                                    $1,116
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Lavanya R Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Lavanya R Reddy (WA)
                                </h6>
                                <div class="donors-amount">
                                    $1,001
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Varun Allam" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Varun Allam(MA)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Yogi &amp; Rama Vanama" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Yogi &amp; Rama Vanama (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Raghu Mudupolu" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Raghu Mudupolu (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Suresh Thanda" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Suresh Thanda (WA)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Venu Gopal Palla" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Venu Gopal Palla (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Srinivas Kodali" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Sridevi & Mr. Srinivas Kodali (MI)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Uma" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. & Mrs. Uma & Surendra Gorrapati (MI)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Anil Nagoli" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Anil Nagoli & Sandeepa Venagapudi (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Anitha" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Anitha & Mr. Sathya Gaggenapally (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Asuini" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Asuini & Vijay Bhat (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Bharathi" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Bharathi & Sekhar V. S. Nelanuthala (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Jaideep Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Jaideep Reddy & Kalpana (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Venugopal Palla" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Venugopal Palla & Anita Punreddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Gaddam Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Gaddam Reddy (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Hemalatha" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Hemalatha & Mr. Udaya Kumar Dommaraju (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Jyothi" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Jyothi & Mr. Raghotham Reddy Katla (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Kathyanani" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Kathyanani & Kallika Reddy Alla (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Madhusudhan Reddy" class="img-fluid mx-auto d-block" src="images/donors/madhusudan-reddy.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Madhusudhan Reddy Nalla (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Navatha Bussa" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Navatha Bussa & Mr. Pashupathinath Siddamshetty (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Nehru Kataru" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Nehru Kataru (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Padmasree" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Padmasree & Mr. Sridhar Gummadavelli (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Pavanaja" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Pavanaja & Rama Joga Venkata Eranki (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Phanibushan" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Phanibushan Thadepalli (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Rajitha Kontham" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Rajitha Kontham & Mr. Chandra Sekhar Reddy Nalla (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Ramya" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Ramya & Mr. Bala Murali Nagisetti (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Salini Allaparthi" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Salini Allaparthi & Mr. Kalyan Kumar Meka (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Saritha" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Saritha & Mr. Ramesh Eligeti (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Shilaja" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Shilaja & Mr. Satya Challapalli (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sirisha Aalla" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Sirisha Aalla & Mr. Phani Kumar Jaggavarapu (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Subba Lakshmi" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Subba Lakshmi & Mr. Dasarath Gurram (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Subbarao Anumolu" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Subbarao Anumolu (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sumanth Ramisetti" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Sumanth Ramisetti (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Tanuja" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Tanuja & Mr. Harisankar Rasaputra (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Venkatasubramanian" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Venkatasubramanian Jayaraman (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vijaya" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Vijaya & Mr. Vishnu Marisetti (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vinitha Marru" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mrs. Vinitha Marru & Mr. Sunil Thakkalapally (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Haripriya & Harish Cherivirala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Haripriya & Harish Cherivirala (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Murali Guthikonda" class="img-fluid mx-auto d-block" src="images/donors/murali-guthikonda.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Murali Guthikonda & Shaila Cordone (MI)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Raja Rajasekhar" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Raja Rajasekhar (DC)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Mahesh Pogaku" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Mahesh Pogaku (NJ)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Krishna Lam" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Krishna Lam (DC)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Kranti Dudam" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Kranti Dudam (DC)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Govardhan Reddy Tokala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Govardhan Reddy Tokala (DC)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Arjun Kamasetti" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Arjun Kamasetti (DC)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Vani & Chandu Singirikonda" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Vani & Chandu Singirikonda (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Suresh Bondugula" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Suresh Bondugula (PA)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sudharshan Chetkuri" class="img-fluid mx-auto d-block" src="images/donors/sudarshan-chetkuri.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Sudharshan Chetkuri (PA)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Subhash Reddy Karra" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Subhash Reddy Karra (PA)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Sanjeev Reddy & Sneha Reddy Bokka" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Sanjeev Reddy & Sneha Reddy Bokka (TX)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Kranthi Dudem" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Kranthi Dudem (DC)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Kiran Kalva" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Mr. Kiran Kalva (DC)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Dr. Aravinda Srikar Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Aravinda Srikar Reddy (PA)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="donors-team">
                            <div>
                                <img alt="Dr. Rajender Reddy Jinna" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">
                                    Dr. Rajender Reddy Jinna (NY)
                                </h6>
                                <div class="donors-amount">
                                    $1,000
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
