@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-5">
    <div class="container shadow-small px-sm-20 py-4 p-md-4 p40">
        <div class="col-12 col-lg-10 offset-lg-1  px-sm-30 p40 p-md-4 mb-5">
            <div class="row">
                <div class="col-12">
                 @foreach ($errors->all() as $error)
                    <div class="error">{{ $error }}</div>
                @endforeach
                    <div class="text-violet fs22">Pay Pending Amount </div>
                    <h5 class="text-violet py-2">Pending Amount {{ \Auth::user()->total_amount -\Auth::user()->amount_paid  }} </h5>
                    <br>
                </div>


                <form action="{{url('pay-pending-amount')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="pending_amount" value="{{ \Auth::user()->total_amount -\Auth::user()->amount_paid  }}">
                    <div class="row mt-2">
                        <div class="col-12">
                           @include('partalles.paymentForm')
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 my-2 my-md-auto">
                        <div class="text-center text-sm-right">

                            <input type="submit" class="input-checkbox btn btn-lg btn-danger text-uppercase px-5" value="Check out" name="">
                            <!--  <div class="btn btn-lg btn-danger text-uppercase px-5"></div> -->
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</section>


@section('javascript')
<script>
    $(".payment-button").click(function(e) {
        if ($(this).prop("checked")) {
            if ($(this).data('name') == "{{ config('conventions.paypal_name_db') }}" ) {
                $('#paypal').show();
                $('#check_payment').hide();
                $('#zelle').hide();
                $('#other').hide();
            }

            if ($(this).data('name') == "{{ config('conventions.check_name_db') }}") {
                $('#check_payment').show();
                $('#paypal').hide();
                $('#zelle').hide();
                $('#other').hide();
            }

            if ($(this).data('name') ==  "{{ config('conventions.zelle_name_db') }}" ) {

                $('#zelle').show();
                $('#paypal').hide();
                $('#check_payment').hide();
                $('#other').hide();
            }
            if ($(this).data('name') == "{{ config('conventions.other_name_db') }}") {

                $('#other').show();
                $('#zelle').hide();
                $('#paypal').hide();
                $('#check_payment').hide();
            }

            //zelle
        }
    });
</script>
@endsection

@endsection
