@extends('layouts.user.base')

@section('content')



<link href="css/slick.min.css" rel="stylesheet" />
<link href="css/slick-theme.min.css" rel="stylesheet" />
<link href="css/vertical-slick-carousel.css" rel="stylesheet" />

<link href="{{ asset('css/gallery.css') }}" rel="stylesheet" />


<style type="text/css">

.wrapper {
    height: auto;
    padding: 0px;
}
.main-heading {
    background: #c86bc5;
}

#scrolll {
    color: #fff;
    height: 355px;
    overflow: hidden;
    padding: 5px 5px;
    width: auto;
}
.donors_list img {
    height: 300px !important;
    width: 100% !important;
}
#jssor_1 {
    width: 100% !important;
}
/*jssor slider bullet skin 051 css*/
.jssorb051 .i {
    position: absolute;
    cursor: pointer;
}
.jssorb051 .i .b {
    fill: #fff;
    fill-opacity: 0.3;
}
.jssorb051 .i:hover .b {
    fill-opacity: 0.7;
}
.jssorb051 .iav .b {
    fill-opacity: 1;
}
.jssorb051 .i.idn {
    opacity: 0.3;
}

/*jssor slider arrow skin 051 css*/
.jssora051 {
    display: block;
    position: absolute;
    cursor: pointer;
}
.jssora051 .a {
    fill: none;
    stroke: #fff;
    stroke-width: 360;
    stroke-miterlimit: 10;
}
.jssora051:hover {
    opacity: 10;
}
.jssora051.jssora051dn {
    opacity: 0.8;
}
.jssora051.jssora051ds {
    opacity: 0.5;
    pointer-events: none;
}
/*jssor slider loading skin spin css*/
.jssorl-009-spin img {
    animation-name: jssorl-009-spin;
    animation-duration: 1.6s;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
}

@keyframes jssorl-009-spin {
    from {
        transform: rotate(0deg);
    }
    to {
        transform: rotate(360deg);
    }
}

/*jssor slider arrow skin 093 css*/
.jssora093 {
    display: block;
    position: absolute;
    cursor: pointer;
}
.jssora093 .c {
    fill: none;
    stroke: #fff;
    stroke-width: 400;
    stroke-miterlimit: 10;
}
.jssora093 .a {
    fill: none;
    stroke: #fff;
    stroke-width: 400;
    stroke-miterlimit: 10;
}
.jssora093:hover {
    opacity: 0.8;
}
.jssora093.jssora093dn {
    opacity: 0.6;
}
.jssora093.jssora093ds {
    opacity: 0.3;
    pointer-events: none;
}

/*jssor slider thumbnail skin 101 css*/
.jssort101 .p {
    position: absolute;
    top: 0;
    left: 0;
    box-sizing: border-box;
    background: #000;
}
.jssort101 .p .cv {
    position: relative;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border: 2px solid #000;
    box-sizing: border-box;
    z-index: 1;
}
.jssort101 .a {
    fill: none;
    stroke: #fff;
    stroke-width: 400;
    stroke-miterlimit: 10;
    visibility: hidden;
}
.jssort101 .p:hover .cv,
.jssort101 .p.pdn .cv {
    border: none;
    border-color: transparent;
}
.jssort101 .p:hover {
    padding: 2px;
}
.jssort101 .p:hover .cv {
    background-color: rgba(0, 0, 0, 6);
    opacity: 0.35;
}
.jssort101 .p:hover.pdn {
    padding: 0;
}
.jssort101 .p:hover.pdn .cv {
    border: 2px solid #fff;
    background: none;
    opacity: 0.35;
}
.jssort101 .pav .cv {
    border-color: #fff;
    opacity: 0.35;
}
.jssort101 .pav .a,
.jssort101 .p:hover .a {
    visibility: visible;
}
.jssort101 .t {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border: none;
    opacity: 0.6;
}
.jssort101 .pav .t,
.jssort101 .p:hover .t {
    opacity: 1;
}
/*#jssor_1{
    height: 450px;
}*/
.slides {
    cursor: default;
    position: relative;
    top: 0px;
    left: 0px;
    right: 0px;
    overflow: hidden;
    width: 100%;
    height: 500px;
}
@media (max-width: 1600px) {
    .slides{
        height: 430px;
    }
}
@media (max-width: 1400px){
        .slides{
        height: 380px;
    }
}
@media (max-width: 768px) {
    .slides {
        width: 100%;
        height: 300px;
    }
}
@media (max-width: 767px) {
    .slides {
        width: 100%;
        height: 270px;
    }
}
@media (max-width: 576px) {
    .slides {
        width: 100%;
        height: 150px;
    }
}


</style>



<div class="container-fluid invitees_bg d-lg-block py-1">

    <div class="row px-2 py-1">

        <div class="col-12">

            <div class="row">

                <div class="col-12 col-lg-2 px-2 px-lg-2 py-lg-0 py-md-0 mb-2 mb-lg-0 border-md-none layout-border order-2 order-lg-1">

                    <div class="row">

                        <div class="col-12">

                            <div class="mt-2">

                                <a href="{{url('registration?page=regpage')}}" class="btn layout-btn text-white" style="background-color: #a8184b;">Convention Registration

                                </a>

                            </div>

                        </div>

                        <div class="col-12">

                            <div class="mt-2">

                                <a href="{{url('vendorexhibits?page=regpage')}}" class="btn layout-btn text-white" style="background-color: #8dbb01;">Exhibit Registration

                                </a>

                            </div>

                        </div>

                        <div class="col-12">

                            <div class="mt-2">

                                <a href="{{url('events-schedule')}}" target="_blank" class="btn layout-btn text-white" style="background-color: #ff0e45; font-weight:bold;">Events Schedule

                                </a> 

                            </div>

                        </div>

                        <div class="col-12 mt-2">
                            <div class="btn layout-btn text-center">Travel Partner</div>
                             <div class="py-1 px-4 px-md-4 py-md-1 px-lg-3 pt-lg-2">
                                <img src="images/vendors/avni_logo.png" class="img-fluid mx-auto d-block" />
                            </div>
                        </div>
                        <div class="col-12 mt-2">
                            <div class="btn layout-btn text-center">Vendor Partner</div>
                             <div class="py-1 px-4 px-md-4 py-md-1 px-lg-3 pt-lg-2">
                                <img src="images/vendors/mahathi_logo.png" class="img-fluid mx-auto d-block" />
                            </div>
                        </div>

                        <div class="col-12">

                            <div class="mt-2 hightlights-section">

                                <div class="btn layout-btn hightlights-layout-btn text-center">Programs</div>

                                <div class="highlights-block">

                                    <div class="row px-2">

                                        <div class="col-12 col-sm-6 col-md-6 col-lg-12 pb-3 px-lg-4">

                                            <a href="#" class="text-decoration-none text-dark">

                                                <div class="pt-2 pb-1 px-4 px-md-3 pt-md-3 pb-md-1 px-lg-0 py-lg-1">

                                                    <img src="images/programs/cultural-programs.webp" class="img-fluid p5 border-radius-10" alt="Cultural Programs">

                                                </div>

                                                <div class="font-weight-bold">Cultural Programs</div>

                                            </a>

                                        </div>

                                        <div class="col-12 col-sm-6 col-md-6 col-lg-12 pb-3 px-lg-4">

                                            <a href="#" class="text-decoration-none text-dark">

                                                <div class="pt-2 pb-1 px-4 px-md-3 pt-md-3 pb-md-1 px-lg-0 py-lg-1">

                                                    <img src="images/programs/tta-star.webp" class="img-fluid p5 border-radius-10" alt="tta-star">

                                                </div>

                                                <div class="font-weight-bold">TTA Star</div>

                                            </a>

                                        </div>

                                        <div class="col-12 col-sm-6 col-md-6 col-lg-12 pb-3 px-lg-4">

                                            <a href="#" class="text-decoration-none text-dark">

                                                <div class="pt-2 pb-1 px-4 px-md-3 pt-md-3 pb-md-1 px-lg-0 py-lg-1">

                                                    <img src="images/programs/business-seminor.webp" class="img-fluid p5 border-radius-10" alt="Business Seminar">

                                                </div>

                                                <div class="font-weight-bold">Business Seminar</div>

                                            </a>

                                        </div>

                                        <div class="col-12 col-sm-6 col-md-6 col-lg-12 pb-3 px-lg-4">

                                            <a href="#" class="text-decoration-none text-dark">

                                                <div class="pt-2 pb-1 px-4 px-md-3 pt-md-3 pb-md-1 px-lg-0 py-lg-1">

                                                    <img src="images/programs/matrimonial.webp" class="img-fluid p5 border-radius-10" alt="Matrimonial">

                                                </div>

                                                <div class="font-weight-bold">Matrimonial</div>

                                            </a>

                                        </div>

                                        <div class="col-12 col-sm-6 col-md-6 col-lg-12 pb-3 px-lg-4">

                                            <a href="#" class="text-decoration-none text-dark">

                                                <div class="pt-2 pb-1 px-4 px-md-3 pt-md-3 pb-md-1 px-lg-0 py-lg-1">

                                                    <img src="images/programs/youth-programs.webp" class="img-fluid p5 border-radius-10" alt="Youth Programs">

                                                </div>

                                                <div class="font-weight-bold">Youth Programs</div>

                                            </a>

                                        </div>

                                        <div class="col-12 col-sm-6 col-md-6 col-lg-12 pb-3 px-lg-4">

                                            <a href="#" class="text-decoration-none text-dark">

                                                <div class="pt-2 pb-1 px-4 px-md-3 pt-md-3 pb-md-1 px-lg-0 py-lg-1">

                                                    <img src="images/programs/cme.webp" class="img-fluid p5 border-radius-10" alt="CME">

                                                </div>

                                                <div class="font-weight-bold">CME</div>

                                            </a>

                                        </div>

                                        <div class="col-12 col-sm-6 col-md-6 col-lg-12 pb-3 px-lg-4">

                                            <a href="#" class="text-decoration-none text-dark">

                                                <div class="pt-2 pb-1 px-4 px-md-3 pt-md-3 pb-md-1 px-lg-0 py-lg-1">

                                                    <img src="images/programs/alumni.webp" class="img-fluid p5 border-radius-10" alt="Alumni">

                                                </div>

                                                <div class="font-weight-bold">Alumni</div>

                                            </a>

                                        </div>

                                        <div class="col-12 col-sm-6 col-md-6 col-lg-12 pb-3 px-lg-4">

                                            <a href="#" class="text-decoration-none text-dark">

                                                <div class="pt-2 pb-1 px-4 px-md-3 pt-md-3 pb-md-1 px-lg-0 py-lg-1">

                                                    <img src="images/programs/spiritual.webp" class="img-fluid p5 border-radius-10" alt="Spiritual">

                                                </div>

                                                <div class="font-weight-bold">Spiritual and Literary</div>

                                            </a>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-12 col-lg-8 px-0 px-lg-2 order-1">

                    <div class="row mx-0 layout-border mb-1">

                        <div class="col-12 mt-2 d-block d-lg-none">

                            <div class="btn invitees-btn px-5">Invitees</div>

                        </div>

                        <div class="col-lg-2 d-none d-lg-block text-left pl-1">

                            <div>

                                <img src="images/invitees/invitee-left-image.webp" alt="" border="0" class="img-fluid left-welcome-image" width="91" />

                            </div>

                            <div class="invitees-heading-lg-left">Invitees</div>

                        </div>

                        <div class="col-12 col-lg-8 px-lg-3 py-lg-0 py-md-0">

                            <div id="slick-carousel" >

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/nani.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Nani</h6>

                                                <div class="text-purple">Actor</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/devisri-prasad.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Devi Sri Prasad</h6>

                                                <div class="text-purple">Music Director</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/koti.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Koti</h6>

                                                <div class="text-purple">Music Director</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/Vandemataram_Srinivas.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Vandemataram Srinivas</h6>

                                                <div class="text-purple">Music Director & Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/anjali.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Anjali</h6>

                                                <div class="text-purple">Actress</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/suma-kanakala.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Suma Kanakala</h6>

                                                <div class="text-purple">Anchor</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/ritu-varma.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="Ritu Varma" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Ritu Varma</h6>

                                                <div class="text-purple">Actress</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/anchor-ravi.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Ravi</h6>

                                                <div class="text-purple">Anchor</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/sunitha.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Sunitha</h6>

                                                <div class="text-purple">Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/singer-usha.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Usha</h6>

                                                <div class="text-purple">Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/sameera.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Sameera Baradwaj</h6>

                                                <div class="text-purple">Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/caprico.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Capricio</h6>

                                                <div class="text-purple">Music Band</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/dil-raju.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Dil Raju</h6>

                                                <div class="text-purple">Producer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/sv-krishna-reddy.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">SV Krishna Reddy</h6>

                                                <div class="text-purple">Producer & Director</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/harish-shankar.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Harish Shankar</h6>

                                                <div class="text-purple">Director</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/manchu-vishnu.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Manchu Vishnu</h6>

                                                <div class="text-purple">Actor</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/swathi-reddy.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Swathi Reddy</h6>

                                                <div class="text-purple">Actress</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/sagar.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Sagar</h6>

                                                <div class="text-purple">Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/ranjith.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Ranjith</h6>

                                                <div class="text-purple">Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/geetha-madhuri.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Geetha Madhuri</h6>

                                                <div class="text-purple">Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/singer-damini.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Damini</h6>

                                                <div class="text-purple">Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/rita-tyagarajan.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Rita Tyagarajan</h6>

                                                <div class="text-purple">Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/mounima.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Mounima</h6>

                                                <div class="text-purple">Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/srikanth-sandugu-singer.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Srikanth Sandugu</h6>

                                                <div class="text-purple">Music Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/deepthi-nag.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Deepthi Nag</h6>

                                                <div class="text-purple">Singer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/prasanna-reddy.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Prasanna Reddy</h6>

                                                <div class="text-purple">Anchor</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/srilaksmi-kulakarni.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Sri Lakshmi Kulakarni</h6>

                                                <div class="text-purple">Anchor</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/sahitya-anchor.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Sahitya</h6>

                                                <div class="text-purple">Anchor</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/adhire-abhi.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Adhire Abhi</h6>

                                                <div class="text-purple">Actor</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/raghava.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="Raghava" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Raghava</h6>

                                                <div class="text-purple">Actor</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/chaitanya.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="Chaitanya" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Chaitanya Poloju</h6>

                                                <div class="text-purple">Fashion Show choreographer</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/k.satyasrinivas.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="Satyasrinivas" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">K. Satyasrinivas</h6>

                                                <div class="text-purple">Film Art Director</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/moka-vijaya-kumar.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="Moka Vijaya Kumar" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Moka Vijaya Kumar</h6>

                                                <div class="text-purple">International Artist</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/celebrities/janardhan-panela.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="janardhan panela" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Janardhan Panela</h6>

                                                <div class="text-purple">Folk Artist</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/kishan-reddy.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">G Kishan Reddy</h6>

                                                <div class="text-purple">Ministry of Tourism, Govt Of India</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/ktr.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">KT Rama Rao</h6>

                                                <div class="text-purple">Minister of IT, Telangana</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/harish-rao.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">T Harish Rao</h6>

                                                <div class="text-purple">Minister of Finance, Telangana</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/venkat-reddy.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Komatireddy Venkat Reddy</h6>

                                                <div class="text-purple">MP, Bhongir</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/revanth-reddy.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">A Revanth Reddy</h6>

                                                <div class="text-purple">PCC President, Telangana</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/sabitha.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Sabitha Indra Reddy</h6>

                                                <div class="text-purple">Minister of Education, Telangana</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/errabelli-dayakar-rao.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Errabelli Dayakar Rao</h6>

                                                <div class="text-purple">Member of Telangana Legislative Assembly</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/talasani-srinivas-yadav.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Talasani Srinivas Yadav</h6>

                                                <div class="text-purple">Minister of Animal Husbandary, Fisheries and Cinematography, Telangana</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/pocharam.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Pocharam Srinivas Reddy</h6>

                                                <div class="text-purple">Speaker of Legislative Assembly, Telangana</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/jagadishwar-reddy.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">G Jagadish Reddy</h6>

                                                <div class="text-purple">Minister of Energy, Telangana</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/santhosh-kumar.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Santhosh Kumar Joginipally</h6>

                                                <div class="text-purple">MP, Rajya Sabha</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/keshavarao.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">K Keshava Rao</h6>

                                                <div class="text-purple">MP, Rajya Sabha</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/mp-aravind.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">D Arvind</h6>

                                                <div class="text-purple">MP, Nizamabad</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/rohith-reddy.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Pilot Rohith Reddy</h6>

                                                <div class="text-purple">MLA, Tandur</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/dk-aruna.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">DK Aruna</h6>

                                                <div class="text-purple">National Vice President, BJP</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/madhu-yashki.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Madhu Yashki Goud</h6>

                                                <div class="text-purple">Ex MP, Nizamabad</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/vivek-venkataswamy.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">G Vivek</h6>

                                                <div class="text-purple">Ex MP, Peddapalli</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/balakishan.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Rasamayi Balakishan</h6>

                                                <div class="text-purple">MLA, Manakonduru</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div>
                                    <a>
                                        <div class="invitees">
                                            <div class="py5 px-2">
                                                <img src="images/invitees/politicians/boddy-reddy-prabhakar-reddy.webp" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                            </div>

                                            <div class="text-center">
                                                <h6 class="text-purple mb-0">Boddyreddy Prabhakar Reddy</h6>

                                                <div class="text-purple">AICC Member & Secretary of TPCC</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 d-none d-lg-block text-right pr-1">

                            <div>

                                <img src="images/invitees/invitee-right-image.webp" alt="" border="0" class="img-fluid right-welcome-image" width="91" />

                            </div>

                            <div class="invitees-heading-lg-right">Invitees</div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-12 pt-1">

                            <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; overflow: hidden; visibility: hidden;">
                                <!-- Loading Screen -->
                                <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                                    <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
                                </div>
                                <div data-u="slides" class="slides">
                                    <div>
                                        <img data-u="image" class="img-fluid w-100 d-block" src="images/home-page-banners/banner-8.webp" />
                                    </div>
                                    <div>
                                        <img data-u="image" class="img-fluid w-100 d-block" src="images/home-page-banners/banner-1.webp" />
                                    </div>
                                    <div>
                                        <img data-u="image" class="img-fluid w-100 d-block" src="images/home-page-banners/banner-2.webp" />
                                    </div>
                                    <div>
                                        <img data-u="image" class="img-fluid w-100 d-block" src="images/home-page-banners/banner-3.webp" />
                                    </div>
                                    <div>
                                        <img data-u="image" class="img-fluid w-100 d-block" src="images/home-page-banners/banner-4.webp" />
                                    </div>
                                    <div>
                                        <img data-u="image" class="img-fluid w-100 d-block" src="images/home-page-banners/banner-5.webp" />
                                    </div>
                                    <div>
                                        <img data-u="image" class="img-fluid w-100 d-block" src="images/home-page-banners/banner-6.webp" />
                                    </div>
                                    <div>
                                        <img data-u="image" class="img-fluid w-100 d-block" src="images/home-page-banners/banner-7.webp" />
                                    </div>
                                </div>
                                <a data-scale="0" href="https://www.jssor.com" style="display: none; position: absolute;">web animation composer</a>
                                <!-- Bullet Navigator -->
                                <div data-u="navigator" class="jssorb051" style="position: absolute; bottom: 16px; right: 16px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                                    <div data-u="prototype" class="i" style="width: 12px; height: 12px;">
                                        <svg viewbox="0 0 16000 16000" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                            <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                                        </svg>
                                    </div>
                                </div>
                            </div>


                            <!-- <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                                <ol class="carousel-indicators">

                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>

                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>

                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>

                                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>

                                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>

                                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>

                                    <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>

                                    <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>

                                </ol>

                                <div class="carousel-inner">

                                    <div class="carousel-item active">

                                        <img class="d-block w-100 my-auto" src="images//home-page-banners/banner-8.webp" alt="Eighth slide" />

                                    </div>

                                    <div class="carousel-item">

                                        <img class="d-block w-100 my-auto" src="images/home-page-banners/banner-1.webp" alt="First slide" />

                                    </div>

                                    <div class="carousel-item">

                                        <img class="d-block w-100 my-auto" src="images//home-page-banners/banner-2.webp" alt="Second slide" />

                                    </div>

                                    <div class="carousel-item">

                                        <img class="d-block w-100 my-auto" src="images//home-page-banners/banner-3.webp" alt="Third slide" />

                                    </div>

                                    <div class="carousel-item">

                                        <img class="d-block w-100 my-auto" src="images//home-page-banners/banner-4.webp" alt="Fourth slide" />

                                    </div>

                                    <div class="carousel-item">

                                        <img class="d-block w-100 my-auto" src="images//home-page-banners/banner-5.webp" alt="Fifth slide" />

                                    </div>

                                    <div class="carousel-item">

                                        <img class="d-block w-100 my-auto" src="images//home-page-banners/banner-6.webp" alt="Sixth slide" />

                                    </div>
                                    <div class="carousel-item">

                                        <img class="d-block w-100 my-auto" src="images//home-page-banners/banner-7.webp" alt="Seventh slide" />

                                    </div>

                                </div>

                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">

                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                                    <span class="sr-only">Previous</span>

                                </a>

                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">

                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>

                                    <span class="sr-only">Next</span>

                                </a>

                            </div> -->

                        </div>

                    </div>

                </div>

                <div class="col-lg-2 px-2 px-lg-2 border-md-none layout-border pb-2 order-3">
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3 col-lg-12 offset-lg-0">
                            <div class="mt-3 mb-2 mt-lg-2 mb-lg-1 layout-headings-bg w-100 py-1">Grand Sponsor</div>
                        </div>

                        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-12 offset-lg-0">
                            <div class="py-1 px-4 px-md-4 py-md-1 px-lg-3 py-lg-1">
                                <img src="images/sponsors/vadilal.webp" class="img-fluid mx-auto d-block" />
                            </div>
                        </div>

                        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-12 offset-lg-0">
                            <div class="py-1 px-4 px-md-4 py-md-1 px-lg-3 py-lg-1">
                                <img src="images/sponsors/highlands.webp" class="img-fluid mx-auto d-block" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3 col-lg-12 offset-lg-0">
                            <div class="font-weight-bold my-2 mt-lg-1 mb-lg-0 layout-headings-bg w-100 py-1">Sponsors</div>
                        </div>
                        <div class="col-12 px-3 px-lg-4">
                            <div class="row">
                                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-12 offset-lg-0 pt-2 pb-1">
                                    <div class="row">
                                        <div class="col-12">
                                            <div id="scrolll">
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/somireddy.webp" class="img-fluid mx-auto d-block" />
                                                </div>
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/newyork-life.webp" class="img-fluid mx-auto d-block" />
                                                </div>
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/wealth-dna.webp" class="img-fluid mx-auto d-block" />
                                                </div>
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/movers.webp" class="img-fluid mx-auto d-block" />
                                                </div>
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/realty-llc.webp" class="img-fluid mx-auto d-block" />
                                                </div>
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/capitalize.webp" class="img-fluid mx-auto d-block" />
                                                </div>
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/four-oaks.webp" class="img-fluid mx-auto d-block" />
                                                </div>
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/swetha-fashions.webp" class="img-fluid mx-auto d-block" />
                                                </div>
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/dakshin.webp" class="img-fluid mx-auto d-block" />
                                                </div>
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/bhiryanis.webp" class="img-fluid mx-auto d-block" />
                                                    <!-- <img src="images/sponsors/bhiryanis-black.webp" class="img-fluid mx-auto d-block" /> -->
                                                </div>
                                                <div class="py-1 px-5 px-md-4 py-md-1 px-lg-0 py-lg-1">
                                                    <img src="images/sponsors/ag-fintax.webp" class="img-fluid mx-auto d-block" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Convention Messages -->



<section class="container-fluid pt20 pb50">

    <div class="row justify-content-center">

        <div class="col-12 col-md-12 col-lg-12 pb-3">

            <div class="row">

                @foreach($messages as $message)

                <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">

                    <div class="msgs-bg">

                        <h6 class="message-{{ $loop->iteration }} mb-3 text-center">{{$message->designation->name}} Message</h6>

                    </div>

                    <div class="shadow-small border-radius-10 message-bg-{{ $loop->iteration }}">

                        <div class="msg_flower_bg d-flex">

                            <div class="p-3 my-auto">

                                <img src="{{asset(config('conventions.message_display').$message->image_url)}}" class="img-fluid border-radius-20 border-orange-3 tta-member-image" alt="{{$message->name}}" />

                            </div>

                            <div class="pt-0 pb-2 text-white my-auto">

                                <div class="text-center mb-1 heading-font fs18 font-weight-bold">{{$message->name}}</div>

                                <div class="text-center text-white fs14">{{$message->designation->name}}</div>

                            </div>

                        </div>

                        <div class="text-white px-3 pb-3">

                            <p class="pb-2 mb-0 text-center description">

                                {!! substr($message->message, 0, 100) !!}....

                            </p>

                            <div class="text-center pt-1">

                                <a href="{{url('message_content',$message->id)}}">

                                    <i class="fas fa-chevron-circle-right text-white fs20"></i>

                                </a>

                            </div>

                        </div>

                    </div>

                </div>

                @endforeach

            </div>

        </div>

    </div>

</section>



<!-- End of Convention Messages -->



<!-- Convention Team -->



<section class="container-fluid">
    <div class="mx-auto main-heading">
        <span>Convention Team</span>
    </div>
</section>
<section class="container-fluid convention-team-bg">
    <div class="container pt30">

        <div class="row">

            <div class="col-12">

                <div>

                    <ul class="list-unstyled text-center horizontal-scroll">

                        <li class="convention-team-nav-item d-inline-flex">

                            <a class="convention-team-nav-link text-white fs18 pt10 pb10 text-decoration-none convention-team_active_tab_btn" target-id="#convention_advisory_committee">Convention&nbsp;Advisory&nbsp;Committee</a>

                        </li>

                        <li class="convention-team-nav-item d-inline-flex">

                            <a class="convention-team-nav-link text-white fs18 pt10 pb10 text-decoration-none" target-id="#convention_executive_committee">Convention&nbsp;Executive&nbsp;Committee</a>

                        </li>

                        <li class="convention-team-nav-item d-inline-flex">

                            <a class="convention-team-nav-link text-white fs18 pt10 pb10 text-decoration-none" target-id="#convention_regional_advisors">Convention&nbsp;Advisors</a>

                        </li>

                    </ul>

                </div>

            </div>

        </div>

        <div class="row justify-content-center convention-team_active_tab" id="convention_advisory_committee">

            <div class="col-12 col-md-10 col-lg-10">

                <div class="row py-3">

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/advisory-committee/malla-reddy-pailla.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Pailla Malla Reddy" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Dr. Pailla Malla Reddy</h6>

                                <div class="text-center pb-2 text-white">Founder</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/advisory-committee/vijayapal-reddy.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Vijayapal Reddy" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Dr. Vijayapal Reddy</h6>

                                <div class="text-center pb-2 text-white">Chair</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/advisory-committee/haranath-policherla.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Haranath Policherla" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Dr. Haranath Policherla</h6>

                                <div class="text-center pb-2 text-white">Co-Chair</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/advisory-committee/mohan-patalolla.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Mohan Reddy Patalolla" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Dr. Mohan Reddy Patalolla</h6>

                                <div class="text-center pb-2 text-white">Council Member</div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>



        <div class="row justify-content-center" id="convention_executive_committee" style="display: none;">

            <div class="col-12 col-md-10 col-lg-10 py-3">

                <div class="row">

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/mohan-patalolla.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Dr.Mohan Patalolla" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Mohan Reddy Patalolla</h6>

                                <div class="text-center pb-2 text-white">President</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/srinivas-ganagoni.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Srinivas Ganagoni" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Srinivas Ganagoni</h6>

                                <div class="text-center pb-2 text-white">Convener</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/vamshi-reddy.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Vamshi Reddy" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Vamshi Reddy</h6>

                                <div class="text-center pb-2 text-white">President-Elect</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/gangadhar-vuppala.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Gangadhar Vuppala" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Gangadhar Vuppala</h6>

                                <div class="text-center pb-2 text-white">Convention Coordinator</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/bharath-madadi.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Bharath Madadi" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Bharath Madadi</h6>

                                <div class="text-center pb-2 text-white">Past President</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/suresh-venkannagari.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Suresh Reddy Venkannagari" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Suresh Reddy Venkannagari</h6>

                                <div class="text-center pb-2 text-white">Executive Vice-President</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/srinivas-manapragada.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Srinivasa Manapragada" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Srinivasa Manapragada</h6>

                                <div class="text-center pb-2 text-white">Genaral Secretary</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/pavan-ravva.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Pavan Ravva" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Pavan Ravva</h6>

                                <div class="text-center pb-2 text-white">Treasurer</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/venkat-gaddam.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Venkat Gaddam" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Venkat Gaddam</h6>

                                <div class="text-center pb-2 text-white">Executive Director</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/kavitha-reddy.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Kavitha Reddy" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Kavitha Reddy</h6>

                                <div class="text-center pb-2 text-white">Joint Secretary</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/naveen-goli.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Naveen Goli" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Naveen Goli</h6>

                                <div class="text-center pb-2 text-white">International Vice-President</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/venkat-aekka.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Venkat Aekka" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Venkat Aekka</h6>

                                <div class="text-center pb-2 text-white">National Coordinator</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/harinder-tallapalli.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Harinder Tallapalli" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Harinder Tallapally</h6>

                                <div class="text-center pb-2 text-white">Joint Treasurer</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/narasimha-reddy.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Narasimha Reddy" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Narasimha Reddy Donthireddy(LN)</h6>

                                <div class="text-center pb-2 text-white">Media & Communication Director</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/madhavi-soleti.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Madhavi Soleti" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Madhavi Soleti</h6>

                                <div class="text-center pb-2 text-white">Ethics Committee Coordinator</div>

                            </div>

                        </div>

                    </div>

                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">

                        <div class="convention-team">

                            <div>

                                <img data-original="images/convention-team/executive-committee/usha-reddy-mannem.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Usha Mannam" />

                            </div>

                            <div class="pt-3">

                                <h6 class="text-white text-center mb-1 font-weight-bold">Usha Mannem</h6>

                                <div class="text-center pb-2 text-white">Board Of Director</div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>



        <div class="row justify-content-center" id="convention_regional_advisors" style="display: none;">

            <div class="col-12 col-md-10 col-lg-10 py-3">

                <div class="row">

                    <div class="col-12 col-lg-6 offset-lg-3">

                        <div class="row">

                            <div class="col-12 col-sm-6 col-md-4 col-lg-6">

                                <div class="convention-team">

                                    <div>

                                        <img data-original="images/convention-team/convention-advisors/santhosh-pathuri.webp" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Santhosh Pathuri" />

                                    </div>

                                    <div class="pt-3">

                                        <h6 class="text-white text-center mb-1 font-weight-bold">Santhosh Pathuri</h6>

                                    </div>

                                </div>

                            </div>

                            <div class="col-12 col-sm-6 col-md-4 col-lg-6">

                                <div class="convention-team">

                                    <div>

                                        <img alt="Damu Gedala" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" data-original="images/convention-team/convention-advisors/damu-gedala.webp">

                                    </div>

                                    <div class="pt-3">

                                        <h6 class="text-white text-center mb-1 font-weight-bold">

                                        Damu Gedala</h6>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>



<!-- End of Convention Team -->



<!-- Convention Committees -->



<div class="side-border-bg">

    
    <div class="side-border-bg">
        <section class="container-fluid pb50 pt80">
            <div class="mx-auto main-heading mb40">
                <div>
                    <span>Convention committees</span>
                </div>
            </div>
            <div class="container">

                <div class="row">

                    <div class="col-12 px-0">

                        <div id="caroselCommitteeIndicators" class="carousel slide" data-ride="carousel" data-interval="5000">

                            <ol class="carousel-indicators carosel">

                                <li data-target="#caroselCommitteeIndicators" data-slide-to="0" class="active"></li>

                                <li data-target="#caroselCommitteeIndicators" data-slide-to="1"></li>

                                <li data-target="#caroselCommitteeIndicators" data-slide-to="2"></li>

                            </ol>

                            <div class="carousel-inner carosel-inner">

                                <div class="carousel-item active">

                                    <div class="col-12">

                                        <div class="row pb-5">

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{'banquet'}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/banquet.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Banquet</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('budgetfinance')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/finance.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Budget & Finance</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('businessforum')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/bi_group.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Business Forum</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('cme')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/cme.webp" class="img-fluid mx-auto d-block" alt="">

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">CE / CME</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('celebritiescoordination')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/coordination.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Celebrities Coordination</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('corporatesponsorship')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/carporate_sponsorship.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Corporate Sponsorships</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('cultural')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/cultural.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Cultural</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('decorations')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/decoration.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Decorations</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('donorrelationship')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/youth.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Donor Relationship</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('food')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/food.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Food</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('fundraising')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/fund_raising.webp" class="img-fluid mx-auto d-block" alt="">

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Fund Raising</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('hospitality')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/hospitality.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Hospitality</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>









                                        </div>

                                    </div>

                                </div>

                                <div class="carousel-item">

                                    <div class="col-12">

                                        <div class="row pb-5">











                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('immigrationforum')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/immigration.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Immigration Forum </div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('kidsactivities')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/cultural.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Kids Activities</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('literacy')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/literacy.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Literacy</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('matrimonial')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/matrimony.webp" class="img-fluid mx-auto d-block" alt="">

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Matrimonial</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>



                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('mediacommunication')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/social_media.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Media & Communication</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('overseascoordination')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/coordination.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Overseas coordination</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('politicalforum')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/political_forum.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Political Forum</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('programevents')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/publicity_r_media.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Programs And Events</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>


                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('registration')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/event_registration.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Registration and Reception</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('safetysecurity')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/security.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Safety & Security</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('shortfilm')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/photography.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Short Film</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('socialmedia')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/social_media.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Social Media</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="carousel-item">

                                    <div class="col-12">

                                        <div class="row pb-5">

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('souvenir')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/souvenir.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Souvenir</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('spiritual')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/spiritual.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Spiritual</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('stage')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/av.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Stage & Venue</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('startupcube')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/fund_raising.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Startup Cube</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('transportation')}}" class=" text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/transportation.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Transportation</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('ttastar')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/tta_star.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">TTA Star</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('vendorexhibits')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/exhibit_booth.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Vendors & Exhibits</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>



                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('volunteer')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/immigration.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Volunteer</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('web')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/flyers.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Web</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('womensforum')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/women.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Women's Forum</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>

                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">

                                                <a href="{{url('youthforum')}}" class="text-decoration-none">

                                                    <div class="convention-cc-bg">

                                                        <div class="p-3">

                                                            <div>

                                                                <img data-original="images/cc-icons/youth.webp" class="img-fluid mx-auto d-block" alt="" />

                                                            </div>

                                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Youth Forum</div>

                                                        </div>

                                                    </div>

                                                </a>

                                            </div>











                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="carosel_slider-btn">

                                <a class="carousel-control-prev" href="#caroselCommitteeIndicators" role="button" data-slide="prev">

                                    <span class="carosel-control-prev-icon" aria-hidden="true"></span>

                                    <span class="sr-only">Previous</span>

                                </a>

                                <a class="carousel-control-next" href="#caroselCommitteeIndicators" role="button" data-slide="next">

                                    <span class="carosel-control-next-icon" aria-hidden="true"></span>

                                    <span class="sr-only">Next</span>

                                </a>

                            </div>

                            <div>

                                <div class="text-right">

                                    <a href="{{url('convention_committee_list')}}" class="text-decoration-none carosel_view-all-btn font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>



        <!-- End of Convention Committees -->



        <!-- Donors -->

        <section class="container-fluid mt30">
            <div class="mx-auto main-heading">
                <span>Donors</span>
            </div>
        </section>
        <section class="container-fluid donors-bg">
            <div class="container mt45 pt40">
                <div class="row pr-4 px-lg-0">
                    <div class="col-12">
                        <div>
                            <ul class="list-unstyled donors-horizontal-scroll d-lg-flex justify-content-center">
                                <li class="donors-nav-item">
                                    <a class="donors-nav-link text-decoration-none donors_active_tab_btn" target-id="#chakravarti-poshakulu">
                                        <span>Chakravarti&nbsp;Poshakulu</span>
                                    </a>
                                </li>
                                <li class="donors-nav-item">
                                    <a class="donors-nav-link text-decoration-none" target-id="#maharaja-poshakulu">
                                        <span>Maharaja Poshakulu</span>
                                    </a>
                                </li>
                                <li class="donors-nav-item">
                                    <a class="donors-nav-link text-decoration-none" target-id="#raja-poshakulu">
                                        <span>Raja&nbsp;Poshakulu</span>
                                    </a>
                                </li>
                                <li class="donors-nav-item">
                                    <a class="donors-nav-link text-decoration-none" target-id="#mukhya-atithi">
                                        <span>Mukhya&nbsp;Atithi</span>
                                    </a>
                                </li>
                                <li class="donors-nav-item">
                                    <a class="donors-nav-link text-decoration-none" target-id="#visishta-atithi">
                                        <span>Visishta&nbsp;Atithi</span>
                                    </a>
                                </li>
                                <li class="donors-nav-item">
                                    <a class="donors-nav-link text-decoration-none" target-id="#pratyeka-atithi">
                                        <span>Pratyeka&nbsp;Atithi</span>
                                    </a>
                                </li>
                                <li class="donors-nav-item">
                                    <a class="donors-nav-link text-decoration-none" target-id="#gourava-atithi">
                                        <span>Gourava&nbsp;Atithi</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center  donors_active_tab" id="chakravarti-poshakulu">
                    <div class="col-12">
                        <div class="row px-0 px-sm-4 px-md-4 px-lg-0 py-3">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img data-original="images/donors/malla-reddy-pailla.webp" class="img-fluid mx-auto d-block" alt="Malla Reddy Pailla" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">Dr. Malla Reddy Pailla & Sadhana (NY)</h6>
                                        <div class="donors-amount">$100,000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center donors_list" id="maharaja-poshakulu" style="display: none;">
                    <div class="col-12">
                        <div class="row px-0 px-sm-4 px-md-4 px-lg-0 py-3">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Premsagar Reddy" class="img-fluid mx-auto d-block" src="images/donors/premsagar-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Premsagar Reddy (CA)
                                        </h6>
                                        <div class="donors-amount">
                                            $50,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="VijayPal Reddy" class="img-fluid mx-auto d-block" src="images/donors/vijaypal-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Vijaypal Reddy & Shobha Reddy (IN)
                                        </h6>
                                        <div class="donors-amount">
                                            $50,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Haranath Policherla" class="img-fluid mx-auto d-block" data-original="images/donors/haranath-policherla.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Haranath Policherla (MI)
                                        </h6>
                                        <div class="donors-amount">
                                            $50,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Mohan Patalolla" class="img-fluid mx-auto d-block" data-original="images/donors/mohan-patalolla.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Mohan Patalolla (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $50,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="MSN Reddy" class="img-fluid mx-auto d-block" src="images/donors/msn-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. MSN Reddy
                                        </h6>
                                        <div class="donors-amount">
                                            $50,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center donors_list" id="raja-poshakulu" style="display: none;">
                    <div class="col-12">
                        <div class="row px-0 px-sm-4 px-md-4 px-lg-0 py-3">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Vamshi Reddy" class="img-fluid mx-auto d-block" src="images/donors/vamshi-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Vamshi Reddy (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $25,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Srinivas Ganagoni" class="img-fluid mx-auto d-block" src="images/donors/srinivas-ganagoni.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Srinivas Ganagoni (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $25,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Bharath Madadi" class="img-fluid mx-auto d-block" data-original="images/donors/bharath-madadi.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Bharath Madadi (GA)
                                        </h6>
                                        <div class="donors-amount">
                                            $25,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Dr. Sudhakar Vidiyala &amp; Geetha" class="img-fluid mx-auto d-block" data-original="images/donors/sudhakar-vidyala.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Sudhakar Vidiyala &amp; Geetha (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $25,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Ramanath Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/ramnath-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ramnath Reddy (CT)
                                        </h6>
                                        <div class="donors-amount">
                                            $25,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Bhuvanesh Boojala" class="img-fluid mx-auto d-block" data-original="images/donors/bhuvanesh-boojala.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Bhuvanesh Boojala (DC)
                                        </h6>
                                        <div class="donors-amount">
                                            $25,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center donors_list" id="mukhya-atithi" style="display: none;">
                    <div class="col-12">
                        <div class="row px-0 px-sm-4 px-md-4 px-lg-0 py-3">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sekhar Vemparala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Sekhar Vemparala
                                        </h6>
                                        <div class="donors-amount">
                                            $20,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Santosh Somireddy" class="img-fluid mx-auto d-block" src="images/donors/santosh-somireddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Santosh Somireddy (VA)
                                        </h6>
                                        <div class="donors-amount">
                                            $20,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="M.S. Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/m.s.reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. M.S. Reddy (Cheese Reddy)
                                        </h6>
                                        <div class="donors-amount">
                                            $15,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Poorna Atluri" class="img-fluid mx-auto d-block" data-original="images/donors/poorna-atluri.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Poorna Atluri & Dr. Shuba (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $15,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Lakshmi Moparthi" class="img-fluid mx-auto d-block" src="images/donors/lakshmi-moparthi.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Lakshmi Moparthi(NYLIFE) (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $15,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sharath Vemuganti" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Sharath Vemuganti (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $12,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Satish Vemana" class="img-fluid mx-auto d-block" src="images/donors/satish-vemana.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Satish Vemana (DC)
                                        </h6>
                                        <div class="donors-amount">
                                            $11,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="HanimiReddy Kakireddy Family Foundation" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. HanimiReddy Kakireddy Family Foundation (CA)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                       

                                        <img alt="Pavan Ravva" class="img-fluid mx-auto d-block" data-original="images/donors/pavan-ravva.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. & Mr. Ragini & Pavan Ravva (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Gangadhar Vuppala" class="img-fluid mx-auto d-block" data-original="images/donors/gangadhar-vuppala.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Saritha & Mr. Gangadhar Vuppala (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Shravan &amp; Shilpa" class="img-fluid mx-auto d-block" data-original="images/donors/shravan-shilpa.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Shravan Reddy & Dr. Shilpa Reddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Suresh Reddy Venkannagari" class="img-fluid mx-auto d-block" data-original="images/donors/suresh-reddy-venkannagari.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Suresh Reddy Venkannagari (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Venkat Gaddam" class="img-fluid mx-auto d-block" data-original="images/donors/venkat-gaddam.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Venkat Gaddam (GA)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Satish Mekala" class="img-fluid mx-auto d-block" data-original="images/donors/satish-mekala.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Satish Mekala (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Amith Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/amith-reddy-surakanti.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Amith Reddy Surakanti (CA)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Vishnu Battula" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Vishnu Battula (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Pailla Arjun Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/arjun-reddy-pailla.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Pailla Arjun Reddy (CA)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Jithender Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/jithender-reddy.webp" />

                                        
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Jithender Reddy (TX)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Kumar Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/kumar-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Kumar Reddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Praveen Guduru" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Praveen Guduru (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Vinod Koduru" class="img-fluid mx-auto d-block" data-original="images/donors/vinod-koduru.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Deepa & Mr. Vinod Koduru (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Sreenivasa Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/srinivas-reddy-gade.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Gade Sreenivasa Reddy (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Bharathi Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/bharathi-reddy.webp" />


                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Bharathi Reddy & Ramu Reddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Mamatha" class="img-fluid mx-auto d-block" data-original="images/donors/mamatha.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Mamatha & Mr. Madhava Reddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Rajender Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/rajender-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Rajender Reddy & Jhansi Reddy (CA)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Kishore Ganji" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Kishore Ganji (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Prasad Chukkapalli" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Prasad Chukkapalli (MI)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Srinivas Anugu" class="img-fluid mx-auto d-block" data-original="images/donors/srinivas-anugu.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Srinivas Anugu (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="PN9 Realty" class="img-fluid mx-auto d-block" src="images/donors/pn9-reality.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            PN9 Realty (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $10,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center donors_list" id="visishta-atithi" style="display: none;">
                    <div class="col-12">
                        <div class="row px-0 px-sm-4 px-md-4 px-lg-0 py-3">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Shiva Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/shiva-reddy-palempalli.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Shiva Reddy Palempalli (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $6,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sanjeeva Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Sanjeeva Reddy (LA)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,001
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Lakshmi Narasimha Reddy Donthireddy" class="img-fluid mx-auto d-block" data-original="images/donors/lakshmi-narasimha-reddy-donthireddy.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Narasimha Reddy Donthireddy & Prashanti Reddy (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Divakar Jandyam" class="img-fluid mx-auto d-block" data-original="images/donors/divakar-jandyam.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Divakar Jandyam (MA)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Shiva Reddy Kolla" class="img-fluid mx-auto d-block" data-original="images/donors/shiva-reddy-kolla.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Shiva Reddy Kolla & Navya Reddy (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Kiran Duddagi" class="img-fluid mx-auto d-block" data-original="images/donors/kiran-duddagi.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Kiran Duddagi (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Vijay Bhaskar Kalal" class="img-fluid mx-auto d-block" data-original="images/donors/vijay-bhaskar.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Vijay Bhaskar Kalal (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Rajeshwar Reddy Gangasani" class="img-fluid mx-auto d-block" data-original="images/donors/rajeshwar-reddy-gangasani.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Rajeshwar Reddy Gangasani (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Anna Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Anna Reddy (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Ram Mohan Chinnala" class="img-fluid mx-auto d-block" data-original="images/donors/ram-mohan.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ram Mohan & Dr. Sandhya R (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="T. P. Srinivas Rao" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. T. P. Srinivas Rao (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Venkateshwar Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/venkateshwar-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Venkateshwar Reddy (OK)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Kavitha Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/kavitha-reddy.webp" />

                                        
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Kavitha Reddy (IN)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Naveen Goli" class="img-fluid mx-auto d-block" data-original="images/donors/naveen-goli.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Veena & Mr. Naveen Goli (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sharath Vemula" class="img-fluid mx-auto d-block" data-original="images/donors/archana-and-sharath-chandra-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. & Mr. Archana & Sharath Chandra Reddy Vemula (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Amar Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/amarender.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Prasanna & Amarender Reddy Paduru (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Chinnababu Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Chinnababu Reddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Manjunath Matada" class="img-fluid mx-auto d-block" data-original="images/donors/manjunath.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Manjunath Matada (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Nathan Godhumala" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Nathan Godhumala (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Naveen Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/naveen-reddy-vodlakonda.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Naveen Reddy Vodlakonda (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Pradeep Samala" class="img-fluid mx-auto d-block" data-original="images/donors/pradeepsamala.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Pradeep Samala (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Rama Surya Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Rama Surya Reddy (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Ravi Dhannapuneni" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ravi Dhannapuneni (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Srinivas Reddy Pinnapureddy" class="img-fluid mx-auto d-block" data-original="images/donors/srinivas-pinnapureddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Srinivas Reddy Pinnapureddy (TX)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Bindu Latha" class="img-fluid mx-auto d-block" data-original="images/donors/bindu-latha-cheedella.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Bindu Latha Cheedella (KS)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sambashiva Rao Venigalla" class="img-fluid mx-auto d-block" data-original="images/donors/sambashiva-rao.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Mahalakshmi & Mr. Sambashiva Rao Venigalla (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Usha Mannem" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. &amp; Mrs. Usha &amp; Suneel Teepireddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Lenina Thumma" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Lenina Thumma & Mr. Rajender Prasad Vavilala (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Vani" class="img-fluid mx-auto d-block" data-original="images/donors/laxman-reddy-anugu.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Vani & Mr. Laxman Reddy Anugu (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Hanumanth Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/hanumanth-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Hanumanth Reddy (IL)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Suresh Puttagunta" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Suresh Puttagunta (MI)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Venkat Aekka" class="img-fluid mx-auto d-block" data-original="images/donors/venkat-aekka.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Venkat Aekka (MI)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Mallik Bolla" class="img-fluid mx-auto d-block" data-original="images/donors/mallik-bolla.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Mallik Bolla (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Raj Anandeshi" class="img-fluid mx-auto d-block" data-original="images/donors/raj-anandeshi.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Raj Anandeshi (TX)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Jay Challa" class="img-fluid mx-auto d-block" data-original="images/donors/jay-challa.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Jay Challa (DC)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Ranjita & Raja Kasukurthi" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Ranjita & Raja Kasukurthi (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Mr.Praveen Tadakamalla" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Praveen Tadakamalla (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Mr. Anjaiah Chowdary Lavu" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Anjaiah Chowdary Lavu (GA)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Mr. Ravi Dhannapuneni" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ravi Dhannapuneni (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $5,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center donors_list" id="pratyeka-atithi" style="display: none;">
                    <div class="col-12">
                        <div class="row px-0 px-sm-4 px-md-4 px-lg-0 py-3">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Raghuram Pannala" class="img-fluid mx-auto d-block" data-original="images/donors/raghuram-pannala.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Raghuram Pannala (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $4,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Sunitha Kanumuri" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Sunitha Kanumuri (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Ajay Reddy Macha" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ajay Reddy Macha (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Chendra Sena Sriramoju" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Chendra Sena Sriramoju (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Ganesh Veramaneni" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ganesh Veramaneni (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Manikyam" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Manikyam Thukkapuram (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Manohar Rao Bodke" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Manohar Rao Bodke (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Pradeep Mettu" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Pradeep Mettu (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Ravinder Veeravalli" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ravinder Veeravalli (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Sangeetha Borra" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Sangeetha Borra (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Sridhar Chaduvu" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Sridhar Chaduvu (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Nagaj" class="img-fluid mx-auto d-block" data-original="images/donors/hanumanthu-davuluri.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Nagaj & Mr. Hanumanthu Davuluri (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                <img alt="Sirisha Donthireddy" class="img-fluid mx-auto d-block" data-original="images/donors/sirisha.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Sirisha Donthireddy & Mr. Chendrashekar Pylla (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Vasu Sabbella" class="img-fluid mx-auto d-block" data-original="images/donors/vasu-sabbella.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Vasu Sabbella (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div><div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Raju Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Raju Reddy (CA)
                                        </h6>
                                        <div class="donors-amount">
                                            $3,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Arun Arkala" class="img-fluid mx-auto d-block" src="images/donors/arun-arkala.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Arun Arkala (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,600
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Vidyasagara Maramareddi" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Vidyasagara Maramareddi (MI)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,501
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Mahesh Sambu" class="img-fluid mx-auto d-block" src="images/donors/mahesh-sambu.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Mahesh Sambu (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Linga Reddy Nathala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Linga Reddy Nathala (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Dr.Silaja & Satish Kalva" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Silaja & Satish Kalva (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Arun Reddy Mekala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Arun Reddy Mekala (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sunil Koganti" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Sunil Koganti (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Satish Tummala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Satish Tummala (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Ravi Potluri" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ravi Potluri (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Anjan Karnati" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Anjan Karnati (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Chandrashekara Reddy" class="img-fluid mx-auto d-block" src="images/donors/chandrashekara-reddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Chandrashekara Reddy (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Santosh Reddy Koram" class="img-fluid mx-auto d-block" src="images/donors/santosh-reddy-koram.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Santosh Reddy Koram (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Srinivas Manapragada" class="img-fluid mx-auto d-block" src="images/donors/srinivasa_manapragada.webp">
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Narasimha Murthy (Simha) , Yuvraj Tejase, Manihar Murthy, Srinivasa & Kavitha Manapragada (CA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Srinivas Guduru" class="img-fluid mx-auto d-block" src="images/donors/srinivas-guduru.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Srinivas Guduru (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Ashok Chintakunta &amp; Madhavi Soleti" class="img-fluid mx-auto d-block" src="images/donors/ashok-chintakunta.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Abhinav, Anirudh, Madhavi &amp; Ashok Chintakunta (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Bhaskar Pinna" class="img-fluid mx-auto d-block" src="images/donors/bhaskar-pinna.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Bhaskar Pinna (DE)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Hari Pyreddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Hari Pyreddy (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Kiran Guduru" class="img-fluid mx-auto d-block" data-original="images/donors/kiran-guduru.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Kiran Guduru (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Mahender Narala" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Mahender Narala
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Ranjeeth Kyatham" class="img-fluid mx-auto d-block" data-original="images/donors/ranjeeth-kyatham.webp" />

                                        
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ranjeeth and Nagasree (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sahodhar Peddireddy" class="img-fluid mx-auto d-block" data-original="images/donors/sahodhar-peddireddy.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Sahodhar Peddireddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Mallik Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Mallik Reddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Narasihma Peruka" class="img-fluid mx-auto d-block" data-original="images/donors/narasimha-peruka.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Narsimha Peruka & Vijaya Vani Peruka Family (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Prasad Kunarapu" class="img-fluid mx-auto d-block" data-original="images/donors/prasad-kunarapu.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Prasad Kunarapu Family (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Ramakrishna Sannidhi" class="img-fluid mx-auto d-block" data-original="images/donors/ramakrishna-sannidhi.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ramakrishna Sannidhi (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Ramana Kotha" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ramana Kotha (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Roopak Kalluri" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Roopak Kalluri (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Vamshi Gullapalli" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Vamshi Gullapalli (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Anjan Karnati" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Anjan Karnati (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Murtuza Mohammad" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Murtuza Mohammad (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Venkat Sunki Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/venkat-aekka.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Venkat Sunki Reddy (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Deepa Jalagam" class="img-fluid mx-auto d-block" data-original="images/donors/deepa-jalagam.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">Mrs. Deepa Jalagam (NJ)</h6>
                                        <div class="donors-amount">$2,500</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Ravinder Kamtam" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp"  />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">Mrs. & Mr. Manorama & Ravinder Kamtam (NY)</h6>
                                        <div class="donors-amount">$2,500</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Karthik Nimmala" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp"  />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">Mr. Karthik Nimmala (GA)</h6>
                                        <div class="donors-amount">$2,500</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Srini Sunkaraneni" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp"  />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">Mr. Srini Sunkaraneni (NJ)</h6>
                                        <div class="donors-amount">$2,500</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sripal Reddy Katanguri" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">Mr. Sripal Reddy Katanguri (GA)</h6>
                                        <div class="donors-amount">$2,500</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sudheer" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">Mr. Sudheer Kotha (GA)</h6>
                                        <div class="donors-amount">$2,500</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Nishanth Sirikonda" class="img-fluid mx-auto d-block" data-original="images/donors/nishanth-sirikonda-and-shravya.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. & Mrs. Nishanth Sirikonda & Shravya (NC)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sudhakar Uppala" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Sudhakar Uppala (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                       <img alt="Srinivas Reddy Alla" class="img-fluid mx-auto d-block" data-original="images/donors/srinivas-reddy-alla.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Srinivas Reddy Alla (TX)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Jagga Rao Alluri" class="img-fluid mx-auto d-block" data-original="images/donors/jagga-rao.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Jagga Rao Alluri & Janaki (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Silaja" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Silaja & Satish Kalva (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sridevi & Sharath Bhoomi" class="img-fluid mx-auto d-block" data-original="images/donors/sridevi.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Sridevi & Sharath Bhoomi (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Subhadra" class="img-fluid mx-auto d-block" data-original="images/donors/dattatreyudu.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Subhadra & Dr. Nori Dattatreyudu (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Kalyan Ramachary" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Kalyan Ramachary (MI)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sandhya Gavva" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Sandhya Gavva (TX)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Stanley" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Stanley & Mr. Sumathy Reddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="V. Nagendra Guptha" class="img-fluid mx-auto d-block" data-original="images/donors/vijaya.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Vijaya & Mr. Nagendra Guptha Vellore (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Arun Mekala" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Arun Mekala (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Gautham Vepur" class="img-fluid mx-auto d-block" data-original="images/donors/goutham-vepur.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Gautham Vepur (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Venu Enugala & Shiva Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Venu Enugala & Shiva Reddy (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Narendar Yarava" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Narender Reddy Yarava & Kavitha (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Srinivas Reddy Nandi" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Srinivas Reddy Nandi (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Raju Kakkerla" class="img-fluid mx-auto d-block" src="images/donors/raju-kakkerla.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Raju Kakkerla (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Giridhar Masireddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Giridhar Masireddy (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center donors_list" id="gourava-atithi" style="display: none;">
                    <div class="col-12">
                        <div class="row px-0 px-sm-4 px-md-4 px-lg-0 py-3">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Anil Kumar Chintalapudi" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Anil Kumar Chintalapudi (MI)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Ramesh Thangellapalli" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Ramesh Thangellapalli (CA)
                                        </h6>
                                        <div class="donors-amount">
                                            $2,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Manohar Chillara" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Manohar Chillara (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,500
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Srikanth Routhu" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Srikanth Routhu (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,250
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Aruna Sridhar Kancharla" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Aruna Sridhar Kancharla (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,200
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Chandana Angitipalli" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Chandana Angitipalli & Mr. Venkat Reddy Guduru (Ny)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,116
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Jaya Prakash" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Jaya Prakash Enjapuri (Ny)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,116
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Lavanya R Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Lavanya R Reddy (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,001
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Varun Allam" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Varun Allam (MA)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Yogi &amp; Rama Vanama" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Yogi &amp; Rama Vanama (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Raghu Mudupolu" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Raghu Mudupolu (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Suresh Thanda" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Suresh Thanda (WA)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Venu Gopal Palla" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Venu Gopal Pylla (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Srinivas Kodali" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Sridevi & Mr. Srinivas Kodali (MI)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Uma" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. & Mrs. Uma & Surendra Gorrapati (MI)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Anil Nagoli" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Anil Nagoli & Sandeepa Venagapudi (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Anitha" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Anitha & Mr. Sathya Gaggenapally (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Asuini" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Asuini & Vijay Bhat (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Bharathi" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Bharathi & Sekhar V. S. Nelanuthala (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Jaideep Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Jaideep Reddy & Kalpana (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Venugopal Palla" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Venugopal Palla & Anita Punreddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Gaddam Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Gaddam Reddy (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Hemalatha" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Hemalatha & Mr. Udaya Kumar Dommaraju (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Jyothi" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Jyothi & Mr. Raghotham Reddy Katla (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Kathyanani" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Kathyanani & Kallika Reddy Alla (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>

                                        <img alt="Madhusudhan Reddy" class="img-fluid mx-auto d-block" data-original="images/donors/madhusudan-reddy.webp" />

                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Madhusudhan Reddy Nalla (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Navatha Bussa" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Navatha Bussa & Mr. Pashupathinath Siddamshetty (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Nehru Kataru" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Nehru Kataru (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Padmasree" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Padmasree & Mr. Sridhar Gummadavelli (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Pavanaja" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Pavanaja & Rama Joga Venkata Eranki (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Phanibushan" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Phanibushan Thadepalli (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Rajitha Kontham" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Rajitha Kontham & Mr. Chandra Sekhar Reddy Nalla (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Ramya" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Ramya & Mr. Bala Murali Nagisetti (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Salini Allaparthi" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Salini Allaparthi & Mr. Kalyan Kumar Meka (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Saritha" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Saritha & Mr. Ramesh Eligeti (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Shilaja" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Shilaja & Mr. Satya Challapalli (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sirisha Aalla" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Sirisha Aalla & Mr. Phani Kumar Jaggavarapu (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Subba Lakshmi" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Subba Lakshmi & Mr. Dasarath Gurram (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Subbarao Anumolu" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Subbarao Anumolu (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sumanth Ramisetti" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Sumanth Ramisetti (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Tanuja" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Tanuja & Mr. Harisankar Rasaputra (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Venkatasubramanian" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Venkatasubramanian Jayaraman (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Vijaya" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Vijaya & Mr. Vishnu Marisetti (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Vinitha Marru" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mrs. Vinitha Marru & Mr. Sunil Thakkalapally (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Haripriya & Harish Cherivirala" class="img-fluid mx-auto d-block" data-original="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Haripriya & Harish Cherivirala (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Murali Guthikonda" class="img-fluid mx-auto d-block" data-original="images/donors/murali-guthikonda.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Murali Guthikonda & Shaila Cordone (MI)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Raja Rajasekhar" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Raja Rajasekhar (DC)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Mahesh Pogaku" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Mahesh Pogaku (NJ)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Krishna Lam" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Krishna Lam (DC)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Kranti Dudam" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Kranti Dudam (DC)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Govardhan Reddy Tokala" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Govardhan Reddy Tokala (DC)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Arjun Kamasetti" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Arjun Kamasetti (DC)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Vani & Chandu Singirikonda" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Vani & Chandu Singirikonda (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Suresh Bondugula" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Suresh Bondugula (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sudharshan Chetkuri" class="img-fluid mx-auto d-block" src="images/donors/sudarshan-chetkuri.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Sudharshan Chetkuri (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Subhash Reddy Karra" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Subhash Reddy Karra (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Sanjeev Reddy & Sneha Reddy Bokka" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Sanjeev Reddy & Sneha Reddy Bokka (TX)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Kranthi Dudem" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Kranthi Dudem (DC)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Kiran Kalva" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Mr. Kiran Kalva (DC)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Dr. Aravinda Srikar Reddy" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Aravinda Srikar Reddy (PA)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="donors-team">
                                    <div>
                                        <img alt="Dr. Rajender Reddy Jinna" class="img-fluid mx-auto d-block" src="images/donors/no-image.webp" />
                                    </div>
                                    <div class="pt-1">
                                        <h6 class="donors-name">
                                            Dr. Rajender Reddy Jinna (NY)
                                        </h6>
                                        <div class="donors-amount">
                                            $1,000
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- End of Donors -->




    <section class="container-fluid">
        <div class="mx-auto main-heading">
            <span>Convention Gallery</span>
        </div>
    </section>
    <section class="container-fluid pb80 mt45 gallery-bg">
        <div class="container pt50">
            <div class="row px-4 px-sm-0">
                <div class="col-sm-6 col-md-6 col-lg-4 thumb">
                    <a href="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-1.webp" class="fancybox" rel="ligthbox">
                        <img data-original="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-1.webp" class="zoom img-fluid" alt="" />
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 thumb">
                    <a href="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-2.webp" class="fancybox" rel="ligthbox">
                        <img data-original="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-2.webp" class="zoom img-fluid" alt="" />
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 thumb">
                    <a href="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-3.webp" class="fancybox" rel="ligthbox">
                        <img data-original="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-3.webp" class="zoom img-fluid" alt="" />
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 thumb">
                    <a href="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-4.webp" class="fancybox" rel="ligthbox">
                        <img data-original="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-4.webp" class="zoom img-fluid" alt="" />
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 thumb">
                    <a href="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-5.webp" class="fancybox" rel="ligthbox">
                        <img data-original="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-5.webp" class="zoom img-fluid" alt="" />
                    </a>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4 thumb">
                    <a href="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-6.webp" class="fancybox" rel="ligthbox">
                        <img data-original="images/gallery/TTA-committees-meet-happened-in-New-Jersey-Photos-6.webp" class="zoom img-fluid" alt="" />
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="text-right">
                        <a href="{{url('gallery')}}" class="text-decoration-none text-white font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End of Gallery -->

    <!-- Video Gallery -->

    <section class="container-fluid mt80">
        <div class="mx-auto main-heading">
            <span>Video Gallery</span>
        </div>
    </section>
    <section class="container-fluid pb80 gallery-bg" style="background-image: none !important;">
        <div class="container mt45 pt40 pb10">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-2">
                    <div class="">
                        <iframe
                        width="100%"
                        class="border-radius-5 gallery-video"
                        src="https://www.youtube.com/embed/CkMNaUjieWM"
                        title="YouTube video player"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen
                        ></iframe>
                    </div>
                    <h6 class="py-2 text-white text-center">Kick Off and Fund-Raising Event</h6>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-2">
                    <div class="">
                         <iframe width="100%"
                         class="border-radius-5 gallery-video" src="https://www.youtube.com/embed/gu5tzB4s_5A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                     </iframe>
                    </div>
                    <h6 class="py-2 text-white text-center">Yadadri Kalyanam Event</h6>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-2">
                    <div class="">
                        <iframe
                        width="100%"
                        class="border-radius-5 gallery-video"
                        src="https://www.youtube.com/embed/dZ1u0-gX5xw"
                        title="YouTube video player"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen
                        ></iframe>
                    </div>
                    <h6 class="py-2 text-white text-center">Political Forum Invitation</h6>
                </div>
            </div>
        </div>
    </section>

<!-- End of Vedio Gallery -->



<!-- Media Partners -->



<section class="container-fluid mt80">
    <div class="mx-auto main-heading mb15">
        <span>Media partners</span>
    </div>
</section>
<section class="container-fluid">
    <div class="container">

        <div class="row justify-content-center">

            <div class="col-12 col-md-12 col-lg-12 py-3">

                <marquee behavior="alternate" class="px-5">

                    <div class="row flex-wrap-nowrap px-5">
                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/tv5.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>

                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/tv9.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>

                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/asia-tv.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>

                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/mana-tv.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>

                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/v6-news.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>

                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/sakshi-tv.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>

                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/telugu-nri-radio.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>

                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/haritha-talks.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>

                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/radiovani.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>

                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/gnn.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>

                        <div class="m-3">
                            <div class="meadia-partner-image">
                                <img data-original="images/media-partners/us1-television.webp" class="img-fluid mx-auto d-block w-100" />
                            </div>
                        </div>
                    </div>
                </marquee>  

            </div>

        </div>

    </div>

</section>



<!-- End of Media Partners -->

</div>

<!-- End Of Content -->



@section('javascript')



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script src='https://code.jquery.com/jquery-1.11.0.min.js'></script>

<script src="js/slick.min.js"></script>

<script src="js/vertical-carousel-slider.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js" integrity="sha512-jNDtFf7qgU0eH/+Z42FG4fw3w7DM/9zbgNPe3wfJlCylVDTT3IgKW5r92Vy9IHa6U50vyMz5gRByIu4YIXFtaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">
    var i = 0;

$(document).ready(function(){

    $('#spinner').show();
     document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
``
   $('img').lazyload();

  var interval = setInterval(function () {
    i += 4; // speed
    $('#scrolll').animate({ scrollTop: i }, 1);
    if (i >= $('#scrolll').prop('scrollHeight') - $('#scrolll').height()) {
      i = 0;
    }
  }, 100);
   $('.donors_list').click(function(){ 

        $(window).scroll();
  });
});
</script>

<!-- Vertical Slider -->

<script type="text/javascript">
$('.vertical-slick-carousel').slick({
        vertical:true,
        verticalSwiping:true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 0,
        speed:1000,
        cssEase: 'linear',
        infinite: true,
        arrows:false,
        touchMove:true,
        swipeToSlide:true,
        swipe:true
  })
</script>

<!-- Horizontal Slick Slider -->

<script type="text/javascript">
    $("#slick-carousel").slick({
        slidesToShow: 2,
        autoplay: true,
        autoplaySpeed: 0,
        speed: 2000,
        cssEase: "linear",
        infinite: true,
        focusOnSelect: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToShow: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                },
            },
        ],
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none",
        });

        $(".zoom").hover(
            function () {
                $(this).addClass("transition");
            },
            function () {
                $(this).removeClass("transition");
            }
            );
    });
</script>

<script>

    $(".convention-team-nav-link").click(function () {

        //active_tab



        $(".convention-team_active_tab_btn").removeClass("convention-team-nav-link nav-item");

        $(".convention-team_active_tab_btn").removeClass("convention-team_active_tab_btn");

        $(this).addClass("convention-team-nav-link nav-item");

        $(this).addClass("convention-team_active_tab_btn");



        $(".convention-team_active_tab").hide();

        $(".convention-team_active_tab").removeClass("convention-team_active_tab");

        $($(this).attr("target-id")).addClass("convention-team_active_tab");

        $($(this).attr("target-id")).show();

    });

</script>

<script>
    $(".es-nav-link").click(function () {
        //active_tab

        $(".es_active_tab_btn").removeClass("es-nav-link nav-item");
        $(".es_active_tab_btn").removeClass("es_active_tab_btn");
        $(this).addClass("es-nav-link nav-item");
        $(this).addClass("es_active_tab_btn");

        $(".es_active_tab").hide();
        $(".es_active_tab").removeClass("es_active_tab");
        $($(this).attr("target-id")).addClass("es_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>

<script>

    $(".convention-committee-nav-link").click(function () {

        //active_tab

        $(".convention-committee_active_tab_btn").removeClass("convention-committee-nav-link");

        $(".convention-committee_active_tab_btn").removeClass("convention-committee_active_tab_btn");

        $(this).addClass("convention-committee-nav-link");

        $(this).addClass("convention-committee_active_tab_btn");



        $(".convention-committee_active_tab").hide();

        $(".convention-committee_active_tab").removeClass("convention-committee_active_tab");

        $($(this).attr("target-id")).addClass("convention-committee_active_tab");

        $($(this).attr("target-id")).show();

    });

</script>

<script type="text/javascript">
    var i = 0;

$(document).ready(function(){
  var interval = setInterval(function () {
    i += 4; // speed
    $('#scrolll').animate({ scrollTop: i }, 1);
    if (i >= $('#scrolll').prop('scrollHeight') - $('#scrolll').height()) {
      i = 0;
    }
  }, 100);

 
});
</script>

<!-- Banners Carousel -->

<script type="text/javascript">
    window.jssor_1_slider_init = function () {
        var jssor_1_SlideshowTransitions = [
            { $Duration: 1200, $Zoom: 1, $Easing: { $Zoom: $Jease$.$InCubic, $Opacity: $Jease$.$OutQuad }, $Opacity: 2 },
            { $Duration: 1000, $Zoom: 11, $SlideOut: true, $Easing: { $Zoom: $Jease$.$InExpo, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
            { $Duration: 1200, $Zoom: 1, $Rotate: 1, $During: { $Zoom: [0.2, 0.8], $Rotate: [0.2, 0.8] }, $Easing: { $Zoom: $Jease$.$Swing, $Opacity: $Jease$.$Linear, $Rotate: $Jease$.$Swing }, $Opacity: 2, $Round: { $Rotate: 0.5 } },
            { $Duration: 1000, $Zoom: 11, $Rotate: 1, $SlideOut: true, $Easing: { $Zoom: $Jease$.$InQuint, $Opacity: $Jease$.$Linear, $Rotate: $Jease$.$InQuint }, $Opacity: 2, $Round: { $Rotate: 0.8 } },
            { $Duration: 1200, x: 0.5, $Cols: 2, $Zoom: 1, $Assembly: 2049, $ChessMode: { $Column: 15 }, $Easing: { $Left: $Jease$.$InCubic, $Zoom: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
            { $Duration: 1200, x: 4, $Cols: 2, $Zoom: 11, $SlideOut: true, $Assembly: 2049, $ChessMode: { $Column: 15 }, $Easing: { $Left: $Jease$.$InExpo, $Zoom: $Jease$.$InExpo, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
            { $Duration: 1200, x: 0.6, $Zoom: 1, $Rotate: 1, $During: { $Left: [0.2, 0.8], $Zoom: [0.2, 0.8], $Rotate: [0.2, 0.8] }, $Opacity: 2, $Round: { $Rotate: 0.5 } },
            { $Duration: 1000, x: -4, $Zoom: 11, $Rotate: 1, $SlideOut: true, $Easing: { $Left: $Jease$.$InQuint, $Zoom: $Jease$.$InQuart, $Opacity: $Jease$.$Linear, $Rotate: $Jease$.$InQuint }, $Opacity: 2, $Round: { $Rotate: 0.8 } },
            { $Duration: 1200, x: -0.6, $Zoom: 1, $Rotate: 1, $During: { $Left: [0.2, 0.8], $Zoom: [0.2, 0.8], $Rotate: [0.2, 0.8] }, $Opacity: 2, $Round: { $Rotate: 0.5 } },
            { $Duration: 1000, x: 4, $Zoom: 11, $Rotate: 1, $SlideOut: true, $Easing: { $Left: $Jease$.$InQuint, $Zoom: $Jease$.$InQuart, $Opacity: $Jease$.$Linear, $Rotate: $Jease$.$InQuint }, $Opacity: 2, $Round: { $Rotate: 0.8 } },
            {
                $Duration: 1200,
                x: 0.5,
                y: 0.3,
                $Cols: 2,
                $Zoom: 1,
                $Rotate: 1,
                $Assembly: 2049,
                $ChessMode: { $Column: 15 },
                $Easing: { $Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Zoom: $Jease$.$InCubic, $Opacity: $Jease$.$OutQuad, $Rotate: $Jease$.$InCubic },
                $Opacity: 2,
                $Round: { $Rotate: 0.7 },
            },
            {
                $Duration: 1000,
                x: 0.5,
                y: 0.3,
                $Cols: 2,
                $Zoom: 1,
                $Rotate: 1,
                $SlideOut: true,
                $Assembly: 2049,
                $ChessMode: { $Column: 15 },
                $Easing: { $Left: $Jease$.$InExpo, $Top: $Jease$.$InExpo, $Zoom: $Jease$.$InExpo, $Opacity: $Jease$.$Linear, $Rotate: $Jease$.$InExpo },
                $Opacity: 2,
                $Round: { $Rotate: 0.7 },
            },
            {
                $Duration: 1200,
                x: -4,
                y: 2,
                $Rows: 2,
                $Zoom: 11,
                $Rotate: 1,
                $Assembly: 2049,
                $ChessMode: { $Row: 28 },
                $Easing: { $Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Zoom: $Jease$.$InCubic, $Opacity: $Jease$.$OutQuad, $Rotate: $Jease$.$InCubic },
                $Opacity: 2,
                $Round: { $Rotate: 0.7 },
            },
            {
                $Duration: 1200,
                x: 1,
                y: 2,
                $Cols: 2,
                $Zoom: 11,
                $Rotate: 1,
                $Assembly: 2049,
                $ChessMode: { $Column: 19 },
                $Easing: { $Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Zoom: $Jease$.$InCubic, $Opacity: $Jease$.$OutQuad, $Rotate: $Jease$.$InCubic },
                $Opacity: 2,
                $Round: { $Rotate: 0.8 },
            },
        ];

        var jssor_1_options = {
            $AutoPlay: 1,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1,
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$,
                $SpacingX: 10,
                $SpacingY: 10,
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Rows: 2,
                $SpacingX: 14,
                $SpacingY: 12,
                $Orientation: 2,
                $Align: 156,
            },
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*#region responsive code begin*/

        var MAX_WIDTH = 1500;

        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {
                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            } else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*#endregion responsive code end*/
    };
</script>

<script src="js/jssor.slider-28.1.0.min.js" type="text/javascript"></script>

<script type="text/javascript">
    jssor_1_slider_init();
</script>

@endsection



@endsection

