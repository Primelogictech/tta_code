@extends('layouts.user.base')
@section('content')


<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <h5>My Dashboard</h5>
            </div>
            <div class="col-12">
                <div class="card-body px-0">
                    <div class="table-responsive">
                        @if(Auth::user()->total_amount!=0)
                        <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>Member ID</th>
                                    <th>Registration ID</th>
                                    <th>Member Details</th>
                                    <th width="150px" >Children Details</th>
                                    
                                   
                                    <th width="150px" >Payment Details</th>
                                    <th>View Payment History </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ Auth::user()->member_id }}</td>
                                    <td>{{ Auth::user()->registration_id }}
                                    @if($first_payment)
                                     <br>
                                    <b>Registered on :</b> 
                                    {{    $first_payment->created_at->format('d-m-Y')}}
                                    @endif
                                    </td>
                                    <td>
                                        <div class="py-1">
                                            <span>Name:</span><span> {{ ucfirst(Auth::user()->first_name) }} {{ ucfirst(Auth::user()->last_name) }}</span>
                                        </div>
                                        <div class="py-1">
                                            <span>Email:</span><span> {{ Auth::user()->email }}</span>
                                        </div>
                                        <div class="py-1">
                                            <span>Mobile:</span><span> {{ Auth::user()->phone_code }} {{ Auth::user()->mobile }} </span>
                                        </div>
                                    </td>
                                    <td> 
                                    <b>Age Below 6</b>: {{ Auth::user()->children_count['below_6']?? "0" }}
                                    <br>
                                    <b>Age 7 to 15 </b>: {{ Auth::user()->children_count['age_7_to_15']?? "0" }}
                                    <br>
                                    <b>Age 16 to 23 </b>: {{ Auth::user()->children_count['age_16_to_23']?? "0" }}
                                    
                                    </td>
                                   
                                    
                                    <td>
                                        <div>
                                        <b> Total Amount :</b> ${{Auth::user()->total_amount }}</div>
                                        <div>
                                        <b>Paid Amount :</b> ${{Auth::user()->amount_paid }}</div>
                                        <div>
                                        <b>Discount Amount :</b> ${{Auth::user()->discount_amount }}
                                        <b>Discount Code :</b> ${{Auth::user()->discount_code??"" }}<br>
                                    </div>
                                        <div>
                                        <b>Due :</b>
                                            @php
                                                $due=Auth::user()->total_amount -Auth::user()->amount_paid-Auth::user()->discount_amount;
                                            @endphp    
                                        $ {{ $due }}</div>
                                        <div>Payment status: {{ucfirst(Auth::user()->payment_status) }}</div>
                                         @if($due>0)
                                        <div>
                                            <a href="{{route('pay-pending-amount')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Pay Pending Amount</a>
                                        </div>
                                        @endif
                                    </td>

                                        <td>
                                             <div>
                                            <a href="{{url('paymenthistory')}}" class="btn btn-sm btn-success text-white my-1 mx-1">View Payment History</a>
                                        </div>
                                        </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        @if(Auth::user()->categorydetails)
                        <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    <th>Sponsorship Type</th>
                                    <th>Sponsorship Category</th>
                                    <th>View Features</th>
                                    
                                  
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Donor</td>
                                    
                                   
                                    <td>
                                         {{  Auth::user()->categorydetails->donortype->name ?? ""}} ($ {{Auth::user()->donor_amount }})</td>
                                    <td>
                                        <div>
                                            <a href="{{route('viewfeatures')}}" class="btn btn-sm btn-success text-white my-1 mx-1">View Features</a>
                                            <a href="{{route('upgrade')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Upgrade Donation Package</a>
                                            
                                        </div>
                                         <!-- <div>
                                            <a href="{{url('bookticket')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Upgrade</a>
                                        </div> -->
                                       
                                       
                                        
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                        @endif
                        <br>
                        @if(Auth::user()->registration_amount)

                         <table class="table-bordered table table-hover table-center mb-0">
                            <thead>
                                <tr>
                                    
                                    
                                    <th>Sponsorship Type</th>
                                    <th>Sponsorship Category</th>
                                    <th>Upgrade</th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Family / Individual</td>
                                    
                                   
                                    <td>
                                        @foreach(Auth::user()->individual_registration as $key => $value)
                                        @if ($value==null)
                                            @continue
                                        @endif
                                        <?php $Individual = \App\Models\Admin\SponsorCategory::where('status', 1)->where('id',$key)->first();
                                        
                                        echo $Individual->benefits[0]." - ".$value;
                                        echo "</br>";

                                         ?>

                                        @endforeach
                                       Total Amount : $ {{Auth::user()->registration_amount }}
                                    <div>
                                        Discount Amount : ${{Auth::user()->discount_amount }}</div>
                                    <div>
                                        Paid Amount : ${{Auth::user()->registration_amount - Auth::user()->discount_amount }}</div></td>
                                    </td>

                                    <td>
                                     <a href="{{route('reg-pkg-upgrade')}}" class="btn btn-sm btn-success text-white my-1 mx-1">Upgrade Registration Package</a>
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                            @endif
                        @else
                        <h5> You have not Registered for the event. Please click <a href="registration?page=regpage">here</a> to register</h5>
                        @endif
                    </div>
                    <div class="fs15 mt-3 font-weight-bold">Comments from Registration Committee: {!! Auth::user()->registration_note !!} </div>
                </div>
            </div>
        </div>
    </div>
</section>



@section('javascript')



@endsection


@endsection
