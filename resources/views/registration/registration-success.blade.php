@extends('layouts.user.base')
@section('content')


<style type="text/css">
    input[type="checkbox"], input[type="radio"] {
        box-sizing: border-box;
        padding: 0;
        position: relative;
        top: 2px;
    }
    .table th {
        vertical-align: middle !important; 
    }
    .input-checkbox {
        position: absolute;
        top: 6px;
    }
    .agree-checkbox{
        position: relative;
        top: 21px !important;
    }
    .sub-input{
        border: 0;
    }
    .subtotal-input{
        border: 0;
    }
    .sub-input-two{
        border: 0;
    }
    </style>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <!--<div class="col-12 shadow-md-none bg-white shadow-small py-0 pt-1 px-1 pb-md-5">
                
               
     <h3>Registration Successful</h3> -->
     <div class="col-12 shadow-md-none bg-white shadow-small py-0 pt-1 p-3 pb-md-5">
<h3 class="text-violet text-center my-4">Your Exhibit Registration is Successful. Please check your email for more details</h3>

     
     <br>
     <br>
     <table class="datatable table table-bordered table-center mb-0" >


         <thead>
             <tr>
                 <td>Type</td>
                 <td>Payment Details</td>
                 <td>Amount</td>
                 <td>Status</td>
             </tr>
         </thead>
         <tbody>
             <tr>
                <td>Exhibit Registration</td>
                 <td>
                 Transaction Id :{{$payment->unique_id_for_payment??""}}<br>
                 Payment Mode :{{$payment->paymentmethord->name??""}}<br>
                 @if($payment->paymentmethord->name=="Other")
                 Payment Date :{{$payment->more_info['transaction_date']??""}}<br>
                 On Behalf Of :{{$payment->more_info['Payment_made_through']??""}}<br>
                 Company Name :{{$payment->more_info['company_name']??""}}<br>
                 @endif

                 @if($payment->paymentmethord->name=="Check")
                 Check Date :{{$payment->more_info['cheque_date']??""}}<br>
                 @endif
                 <br>   
                 <br>   
                 </td>
                 <td>${{$payment->payment_amount??0}}</td>
                 <td>Success</td>
             </tr>
         </tbody>
     </table>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
    
     </div>
        </div>
    </div>
</section>




@section('javascript')
@endsection


@endsection
