@extends('layouts.user.base') @section('content')

<style type="text/css">
    .fade:not(.show) {
        opacity: 1;
    }
</style>

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Souvenir Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">souvenir@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-10 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll" role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-2 px-sm-3 px-md-4 border active" href="#committee-members" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-2 px-sm-3 px-md-4 border" href="#packages" role="tab" data-toggle="tab">Package&nbsp;Details</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-2 px-sm-3 px-md-4 border" id="reg-page" href="#souvenir-registration" role="tab" data-toggle="tab">Submit&nbsp;Your&nbsp;Article</a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee-members">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Srinivas_Guduru.webp" class="img-fluid w-100" alt="" />
                                                </div>
                                                <div class="committee-member-name">Srinivas Guduru</div>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Srini_Todupunoori.webp" class="img-fluid w-100" alt="Srini Todupunoori" />
                                                </div>
                                                <div class="committee-member-name">Srini Todupunoori</div>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Nikshipta_Reddy_Kura.webp" class="img-fluid w-100" alt="Nikshipta Reddy Kura" />
                                                </div>
                                                <div class="committee-member-name">Nikshipta Reddy Kura</div>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
										<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Rajeswari_Burra.webp" class="img-fluid w-100" alt="Rajeswari Burra" />
                                                </div>
                                                <div class="committee-member-name">Rajeswari Burra</div>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Yogiswar_Vanama.webp" class="img-fluid w-100" alt="Yogiswar Vanama" />
                                                </div>
                                                <div class="committee-member-name">Yogiswar Vanama</div>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Sudhakar_Uppala.webp" class="img-fluid w-100" alt="" />
                                                </div>
                                                <div class="committee-member-name">Sudhakar Uppala</div>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Srinivas_Mannapragada.webp" class="img-fluid w-100" alt="" />
                                                </div>
                                                <div class="committee-member-name">Srinivasa Manapragada</div>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Lakshmi_Rayavarapu.webp" class="img-fluid w-100" alt="" />
                                                </div>
                                                <div class="committee-member-name">Lakshmi Rayavarapu</div>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>

                            <!-- Packages -->

                            <div role="tabpanel" class="tab-pane fade" id="packages">
                                <div class="col-12 shadow-small py-3">
                                    <div class="row">
                                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                                            <div class="">
                                                <h5 class="d-inline-block text-skyblue">Digital/Web/Souvenir Package Details:</h5>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                                            <div class="text-left text-lg-right">
                                                <a href="{{ url('vendorexhibits') }}?page=regpage" class="btn btn-danger px-2 px-sm-3 px-md-5 text-white fs-xs-15 fs-xss-11 fs-sm-15" target="_blank">
                                                    Click here to register for Exhibit & Souvenir Advertising
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-12 mt-3">
                                            <div class="table-responsive">
                                                <table class="datatable table table-bordered table-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th rowspan="2">Package Code</th>

                                                            <th rowspan="2">Description</th>

                                                            <th class="text-center" colspan="2">Package Amount</th>
                                                        </tr>

                                                        <tr>
                                                            <th>Size</th>

                                                            <th>Amount</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <tr>
                                                            <td>DIG01</td>

                                                            <td>Digital Ads</td>

                                                            <td>10 - 20 sec</td>

                                                            <td>$2,000 - $5000</td>
                                                        </tr>

                                                        <tr>
                                                            <td>SOU01</td>

                                                            <td>Souvenir Ad (Color)</td>

                                                            <td>Full Page</td>

                                                            <td>$1,000</td>
                                                        </tr>

                                                        <tr>
                                                            <td>SOU02</td>

                                                            <td>Souvenir Ad (Color)</td>

                                                            <td>Half Page</td>

                                                            <td>$500</td>
                                                        </tr>

                                                        <tr>
                                                            <td>SOU03</td>

                                                            <td>Souvenir Ad (Color)</td>

                                                            <td>Quarter</td>

                                                            <td>$250</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Souvenir registration -->

                            <div role="tabpanel" class="tab-pane fade" id="souvenir-registration">
                                <div class="col-12 shadow-small py-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="text-left text-md-right text-lg-right">
                                                <a href="{{ url('vendorexhibits') }}?page=regpage" class="btn btn-danger px-2 px-sm-3 px-md-5 text-white fs-xs-15 fs-xss-11 fs-sm-15" target="_blank">
                                                    Click here to register for Exhibit & Souvenir Advertising
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-12 mt-3">
                                            <h5 class="text-violet">
                                                Submit your article :
                                            </h5>
                                            <div class="text-center">
                                                <div class="btn btn-danger px-2 px-sm-3 px-md-5">
                                                    <!--<a href="https://docs.google.com/forms/d/e/1FAIpQLSfWRS_UsSbJ7Bz_4DpuxWIbqz6zlyub-7jx748km2xs910T0w/viewform" class="text-white fs-xs-15 fs-xss-11 fs-sm-15" target="_blank">
                                                        <i class="fas fa-hand-point-right pr-2"></i>Click here to submit your article
                                                    </a>-->
													    <i class="fas fa-hand-point-right pr-2"></i>We are longer accepting articles.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <h5 class="text-violet mt-3">
                                                Guidelines :
                                            </h5>
                                            <p>
                                                TTA (Telangana American Telugu Association) will be releasing a Convention Special Souvenir during the convention in May 2022. TTA requests all writers to send the following articles in Telugu
                                                or English for consideration.
                                            </p>
                                            <ul class="flower-list fs15">
                                                <li>Stories, Poems (Padyalu, Kavitalu)</li>
                                                <li>Essays, Cartoons, Art, Photography</li>
                                                <li>Other types of content that reflect Telangana Culture, Traditions, Festivals are welcome.</li>
                                            </ul>

                                            <p>Preference will be given to unpublished articles. Published articles can also be submitted, provided they include details of earlier publications.</p>
                                            <p>
                                                <span class="font-weight-bold">Last date for submissions : </span>
                                                <span>February 25th, 2022</span>
                                            </p>
                                            <p>The Editorial Committee decision is final.</p>

                                            <div class="mt-3">
                                                <img src="images/flyers/Souvenir_Reg1.webp" class="img-fluid mx-auto d-block" />
                                            </div>

                                            <div class="mt-3">
                                                <img src="images/flyers/Souvenir_Reg2.webp" class="img-fluid mx-auto d-block" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')
<script type="text/javascript">
    $(document).ready(function () {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split("&"),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split("=");

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };

        var page = getUrlParameter("page");

        if (page == "regpage") {
            $("#reg-page").trigger("click");
        }
    });
</script>

@endsection 

@endsection
