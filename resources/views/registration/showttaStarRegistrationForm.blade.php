@extends('layouts.user.base')

@section('content')

<style type="text/css">
    .fade:not(.show) {
        opacity: 1;
    }
</style>

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">TTA Star Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">ttastar@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-10 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                    	<div>
	                        <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
	                            <li class="nav-item d-inline-flex">
	                                <a class="nav-link px-2 px-sm-3 px-md-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
	                            </li>
	                            <li class="nav-item d-inline-flex">
	                                <a class="nav-link px-2 px-sm-3 px-md-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines</a>
	                            </li>
	                            <li class="nav-item d-inline-flex" >
	                                <a class="nav-link px-2 px-sm-3 px-md-4 border" id="reg-page" href="#souvenir-registration" role="tab" data-toggle="tab">Registration</a>
	                            </li>
	                        </ul>
	                    </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Ravi Kamarasu.jpg" class="img-fluid w-100" alt="Ravi Kamarasu" />
                                                </div>
                                                <div class="committee-member-name">Ravi Kamarasu</div>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Rama Prabhala.jpg" class="img-fluid w-100" alt="Rama Prabhala" />
                                                </div>
                                                <div class="committee-member-name">Rama Prabhala</div>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Srinivas-Guduru.jpg" class="img-fluid w-100" alt="Srinivas Srinivas-Guduru" />
                                                </div>
                                                <div class="committee-member-name">Srinivas Guduru</div>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <!-- <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/no-image1.jpg" class="img-fluid w-100" alt="Venu" />
                                                </div>
                                                <div class="committee-member-name">Venu (CT)</div>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/no-image1.jpg" class="img-fluid w-100" alt="Vamsi Priya" />
                                                </div>
                                                <div class="committee-member-name">Vamsi Priya</div>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/no-image1.jpg" class="img-fluid w-100" alt="Shiva ReddSuman Mudambay" />
                                                </div>
                                                <div class="committee-member-name">Suman Mudamba</div>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="souvenir-content">
                                <div class="col-12 shadow-small py-3">
                                    <p><span class="font-weight-bold">TTA Star:</span> Sing a song and shine like a sTTAr by participating in TTA Star music competitions.</p>
                                    <p>
                                        Telangana American Telugu Association (TTA) is very excited to conduct their Music competitions. The Semi Finals and Finals will be held during TTA’s Grand Convention in May 2022 and will be judged by
                                        esteemed celebrity judges from Telugu Film Industry.
                                    </p>
                                    <h5 class="text-violet">
                                        TTA Star competition participation guidelines:
                                    </h5>
                                    <p class="mb10">
                                        There are 4 rounds of competitions out of which 2 rounds are online and 2 on stage.
                                    </p>
                                    <ul class="fs15">
                                        <li>Audition round - Online</li>
                                        <li>Preliminary round - Online</li>
                                        <li>Semi-finals - On Stage during Convention</li>
                                        <li>Finals – On Stage during Convention</li>
                                    </ul>
                                    <h5 class="text-violet">Audition Round requirements:</h5>
                                    <ul class="flower-list fs15">
                                        <li>Age group to participate is 13- 45 years</li>
                                        <li>Need both Audio & Video of singer singing the song</li>
                                        <li>Mention your Full name, Song details at the beginning of the recording</li>
                                        <li>Sing without using a Karaoke track – Just your voice</li>
                                        <li>Song must be from a Telugu movie.</li>
                                        <li>Pallavi and one Charanam of the song (you can choose your charanam when song has more than one charanam)</li>
                                        <li> Upload your video to YouTube, copy and paste the YouTube link on the registration page</li>
                                        <li>Deadline to send your audition is <b>March 17th, 2022</b></li>
                                        <li>Singers selected from Audition round will be invited to participate in the preliminary round on zoom call. Preliminary round will be live telecasted on TV Asia Telugu channel,</li>
                                    </ul>

                                    <p><span class="font-weight-bold">Note:</span> Audition results are final.</p>

                                    <h5 class="text-violet">Other rounds :</h5>

                                    <ul class="flower-list fs15">
                                        <li>In Preliminary, Semi and Final rounds the Singer can use a Karaoke track. The song must not exceed 5 minutes.</li>
                                        <li>
                                            The guidelines & timelines for other rounds will be communicated to the short listed participants from each of the previous rounds via Singer’s email that was provided at the time of registration.
                                        </li>
                                        <li>A chance for the Winners to get introduced to the TFI (Telugu Film Industry)</li>
                                        <li>All FINALISTS will get an opportunity to sing for TV Asia (Telugu) channel</li>
                                    </ul>

                                    <p>All the best,</p>

                                    <p>TTA Star Committee</p>

                                    <div>
                                        <img src="images/tta-reg1.jpeg" class="img-fluid mx-auto d-block">
                                    </div>

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="souvenir-registration">
                                <div class="shadow-small py-3">
                                    <h4 class="font-weight-bold text-center mb-3 px-3 px-md-0">Registration For TTA Music Competition</h4>
                                    <h6 class="font-weight-bold text-center mb-3">Please upload your contents in the below form</h6>
                                    <div class="col-12 col-lg-10 offset-lg-1 mt-4">
                                        <div class="shadow-small shadow-small-sm-none py-4 px-0 px-md-0 px-lg-4">
                                            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfRckCkal_VkXgqcFwfit6WQozezTgVBzn8_LrXXiz4ZUbbhQ/viewform?embedded=true" class="width-fill-available" height="1410" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




@section('javascript')
<script type="text/javascript">

    $(document).ready(function() {

            var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };
            
            var page = getUrlParameter('page');
            
            if(page=='regpage'){
                $('#reg-page').trigger("click");
            }
                


    })
</script>


@endsection



@endsection
