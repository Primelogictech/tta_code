<style type="text/css">
    .invitees-team {
        padding: 15px 10px;
        border-radius: 20px;
        height: 275px;
    }
    li.invitees-nav-item.d-inline-flex {
        padding: 0px 15px;
        cursor: pointer;
    }
    .invitees-nav-item a {
        cursor: pointer;
        font-family: "Eurostar";
        font-weight: 600;
        color: #92278f;
    }
    .invitees_active_tab_btn {
        border-bottom: 2px solid #fff;
    }
    .invitees-team img {
        box-shadow: 1px 1px 10px grey;
        border: 1px solid #fff;
        padding: 2px;
        background: #efefef;
    }
    .invitees-name {
        color: #fff;
        text-align: center;
        font-weight: bold;
        margin-bottom: 5px;
    }
    .invitees-role {
        color: #fff;
        text-align: center;
        padding-bottom: 10px;
    }
    @media (min-width: 1200px) {
        .container,
        .container-lg,
        .container-md,
        .container-sm,
        .container-xl {
            max-width: 1200px;
        }
    }
</style>

<section class="container mt60">
    <div class="mx-auto main-heading">
        <span>Invitees</span>
    </div>
</section>

<section class="container mt60 mb80 convention-team-bg">
    <div class="row">
        <div class="col-12 pt40">
            <div>
                <ul class="list-unstyled text-center">
                    <li class="invitees-nav-item d-inline-flex">
                        <a class="invitees-nav-link text-white fs20 text-decoration-none invitees_active_tab_btn" target-id="#celebrities">Celebrities</a>
                    </li>
                    <li class="invitees-nav-item d-inline-flex">
                        <a class="invitees-nav-link text-white fs20 text-decoration-none" target-id="#politicians">Politicians</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row justify-content-center invitees_active_tab" id="celebrities">
        <div class="col-12 col-md-10 col-lg-10">
            <div class="row py-3">
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Devi Sri Prasad" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/devisri-prasad.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Devi Sri Prasad
                            </h6>
                            <div class="invitees-role">
                                Music Director
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Devi Sri Prasad" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/nikhil.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Nikhil
                            </h6>
                            <div class="invitees-role">
                                Actor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Devi Sri Prasad" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/anjali.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Anjali
                            </h6>
                            <div class="invitees-role">
                                Actress
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Suma Kanakala" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/suma-kanakala.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Suma Kanakala
                            </h6>
                            <div class="invitees-role">
                                Anchor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Ritu Varma" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/ritu-varma.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Ritu Varma
                            </h6>
                            <div class="invitees-role">
                                Actress
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Ravi" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/anchor-ravi.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Ravi
                            </h6>
                            <div class="invitees-role">
                                Anchor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Sunitha" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/sunitha.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Sunitha
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Usha" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/singer-usha.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Usha
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Usha" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/sameera.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Sameera Baradwaj
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Capricio" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/caprico.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Capricio
                            </h6>
                            <div class="invitees-role">
                                Music Band
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Dil Raju" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/dil-raju.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Dil Raju
                            </h6>
                            <div class="invitees-role">
                                Producer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="SV Krishna Reddy" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/sv-krishna-reddy.webp">
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                SV Krishna Reddy
                            </h6>
                            <div class="invitees-role">
                                Producer &amp; Director
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Harish Shankar" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/harish-shankar.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Harish Shankar
                            </h6>
                            <div class="invitees-role">
                                Director
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Manchu Vishnu" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/manchu-vishnu.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Manchu Vishnu
                            </h6>
                            <div class="invitees-role">
                                Actor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Swathi Reddy" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/swathi-reddy.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Swathi Reddy
                            </h6>
                            <div class="invitees-role">
                                Actress
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Sagar" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/sagar.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Sagar
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Ranjith" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/ranjith.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Ranjith
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Geetha Madhuri" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/geetha-madhuri.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Geetha Madhuri
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Damini" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/singer-damini.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Damini
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Rita Tyagarajan" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/rita-tyagarajan.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Rita Tyagarajan
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Mounima" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/mounima.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Mounima
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Srikanth Sandugu" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/srikanth-sandugu-singer.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Srikanth Sandugu
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Deepthi Nag" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/deepthi-nag.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Deepthi Nag
                            </h6>
                            <div class="invitees-role">
                                Singer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Prasanna Reddy" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/prasanna-reddy.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Prasanna Reddy
                            </h6>
                            <div class="invitees-role">
                                Anchor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Sri Lakshmi Kulakarni" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/srilaksmi-kulakarni.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Sri Lakshmi Kulakarni
                            </h6>
                            <div class="invitees-role">
                                Anchor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Sahitya" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/sahitya-anchor.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Sahitya
                            </h6>
                            <div class="invitees-role">
                                Anchor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Adhire Abhi" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/adhire-abhi.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Adhire Abhi
                            </h6>
                            <div class="invitees-role">
                                Actor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Raghava" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/raghava.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Raghava
                            </h6>
                            <div class="invitees-role">
                                Actor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Chaitanya" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/chaitanya.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Chaitanya Poloju
                            </h6>
                            <div class="invitees-role">
                                Fashion Show choreographer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Satyasrinivas" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/k.satyasrinivas.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                K. Satyasrinivas
                            </h6>
                            <div class="invitees-role">
                                Film Art Director
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Moka Vijaya Kumar" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/celebrities/moka-vijaya-kumar.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Moka Vijaya Kumar
                            </h6>
                            <div class="invitees-role">
                                International Artist
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 px-5 px-lg-3">
                    <div class="my-3">
                        <img src="images/invitees/celebrities/celebrities-banner1.webp" class="img-fluid mx-auto d-block w-100" alt="">
                    </div>
                    <!-- <div class="my-4">
                        <img src="images/invitees/celebrities/celebrities-banner2.webp" class="img-fluid mx-auto d-block w-100" alt="">
                    </div> -->
                </div>
            </div>
        </div>
    </div>

     <div class="row justify-content-center" id="politicians" style="display: none;">
        <div class="col-12 col-md-10 col-lg-10 py-3">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="G Kishan Reddy" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/kishan-reddy.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                G Kishan Reddy
                            </h6>
                            <div class="invitees-role">
                                Ministry of Tourism, Govt Of India
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="KT Rama Rao" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/ktr.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                KT Rama Rao
                            </h6>
                            <div class="invitees-role">
                                Minister of IT, Telangana
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="T Harish Rao" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/harish-rao.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                T Harish Rao
                            </h6>
                            <div class="invitees-role">
                                Minister of Finance, Telangana
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Venkat Reddy" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/venkat-reddy.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Komatireddy Venkat Reddy
                            </h6>
                            <div class="invitees-role">
                                MP, Bhongir
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="A Revanth Reddy" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/revanth-reddy.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                A Revanth Reddy
                            </h6>
                            <div class="invitees-role">
                                PCC President, Telangana
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Sabitha Indra Reddy" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/sabitha.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Sabitha Indra Reddy
                            </h6>
                            <div class="invitees-role">
                                Minister of Education, Telangana
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Talasani Srinivas Yadav" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/erabelli-dayakar-rao.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Errabelli Dayakar Rao
                            </h6>
                            <div class="invitees-role">
                                Minister of Rural Development and RWS, Telangana
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Talasani Srinivas Yadav" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/talasani-srinivas-yadav.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Talasani Srinivas Yadav
                            </h6>
                            <div class="invitees-role">
                                Minister of Animal Husbandary, Fisheries and Cinematography, Telangana
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Pocharam Srinivas Reddy" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/pocharam.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Pocharam Srinivas Reddy
                            </h6>
                            <div class="invitees-role">
                                Speaker of Legislative Assembly, Telangana
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="G Jagadish Reddy" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/jagadishwar-reddy.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                G Jagadish Reddy
                            </h6>
                            <div class="invitees-role">
                                Minister of Energy, Telangana
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Santhosh Kumar Joginipally" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/santhosh-kumar.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Santhosh Kumar Joginipally
                            </h6>
                            <div class="invitees-role">
                                MP, Rajya Sabha
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="K Keshava Rao" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/keshavarao.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                K Keshava Rao
                            </h6>
                            <div class="invitees-role">
                                MP, Rajya Sabha
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="D Arvind" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/mp-aravind.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                D Arvind
                            </h6>
                            <div class="invitees-role">
                                MP, Nizamabad
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="Pilot Rohith Reddy" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/rohith-reddy.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Pilot Rohith Reddy
                            </h6>
                            <div class="invitees-role">
                                MLA, Tandur
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="adhu Yashki Goud" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/balakishan.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Rasamayi Balakishan
                            </h6>
                            <div class="invitees-role">
                                MLA, Manakonduru
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="DK Aruna" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/dk-aruna.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                DK Aruna
                            </h6>
                            <div class="invitees-role">
                                National Vice President, BJP
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="adhu Yashki Goud" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/vivek-venkataswamy.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                G. Vivek
                            </h6>
                            <div class="invitees-role">
                                Ex MP, Peddapalli
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="adhu Yashki Goud" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/madhu-yashki.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Madhu Yashki Goud
                            </h6>
                            <div class="invitees-role">
                                Ex MP, Nizamabad
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="invitees-team">
                        <div>
                            <img alt="adhu Yashki Goud" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" src="images/invitees/politicians/boddy-reddy-prabhakar-reddy.webp" />
                        </div>
                        <div class="pt-3">
                            <h6 class="invitees-name">
                                Boddy Reddy Prabhakar Reddy
                            </h6>
                            <div class="invitees-role">
                                AICC Member &amp; Secretary TPCC
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>