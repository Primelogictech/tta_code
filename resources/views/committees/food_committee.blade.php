@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Food Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">food@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <!--<li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-registration" role="tab" data-toggle="tab">Submit&nbsp;Your&nbsp;Article</a>
                                </li> -->
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Vijay_Bhaskar_Kalal.webp" class="img-fluid" alt="Vijay Bhaskar Kalal">
                                                </div>
                                                <h6 class="committee-member-name">Vijay Bhaskar Kalal</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Krishna_Siddhada.webp" class="img-fluid" alt="Krishna Siddhada">
                                                </div>
                                                <h6 class="committee-member-name">Krishna Siddada</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/no-image.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Ramana Kotha</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Mallik_Reddy_Alla.webp" class="img-fluid"  alt="Mallik Reddy Alla">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Mallik Reddy Alla</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Venkatesh_Sunkireddy.webp" class="img-fluid"alt="">
                                                </div>
                                                <h6 class="committee-member-name">Venkat Sunki Reddy</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Prema.webp" class="img-fluid"alt="">
                                                </div>
                                                <h6 class="committee-member-name">Prema</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Pavani_Dontagani.webp" class="img-fluid" alt="Pavani Dontagani">
                                                </div>
                                                <h6 class="committee-member-name">Pavani Dontagani</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Gautham_Garg.webp" class="img-fluid"alt="Gautham Garg">
                                                </div>
                                                <h6 class="committee-member-name">Gautham Garg</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Siva_Krishna_Burra.webp" class="img-fluid" alt="Siva Krishna Burra">
                                                </div>
                                                <h6 class="committee-member-name">Siva Krishna Burra</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Shweta_Kalal.webp" class="img-fluid" alt="Siva Krishna Burra">
                                                </div>
                                                <h6 class="committee-member-name">Swetha Kalal</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Divakar_Jandyam.webp" class="img-fluid"  alt="">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Dr Divakar Jandyam </h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Suresh_Kumar.webp" class="img-fluid"  alt="">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Suresh Kumar </h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Lakshman_Oduri.webp" class="img-fluid"  alt="Lakshman Oduri">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Lakshman Oduri</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Vidyasagar_Ponnam.webp" class="img-fluid"  alt="Vidyasagar Ponnam">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Vidyasagar Ponnam</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Viswanadham_Potnuru.webp" class="img-fluid"  alt="Viswanadham Potnuru">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Viswanadham Potnuru</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Praveen_Reddy.webp" class="img-fluid"  alt="Praveen Reddy Kethireddy">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Praveen Reddy Kethireddy</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Govardhan_Akkenapally.webp" class="img-fluid"  alt="Govardhan Chary Akkenapally">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Govardhan Chary Akkenapally</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Venkata_Musti.webp" class="img-fluid"  alt="Venkata Musti">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Venkata Musti</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Gopi_Krishna_Karnati.jpg" class="img-fluid"  alt="Gopi Krishna Karnati">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Gopi Krishna Karnati</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Prabhakar_Katepalli.jpg" class="img-fluid"  alt="Prabhakar Katepalli">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Prabhakar Katepalli</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/RaviKumar_Ganta.jpg" class="img-fluid"  alt="Ravikumar Ganta">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Ravi RG</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Sathya_kovvuri.jpg" class="img-fluid" alt="Satya kovvuri">
                                                </div>
                                                <h6 class="committee-member-name">Satya kovvuri</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div> 
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/Shekar_Pendem.jpg" class="img-fluid"  alt="Ravikumar Ganta">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Shekar Pendem</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>   
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/siva_B.jpeg" class="img-fluid"  alt="Ravi Burra">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Ravi Burra</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>      
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Rocky_Rocks.png" class="img-fluid"  alt="Rakesh Burra">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Rakesh Burra</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>  
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Goverdhana_Akkenapally.jpeg" class="img-fluid"  alt="Goverdhana Chary Akkenapally">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Goverdhana Chary Akkenapally</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>    
                                       <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Swathi_M.jpg" class="img-fluid"  alt="Swathi M">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Swathi M</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>   

                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/HemaL.jpeg" class="img-fluid"  alt="Hema">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Hema P</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>  
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Indira_chaluvadi.jpg" class="img-fluid"  alt="Indira chaluvadi">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Indira chaluvadi</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>  

                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Satyanarayana_chaluvadi.jpg" class="img-fluid"  alt="Satyanarayana chaluvadi">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Satyanarayana chaluvadi</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>   

                                    </div>
                                </div>
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

@endsection


@endsection
