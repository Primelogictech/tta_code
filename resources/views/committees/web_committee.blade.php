@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Web Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">web@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <!--<li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-registration" role="tab" data-toggle="tab">Submit&nbsp;Your&nbsp;Article</a>
                                </li> -->
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Narendar Reddy Yarava.jpg" class="img-fluid" alt="Narendar Yarava">
                                                </div>
                                                <h6 class="committee-member-name">Narendar Reddy Yarava</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Sai Gundur.jpg" class="img-fluid" alt="Sai Gundur">
                                                </div>
                                                <h6 class="committee-member-name">Sai Gundur</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                       <img src="images/Gangadhar Vuppala.jpg" class="img-fluid" alt="Gangadhar Vuppala">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Gangadhar Vuppala</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                       <img src="images/Sudharshan_Chetkuri.jpg" class="img-fluid" alt="Sudershan Chetukuri">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Sudharshan Chetkuri</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                       <img src="images/no-image1.jpg" class="img-fluid" alt="Sameer Kumar Mandalemula">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Sameer Kumar Mandalemula</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

@endsection


@endsection
