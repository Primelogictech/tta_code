@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Matrimonial Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">matrimonial@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <!--<li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
                                </li> -->
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" id="reg-page" href="#registration" role="tab" data-toggle="tab">Registration</a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Suresh Kumar Thanda.jpg" class="img-fluid" alt="Suresh Kumar Thanda">
                                                </div>
                                                <h6 class="committee-member-name">Suresh Kumar Thanda</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Swapna_Reddy.jpeg" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Swapna Reddy</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Naga_Mahendar_Velishala.jpg" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Naga Mahendar Velishala</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Sadanandam Bharatha.jpg" class="img-fluid" alt="Sadanandam Bharatha">
                                                </div>
                                                <h6 class="committee-member-name">Sadanandam Bharatha</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Koti_Reddy.JPG" class="img-fluid" alt="D. V. Koti Reddy">
                                                </div>
                                                <h6 class="committee-member-name">D. V. Koti Reddy </h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Sudhaker_Uppala.png" class="img-fluid" alt="Suresh Kumar Thanda">
                                                </div>
                                                <h6 class="committee-member-name">Sudhaker Uppala</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Indira.jpg" class="img-fluid" alt="Indira Sreeram Dixith">
                                                </div>
                                                <h6 class="committee-member-name">Indira Sreeram Dixith</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                          <!--   <div role="tabpanel" class="tab-pane fade in " id="registration">
                                <div class="col-12 shadow-small py-3">
                                    <div class="row">
                                       {{--  <div class="col-12">
                                            <div class="text-left text-md-right text-lg-right">
                                                <a href="{{ url('vendorexhibits') }}?page=regpage" class="btn btn-danger px-2 px-sm-3 px-md-5 text-white fs-xs-15 fs-xss-11 fs-sm-15" target="_blank">Click here to register for Exhibit & Souvenir Advertising</a>
                                            </div>
                                        </div> --}}
                                        <div class="col-12 mt-3">

                                            <div class=" text-center">
                                                <div class="btn btn-danger px-2 px-sm-3 px-md-5">
                                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSfCHSnu38RAIGFDrLVnWSgqbQL-fLUIQRusgVJQpz124EkLHg/viewform" class="text-white fs-xs-15 fs-xss-11 fs-sm-15" target="_blank"><i class="fas fa-hand-point-right pr-2"></i>Click here for Matrimony Registration</a>

                                            
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                               <div role="tabpanel" class="tab-pane fade" id="registration">
                                <div class="shadow-small py-3">
                                    <h4 class="font-weight-bold text-center mb-3 px-3 px-md-0">Registration For TTA Matrimony Committee</h4>
                                    <h6 class="font-weight-bold text-center mb-3">Please upload your contents in the below form</h6>
                                    <div class="col-12 col-lg-10 offset-lg-1 mt-4">
                                        <div class="shadow-small shadow-small-sm-none py-4 px-0 px-md-0 px-lg-4">
                                            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfCHSnu38RAIGFDrLVnWSgqbQL-fLUIQRusgVJQpz124EkLHg/viewform?embedded=true" class="width-fill-available" height="2000" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>


@section('javascript')
<script type="text/javascript">

    $(document).ready(function() {

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };

        var page = getUrlParameter('page');

        if(page=='regpage'){
            $('#reg-page').trigger("click");
        }



    })
</script>


@endsection



@endsection
