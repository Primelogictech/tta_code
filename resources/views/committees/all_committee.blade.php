@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg p-3">
                    <h4 class="mb-0">Convention Committees</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 py-0 pt-1 px-1 p40 px-sm-30">
                <!-- <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/convention-committees.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-12">
                        <div class="row pb-0">
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{'banquet'}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/banquet.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Banquet</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('budgetfinance')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/finance.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Budget & Finance</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('businessforum')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/bi_group.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Business Forum</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('cme')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/cme.webp" class="img-fluid mx-auto d-block" alt="">
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">CE / CME</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('celebritiescoordination')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/coordination.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Celebrities Coordination</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('corporatesponsorship')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/carporate_sponsorship.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Corporate Sponsorships</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('cultural')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/cultural.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Cultural</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('decorations')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/decoration.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Decorations</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                              <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('donorrelationship')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/youth.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Donor Relationship</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('food')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/food.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Food</div>
                                        </div>
                                    </div> 
                                </a> 
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('fundraising')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/fund_raising.webp" class="img-fluid mx-auto d-block" alt="">
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Fund Raising</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('hospitality')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/hospitality.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Hospitality</div>
                                        </div>
                                    </div> 
                                </a> 
                            </div>
                             <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('immigrationforum')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/immigration.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Immigration Forum </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                             <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('kidsactivities')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/cultural.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Kids Activities</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('literacy')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/literacy.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Literacy</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('matrimonial')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/matrimony.webp" class="img-fluid mx-auto d-block" alt="">
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Matrimonial</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                             <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('mediacommunication')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/social_media.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Media & Communication</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                             <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('overseascoordination')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/coordination.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Overseas coordination</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                             <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('politicalforum')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/political_forum.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Political Forum</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                             <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('programevents')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/publicity_r_media.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Programs And Events</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('registration')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/event_registration.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Registration and Reception</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('safetysecurity')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/security.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Safety & Security</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('shortfilm')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/photography.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Short Film</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('socialmedia')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/social_media.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Social Media</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                             <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('souvenir')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/souvenir.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Souvenir</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                             <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('spiritual')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/spiritual.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Spiritual</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('stage')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/av.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Stage & Venue</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('startupcube')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/fund_raising.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Startup Cube</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                             <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('transportation')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/transportation.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Transportation</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                              <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('ttastar')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/tta_star.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">TTA Star</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                             <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('vendorexhibits')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/exhibit_booth.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Vendors & Exhibits</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('volunteer')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/immigration.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Volunteer</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('web')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/flyers.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Web</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('womensforum')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/women.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Women's Forum</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="{{url('youthforum')}}" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/youth.webp" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Youth Forum</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

@endsection


@endsection
