@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Hospitality Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">hospitality@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <!--<li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-registration" role="tab" data-toggle="tab">Submit&nbsp;Your&nbsp;Article</a>
                                </li> -->
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Shiva Reddy  Kolla.jpg" class="img-fluid" alt="Shiva Reddy Kolla">
                                                </div>
                                                <h6 class="committee-member-name">Shiva Reddy Kolla</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Vani Singirikonda.jpg" class="img-fluid" alt="Vani Singirikonda">
                                                </div>
                                                <h6 class="committee-member-name">Vani Singirikonda</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Mallik Reddy Akinapalli.jpg" class="img-fluid" alt="Mallik Reddy Akinapalli">
                                                </div>
                                                <h6 class="committee-member-name">Mallik Reddy Akinapalli</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Shiva Reddy Jajapuram.jpg" class="img-fluid" alt="Shiva Reddy Jajapuram">
                                                </div>
                                                <h6 class="committee-member-name">Shiva Reddy Jajapuram</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Naveen Yalamandala.jpg" class="img-fluid" alt="Naveen Yalamandala">
                                                </div>
                                                <h6 class="committee-member-name">Naveen Yalamandala</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Sunil Reddy Gaddam.jpg" class="img-fluid" alt="Sunil Reddy Gaddam">
                                                </div>
                                                <h6 class="committee-member-name">Sunil Reddy Gaddam</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Venkat Gaddam.jpg" class="img-fluid" alt="Venkat Gaddam">
                                                </div>
                                                <h6 class="committee-member-name">Venkat Gaddam</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Bhargav Koppolu.jpg" class="img-fluid" alt="Bhargav Koppolu">
                                                </div>
                                                <h6 class="committee-member-name">Bhargav Koppolu</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Swathi Chennuri.jpg" class="img-fluid" alt="Swathi Chennuri">
                                                </div>
                                                <h6 class="committee-member-name">Swathi Chennuri</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Satish Sakilam.jpg" class="img-fluid" alt="Satish Sakilam">
                                                </div>
                                                <h6 class="committee-member-name">Satish Sakilam</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Nidheesh Raju.jpg" class="img-fluid"alt="Nidheesh Raju">
                                                </div>
                                                <h6 class="committee-member-name">Nidheesh Raju Baskaruni</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Madhukar Reddy Devarapally.jpg" class="img-fluid"alt="Madhukar Reddy Devarapally">
                                                </div>
                                                <h6 class="committee-member-name">Madhukar Reddy Devarapally</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Srinivas Reddy.jpg" class="img-fluid"alt="Srinivas Reddy">
                                                </div>
                                                <h6 class="committee-member-name">Srinivas Reddy</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Siva Emmadi.jpg" class="img-fluid"alt="Siva Emmadi">
                                                </div>
                                                <h6 class="committee-member-name">Siva Emmadi</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Vijay Jinugu.jpg" class="img-fluid"alt="Vijay Jinugu">
                                                </div>
                                                <h6 class="committee-member-name">Vijay Jinugu</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Narendar Ambati.jpg" class="img-fluid"alt="Narendar Ambati">
                                                </div>
                                                <h6 class="committee-member-name">Narendar Ambati</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Raja Neelam.jpg" class="img-fluid"alt="Raja Neelam">
                                                </div>
                                                <h6 class="committee-member-name">Raja Neelam</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Deepak_Katta.jpeg" class="img-fluid" alt="Deepak_Katta">
                                                </div>
                                                <h6 class="committee-member-name">Deepak Katta</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Moole_Krishna_Mohan_Reddy.jpeg" class="img-fluid" alt="Deepak_Katta">
                                                </div>
                                                <h6 class="committee-member-name">Moole Krishna Mohan Reddy </h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Sivasankar_Nara.jpeg" class="img-fluid" alt="Sivashankar">
                                                </div>
                                                <h6 class="committee-member-name">Sivasankar Nara</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Harish_Akkarapaka.jpeg" class="img-fluid" alt="Sivashankar">
                                                </div>
                                                <h6 class="committee-member-name">Harish Akkarapaka</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/prashanth_kumar.PNG" class="img-fluid" alt="Sivashankar">
                                                </div>
                                                <h6 class="committee-member-name">Prashanth</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Nithin_Kotla.jpeg" class="img-fluid" alt="Nithin">
                                                </div>
                                                <h6 class="committee-member-name">Nithin R Kotla</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Vishnu_Vardhan_Reddy.jpeg" class="img-fluid" alt="Vishnu vardhan">
                                                </div>
                                                <h6 class="committee-member-name">Vishnu vardhan reddy Kolla</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Raghuveer_Penmetcha.jpg" class="img-fluid" alt="Raghuveer Penmetcha">
                                                </div>
                                                <h6 class="committee-member-name">Raghuveer Penmetcha</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Balaganesh_Chakrahari.jpeg" class="img-fluid" alt="Balaganesh_Chakrahari">
                                                </div>
                                                <h6 class="committee-member-name">Balaganesh Chakrahari</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/manoj_kumar.jpeg" class="img-fluid" alt="Manoj Kumar Gajawada ">
                                                </div>
                                                <h6 class="committee-member-name">Manoj Kumar Gajawada </h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Anusha_Reddy.jpeg" class="img-fluid" alt="Anusha Medarametla">
                                                </div>
                                                <h6 class="committee-member-name">Anusha Reddy</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>                                      
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Neha_Namballa.jpeg" class="img-fluid" alt="Neha Namballa">
                                                </div>
                                                <h6 class="committee-member-name">Neha Namballa</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>    
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Vandhana_Suresh_Kumar.jpeg" class="img-fluid" alt="Vandana Suresh">
                                                </div>
                                                <h6 class="committee-member-name">Vandana Suresh</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div> 
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/sushanth_gopireddy.jpeg" class="img-fluid" alt="Sushanth Gopireddy">
                                                </div>
                                                <h6 class="committee-member-name">Sushanth Gopireddy</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>  
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Manish_Krishna_Damarakonda.jpg" class="img-fluid" alt="Manish Krishna Damarakonda">
                                                </div>
                                                <h6 class="committee-member-name">Manish Krishna Damarakonda</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>   
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Vamshikrishna_Achutha.jpg" class="img-fluid" alt="Vamshi Krishna Achutha">
                                                </div>
                                                <h6 class="committee-member-name">Vamshi Krishna Achutha</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>  
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Ravinder_Reddy.jpg" class="img-fluid" alt="Ravinder Reddy Sadu">
                                                </div>
                                                <h6 class="committee-member-name">Ravinder Reddy Sadu</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div> 
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Ramakrishna_Yennam.jpg" class="img-fluid" alt="Ramakrishna Yennam">
                                                </div>
                                                <h6 class="committee-member-name">Ramakrishna Yennam</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div> 
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/sairam_gajula.jpeg" class="img-fluid" alt="Sairam Gajula">
                                                </div>
                                                <h6 class="committee-member-name">Sairam Gajula</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>            
                                    </div>
                                </div>
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

@endsection


@endsection
