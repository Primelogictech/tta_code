@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg p-3">
                    <h4 class="mb-0">TTA Star Committee</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 py-4 p-md-4 p40">
            	<div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/tta-star-committee.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
            	<div class="row">
		            <div class="col-12 py-4">
		            	<div class="text-center">
		            		<h5><span class="text-violet">Committee Contact : </span><span class="text-danger overflow-wrap_break-word">ttastar@ttaconvention.org</span></h5>
		            	</div>
		            </div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Ravi Kamarasu.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Ravi Kamarasu</h6>
		            				<div class="pt-1">Chair</div>
		            				<!-- <div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">Rkamarasu@gmail.com</a>
		            				</div>
		            				<div class="mb-0 pt-1 text-violet font-weight-bold">(201) 232-3347</div> -->
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Vamsi Priya</h6>
		            				<div class="pt-1">Co-Chair</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Rama Prabhala.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="Rama Prabhala">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Rama Prabhala</h6>
		            				<div class="pt-1">Co-Chair</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Venu (CT)</h6>
		            				<div class="pt-1">Co-Chair</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
					<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/no-image1.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Suman Mudamba</h6>
		            				<div class="pt-1">Member</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</section>


@section('javascript')

@endsection


@endsection
