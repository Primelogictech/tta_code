@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Political Forum Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">politicalforum@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <!--<li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-registration" role="tab" data-toggle="tab">Submit&nbsp;Your&nbsp;Article</a>
                                </li> -->
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/Satish_mekala.jpeg" class="img-fluid" alt="">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Satish Reddy Mekala</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/Bhaskar Pinna.jpg" class="img-fluid" alt="" />
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Bhaskar Pinna</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>										
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Ravinder Reddy Sadu.jpg" class="img-fluid" alt="Ravinder Reddy Sadu">
                                                </div>
                                                <h6 class="committee-member-name">Ravinder Reddy Sadu</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Hari_GampaSpiritual,  Startup Cube ,Political Forum -cochair.jpeg" class="img-fluid" alt="Prabhavathi Maddula">
                                                </div>
                                                <h6 class="committee-member-name">Hari Gampa</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Rajesh_G.jpeg" class="img-fluid" alt="Rajesh">
                                                </div>
                                                <h6 class="committee-member-name">Rajesh Guggilla</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/Raju_Anandeshi.jpg" class="img-fluid" alt="" />
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Raj Anandeshi</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/durga-prasad.jpg" class="img-fluid" alt="Durga Prasad">
                                                </div>
                                                <h6 class="committee-member-name">Durga Prasad Seloj</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Anil_Arabelli.jpeg" class="img-fluid" alt="Anil Arabelli">
                                                </div>
                                                <h6 class="committee-member-name">Anil Arrabelli</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                    <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Pavan_Kumar_Reddy_Madini.jpeg" class="img-fluid" alt="Pavan Kumar Reddy Madini">
                                                </div>
                                                <h6 class="committee-member-name">Pavan Kumar Reddy Madini</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

@endsection


@endsection
