@extends('layouts.user.base')
@section('content')


<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg p-3">
                    <h4 class="mb-0">Souvenir Committee</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 py-4 p-md-4 p40">
            	<div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/souvenir-committee.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
            	<div class="row">
		            <div class="col-12 py-4">
		            	<div class="text-center">
		            		<h5><span class="text-violet">Committee Contact : </span><span class="text-danger overflow-wrap_break-word">souvenir@ttaconvention.org</span></h5>
		            	</div>
		            </div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/srini pp photo - Srini G.jpeg" class="img-fluid rounded border-radius-5 w-100"  alt="">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Srinivas Guduru</h6>
		            				<div class="pt-1">Chair</div>
		            				<!-- <div class="pt-1">
		            					<a href="#" class="text-danger text-decoration-none">sriniguduru@gmail.com</a>
		            				</div>
		            				<div class="mb-0 pt-1 text-violet font-weight-bold">(917) 547-5844</div> -->
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Nikshipta Reddy Kura.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="Nikshipta Reddy Kura">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Nikshipta Reddy Kura</h6>
		            				<div class="pt-1">Co-Chair</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Srini Todupunoori.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="Srini Todupunoori">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Srini Todupunoori</h6>
		            				<div class="pt-1">Co-Chair</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Sudhakar Uppala.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="Sudhakar Uppala">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Sudhaker Uppala</h6>
		            				<div class="pt-1">Advisor</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Yogiswar Vanama.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="Yogiswar Vanama">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Yogiswar Vanama</h6>
		            				<div class="pt-1">Member</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
            		<div class="col-12 col-md-6 col-lg-4 my-1 px-2">
            			<div class="convention-leaders-bg">
            				<div class="row">
	            				<div class="col-5 pr-0 my-auto">
		            				<div>
		            					<img src="images/Srini-M_4.jpg" class="img-fluid rounded border-radius-5 w-100"  alt="Srinivasa Manapragada">
		            				</div>
		            			</div>
		            			<div class="col-7 pr-3 my-auto">
		            				<h6 class="mb-0">Srinivasa Manapragada</h6>
		            				<div class="pt-1">Member</div>
		            			</div>
		            		</div>
            			</div>
            		</div>
        		</div>
            </div>
        </div>
    </div>
</section>


@section('eaxtra-javascript')

@endsection


@endsection
