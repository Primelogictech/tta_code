@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Safety and Security Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">safetysecurity@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <!--<li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-registration" role="tab" data-toggle="tab">Submit&nbsp;Your&nbsp;Article</a>
                                </li> -->
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Arun Arkala.jpg" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Arun Arkala</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/no-image1.jpg" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Konda Lakshmi Narasimha Reddy</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Shankar_Vullupala.jpg" class="img-fluid" alt="Shankar Reddy Vullupala">
                                                </div>
                                                <h6 class="committee-member-name">Shankar Reddy Vullupala</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Yatheeswar Guntko.jpg" class="img-fluid" alt="Yatheeswar Guntko">
                                                </div>
                                                <h6 class="committee-member-name">Yatheeswar Guntko</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Naveen_Kouluru.jpeg" class="img-fluid" alt="Naveen_Kouluru">
                                                </div>
                                                <h6 class="committee-member-name">Naveen Kouluru</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Gowri_Shankar.JPG" class="img-fluid" alt="Gowri Shankar Medi">
                                                </div>
                                                <h6 class="committee-member-name">Gowri Shankar Medi</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Kiran Guduru.jpg" class="img-fluid" alt="Kiran R Guduru">
                                                </div>
                                                <h6 class="committee-member-name">Kiran R Guduru</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                         
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Srinivas_Reddy_Maly.jpeg" class="img-fluid" alt="Srinivas Reddy Maly">
                                                </div>
                                                <h6 class="committee-member-name">Srinivas Reddy Maly</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Kamesh.jpg" class="img-fluid" alt="Kamesh">
                                                </div>
                                                <h6 class="committee-member-name">Kamesh</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Venkat_Panchedula.jpg" class="img-fluid" alt="Venkat">
                                                </div>
                                                <h6 class="committee-member-name">Venkat Panchedula</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Raghunath_Reddy.jpg" class="img-fluid" alt="Raghunath Reddy">
                                                </div>
                                                <h6 class="committee-member-name">Raghunath Reddy</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Gowtam_Reddy.jpg" class="img-fluid" alt="Gowtam Reddy">
                                                </div>
                                                <h6 class="committee-member-name">Gowtam Reddy</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Sridhar_Kura.jpg" class="img-fluid" alt="Sridhar Kura">
                                                </div>
                                                <h6 class="committee-member-name">Sridhar Kura</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Vishnu_marri.jpg" class="img-fluid" alt="Vishnu Marri">
                                                </div>
                                                <h6 class="committee-member-name">Vishnu marri</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Harish_Reddy_Vavilala.jpg" class="img-fluid" alt="Harish Reddy Vavilala">
                                                </div>
                                                <h6 class="committee-member-name">Harish Reddy Vavilala</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Aravindar_Reddy_Gutta.jpg" class="img-fluid" alt="Aravindar Reddy Gutta">
                                                </div>
                                                <h6 class="committee-member-name">Aravindar Reddy Gutta</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>


                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

@endsection


@endsection
