@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Startup Cube Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">startupcube@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" id="comm" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <!--<li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
                                </li>-->
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-registration" role="tab" id="reg-page" data-toggle="tab">Registration</a>
                                </li> 
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/ChandraSen_Sriramoju.jpeg" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Chandra Sena</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Mahesh_Sambu.jpeg" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Mahesh Sambu</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Arun_Kumar(Startup-cochair).jpeg" class="img-fluid" alt="Arun">
                                                </div>
                                                <h6 class="committee-member-name">Arun Kumar</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Hari_GampaSpiritual,  Startup Cube ,Political Forum -cochair.jpeg" class="img-fluid" alt="Prabhavathi Maddula">
                                                </div>
                                                <h6 class="committee-member-name">Hari Gampa</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Ganesh Veeramaneni.jpg" class="img-fluid" alt="Ganesh Veeramaneni">
                                                </div>
                                                <h6 class="committee-member-name">Ganesh Veeramaneni</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                              <div role="tabpanel" class="tab-pane fade" id="souvenir-registration">
                                <div class="shadow-small py-3">
                                    <h4 class="font-weight-bold text-center mb-3 px-3 px-md-0">Registration For TTA Startup Cube Committee</h4>
                                    <h6 class="font-weight-bold text-center mb-3">Please upload your contents in the below form</h6>
                                    <div class="col-12 col-lg-10 offset-lg-1 mt-4">
                                        <div class="shadow-small shadow-small-sm-none py-4 px-0 px-md-0 px-lg-4" id="framse1">

                                            <!-- <iframe src="https://forms.office.com/Pages/ResponsePage.aspx?id=fretDtxC-Ee74-wjleBxLIXpMNHVmfBHgduJMPC5w9tUOUlSQTVXQUJYSEM0RlhYVldMSUtFR1o3MS4u&embed=true" class="width-fill-available" height="1000" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe> -->
                                          <!-- <iframe width="640px" height= "480px" src= "https://forms.office.com/Pages/ResponsePage.aspx?id=fretDtxC-Ee74-wjleBxLIXpMNHVmfBHgduJMPC5w9tUOUlSQTVXQUJYSEM0RlhYVldMSUtFR1o3MS4u&embed=true" frameborder= "0" marginwidth= "0" marginheight= "0" style= "border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

<script type="text/javascript">

    $(document).ready(function() {
        is_loaded=false
            $('#reg-page').click(function(){
                if(!is_loaded){
                  is_loaded=true
                  $('#framse1').append('<iframe src="https://forms.office.com/Pages/ResponsePage.aspx?id=fretDtxC-Ee74-wjleBxLIXpMNHVmfBHgduJMPC5w9tUOUlSQTVXQUJYSEM0RlhYVldMSUtFR1o3MS4u&embed=true" class="width-fill-available" height="1000" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>')

                }
           })

            var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };
            
            var page = getUrlParameter('page');
            
            if(page=='regpage'){
                $('#reg-page').trigger("click");
            }
                


    })
</script>



@endsection


@endsection
