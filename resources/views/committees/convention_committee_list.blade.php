@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg p-3">
                    <h4 class="mb-0">Convention Committees</h4>
                </div>
            </div>
            <div class="col-12 border-top mt2 py-0 pt-1 px-1 p40 px-sm-30">
                <!-- <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/convention-committees.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-12">
                        <div class="row pb-0">
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="banquet_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/banquet.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Banquet</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="budget_n_finance_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/finance.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Budget & Finance</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="corporate_sponsorship_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/carporate_sponsorship.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Corporate Sponsorships</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="cultural_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/cultural.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Cultural</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="decorations_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/decoration.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Decorations</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="food_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/food.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Food</div>
                                        </div>
                                    </div> 
                                </a> 
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="hospitality_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/hospitality.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Hospitality</div>
                                        </div>
                                    </div> 
                                </a> 
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="media_n_communication_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/social_media.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Media & Communication</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="program_n_events_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/publicity_r_media.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Programs And Events</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="reception_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/cme.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Reception</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="registration_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/event_registration.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Registration</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="safety_n_security_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/security.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Safety & Security</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="souvenir_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/souvenir.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Souvenir</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="stage_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/av.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Stage / Venue / AV / IT Infrastructure</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="transportation_committee" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/transportation.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Transportation</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="vender_n_exhibits_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/exhibit_booth.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Vendors & Exhibits</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="volunteer_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/immigration.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Volunteer</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="tta_star_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/tta_star.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">TTA Star</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="page-under-constrution.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/cultural.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Kids Activities</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="celebrities_coordination_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/coordination.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Celebrities Coordination</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="page-under-constrution.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/youth.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Donor Relationship</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="web_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/flyers.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Web</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="fund_raising_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/fund_raising.png" class="img-fluid mx-auto d-block" alt="">
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Fund Raising</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="cme_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/cme.png" class="img-fluid mx-auto d-block" alt="">
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">CE / CME</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="business_forum_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/bi_group.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Business Forum</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="immigration_forum_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/immigration.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Immigration Forum </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="political_forum_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/political_forum.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Political Forum</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="page-under-constrution.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/literacy.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Literary</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="matrimonial_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/matrimony.png" class="img-fluid mx-auto d-block" alt="">
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Matrimonial</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="overseas_coordination_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/coordination.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Overseas coordination</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="page-under-constrution.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/photography.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Short Film</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="social_media_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/social_media.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Social Media</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="spiritual_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/spiritual.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Spiritual</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="startup_forum_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/fund_raising.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Startup Forum</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="womens_forum_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/women.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Women's Forum</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                <a href="youth_forum_committee.php" class="text-decoration-none">
                                    <div class="convention-cc-bg">
                                        <div class="p-3">
                                            <div>
                                                <img src="images/cc-icons/youth.png" class="img-fluid mx-auto d-block" alt="" />
                                            </div>
                                            <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Youth Forum</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

@endsection


@endsection
