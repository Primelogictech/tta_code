@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Stage & Venue Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">stageav@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <!--<li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-registration" role="tab" data-toggle="tab">Submit&nbsp;Your&nbsp;Article</a>
                                </li> -->
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Narsimha Peruka.jpg" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Narsimha Peruka</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Goutam_Photo - vepurgoutam.JPEG" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Goutham Reddy Vepur</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Gangadhar Vuppala.jpg" class="img-fluid" alt="Gangadhar Vuppala">
                                                </div>
                                                <h6 class="committee-member-name">Gangadhar Vuppala</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Srinivas Mettu (AV-cochair).JPG" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Srinivas Mettu</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Srinivas Burra.jpg" class="img-fluid" alt="Srinivas Burra">
                                                </div>
                                                <h6 class="committee-member-name">Srinivas Burra</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Sureshkumar_Karamshetty.jpeg" class="img-fluid" alt="Suresh Karamshetty">
                                                </div>
                                                <h6 class="committee-member-name">Suresh Karamshetty</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/swapna mavuri.jpeg" class="img-fluid" alt="Surendra Mavuri">
                                                </div>
                                                <h6 class="committee-member-name">Surendra Mavuri</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/eswer_kumar.jpeg" class="img-fluid" alt="Eswerkumar Kilaru">
                                                </div>
                                                <h6 class="committee-member-name">Eswerkumar Kilaru</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Kiran Kumar Gande.jpeg" class="img-fluid" alt="Kiran Kumar Gande">
                                                </div>
                                                <h6 class="committee-member-name">Kiran Kumar Gande</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Ravi_Kumar.jpg" class="img-fluid" alt="Ravi Jonna">
                                                </div>
                                                <h6 class="committee-member-name">Ravi Jonna</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>                                          
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Chaitanya_G.jpg" class="img-fluid" alt="Chaitanya Gonnabhaktula  ">
                                                </div>
                                                <h6 class="committee-member-name">Chaitanya Gonnabhaktula</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>                                              
                                            <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/Raj_H4U.jpeg" class="img-fluid" alt="Raj Neeli">
                                                </div>
                                                <h6 class="committee-member-name">Raj Neeli</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>  
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/NarsimhaRao_P.jpeg" class="img-fluid" alt="Ramakrishna Alvakonda">
                                                </div>
                                                <h6 class="committee-member-name">Ramakrishna Alvakonda </h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>  
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                   <img src="images/VidyaSagar.jpg" class="img-fluid" alt="Vidya Sagar Dandu">
                                                </div>
                                                <h6 class="committee-member-name">Vidya Sagar Dandu </h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

@endsection


@endsection
