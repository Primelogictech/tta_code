@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Registration and Reception Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">registration@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#registration-content" role="tab" data-toggle="tab">Registration&nbsp;Packages</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#registration-form" id="reg-page" role="tab" data-toggle="tab">Register&nbsp;Here</a>
                                </li> 
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Kiran_Duddagi.webp" class="img-fluid" alt="Kiran Duddagi">
                                                </div>
                                                <h6 class="committee-member-name">Kiran Duddagi</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Roopak_Kalluri.webp" class="img-fluid" alt="Roopak Kalluri">
                                                </div>
                                                <h6 class="committee-member-name">Roopak Kalluri</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Vamshi_Gullpalli.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Vamshi Gullpalli</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Gopi_Vutkuri.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Gopi Vutkuri</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/no-image.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Smitha Peddireddy</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Seshagiri_Rao_Kambhammettu.webp" class="img-fluid" alt="Seshagiri Rao Kambhammettu">
                                                </div>
                                                <h6 class="committee-member-name">Seshagiri Rao Kambhammettu</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Venu_Gopal_Giri.webp" class="img-fluid" alt="Venu Giri">
                                                </div>
                                                <h6 class="committee-member-name">Venu Gopal Giri</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Prasada_Rao_Teki.webp" class="img-fluid" alt="Prasad Teki">
                                                </div>
                                                <h6 class="committee-member-name">Prasad Teki</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Satya_Nemana.jpg" class="img-fluid" alt="Satya Nemana">
                                                </div>
                                                <h6 class="committee-member-name">Satya Nemana</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Karthik_Nimmala.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Karthik Nimmala</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Divakar_Jandyam.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Dr. Divakar Jandyam</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/no-image.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Srinivas Kairi</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Niveditha_Goud.webp" class="img-fluid" alt="Niveditha Goud">
                                                </div>
                                                <h6 class="committee-member-name">Niveditha Goud</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Srinivasa_Kolla.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Srinivasa Kolla</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Srividya_Manikonda.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Srividya Manikonda</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Arun_Kumar_Reddy.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Arun Kumar Reddy Mekala</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Raghuram_RenduChintala.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Raghuram Renduchintala</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Narayan_Chinnam.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Narayan Chinnam</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Devender_Reddy.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Devender Reddy</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Nalabothu_Anjaneyulu.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Nalabothu Anjaneyulu</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Manohar_Kumar_Majyari.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Manohar Kumar Majyari</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Krishna_Murthy.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Krishna</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Tirupathi_Rao_Bammidi.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Tirupathi Rao Bammidi</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <!-- Content -->

                            <div role="tabpanel" class="tab-pane fade in " id="registration-content">
                                @include('partalles.registration_package_details')
                            </div>

                            <!-- Registration Form -->

                            <div role="tabpanel" class="tab-pane fade in " id="registration-form">
                                <div class="col-12 shadow-small py-2">
                                    @include('partalles.registration_form')
                                </div> 
                            </div>
                        </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>

@section('more-javascript')
<script type="text/javascript">
    $(document).ready(function() {
            var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };
            
            var page = getUrlParameter('page');
            
            if(page=='regpage'){
                $('#reg-page').trigger("click");
            }
    })
</script>

@endsection



@endsection
