@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Business Forum Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">business@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                    	<div>
	                        <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
	                            <li class="nav-item d-inline-flex">
	                                <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
	                            </li>
	                            <!-- <li class="nav-item d-inline-flex">
	                                <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
	                            </li>
	                            <li class="nav-item d-inline-flex">
	                                <a class="nav-link px-4 border" href="#souvenir-registration" role="tab" data-toggle="tab">Submit&nbsp;Your&nbsp;Article</a>
	                            </li> -->
	                        </ul>
	                    </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Manikyam_Thukkapuram.JPG" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Manikyam Thukkapuram</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Pradeep Mettu.jpg" class="img-fluid"  alt="Pradeep Mettu">
                                                </div>
                                                <h6 class="committee-member-name">Pradeep Mettu</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Rajesh G.jpeg" class="img-fluid"  alt="Rajesh Guggilla">
                                                </div>
                                                <h6 class="committee-member-name">Rajesh Guggilla</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Manohar_Bodke.png" class="img-fluid"  alt="Manohar Bodke">
                                                </div>
                                                <h6 class="committee-member-name">Manohar Bodke</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    </div>
</section>

@section('javascript')

@endsection


@endsection
