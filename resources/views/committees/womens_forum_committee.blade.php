@extends('layouts.user.base')
@section('content')

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Women's Forum Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">women@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border " href="#muggulu-registration" id="reg-page" role="tab" data-toggle="tab">Registration For Muggula Poti</a>
                                </li>
                                <!--<li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-registration" role="tab" data-toggle="tab">Submit&nbsp;Your&nbsp;Article</a>
                                </li> -->
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Sangeeta Reddy.jpg" class="img-fluid" alt="Sangeeta Reddy">
                                                </div>
                                                <h6 class="committee-member-name">Sangeeta Reddy</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/saru_varakur.jpeg" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Saraswathi Varakur</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                       <img src="images/Nandadevi Srirama.jpg" class="img-fluid" alt="Nandadevi SriRama">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Nandadevi SriRama</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                       <img src="images/Koumudi Thanda.jpg" class="img-fluid" alt="Koumudi Thanda">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Koumudi Thanda</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                       <img src="images/Jyothsna_Kallu.jpg" class="img-fluid" alt="Jyothsna Kallu">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Jyothsna Kallu</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                       <img src="images/Srujana.jpeg" class="img-fluid" alt="Srujana Racha">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Srujana Racha</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                       <img src="images/Prema_Gargapati(Womens-member).jpeg" class="img-fluid" alt="Srujana Racha">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Prema Gargapati</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
					<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Sharanya_Seloj.jpg" class="img-fluid" alt="Sharanya Seloj">
                                                </div>
                                                <h6 class="committee-member-name">Sharanya Seloj</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="muggulu-registration">
                                <div class="shadow-small py-3">
                                    <h4 class="font-weight-bold text-center mb-3 px-3 px-md-0">Registration For Muggula Poti</h4>
                                    <div class="col-12 col-lg-10 offset-lg-1 mt-4">
                                        <div class="shadow-small shadow-small-sm-none py-4 px-0 px-md-0 px-lg-4">
                                            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfBmoq8adPOBt0H-3Dq7eMAUoZ8KtfgfbTbCUIiKrFwLTbP_w/viewform?embedded=true" class="width-fill-available" width="640" height="1038" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</section>


@section('javascript')

<script type="text/javascript">

    $(document).ready(function() {

            var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };
            
            var page = getUrlParameter('page');
            
            if(page=='regpage'){
                $('#reg-page').trigger("click");
            }
                


    })
</script>

@endsection


@endsection
