@extends('layouts.user.base')
@section('content')


<style type="text/css">
    .fade:not(.show) {
        opacity: 1;
    }
</style>

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Youth Forum Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">youth@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll"role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <!--<li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-content" role="tab" data-toggle="tab">Guidelines&nbsp;For&nbsp;Submitting&nbsp;Literary&nbsp;Content&nbsp;for&nbsp;the&nbsp;TTA&nbsp;Special&nbsp;Souvenir</a>
                                </li>-->

                               <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#souvenir-registration" id="reg-page" role="tab" data-toggle="tab">Registration</a>
                                </li> 
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Shabari_Vanama.webp" class="img-fluid" alt="Shabari Vanama">
                                                </div>
                                                <h6 class="committee-member-name">Shabari Vanama</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Lasya_Vuday.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Lasya Vudayagiri</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Anuja_Gedala.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Anuja Gedala</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Esha_Varakur.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Esha Varakur</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Jyothsna_Pachha.webp" class="img-fluid" alt="Jyothsna Chowdary">
                                                </div>
                                                <h6 class="committee-member-name">Jyothsna Chowdary</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Sharanya_Seloj.webp" class="img-fluid" alt="Sharanya Seloj">
                                                </div>
                                                <h6 class="committee-member-name">Sharanya Seloj</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Harika_Vemulapalli.webp" class="img-fluid" alt="Harika Vemulapalli">
                                                </div>
                                                <h6 class="committee-member-name">Harika Vemulapalli</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Alekya_Kaveti.webp" class="img-fluid" alt="Alekya Kaveti">
                                                </div>
                                                <h6 class="committee-member-name">Alekya Kaveti</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Shivani_Sunkireddy.webp" class="img-fluid" alt="Shivani Sunkireddy">
                                                </div>
                                                <h6 class="committee-member-name">Shivani Sunkireddy</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Varenya_Alvakonda.webp" class="img-fluid" alt="Varenya Alvakonda">
                                                </div>
                                                <h6 class="committee-member-name">Varenya Alvakonda</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Keerthi_Peruka.webp" class="img-fluid" alt="Keerthi Peruka">
                                                </div>
                                                <h6 class="committee-member-name">Keerthi Peruka</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Swetha_c.webp" class="img-fluid" alt="Swetha C">
                                                </div>
                                                <h6 class="committee-member-name">Swetha C</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <!--<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Sai_Gadiraju.webp" class="img-fluid w-100" alt="Srinivas Saipriya Gadiraju">
                                                </div>
                                                <div class="committee-member-name">Saipriya Gadiraju</div>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>-->
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Shreya_Kaveti.webp" class="img-fluid w-100" alt="Srinivas Saipriya Gadiraju">
                                                </div>
                                                <div class="committee-member-name">Shreya Kaveti</div>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Vidvathi_Alvakonda.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">  Vidvathi Alvakonda</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/nimisha.jpeg" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Nimisha Peruka</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <!--<div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Jaswitha_Basu.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Jaswitha Basu</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>-->
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Trishala_Siddhada.jpeg" class="img-fluid" alt="Trishala Siddhada">
                                                </div>
                                                <h6 class="committee-member-name">Trishala Siddhada</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Sanjana_Buddi.webp" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Sanjana Buddi</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/Sri_Giri.jpg" class="img-fluid" alt="">
                                                </div>
                                                <h6 class="committee-member-name">Sri Giri</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div role="tabpanel" class="tab-pane fade" id="souvenir-registration">
                                <div class="shadow-small py-3">
                                    <h4 class="font-weight-bold text-center mb-3 px-3 px-md-0">Registration For TTA Youth Committee</h4>
                                    <h6 class="font-weight-bold text-center mb-3">Please upload your contents in the below form</h6>
                                    <div class="col-12 col-lg-10 offset-lg-1 mt-4">
                                        <div class="shadow-small shadow-small-sm-none py-4 px-0 px-md-0 px-lg-4">
                                            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfWM92oVHPhycPAoHbkJTd4rn8mYfrdvblRbFUDQRHQMs2KKA/viewform?embedded=true" class="width-fill-available" height="1700" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')


<script type="text/javascript">

    $(document).ready(function() {
        

            var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };
            
            var page = getUrlParameter('page');
            
            if(page=='regpage'){
                $('#reg-page').trigger("click");
            }
                


    })
</script>


@endsection


@endsection
