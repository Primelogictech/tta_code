@extends('layouts.user.base') @section('content')

<style type="text/css">
    input[type="checkbox"],
    input[type="radio"] {
        box-sizing: border-box;
        padding: 0;
        position: relative;
        top: 2px;
    }

    .table th {
        vertical-align: middle !important;
    }

    .input-checkbox {
        position: absolute;
        top: 6px;
    }

    .agree-checkbox {
        position: relative;
        top: 21px !important;
    }

    .sub-input {
        border: 0;
    }

    .subtotal-input {
        border: 0;
    }

    .sub-input-two {
        border: 0;
    }

    .error {
        color: red;
    }

    input:focus {
        outline: none;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type="number"] {
        -moz-appearance: textfield;
    }
</style>

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Vendor and Exhibits Committee</h5>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div class="text-left text-lg-right">
                                <div class="committee-contact"><span>Committee Contact : </span><span class="overflow-wrap_break-word">vendorexhibits@ttaconvention.org</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll" role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#committee" role="tab" data-toggle="tab">Committee</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#registration-packages" role="tab" data-toggle="tab">Exhibit&nbsp;Packages</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#exhibit-registration" id="reg-page" role="tab" data-toggle="tab">Register&nbsp;Here</a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="committee">
                                <div class="col-12 shadow-small py-2">
                                    <div class="row">
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/Naresh_Chintalacheruvu.jpg" class="img-fluid" alt="" />
                                                </div>
                                                <h6 class="committee-member-name">Naresh Chintalacheruvu</h6>
                                                <div class="committee-member-designation">Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <img src="images/committee-members/no-image.webp" class="img-fluid" alt="" />
                                                </div>
                                                <h6 class="committee-member-name">Amith</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Mallik_Bolla.webp" class="img-fluid" alt="" />
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Mallik Bolla</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Raveen_Sepuri.webp" class="img-fluid" alt="" />
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Raveen Sepuri</h6>
                                                <div class="committee-member-designation">Co-Chair</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/no-image.webp" class="img-fluid" alt="" />
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Shekar Konala</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Bhaskar_Pinna.webp" class="img-fluid" alt="" />
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Bhaskar Pinna</h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Divakar_Jandyam.webp" class="img-fluid"  alt="">
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Dr Divakar Jandyam </h6>
                                                <div class="committee-member-designation">Advisor</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Prabhakar_Baluguri.webp" class="img-fluid" alt="Prabhakar Baluguri" />
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Prabhakar Baluguri</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-4 col-md-3 col-lg-2 px-5 px-sm-3 px-md-3 py-2">
                                            <div class="committee-member">
                                                <div>
                                                    <div>
                                                        <img src="images/committee-members/Prashanth_Vaida.webp" class="img-fluid" alt="Prashanth" />
                                                    </div>
                                                </div>
                                                <h6 class="committee-member-name">Prashanth Vaida</h6>
                                                <div class="committee-member-designation">Member</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- Registration Package Details -->

                            <div role="tabpanel" class="tab-pane fade in" id="registration-packages">
                                <div class="col-12 shadow-small bg-white py-3">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h5 class="text-violet mb-0">EXHIBITS/ DIGITAL/SOUVENIR PACKAGE DETAILS</h5>
                                                </div>
                                            </div>

                                            <div class="row px-3">
                                                <div class="col-12 col-md-12 col-lg-12 shadow-small p-3 py-md-4 px-md-4 mt-3">
                                                    <div class="row">
                                                        <div class="col-12"></div>

                                                        <div class="col-12">
                                                            <div class="my-2 text-center">
                                                                <h5 class="d-inline-block text-skyblue">Exhibits Package Details:</h5>
                                                            </div>

                                                            <div class="table-responsive">
                                                                <table class="datatable table table-bordered table-center mb-0">
                                                                    <thead>
                                                                        <tr>
                                                                            <th rowspan="2">Package Code</th>

                                                                            <th rowspan="2">Exhibitor Type/ Booth Size</th>

                                                                            <th class="text-center" colspan="2">Package Amount</th>
                                                                        </tr>

                                                                        <tr>
                                                                            <th>Before 03/31/2022</th>

                                                                            <th>After 03/31/2022</th>
                                                                        </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                        <tr>
                                                                            <td>EXE01</td>

                                                                            <td>Jewelry Booths (10'X20')</td>

                                                                            <td>$12,000</td>

                                                                            <td>$14,000</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>EXE02</td>

                                                                            <td>Jewelry Booths (10’X10’)</td>

                                                                            <td>$8,000</td>

                                                                            <td>$10,000</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>EXE03</td>

                                                                            <td>Corporations / Real Estate (10’X20’)</td>

                                                                            <td>$10,000</td>

                                                                            <td>$12,000</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>EXE04</td>

                                                                            <td>Corporations / Real Estate (10’X10’)</td>

                                                                            <td>$6,000</td>

                                                                            <td>$7,000</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>EXE05</td>

                                                                            <td>Semi-Precious/Pearls/Gems (10’X10’)</td>

                                                                            <td>$2,000</td>

                                                                            <td>$3,000</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>EXE06</td>

                                                                            <td>Sarees / Boutique (10’X10’)</td>

                                                                            <td>$2,000</td>

                                                                            <td>$3,000</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>EXE07</td>

                                                                            <td>Miscellaneous (10’X10’)</td>

                                                                            <td>$2,000</td>

                                                                            <td>$3,000</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>EXE08</td>

                                                                            <td>Non-Profit Groups (10’X10’)</td>

                                                                            <td>$1,000</td>

                                                                            <td>$1,000</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                        <div class="col-12 mt-4">
                                                            <div class="my-2 text-center">
                                                                <h5 class="d-inline-block text-skyblue">Digital/Web/Souvenir Package Details:</h5>
                                                            </div>

                                                            <div class="table-responsive">
                                                                <table class="datatable table table-bordered table-center mb-0">
                                                                    <thead>
                                                                        <tr>
                                                                            <th rowspan="2">Package Code</th>

                                                                            <th rowspan="2">Description</th>

                                                                            <th class="text-center" colspan="2">Package Amount</th>
                                                                        </tr>

                                                                        <tr>
                                                                            <th>Size</th>

                                                                            <th>Amount</th>
                                                                        </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                        <tr>
                                                                            <td>DIG01</td>

                                                                            <td>Digital Ads</td>

                                                                            <td>10 - 20 sec</td>

                                                                            <td>$2,000 - $5000</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>SOU01</td>

                                                                            <td>Souvenir Ad (Color)</td>

                                                                            <td>Full Page</td>

                                                                            <td>$1,000</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>SOU02</td>

                                                                            <td>Souvenir Ad (Color)</td>

                                                                            <td>Half Page</td>

                                                                            <td>$500</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>SOU03</td>

                                                                            <td>Souvenir Ad (Color)</td>

                                                                            <td>Quarter</td>

                                                                            <td>$250</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 mt-4">
                                                            <h5 class="text-violet">For More Details:</h5>
                                                            <div class="fs15">
                                                                <span>Contact :</span>
                                                                <span>732-763-9650</span>
                                                            </div>
                                                            <div class="fs15">
                                                                <span>Email :</span>
                                                                <span>naresh.chintala2012@gmail.com</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Registration Form -->

                            <div role="tabpanel" class="tab-pane fade in" id="exhibit-registration">
                                <div class="col-12 shadow-small py-2">
                                    @include('partalles.vendorexhibits_form')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('more-javascript')
<script type="text/javascript">
    $(document).ready(function () {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split("&"),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split("=");

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };

        var page = getUrlParameter("page");

        if (page == "regpage") {
            $("#reg-page").trigger("click");
        }
    });
</script>

@endsection @endsection
