@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-5">
    <div class="container vertical-height pt-3">
    		<div class="row">
    			<div class="col-12 vertical-center">
					<div>
    					<img src="images/under-construction1.png" class="img-fluid mx-auto d-block" alt="">
    				</div>
    				<h3 class="text-center pt-4">This Page is Under Construction</h3>
    				<h5 class="text-center pt-1 text-muted">We're working on it</h5>
    			</div>
    		</div>
    	</div>
    
</section>



@section('javascript')

@endsection


@endsection