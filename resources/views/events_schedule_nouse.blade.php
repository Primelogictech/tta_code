@extends('layouts.user.base')
@section('content')

<style type="text/css">
    .fade:not(.show) {
        opacity: 1;
    }
</style>

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small bg-white">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Events Schedule</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll" role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border active" href="#twenty-seventh-may" role="tab" data-toggle="tab">27&nbsp;May,&nbsp;2022</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border " href="#twenty-eighth-may" role="tab" data-toggle="tab">28&nbsp;May,&nbsp;2022</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 border" href="#twenty-ninth-may" role="tab" data-toggle="tab">29&nbsp;May,&nbsp;2022</a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active es_active_tab" id="twenty-seventh-may">
                                <div class="col-12 shadow-small py-3">
                                    <div class="my-5 py-5">
                                        <img src="images/events/TTAProgramSheetP2.jpg" class="img-fluid" />
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="twenty-eighth-may">
                                <div class="col-12 shadow-small py-3">
                                    <div class="my-5 py-5">
                                        <img src="images/events/TTAProgramSheetP3.jpg" class="img-fluid" />
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="twenty-ninth-may">
                                <div class="col-12 shadow-small py-3">
                                    <div class="my-5 py-5">
                                        <img src="images/events/TTAProgramSheetP4.jpg" class="img-fluid" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

@endsection

@endsection
