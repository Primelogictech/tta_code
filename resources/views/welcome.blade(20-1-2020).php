@extends('layouts.user.base')
@section('content')

<link href="css/slick.min.css" rel="stylesheet" />
<link href="css/slick-theme.min.css" rel="stylesheet" />

<style type="text/css">
    .wrapper {
        height: auto;
        padding: 0px;
    }

</style>

<div class="container-fluid invitees_bg">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-lg-2 d-none d-lg-block">
                    <div>
                        <img src="images/left-welcome-image.png" alt="" border="0" class="img-fluid left-welcome-image" width="91" />
                    </div>
                    <div class="invitees-heading-lg-left">Invitees</div>
                </div>
                <div class="col-12 col-lg-8 px-lg-3 py-lg-0 py-md-0">
                    <div class="wrapper">
                        <div class="invitees-carousel">
                            <div>
                                <div class="invitees">
                                    <div class="py5 px-2">
                                        <img src="images/suma.kanakala.jpg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <h6 class="text-purple font-weight-bold mb-0">Suma Kanakala</h6>
                                        <div class="text-purple">Anchor</div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="invitees">
                                    <div class="py5 px-2">
                                        <img src="images/sp.sailaja.jpg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <h6 class="text-purple font-weight-bold mb-0">S.P. Sailaja</h6>
                                        <div class="text-purple">Singer</div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="invitees">
                                    <div class="py5 px-2">
                                        <img src="images/sp.charan.jpg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <h6 class="text-purple font-weight-bold mb-0">S.P. Charan</h6>
                                        <div class="text-purple">Singer</div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="invitees">
                                    <div class="py5 px-2">
                                        <img src="images/sunitha.jpg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <h6 class="text-purple font-weight-bold mb-0">Sunitha</h6>
                                        <div class="text-purple">Singer</div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="invitees">
                                    <div class="py5 px-2">
                                        <img src="images/usha-singer.jpg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                                    </div>
                                    <div class="text-center">
                                        <h6 class="text-purple font-weight-bold mb-0">Usha</h6>
                                        <div class="text-purple">Singer</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 d-none d-lg-block">
                    <div>
                        <img src="images/right-welcome-image.png" alt="" border="0" class="img-fluid right-welcome-image" width="91" />
                    </div>
                    <div class="invitees-heading-lg-right">Invitees</div>
                </div>
            </div>
        </div>
    </div>
</div>



{{-- <div class="container-fluid invitees_bg d-block d-lg-none">
    <div class="row">
        <div class="col-12 col-md-12 px-md-5 py-md-0">
            <marquee behavior="alternate" class="px-3">
                <div class="row flex-wrap-nowrap">
                    <div>
                        <div class="invitees d-flex">
                            <div class="pt5 pb5 px-2">
                                <img src="images/suma.kanakala.jpg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                            </div>
                            <div class="my-auto">
                                <h6 class="text-purple font-weight-bold mb-0">Suma Kanakala</h6>
                                <div class="text-purple">Anchor</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="invitees d-flex">
                            <div class="pt5 pb5 px-2">
                                <img src="images/sp.sailaja.jpg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                            </div>
                            <div class="my-auto">
                                <h6 class="text-purple font-weight-bold mb-0">S.P. Sailaja</h6>
                                <div class="text-purple">Singer</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="invitees d-flex">
                            <div class="pt5 pb5 px-2">
                                <img src="images/sp.charan.jpg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                            </div>
                            <div class="my-auto">
                                <h6 class="text-purple font-weight-bold mb-0">S.P. Charan</h6>
                                <div class="text-purple">Singer</div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="invitees d-flex">
                            <div class="pt5 pb5 px-2">
                                <img src="images/sunitha.jpg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                            </div>
                            <div class="my-auto">
                                <h6 class="text-purple font-weight-bold mb-0">Sunitha</h6>
                                <div class="text-purple">Singer</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="invitees d-flex">
                            <div class="pt5 pb5 px-2">
                                <img src="images/usha-singer.jpg" class="img-fluid mx-auto d-block border-radius-10" width="150px" alt="" />
                            </div>
                            <div class="my-auto">
                                <h6 class="text-purple font-weight-bold mb-0">Usha</h6>
                                <div class="text-purple">Singer</div>
                            </div>
                        </div>
                    </div>
                </div>
            </marquee>
        </div>
    </div>
</div>
 --}}

<section class="container-fluid my-0">
    <div class="row">
        <div class="col-12 pt2 pb-0 px-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100 my-auto" src="images/tta_banner.jpg" alt="First slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 my-auto" src="images/tta_banner.jpg" alt="Second slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 my-auto" src="images/tta_banner.jpg" alt="Third slide" />
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- End of Carousel Banners -->

<!-- Event Schedule -->

<section class="container-fluid" style="margin-top: -16px;">
    <div class="row px-1">
        <div class="col-12 col-md-12 col-lg-3 my-1 my-lg-auto px-2 schedule-date-before-triangle">
            <div class="text-decoration-none">
                <div class="schedule-date-bg">
                    <div class="schedule-date-bg-dots-img">
                        <div class="icon1">
                            <div class="py-5 py-lg-75 text-center">
                                <span class="fs25 text-white">May 27<sup>th</sup> to 29<sup>th</sup>, 2022<br /></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-6 my-1 my-lg-auto px-2 location-before-triangle">
            <div class="schedule-location-bg">
                <div class="location-bg-dots-img">
                    <div class="icon2">
                        <div class="py-5 py-lg-63point5 px-1 px-sm-0">
                            <h4 class="text-white text-left text-center fs23">New Jersey Convention And Exposition Center</h4>
                            <div class="text-left text-white text-center fs16">97 Sunfield Ave, Edison, NJ 08837, United States</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-3 my-1 my-lg-auto px-2 schedule-register-before-triangle">
            <div class="schedule-register-bg">
                <div class="schedule-date-bg-dots-img">
                    <div class="text-center text-lg-right text-xl-center py-5 py-lg-70">
                        <a href="{{url('convention_details')}}" class="btn register-today-btn">REGISTER TODAY</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Event Schedule -->

<!-- Convention Messages -->

<section class="container-fluid pt20 pb80">
    <div class="row justify-content-center">
        <div class="col-12 col-md-12 col-lg-12 py-3">
            <div class="row">
                @foreach($messages as $message)
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                    <h6 class="message-{{ $loop->iteration }} mb-3 text-center">{{$message->designation->name}} Message</h6>
                    <div class="shadow-small border-radius-10 president-msg_bg message-bg-{{ $loop->iteration }}">
                        <div class="msg_flower_bg d-flex">
                            <div class="p-3 my-auto">
                                <img src="{{asset(config('conventions.message_display').$message->image_url)}}" class="img-fluid border-radius-20 border-orange-3 tta-member-image" alt="{{$message->name}}" />
                            </div>
                            <div class="pt-0 pb-2 text-white my-auto">
                                <div class="text-center mb-1 heading-font fs18 font-weight-bold">{{$message->name}}</div>
                                <div class="text-center text-white fs14">{{$message->designation->name}}</div>
                            </div>
                        </div>
                        <div class="text-white px-3 pb-4">
                            <p class="pb-2 mb-0 text-center description">
                                {!! substr($message->message, 0, 100) !!}....
                            </p>
                            <div class="text-center pt-3">
                                <a href="{{url('message_content',$message->id)}}">
                                    <i class="fas fa-chevron-circle-right text-white fs20"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</section>

<!-- End of Convention Messages -->

<!-- Convention Team -->


<section class="container-fluid pb80 convention-team-bg">
    <div class="mx-auto main-heading">
        <span>Convention Team</span>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div>
                    <ul class="list-unstyled text-center horizontal-scroll">
                        <li class="convention-team-nav-item d-inline-flex">
                            <a class="convention-team-nav-link text-white fs18 pt10 pb10 text-decoration-none convention-team_active_tab_btn" target-id="#convention_advisory_committee">Convention&nbsp;Advisory&nbsp;Committee</a>
                        </li>
                        <li class="convention-team-nav-item d-inline-flex">
                            <a class="convention-team-nav-link text-white fs18 pt10 pb10 text-decoration-none" target-id="#convention_executive_committee">Convention&nbsp;Executive&nbsp;Committee</a>
                        </li>
                        <li class="convention-team-nav-item d-inline-flex">
                            <a class="convention-team-nav-link text-white fs18 pt10 pb10 text-decoration-none" target-id="#convention_regional_advisors">Convention&nbsp;Advisors</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row justify-content-center convention-team_active_tab" id="convention_advisory_committee">
            <div class="col-12 col-md-10 col-lg-10">
                <div class="row py-3">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/mallareddy.png" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Pailla Malla Reddy" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Dr. Pailla Malla Reddy</h6>
                                <div class="text-center pb-2 text-white fs15">Founder</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/vijay.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Vijayapal Reddy" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Dr. Vijayapal Reddy</h6>
                                <div class="text-center pb-2 text-white fs15">Chair</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/haranath_policherla.png" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Haranath Policherla" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Dr. Haranath Policherla</h6>
                                <div class="text-center pb-2 text-white fs15">Co-Chair</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Mohan-Patalolla_1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Mohan Reddy Patalolla" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Dr. Mohan Reddy Patalolla</h6>
                                <div class="text-center pb-2 text-white fs15">Council Member</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" id="convention_executive_committee" style="display: none;">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Mohan-Patalolla_1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Dr.Mohan Patalolla" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Mohan Reddy Patalolla</h6>
                                <div class="text-center pb-2 text-white fs15">President</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Srinivas_Ganagoni.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Dr.Mohan Patalolla" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Srinivas Ganagoni</h6>
                                 <div class="text-center pb-2 text-white fs15">Convener</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/vamshi_Reddy_2.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Vamshi Reddy" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Vamshi Reddy</h6>
                                 <div class="text-center pb-2 text-white fs15">President-Elect</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Bharath_Madadi_12.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Bharath Madadi" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Bharath Madadi</h6>
                                 <div class="text-center pb-2 text-white fs15">Past President</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Suresh_venkannagari_3.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Suresh Reddy Venkannagari" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Suresh Reddy Venkannagari</h6>
                                 <div class="text-center pb-2 text-white fs15">Executive Vice-President</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Srini-M_4.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Srinivasa Manapragada" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Srinivasa Manapragada</h6>
                                 <div class="text-center pb-2 text-white fs15">Genaral Secretary</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Pavan Ravva_6.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Pavan Ravva" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Pavan Ravva</h6>
                                 <div class="text-center pb-2 text-white fs15">Treasurer</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/venkat-gaddam_5.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Venkat Gaddam" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Venkat Gaddam</h6>
                                 <div class="text-center pb-2 text-white fs15">Executive Director</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/kvmadam.jpeg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Kavitha Reddy" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Kavitha Reddy</h6>
                                 <div class="text-center pb-2 text-white fs15">Joint Secretary</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Naveen_Goli_10.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Naveen Goli</h6>
                                 <div class="text-center pb-2 text-white fs15">International Vice-President</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/venkat_aekka_9.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Venkat Aekka" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Venkat Aekka</h6>
                                 <div class="text-center pb-2 text-white fs15">National Coordinator</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/harinder-tallapalli_8.png" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Harinder Tallapalli" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Harinder Tallapally</h6>
                                  <div class="text-center pb-2 text-white fs15">Joint Treasurer</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/narasimha_reddy_ln.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Narasimha Reddy Donthireddy" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Narasimha Reddy Donthireddy(LN)</h6>
                                 <div class="text-center pb-2 text-white fs15">Media & Communication Director</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/sole_madhavi_11.png" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Madhavi Soleti" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Madhavi Soleti</h6>
                                 <div class="text-center pb-2 text-white fs15">Ehtics Committee Coordinator</div> 
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Gangadhar_Vuppala.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="user" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Gangadhar Vuppala</h6>
                                <div class="text-center pb-2 text-white fs15">Convention Coordinator</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Usha Reddy Mannem.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Usha Reddy Mannem" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-white text-center mb-1 font-weight-bold">Usha Reddy Mannem</h6>
                                <div class="text-center pb-2 text-white fs15">Board Of Director</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" id="convention_regional_advisors" style="display: none;">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-lg-6 offset-lg-3">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-4 col-lg-6">
                                <div class="convention-team">
                                    <div>
                                        <img src="images/Santhosh_Pathuri.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Santhosh Pathuri" />
                                    </div>
                                    <div class="pt-3">
                                        <h6 class="text-white text-center mb-1 font-weight-bold">Santhosh Pathuri</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-6">
                                <div class="convention-team">
                                    <div>
                                        <img src="images/Damu Gedala.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Damu Gedala" />
                                    </div>
                                    <div class="pt-3">
                                        <h6 class="text-white text-center mb-1 font-weight-bold">Damu Gedala</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Convention Team -->

<!-- Convention Committees -->

<div class="side-border-bg">
    <section class="container-fluid pb80">
        <div class="mx-auto main-heading">
            <div>
                <span>Convention committees</span>
            </div>
        </div>
        <div class="container pt-4">
            <div class="row">
                <div class="col-12 px-0">
                    <div id="caroselCommitteeIndicators" class="carousel slide" data-ride="carousel" data-interval="5000">
                        <ol class="carousel-indicators carosel">
                            <li data-target="#caroselCommitteeIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#caroselCommitteeIndicators" data-slide-to="1"></li>
                            <li data-target="#caroselCommitteeIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner carosel-inner">
                            <div class="carousel-item active">
                                <div class="col-12">
                                    <div class="row pb-5">
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{'banquet'}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/banquet.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Banquet</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('budgetfinance')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/finance.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Budget & Finance</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('businessforum')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/bi_group.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Business Forum</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('cme')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/cme.png" class="img-fluid mx-auto d-block" alt="">
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">CE / CME</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('celebritiescoordination')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/coordination.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Celebrities Coordination</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('corporatesponsorship')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/carporate_sponsorship.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Corporate Sponsorships</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('cultural')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/cultural.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Cultural</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('decorations')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/decoration.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Decorations</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('page-under')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/youth.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Donor Relationship</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('food')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/food.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Food</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('fundraising')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/fund_raising.png" class="img-fluid mx-auto d-block" alt="">
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Fund Raising</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                         <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('hospitality')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/hospitality.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Hospitality</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>




                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-12">
                                    <div class="row pb-5">




                                       
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('immigrationforum')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/immigration.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Immigration Forum </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('page-under')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/cultural.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Kids Activities</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('page-under')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/literacy.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Literary</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('matrimonial')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/matrimony.png" class="img-fluid mx-auto d-block" alt="">
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Matrimonial</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('mediacommunication')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/social_media.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Media & Communication</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('overseascoordination')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/coordination.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Overseas coordination</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('politicalforum')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/political_forum.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Political Forum</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('programevents')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/publicity_r_media.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Programs And Events</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('reception')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/cme.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Reception</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('registration')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/event_registration.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Registration</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('safetysecurity')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/security.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Safety & Security</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('page-under')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/photography.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Short Film</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-12">
                                    <div class="row pb-5">


                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('socialmedia')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/social_media.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Social Media</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('souvenir')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/souvenir.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Souvenir</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('spiritual')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/spiritual.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Spiritual</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('stage')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/av.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Stage / Venue / AV / IT Infrastructure</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('startupforum')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/fund_raising.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Startup Forum</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('transportation')}}=" text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/transportation.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Transportation</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('ttastar')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/tta_star.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">TTA Star</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('vendorexhibits')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/exhibit_booth.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Vendors & Exhibits</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('volunteer')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/immigration.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Volunteer</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('web')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/flyers.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Web</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('womensforum')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/women.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Women's Forum</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-2 px-2">
                                            <a href="{{url('page-under')}}" class="text-decoration-none">
                                                <div class="convention-cc-bg">
                                                    <div class="p-3">
                                                        <div>
                                                            <img src="images/cc-icons/youth.png" class="img-fluid mx-auto d-block" alt="" />
                                                        </div>
                                                        <div class="text-white fs15 text-center pt-2 overflow-ellipsis">Youth Forum</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>





                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carosel_slider-btn">
                            <a class="carousel-control-prev" href="#caroselCommitteeIndicators" role="button" data-slide="prev">
                                <span class="carosel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#caroselCommitteeIndicators" role="button" data-slide="next">
                                <span class="carosel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <div>
                            <div class="text-right">
                                <a href="{{url('convention_committee_list')}}" class="text-decoration-none carosel_view-all-btn font-weight-bold fs14">VIEW ALL<i class="fas fa-angle-right pl-2"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End of Convention Committees -->

    <!-- Donors -->

    <section class="container-fluid pb80 donors-bg">
        <div class="mx-auto main-heading">
            <span>Convention Donors</span>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 my-5 pt-4 pb-5">
                    <h4 class="text-center text-white">Coming Soon</h4>
                </div>
            </div>
            <!-- <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-10 py-3">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-3 my-1">
                            <div class="convention-donor">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10 tta-member-image" alt="" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Harnath Policherla</h6>
                                    <div class="text-center pb-2">Michigan</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-3 my-1">
                            <div class="convention-donor">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10 tta-member-image" alt="" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Dr.Mohan Patalolla</h6>
                                    <div class="text-center pb-2">New Jersey</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-3 my-1">
                            <div class="convention-donor">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10 tta-member-image" alt="" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Malla Reddy Karra</h6>
                                    <div class="text-center pb-2">Boston</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3 py-3 my-1">
                            <div class="convention-donor">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10 tta-member-image" alt="" />
                                </div>
                                <div class="pt-3">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Vishnu Maddu</h6>
                                    <div class="text-center pb-2">Atlanta</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>

    <!-- End of Donors -->

    <!-- Events -->

    <section class="container-fluid pb80">
        <div class="mx-auto main-heading">
            <span>Convention Events Schedule</span>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 my-5 pt-5 pb-4">
                    <h4 class="text-center">Coming Soon</h4>
                </div>
            </div>
            <!-- <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-10 py-3">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Breakfast-and-matrimony.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Breakfast and Matrimony</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>27 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Business-and-womens_conference3.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Business conference, Women’s Con...</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>28 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Laxmi-narsimhaswamy-kalyanam2.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Sri Laxmi Narasimha Swamy Kalyanam</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>29 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Breakfast-and-matrimony.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Breakfast and Matrimony</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>27 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Business-and-womens_conference3.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Business conference, Women’s Con...</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>28 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Laxmi-narsimhaswamy-kalyanam2.jpg" class="img-fluid border-radius-15 w-100" />
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Sri Laxmi Narasimha Swamy Kalyanam</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class=""><i class="far fa-calendar-alt pr-2"></i>29 May 2022</div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm fs-xss-10">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>

    <!-- End of Events -->

    <!-- Media Partners -->

    
    <section class="container-fluid pb80">
        <div class="mx-auto main-heading">
            <span>Media partners</span>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-12 col-lg-12 py-3">
                    <marquee behavior="alternate" class="px-5">
                        <div class="row flex-wrap-nowrap px-5">
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/tv5.png" class="img-fluid mx-auto d-block w-100" />
                                </div>
                            </div>
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/tv9.png" class="img-fluid mx-auto d-block w-100" />
                                </div>
                            </div>
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/asia_tv.png" class="img-fluid mx-auto d-block w-100" />
                                </div>
                            </div>
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/mana-tv.png" class="img-fluid mx-auto d-block w-100" />
                                </div>
                            </div>
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/v6-news.png" class="img-fluid mx-auto d-block w-100" />
                                </div>
                            </div>
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/sakshi-tv.png" class="img-fluid mx-auto d-block w-100" />
                                </div>
                            </div>
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/telugu-nri-radio.png" class="img-fluid mx-auto d-block w-100" />
                                </div>
                            </div>
                            <div class="m-3">
                                <div class="meadia-partner-image">
                                    <img src="images/haritha_talks1.png" class="img-fluid mx-auto d-block w-100" />
                                </div>
                            </div>
                        </div>
                    </marquee>  
                </div>
            </div>
        </div>
       
    </section>

    <!-- End of Media Partners -->
</div>
<!-- End Of Content -->



@section('javascript')

<script src="js/slick.min.js"></script>

<script>
    $(".convention-team-nav-link").click(function() {
        //active_tab

        $(".convention-team_active_tab_btn").removeClass("convention-team-nav-link nav-item");
        $(".convention-team_active_tab_btn").removeClass("convention-team_active_tab_btn");
        $(this).addClass("convention-team-nav-link nav-item");
        $(this).addClass("convention-team_active_tab_btn");

        $(".convention-team_active_tab").hide();
        $(".convention-team_active_tab").removeClass("convention-team_active_tab");
        $($(this).attr("target-id")).addClass("convention-team_active_tab");
        $($(this).attr("target-id")).show();
    });

</script>

<script>
    $(".convention-committee-nav-link").click(function() {
        //active_tab
        $(".convention-committee_active_tab_btn").removeClass("convention-committee-nav-link");
        $(".convention-committee_active_tab_btn").removeClass("convention-committee_active_tab_btn");
        $(this).addClass("convention-committee-nav-link");
        $(this).addClass("convention-committee_active_tab_btn");

        $(".convention-committee_active_tab").hide();
        $(".convention-committee_active_tab").removeClass("convention-committee_active_tab");
        $($(this).attr("target-id")).addClass("convention-committee_active_tab");
        $($(this).attr("target-id")).show();
    });

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.invitees-carousel').slick({
            speed: 500, 
            slidesToShow: 4, 
            slidesToScroll: 1,
             autoplay: true,
             autoplaySpeed: 2000,
             dots: true,
             centerMode: true,
             variableWidth: true,
             responsive: [{
                breakpoint: 1024
                , settings: {
                    slidesToShow: 3
                    , slidesToScroll: 1
                    , variableWidth: true,
                    // centerMode: true,

                }

            }, {
                breakpoint: 800
                , settings: {
                    slidesToShow: 2
                    , slidesToScroll: 2
                    , dots: true
                    , infinite: true,

                }
            }, {
                breakpoint: 480
                , settings: {
                    slidesToShow: 1
                    , slidesToScroll: 1
                    , dots: true
                    , infinite: true
                    , autoplay: true
                    , autoplaySpeed: 2000
                , }
            }]
        });
    });

</script>

@endsection

@endsection

