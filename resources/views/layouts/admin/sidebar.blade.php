	<!-- Sidebar -->
	<div class="sidebar" id="sidebar">
	    <div class="sidebar-inner slimscroll">
	        <div id="sidebar-menu" class="sidebar-menu">
	            <ul>
	            	@if(Auth::user())
	                <li class="">
	                    <a href="{{url('admin')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
	                </li>
	                <!-- <li class="">
	                    <a href="{{url('admin/registrations')}}"><i class="fe fe-home"></i> <span>Registrations</span></a>
	                </li> -->
	                <li class="submenu">
	                    <a href="#"><i class="fe fe-document"></i> <span>Registrations</span> <span class="menu-arrow"></span></a>
	                    <ul style="display: none;">
	                        <li><a href="{{url('admin/registrations')}}">Member Registrations</a></li>
	                        <li><a href="{{url('admin/exhibit-registrations')}}">Exhibits Registration</a></li>
	                        <li><a href="{{url('admin/youth-activities-registrations')}}">Youth Activities Registration</a></li>
	                        <li><a href="{{url('admin/cruise_registrations')}}">Cruise Registrations</a></li>
	                    </ul>
	                </li>
	                <li class="submenu">
	                    <a href="#"><i class="fe fe-document"></i> <span> Home Page </span> <span class="menu-arrow"></span></a>
	                    <ul style="display: none;">
	                        <li><a href="{{route('banner.index')}}">Upload Banners</a></li>
	                        <li><a href="{{route('venue.index')}}">Change Venue Date & Time</a></li>
	                        <li><a href="{{route('message.index')}}">Add Messages</a></li>
	                        <li><a href="{{route('program.index')}}">Programs</a></li>
	                        <li><a href="{{route('member.index')}}">Members</a></li>
	                        <li><a href="{{route('event.index')}}">Events</a></li>
	                        <li><a href="{{route('schedule.index')}}">Schedule</a></li>
	                        <li><a href="{{route('video.index')}}">Youtube Videos</a></li>
	                    </ul>
	                </li>
	                <li class="submenu">
	                    <a href="#"><i class="fe fe-document"></i> <span> Content Management </span> <span class="menu-arrow"></span></a>
	                    <ul style="display: none;">
	                        <li><a href="{{route('menu.index')}}">Menu Management</a></li>
	                        <li><a href="{{route('submenu.index')}}">Menu Items Management</a></li>
	                        <li><a href="{{route('page.index')}}">Manage Pages</a></li>
	                        <li><a href="{{route('registration-page-content-update')}}">Registration Page Content</a></li>
	                    </ul>
	                </li>
	                <li class="submenu">
	                    <a href="#"><i class="fe fe-document subdrop"></i> <span> Master </span> <span class="menu-arrow"></span></a>
	                    <ul>
	                        <li><a href="{{route('sponsor-category-type.index') }}">Sponsor Category Type</a></li>
	                        <li><a href="{{route('sponsor-category.index')}}">Manage Sponsor Category</a></li>
	                        <li><a href="{{route('right-logo.index') }}">Upload Right Side Logo</a></li>
	                        <li><a href="{{route('left-logo.index') }}">Upload Left Side Logo</a></li>
	                        <li><a href="{{ route('leadership-type.index')}}">Leadership Type</a></li>
	                        <li><a href="{{ route('invitee.index')}}">Invitees Type</a></li>
	                        <li><a href="{{ route('vouchers.index')}}">Vouchers Type</a></li>
	                        <li><a href="{{ route('donortype.index')}}">Donor Type</a></li>
	                        <li><a href="{{route('designation.index')}}">Designation Type</a></li>
	                        <li><a href="{{route('paymenttype.index')}}">Payment Type</a></li>
	                        <li><a href="{{route('benefittype.index')}}">Benifits Type</a></li>
	                        <li><a href="{{route('exhibitor-type.index')}}">Exhibitor Type</a></li>
	                        <li><a href="{{route('committe.index')}}">Committes </a></li>
	                        <li><a href="{{url('admin/auditlogs')}}">Audit Logs </a></li>
	                    </ul>
	                </li>
	                @endif
	            </ul>
	        </div>
	    </div>
	</div>
	<!-- /Sidebar -->
