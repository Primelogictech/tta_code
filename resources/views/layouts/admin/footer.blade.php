<script src="{{asset('admin_asserts/js/jquery-3.2.1.min.js')}}"></script>

<!-- Bootstrap Core JS -->
<script src="{{asset('admin_asserts/js/popper.min.js')}}"></script>
<script src="{{asset('admin_asserts/js/bootstrap.min.js')}}"></script>

<!-- Slimscroll JS -->
<script src="{{asset('admin_asserts/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<script src="{{asset('admin_asserts/plugins/raphael/raphael.min.js')}}"></script>
<script src="{{asset('admin_asserts/plugins/morris/morris.min.js')}}"></script>
<script src="{{asset('admin_asserts/js/chart.morris.js')}}"></script>

<!-- Custom JS -->

<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>


<script src="{{asset('admin_asserts/js/script.js')}}"></script>