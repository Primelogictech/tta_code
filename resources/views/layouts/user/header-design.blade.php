    
    <!-- Header Desktop View -->

    <header class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 d-none d-lg-block">
                    <div class="row header-logos-bg">
                        <article class="left-logo-positions">
                            <a href="https://mytelanganaus.org" target="_blank">
                                @if($data['left_logo'] !=null )
                                <img src="{{asset(config('conventions.logo_display').$data['left_logo']->image_url)}}" alt="" border="0" class="img-fluid" width="120" />
                                @endif
                            </a>
                        </article>
                        <div class="col-12">
                            <div class="text-center">
                                <div class="web-name text-white website-name">TELANGANA AMERICAN TELUGU ASSOCIATION</div>
                                <div class="social-media-info">
                                    <span class="text-white">Follow us on : </span>
                                    <a href="https://www.facebook.com/Telangana-American-Telugu-Convention-2022-107145630850367" class="text-white"><i class="fab fa-facebook-f"></i></a>
                                </div>
                                <div class="fs15 text-white d-block website-name lh12">తెలుగు కళల తోట | తెలంగాణ సేవల కోట </div>
                                <div class="text-center pt-1">
                                    <img src="../images/images-in-header/mega-convenion-image.webp" class="img-fluid mega-convention-image" alt="">
                                </div>
                                <div class="pb-1 text-center">
                                    <div class="tax-text">501(c)(3) Non-Profit Organization &nbsp; Tax ID: 47-3132296</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center menu-bg" data-toggle="sticky-onscroll">
                        <div>
                            <nav class="navbar navbar-expand-lg navbar-light top-navbar py-0 justify-content-center d-none d-lg-block">
                                <!-- Navbar links -->

                                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                                    <ul class="navbar-nav">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url('/')}}">Home</a>
                                        </li>

                                        <li class="nav-item dropdown dropdownn">
                                            <a class="nav-link" href="" id="navbardrop" data-toggle="dropdown">
                                                Registration
                                            </a>
                                            <div class="dropdown-menu submenu l-menu s-row">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                          <ul class="list-unstyled">
                                                            <li>
                                                                <a href="{{ url('registration') }}?page=regpage" class="">Convention Registration</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{ url('vendorexhibits') }}?page=regpage" class="">Exhibit Registration</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{ url('souvenir') }}?page=regpage" class="">Souvenir Registration</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{ url('ttastar') }}?page=regpage" class=""?>TTA Star Registration</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{ url('matrimonial') }}?page=regpage" class=""?>Matrimony Registration</a>
                                                            </li>
                                                             <li>
                                                                <a href="{{ url('youthforum') }}?page=regpage" class=""?>Youth Registration</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{ url('startupcube') }}?page=regpage" class=""?>Startup Cube Registration</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                        </li>


                                        @foreach($data['menu'] as $menu)
                                        <li class="nav-item  {{$menu->slug==null? 'dropdown dropdownn': '' }}  ">
                                            <a class="nav-link" href="{{config('conventions.APP_URL')}}/{{$menu->slug==null ? '#': $menu->slug }}" id="navbardrop" {{$menu->slug==null? 'data-toggle="dropdown"': '' }}>
                                                {{$menu->name}}
                                            </a>
                                            <div class="dropdown-menu submenu  l-menu ">
                                                <div class="row">
                                                    {{-- $menu->submenus --}}
                                                    @foreach( $menu->submenus->chunk(1) as $chunk)
                                                    <div class="col-lg-4">
                                                        <ul class="list-unstyled">
                                                            @foreach ($chunk as $submenu)
                                                            <li>
                                                                <a href="{{config('conventions.APP_URL')}}/{{$submenu->slug }}" class="">{{ $submenu->name }}</a>
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </li>

                                        @endforeach

                                        @guest
                                        <li class="nav-item active">
                                            <a class="nav-link lh25 text-white" href="{{route('login')}}">
                                                Login
                                            </a>
                                        </li>
                                        @endguest

                                        @auth
                                        <li class="nav-item dropdown dropdownn">
                                            <a class="nav-link py-1" href="#" id="navbardrop" data-toggle="dropdown">
                                                <img src="../images/images-in-header/avtar.webp" alt="">
                                            </a>
                                            <div class="dropdown-menu user-icon submenu s-row">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <a href="{{ url('myaccount') }}">My Dashboard</a>
                                                            </li>
                                                            <li>
                                                                <form method="POST" action="{{ route('logout') }}">
                                                                    @csrf

                                                                    <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                                                        {{ __('Log Out') }}
                                                                    </x-dropdown-link>
                                                                </form>
                                                                <!--  <a href="#">Logout</a> -->
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        @endauth

                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>

                    <article class="tabhorizontal-hide right-logo-positions r-p8">
                        <a href="{{url('/')}}">
                            @if($data['right_logo'] !=null )
                            <img src="{{asset(config('conventions.logo_display').$data['right_logo']->image_url)}}" class="img-fluid" width="108" border="0" alt="" />
                            @endif
                        </a>
                    </article>
                </div>
            </div>
        </div>
    </header>

    <!-- Header End of Desktop View -->

    <!-- Header Mobile View -->

    <div class="container-fluid d-block d-lg-none header-bg" data-toggle="sticky-onscroll">
        <div class="row">
            <div class="col-12 px-0">
                <div class="mobile-header-bg">
                    <div class="row">
                        <!-- <div class="col-12 col-md-12 pt5">
                            <div class="text-center">
                                <span class="text-white">Follow Us on : </span>
                                <a href="https://www.facebook.com/Telangana-American-Telugu-Convention-2022-107145630850367" class="text-white"><i class="fab fa-facebook-f"></i></a>
                            </div>
                        </div> -->
                        
                        <div class="col-12 pt-1">
                            <div class="text-right px-3 px-lg-4">
                                <span class="text-white">Follow us on : </span>
                                <a href="https://www.facebook.com/Telangana-American-Telugu-Convention-2022-107145630850367" class="text-white"><i class="fab fa-facebook-f"></i></a>
                            </div>
                        </div>
                    </div>

                <nav class="navbar-dark d-block d-lg-none mobile-header-bg  pt-0 pb-2" data-toggle="sticky-onscroll">
                    <div class="navbar px-0 px-sm-1">
                            <div class="col-2 col-md-2">
                                <div>
                                    <a class="navbar-brand py-0 mr-0" href="{{url('/')}}">
                                        <img src="images/logo.png" alt="" border="0" class="img-fluid logo-size-responsive" />
                                    </a>
                                </div>
                            </div>
                            
                            <div class="col-8 col-md-8 px-0">
                                <!-- <div>
                                    <img src="images/mega-convenion-bg.png" class="img-fluid" alt="">
                                </div> -->
                                <div class="my-auto">
                                    <div class="text-center py-1">
                                        <div class="web-name  text-white website-name">TELANGANA&nbsp;AMERICAN&nbsp;TELUGU&nbsp;ASSOCIATION</div>
                                        <strong class="web-name-telugu text-white d-block website-name">తెలుగు కళల తోట తెలంగాణ సేవల కోట </strong>
                                    </div>
                                </div>
                            </div>

                            <div class="col-2 col-md-2">
                                <div class="text-right">
                                    <a href="{{url('/')}}">
                                        <img src="images/tta_convention.png" class="img-fluid logo-size-responsive" border="0" alt="" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </nav>

                    <div class="row pb-2"> 
                            <div class="col-2 col-sm-2 col-md-2 my-auto pr-0">
                                <div class="nav-item text-center pl-0">
                                    <a class="nav-link pl-2 pr-0 py-0" href="#">
                                        <span class="text-dark fs40 font-weight-bold" onclick="openNav()"><i class="fas fs30">&#xf039;</i></span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-8 col-sm-8 col-md-8 my-auto">
                               <div>
                                    <img src="images/mega-convenion-bg.png" class="img-fluid" alt="">
                                </div>
                                <div class="py-1 text-center">
                                    <div class="tax-text">501(c) Non-Profit Organization &nbsp; Tax ID: 47-3132296</div>
                                </div>
                            </div>

                            <div class="col-2 col-sm-2 col-md-2 my-auto">
                                
                            </div>
                        </div>
                    </div>
                    <div id="mySidenav" class="sidenav d-block d-lg-none">
                    <a href="javascript:void(0)" class="closebtn px-3 py-2" onclick="closeNav()">&times;</a>
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-headerr collapsed" data-toggle="collapse" aria-expanded="false">
                                <span class="title">
                                    <a href="{{url('/')}}">Home
                                    </a>
                                </span>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-headerr collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                <span class="title">Registration</span>
                                <span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>
                            </div>
                            <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                                <div class="card-bodyy p0">
                                    <ul>
                                        <li>
                                            <a href="{{ url('registration') }}?page=regpage" class="">Convention Registration</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('vendorexhibits') }}?page=regpage" class="">Exhibit Registration</a>
                                        </li>
                                        <li>
                                             <a href="{{ url('souvenir') }}?page=regpage" class="">Souvenir Registration</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('ttastar') }}?page=regpage" class=""?>TTA Star Registration</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('matrimonial') }}?page=regpage" class=""?>Matrimony Registration</a>
                                        </li>
                                         <li>
                                            <a href="{{ url('youthforum') }}?page=regpage" class=""?>Youth Registration</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('startupcube') }}?page=regpage" class=""?>Startup Cube Registration</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @foreach($data['menu'] as $menu)
                        <div class="card">
                            <div class="card-headerr collapsed" data-toggle="collapse" data-target="#{{$menu->name}}" aria-expanded="false" aria-controls="{{$menu->name}}">
                                <span class="title">
                                    @if($menu->slug==null)
                                    <span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>
                                    @endif
                                    <a class="nav-link p-0" href="{{config('conventions.APP_URL')}}/{{$menu->slug==null ? '#': $menu->slug }}" id="navbardrop" {{$menu->slug==null? 'data-toggle="dropdown"': '' }}>
                                        {{$menu->name}}
                                    </a>
                                </span>
                            </div>
                            <div id="{{$menu->name}}" class="collapse" data-parent="#accordionExample">
                                <div class="card-bodyy p0">
                                    {{-- $menu->submenus --}}
                                    @foreach( $menu->submenus->chunk(4) as $chunk)
                                    <div class="col-md-12">
                                        <ul class="list-unstyled">
                                            @foreach ($chunk as $submenu)
                                            <li>
                                                <a href="{{config('conventions.APP_URL')}}/{{$submenu->slug }}" class="">{{ $submenu->name }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @guest

                        <div class="card">
                            <div class="card-headerr collapsed" data-toggle="collapse" aria-expanded="false">
                                <span class="title">
                                    <a href="{{route('login')}}">Login
                                    </a>
                                </span>
                            </div>
                        </div>

                        @endguest



                           @auth

                        <div class="card">
                            <div class="card-headerr collapsed" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="false" aria-controls="collapseOne1">
                                <span class="title">{{Auth::user()->first_name}}</span>
                                <span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>
                            </div>
                            <div id="collapseOne1" class="collapse" data-parent="#accordionExample">
                                <div class="card-bodyy p0">
                                    <ul>
                                        <li>
                                             <a href="{{ url('myaccount') }}">My Dashboard</a>
                                        </li>
                                        <li>
                                            <form method="POST" action="{{ route('logout') }}">
                                                        @csrf
                                            <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                            this.closest('form').submit();">
                                                            {{ __('Log Out') }}
                                                    </x-dropdown-link>
                                        </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        @endauth

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End of Mobile Version -->

