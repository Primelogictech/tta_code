    
    <!-- Header Desktop View -->

    <header class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 px-0">
                    <div>
                        <img src="images/images-in-header/header.jpg" class="img-fluid" alt="">
                    </div>
                </div>
            </div> 

            <div class="row justify-content-center menu-bg" data-toggle="sticky-onscroll">
                <div>
                    <nav class="navbar navbar-expand-lg navbar-light top-navbar py-0 justify-content-center d-none d-lg-block">
                        <!-- Navbar links -->

                        <div class="collapse navbar-collapse" id="collapsibleNavbar">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/')}}">Home</a>
                                </li>

                                <li class="nav-item dropdown dropdownn">
                                    <a class="nav-link" href="" id="navbardrop" data-toggle="dropdown">
                                        Registration
                                    </a>
                                    <div class="dropdown-menu submenu l-menu s-row">
                                        <div class="row">
                                            <div class="col-md-12">
                                                  <ul class="list-unstyled">
                                                    <li>
                                                        <a href="{{ url('registration') }}?page=regpage" class="">Convention Registration</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ url('vendorexhibits') }}?page=regpage" class="">Exhibit Registration</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ url('souvenir') }}?page=regpage" class="">Souvenir Registration</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ url('ttastar') }}?page=regpage" class=""?>TTA Star Registration</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ url('matrimonial') }}?page=regpage" class=""?>Matrimony Registration</a>
                                                    </li>
                                                     <li>
                                                        <a href="{{ url('youthforum') }}?page=regpage" class=""?>Youth Registration</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ url('startupcube') }}?page=regpage" class=""?>Startup Cube Registration</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                </li>


                                @foreach($data['menu'] as $menu)
                                <li class="nav-item  {{$menu->slug==null? 'dropdown dropdownn': '' }}  ">
                                    <a class="nav-link" href="{{config('conventions.APP_URL')}}/{{$menu->slug==null ? '#': $menu->slug }}" id="navbardrop" {{$menu->slug==null? 'data-toggle="dropdown"': '' }}>
                                        {{$menu->name}}
                                    </a>
                                    <div class="dropdown-menu submenu  l-menu ">
                                        <div class="row">
                                            {{-- $menu->submenus --}}
                                            @foreach( $menu->submenus->chunk(1) as $chunk)
                                            <div class="col-lg-4">
                                                <ul class="list-unstyled">
                                                    @foreach ($chunk as $submenu)
                                                    <li>
                                                        <a href="{{config('conventions.APP_URL')}}/{{$submenu->slug }}" class="">{{ $submenu->name }}</a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>

                                @endforeach

                                @guest
                                <li class="nav-item active">
                                    <a class="nav-link lh25 text-white" href="{{route('login')}}">
                                        Login
                                    </a>
                                </li>
                                @endguest

                                @auth
                                <li class="nav-item dropdown dropdownn">
                                    <a class="nav-link py-1" href="#" id="navbardrop" data-toggle="dropdown">
                                        <img src="../images/images-in-header/avtar.webp" alt="">
                                    </a>
                                    <div class="dropdown-menu user-icon submenu s-row">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a href="{{ url('myaccount') }}">My Dashboard</a>
                                                    </li>
                                                    <li>
                                                        <form method="POST" action="{{ route('logout') }}">
                                                            @csrf

                                                            <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                        this.closest('form').submit();">
                                                                {{ __('Log Out') }}
                                                            </x-dropdown-link>
                                                        </form>
                                                        <!--  <a href="#">Logout</a> -->
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                @endauth

                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <!-- Header End of Desktop View -->

    <!-- Header Mobile View -->

    <div class="container-fluid d-block d-lg-none header-bg" data-toggle="sticky-onscroll">
        <div class="row">
            <div class="col-12 py-2 px-0 px-md-3" style="background: #f9af00;">
                <div class="nav-item pl-0">
                    <a class="nav-link py-0 lh10" href="#">
                        <span class="font-weight-bold text-dark" onclick="openNav()"><i class="fas fs20">&#xf039;</i><span class="menuu-text">MENU</span></span>
                    </a>
                </div>
            </div>
                
            <div id="mySidenav" class="sidenav d-block d-lg-none">
            <a href="javascript:void(0)" class="closebtn px-3 py-2" onclick="closeNav()">&times;</a>
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-headerr collapsed" data-toggle="collapse" aria-expanded="false">
                        <span class="title">
                            <a href="{{url('/')}}">Home
                            </a>
                        </span>
                    </div>
                </div>
                <div class="card">
                    <div class="card-headerr collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        <span class="title">Registration</span>
                        <span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>
                    </div>
                    <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                        <div class="card-bodyy p0">
                            <ul>
                                <li>
                                    <a href="{{ url('registration') }}?page=regpage" class="">Convention Registration</a>
                                </li>
                                <li>
                                    <a href="{{ url('vendorexhibits') }}?page=regpage" class="">Exhibit Registration</a>
                                </li>
                                <li>
                                     <a href="{{ url('souvenir') }}?page=regpage" class="">Souvenir Registration</a>
                                </li>
                                <li>
                                    <a href="{{ url('ttastar') }}?page=regpage" class=""?>TTA Star Registration</a>
                                </li>
                                <li>
                                    <a href="{{ url('matrimonial') }}?page=regpage" class=""?>Matrimony Registration</a>
                                </li>
                                 <li>
                                    <a href="{{ url('youthforum') }}?page=regpage" class=""?>Youth Registration</a>
                                </li>
                                <li>
                                    <a href="{{ url('startupcube') }}?page=regpage" class=""?>Startup Cube Registration</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @foreach($data['menu'] as $menu)
                <div class="card">
                    <div class="card-headerr collapsed" data-toggle="collapse" data-target="#{{$menu->name}}" aria-expanded="false" aria-controls="{{$menu->name}}">
                        <span class="title">
                            @if($menu->slug==null)
                            <span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>
                            @endif
                            <a class="nav-link p-0" href="{{config('conventions.APP_URL')}}/{{$menu->slug==null ? '#': $menu->slug }}" id="navbardrop" {{$menu->slug==null? 'data-toggle="dropdown"': '' }}>
                                {{$menu->name}}
                            </a>
                        </span>
                    </div>
                    <div id="{{$menu->name}}" class="collapse" data-parent="#accordionExample">
                        <div class="card-bodyy p0">
                            {{-- $menu->submenus --}}
                            @foreach( $menu->submenus->chunk(4) as $chunk)
                            <div class="col-md-12">
                                <ul class="list-unstyled">
                                    @foreach ($chunk as $submenu)
                                    <li>
                                        <a href="{{config('conventions.APP_URL')}}/{{$submenu->slug }}" class="">{{ $submenu->name }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endforeach

                @guest

                <div class="card">
                    <div class="card-headerr collapsed" data-toggle="collapse" aria-expanded="false">
                        <span class="title">
                            <a href="{{route('login')}}">Login
                            </a>
                        </span>
                    </div>
                </div>

                @endguest



                   @auth

                <div class="card">
                    <div class="card-headerr collapsed" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="false" aria-controls="collapseOne1">
                        <span class="title">{{Auth::user()->first_name}}</span>
                        <span class="accicon"><i class="fas fa-angle-down rotate-icon"></i></span>
                    </div>
                    <div id="collapseOne1" class="collapse" data-parent="#accordionExample">
                        <div class="card-bodyy p0">
                            <ul>
                                <li>
                                     <a href="{{ url('myaccount') }}">My Dashboard</a>
                                </li>
                                <li>
                                    <form method="POST" action="{{ route('logout') }}">
                                                @csrf
                                    <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                    this.closest('form').submit();">
                                                    {{ __('Log Out') }}
                                            </x-dropdown-link>
                                </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                @endauth

            </div>
        </div>
    </div>
</div>
    

    <!-- End of Mobile Version -->

