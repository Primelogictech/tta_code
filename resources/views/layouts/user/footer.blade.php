<!-- Footer -->

    <footer class="py-4 mt-4">
        <div class="container">
            <div class="row">
                <div class="col-6 col-sm-6 col-md-3 col-lg-3 py-3 my-auto">
                    <div>
                        <img src="images/logos/logo-1.webp" class="logo img-fluid" />
                    </div>
                </div>
             
                <div class="col-12 col-sm-6 col-md-3 col-lg-3 py-3">
                    <ul class="list-unstyled mb-0">
                        <li>
                            <div class="border-bottom d-inline-block text-white"> Registrations</div>
                        </li>
                        <li>
                            <a class="fs15" href="{{url('convention_details')}}"><span>Convention Registration</span></a>
                        </li>
                        <li>
                            <a class="fs15" href="{{url('exhibits-reservation')}}"><span>Exhibit Registration</span></a>
                        </li>
                        <li>
                            <a class="fs15" href="{{ url('souvenir') }}?page=regpage"><span>Souvenir Registration</span></a>
                        </li>
                        <li>
                            <a class="fs15" href="{{ url('ttastar') }}?page=regpage"><span>TTA Star Registration</span></a>
                        </li>
                        <li>
                            <a class="fs15" href="{{ url('youthforum') }}?page=regpage"><span>Youth Registration</span></a>
                        </li>
                         <li>
                            <a class="fs15" href="{{ url('startupcube') }}?page=regpage"><span>Startup Cube Registration</span></a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3">
                    <div class="text-white border-bottom d-inline-block" style="font-size: 16px;">Contact Us</div>
                             <div class="pt10"><a target="_blank" href="https://mytelanganaus.org/about.php" class="text-white fs15 text-decoration-none">About Us</a></div>
                            <div class="form-group mb10 pt10">
                           
                            <div class="fs15 text-white">registration@ttaconvention.org</div>
                            <div class="pt10 fs15 text-white">Helpline : 1-866-TTA-SEVA (1-866-882-7382)</div>
                        <div class="pt10 fs15 text-white">Total Visitor Count :{{4100+$data['visitors_count']}}</div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="container-fluid copy-right-bg">
        <div class="container">
            <div class="row">
                <div class="col-12 py-2">
                    <div class="text-center text-white font-weight-bold">
                        @ Copyrights 2022 Telangana Telugu Association All rights reserved.
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End of Footer -->

        <!-- All scripts -->
        <!-- jQuery library -->

        <script src="{{asset('js/jquery.js')}}"></script>

        <!-- Bootstrap JavaScript -->

        <script src="{{asset('js/bootstrapjs.js')}}"></script>

        <!--Popups js-->

        <script src="{{asset('js/popperjs.js')}}"></script>

        <!--Datatables js-->

        <!-- Extra js -->

        <script src="{{asset('js/extrajquery.js')}}"></script>

        <script src="{{asset('js/common.js')}}"></script>

        <script src="js/gallery.js"></script>

        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
        <script>
            AOS.init({
                duration: 2000,
            });
        </script>

<script>
    jQuery(window).load(function() {
        setTimeout(function() {
            
            $('body').find('#spinner').hide();
        }, 500);
    });
</script>

<script type="text/javascript">
    // Sticky navbar
    // =========================
    $(document).ready(function() {
        // Custom function which toggles between sticky class (is-sticky)
        var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
            var stickyHeight = sticky.outerHeight();
            var stickyTop = stickyWrapper.offset().top;
            if (scrollElement.scrollTop() >= stickyTop) {
                stickyWrapper.height(stickyHeight);
                sticky.addClass("is-sticky");
            } else {
                sticky.removeClass("is-sticky");
                stickyWrapper.height("auto");
            }
        };



        // Find all data-toggle="sticky-onscroll" elements
        $('[data-toggle="sticky-onscroll"]').each(function() {
            var sticky = $(this);
            var stickyWrapper = $("<div>").addClass("sticky-wrapper"); // insert hidden element to maintain actual top offset on page
            sticky.before(stickyWrapper);
            sticky.addClass("sticky");

            // Scroll & resize events
            $(window).on("scroll.sticky-onscroll resize.sticky-onscroll", function() {
                stickyToggle(sticky, stickyWrapper, $(this));
            });

            // On page load
            stickyToggle(sticky, stickyWrapper, $(window));
        });
    });

</script>


<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "280px";
        document.getElementById("mySidenav").style.height = "100%";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("mySidenav").style.width = "0";
    }
    $(".convention-team-nav-link").click(function () {
        //active_tab
        $(".convention-team_active_tab_btn").removeClass("convention-team-nav-link");
        $(".convention-team_active_tab_btn").removeClass("convention-team_active_tab_btn");
        $(this).addClass("convention-team-nav-link");
        $(this).addClass("convention-team_active_tab_btn");

        $(".convention-team_active_tab").hide();
        $(".convention-team_active_tab").removeClass("convention-team_active_tab");
        $($(this).attr("target-id")).addClass("convention-team_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>
</body>
</html>