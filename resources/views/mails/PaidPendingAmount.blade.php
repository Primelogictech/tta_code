<!DOCTYPE html>
<html>
<head>
    <style>

     /* Custom CSS */
     body{
        margin: 0px;
    }
    .pt4{
        padding-top: 4px;
    }
    .pt15{
        padding-top: 15px;
    }
    .pr15{
        padding-right: 15px;
    }
    .pb15{
        padding-bottom: 15px;
    }
    .pl15{
        padding-left: 15px;
    }
    .px-0{
        padding-left: 0px !important;
        padding-right: 0px !important;
    }
    .text-right{
        text-align: right;
    }
    .text-left{
        text-align: left;
    }
    .container,
    .container-fluid,
    .container-lg,
    .container-md,
    .container-sm,
    .container-xl {
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }
    .row {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: -15px;
    }
    *, ::after, ::before {
        box-sizing: border-box;
    }
    .col-12 {
        -ms-flex: 0 0 100%;
        flex: 0 0 100%;
        max-width: 100%;
    }
    .col-lg-2 {
        -ms-flex: 0 0 16.666667%;
        flex: 0 0 16.666667%;
        max-width: 16.666667%;
    }
    .col-lg-8 {
        -ms-flex: 0 0 66.666667%;
        flex: 0 0 66.666667%;
        max-width: 66.666667%;
    }
    .col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
        position: relative;
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
    }
    .web-name{
        font-size: 20px;
        color: #fff;
    }
    .web-name-telugu{
        font-size: 19px !important;
        color: #fff;
        display: block;
    }
    @media (max-width:768px){
        .web-name{
            font-size: 15px;
        }
    }
    @media (max-width: 576px){
        .web-name{
            font-size: 12px !important;
        }
        .web-name-telugu{
            font-size: 10px !important;
            color: #fff;
            display: block;
        }
    }
    @media (max-width:320px){
        .web-name{
            font-size: 10px !important;
        }
        .web-name-telugu{
            font-size: 8px !important;
            color: #fff;
            display: block;
        }
    }
    .table {
        width: 100%;
        margin-bottom: 1rem;
        color: #212529;
        border-collapse: collapse;
    }
    .table-responsive {
        display: block;
        width: 100%;
        overflow-x: auto;
    }
    .table-bordered td, .table-bordered th {
        border: 1px solid #dee2e6;
        padding: 10px;
    }
    .img-fluid {
        max-width: 100%;
        height: auto;
    }
    img {
        vertical-align: middle;
        border-style: none;
    }
    .text-center{
        text-align: center;
    }
    .bg-purple {
        background-color: #92278f;
    }
    .left-logo-positions {
        position: absolute;
        left: 15px;
        top: 3px;
        z-index: 9;
    }
    .right-logo-positions {
        position: absolute;
        right: 15px;
        top: 3px;
        z-index: 9;
    }
    .d-none {
        display: none !important;
    }
    @media (min-width: 992px){
        .d-lg-block {
            display: block !important;
        }
    }

</style>

</head>
<body  class="container" >

<div class="row bg-purple">
            <div class="col-lg-2 d-none d-lg-block">
                <div class="text-left">
                    <img src="{{config('conventions.APP_URL')}}/images/logo.png" alt="" border="0" class="img-fluid pt4" width="80" />
                </div>
            </div>
            <div class="col-12 col-lg-8 pt15 pb15 px-0">
                <div class="text-center pr15 pl15">
                    <strong class="web-name text-white website-name">TELANGANA AMERICAN TELUGU ASSOCIATION</strong>
                    <strong class="web-name-telugu">తెలుగు కళల తోట | తెలంగాణ సేవల కోట </strong>
                </div>
            </div>
            <div class="col-lg-2 d-none d-lg-block">
                <div class="text-right">
                    <img src="{{config('conventions.APP_URL')}}/images/tta_convention.png" class="img-fluid pt4" width="72" border="0" alt="" />
                </div>
            </div>
        </div>
 <br>
<p>Auto generated email, pls do not reply to this.</p>
<h1>Hi {{ucfirst($user->name)}},</h1>
<h3> you have successfully paid due amount of $ {{ $payment->payment_amount }}.</h3>


<h2>Payment Details:</h2>

    <table   class="table table-responsive" >
        <tr>
            <td><b>Mode of Payment:</b></td>
            <td>{{ $PaymentType->name }}</td>
        </tr>

        <tr>
            <td><b>Total Amount:</b></td>
            <td>$ {{ $user->total_amount }}</td>
        </tr>
        <tr>
            <td><b>Paid Amount:</b></td>
            <td>$ {{ $payment->payment_amount }}</td>
        </tr>
        <tr>
            <td><b>Due Amount:</b></td>
            <td>$ 0</td>
        </tr>
         <tr>
            <td><b>Payment Details: </b></td>
            <td> 
            <b>Transaction Id : </b>  {{$payment->unique_id_for_payment}}
            @if ($PaymentType->name=='Other')
            <br>
                <b>On Behalf Of : </b>{{$payment->more_info['Payment_made_through']}}
            <br>
                <b>Company Name : </b>{{$payment->more_info['company_name']}}
            <br>
                <b>Transaction Date : </b>{{$payment->more_info['transaction_date']}}
            @endif
                
            @if( $PaymentType->name=='Check')
                <br>
                    <b>Check Date : </b>{{$payment->more_info['cheque_date']??""}}
            @endif
            </td>
        </tr>
    </table>

            
  <p>Please reach out to us if you have any questions.</p>
  Regards,
<address>
Registration Committee<br> 

Telangana American Telugu Association - Mega Convention<br> 

registration@ttaconvention.org (vendorexhibits@ttaconvention.org)<br> 

For online registration and Convention programs & updates, please visit www.ttaconvention.org<br> 

Date and Venue: May 27th to 29th, 2022. <br> 

New Jersey Convention And Exposition Center<br> 

97 Sunfield Ave, Edison, NJ 08837, United States<br> 
</address>
</body>
</html>