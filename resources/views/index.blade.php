﻿@extends('layouts.user.base')
@section('content')
    <section class="container-fluid my-0">
    <div class="row">
        <div class="col-12 pt2 pb-0 px-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    @foreach ($banners as $banner)
                    <div class="carousel-item active">
                        <!-- <img class="d-block w-100 my-auto" src="{{asset('images/banner1.jpeg')}}" alt="First slide" /> -->
                          <img class="d-block w-100 my-auto" src="{{asset(config('conventions.banner_display').$banner->image_url)}}" alt="First slide" />
                    </div>
                    @endforeach
                    <!-- <div class="carousel-item">
                        <img class="d-block w-100 my-auto" src="images/banner1.jpeg" alt="Second slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 my-auto" src="images/banner1.jpeg" alt="Third slide" />
                    </div> -->
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- End of Carousel Banners -->
<section class="container-fluid" style="margin-top: -16px;">
    <div class="row px-1">
        <div class="col-12 col-md-12 col-lg-3 my-1 my-lg-auto px-2 schedule-date-before-triangle">
            <a href="{{url('show-events-on-calander')}}" class="text-decoration-none">
                <div class="schedule-date-bg">
                    <div class="schedule-date-bg-dots-img">
                        <div class="icon1">
                            <div class="py-5 py-lg-75 text-center">
                                <!-- <span class="fs25 text-white">May 27<sup>th</sup> to 29<sup>th</sup>, 2022<br></span> -->
                                    <span class="fs25 text-white">{{ \Carbon\Carbon::parse($venue->Event_date_time)->isoFormat('MMM Do')}} To {{ \Carbon\Carbon::parse($venue->end_date)->isoFormat('MMM Do')}}, {{ \Carbon\Carbon::parse($venue->end_date)->isoFormat('YYYY')}} </span>
                            </div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-12 col-lg-6 my-1 my-lg-auto px-2 location-before-triangle">
            <div class="schedule-location-bg">
                <div class="location-bg-dots-img">
                    <div class="icon2">
                        <div class="py-5 py-lg-63point5">
                            <h4 class="text-white text-left text-center fs23">{{$venue->location}}</h4>
                            <div class="text-left text-white text-center fs16">97 Sunfield Ave, Edison, NJ 08837, United States</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-3 my-1 my-lg-auto px-2 schedule-register-before-triangle">
            <div class="schedule-register-bg">
                <div class="schedule-date-bg-dots-img">
                    <div class="text-center text-lg-right text-xl-center py-5 py-lg-70">
                        <a href="{{route('login')}}" class="btn register-today-btn">REGISTER TODAY</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Event Schedule -->

<!-- Convention Messages -->

<section class="container-fluid mt20">
    <div class="row justify-content-center">
        <div class="col-12 col-md-12 col-lg-12 py-3">
            <div class="row">
                 @foreach($messages as $message)
                 <?php //dd($message); ?>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                    <h6 class="president-message mb-3 text-center">{{$message->name}}</h6>
                    <div class="shadow-small border-radius-10 president-msg_bg">
                        <div class="msg_flower_bg d-flex">
                            <div class="p-3 my-auto">
                                <!-- <img src="{{asset(config('conventions.message_display').$message->image_url)}}" class="mx-auto d-block rounded-circle" alt="" /> -->
                                <img src="{{asset(config('conventions.message_display').$message->image_url)}}" class="img-fluid rounded-circle" width="110px" height="110px" alt="Mohan Patalolla" style="border: 3px solid #f9a565;">
                            </div>
                            <div class="pt-0 pb-2 text-white my-auto">
                                <div class="text-center mb-1 fs16 font-weight-bold">Mohan Reddy Patalolla</div>
                                <div class="text-center text-white fs14">{{$message->designation->name}}</div>
                            </div>
                        </div>
                        <div class="text-white px-3 pb-4">
                            <p class="pb-2 mb-0 text-center description">
                                {!! substr($message->message, 0, 200) !!}....
                            </p>
                            <div class="text-center pt-3">
                                <a href="{{url('message_content',$message->id)}}">
                                    <i class="fas fa-chevron-circle-right text-white fs20"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                    <h6 class="president-elect-message mb-3 text-center">President Elect Message</h6>
                    <div class="shadow-small border-radius-10 president-elect-msg_bg">
                        <div class="msg_flower_bg d-flex">
                            <div class="p-3 my-auto">
                                <img src="images/vamshi_Reddy_2.jpg')}}" class="img-fluid mx-auto d-block rounded-circle" width="110px" height="110px" alt="Vamshi Reddy" style="border: 3px solid #f9a565;">
                            </div>
                            <div class="pt-0 pb-2 text-white my-auto">
                                <div class="text-center mb-1 fs16 font-weight-bold">Vamshi Reddy</div>
                                <div class="text-center text-white fs14">President Elect</div>
                            </div>
                        </div>
                        <div class="text-white px-3 pb-4">
                            <p class="pb-2 mb-0 text-center description">
                                With great privilege and honor, I assume the role of President of the Telangana American Telugu Association for the next two years....
                            </p>
                            <div class="text-center pt-3">
                                <a href="president_elect_details.php">
                                    <i class="fas fa-chevron-circle-right text-white fs20"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                    <h6 class="convenor-message mb-3 text-center">Convenor Message</h6>
                    <div class="shadow-small border-radius-10 convenor-msg_bg">
                        <div class="msg_flower_bg d-flex">
                            <div class="p-3 my-auto">
                                <img src="images/Srinivas_Ganagoni.jpg')}}" class="img-fluid mx-auto d-block rounded-circle" width="110px" height="110px" alt="Srinivas Ganagoni" style="border: 3px solid #f9a565;">
                            </div>
                            <div class="pt-0 pb-2 text-white my-auto">
                                <div class="text-center mb-1 fs16 font-weight-bold">Srinivas Ganagoni</div>
                                <div class="text-center text-white fs14">Convenor</div>
                            </div>
                        </div>
                        <div class="text-white px-3 pb-4">
                            <p class="pb-2 mb-0 text-center description">
                                With great privilege and honor, I assume the role of President of the Telangana American Telugu Association for the next two years....
                            </p>
                            <div class="text-center pt-3">
                                <a href="president_convenor_details.php">
                                    <i class="fas fa-chevron-circle-right text-white fs20"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->
                @endforeach
            </div>
        </div>
    </div>
</section>

<!-- End of Convention Messages -->

<!-- Convention Team -->

<section class="container-fluid mt80">
    <div class="mx-auto main-heading">
        <span>Convention Team</span>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div>
                    <ul class="list-unstyled text-center horizontal-scroll">
                         @foreach ($leadershiptypes as $leadershiptype)
                        <li class="convention-team-nav-item d-inline-flex">
                            <a class="convention-team-nav-link text-dark fs18 pt10 pb10 text-decoration-none {{$loop->first ? 'first-convention-type': '' }} convention-team_active_tab_btn" target-id="#{{str_replace(' ','_',$leadershiptype->name)}}" data-id="{{$leadershiptype->id}}" data-url="{{env('APP_URL')}}/api/get-leadership-with-typeid/{{$leadershiptype->id}}">{{$leadershiptype->name}}</a>
                        </li>
                        @endforeach
                        <!-- <li class="convention-team-nav-item d-inline-flex">
                            <a class="convention-team-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#convention_executive_committee">Convention Executive Committee</a>
                        </li>
                        <li class="convention-team-nav-item d-inline-flex">
                            <a class="convention-team-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#convention_regional_advisors">Convention Regional Advisors</a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <div id="Conv_committee">
        <div class="row justify-content-center convention-team_active_tab convention-add-div" id="convention_advisory_committee">
            <div class="col-12 col-md-10 col-lg-10">
                <div class="row py-3 conv_div_add">
                    {{-- <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/mallareddy.png')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Pailla Malla Reddy" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Pailla Malla Reddy</h6>
                                <div class="text-center pb-2">FOUNDER</div>
                            </div>
                        </div>
                    </div> --}}
                     <!-- <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/vijay.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Vijayapal Reddy" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Vijayapal Reddy</h6>
                                <div class="text-center pb-2">CHAIR</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/haranath_policherla.png')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Haranath Policherla" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Haranath Policherla</h6>
                                <div class="text-center pb-2">CO-CHAIR</div>
                            </div>
                        </div>
                    </div>
                   <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/Mohan-Patalolla_1.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Mohan Reddy Patalolla" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Mohan Reddy Patalolla</h6>
                                <div class="text-center pb-2">COUNCIL CHAIR</div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

        <!-- <div class="row justify-content-center" id="convention_executive_committee" style="display: none;">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/Mohan-Patalolla_1.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Dr.Mohan Patalolla" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Mohan Reddy Patalolla</h6>
                                <div class="text-center pb-2">PRESIDENT</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/Srinivas_Ganagoni.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Dr.Mohan Patalolla" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Srinivas Ganagoni</h6>
                                <div class="text-center pb-2">Convener</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/vamshi_Reddy_2.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Vamshi Reddy" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Vamshi Reddy</h6>
                                <div class="text-center pb-2">PRESIDENT-ELECT</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/Bharath_Madadi_12.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Bharath Madadi" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Bharath Madadi</h6>
                                <div class="text-center pb-2">PAST PRESIDENT</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/Suresh_venkannagari_3.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Suresh Reddy Venkannagari" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Suresh Reddy Venkannagari</h6>
                                <div class="text-center pb-2">EXECUTIVE VICE-PRESIDENT</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/Srini-M_4.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Srinivasa Manapragada" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Srinivasa Manapragada</h6>
                                <div class="text-center pb-2">GENERAL SECRETARY</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/Pavan Ravva_6.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Pavan Ravva" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Pavan Ravva</h6>
                                <div class="text-center pb-2">TREASURER</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/venkat-gaddam_5.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Venkat Gaddam" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Venkat Gaddam</h6>
                                <div class="text-center pb-2">EXECUTIVE DIRECTOR</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/kvmadam.jpeg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Kavitha Reddy" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Kavitha Reddy</h6>
                                <div class="text-center pb-2">JOINT SECRETARY</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/Naveen_Goli_10.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" width="150" alt="" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Naveen Goli</h6>
                                <div class="text-center pb-2">INTERNATIONAL VICE-PRESIDENT</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/venkat_aekka_9.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Venkat Aekka" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Venkat Aekka</h6>
                                <div class="text-center pb-2">NATIONAL CO-ORDINATOR</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/harinder-tallapalli_8.png')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Harinder Tallapalli" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Harinder Tallapally</h6>
                                <div class="text-center pb-2">JOINT TREASURER</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/narasimha_reddy_ln.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Narasimha Reddy Donthireddy" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Narasimha Reddy Donthireddy(LN)</h6>
                                <div class="text-center pb-2">MEDIA & COMMUNICATION DIRECTOR</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/sole_madhavi_11.png')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Madhavi Soleti" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Madhavi Soleti</h6>
                                <div class="text-center pb-2">EHTICS COMMITTEE CO-ORDINATOR</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="user" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Gangadhar Vuppala</h6>
                                <div class="text-center pb-2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/Usha-Mannem.jpg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Usha Mannam" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Usha Mannam</h6>
                                <div class="text-center pb-2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         -->
        <!-- <div class="row justify-content-center" id="convention_regional_advisors" style="display: none;">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-lg-6 offset-lg-3">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-4 col-lg-6 py-3 my-1">
                                <div class="executive-comite">
                                    <div>
                                        <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Santhosh Pathuri" />
                                    </div>
                                    <div class="pt-3 px-2">
                                        <h6 class="text-violet text-center mb-1 font-weight-bold">Santhosh Pathuri, NJ</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-6 py-3 my-1">
                                <div class="executive-comite">
                                    <div>
                                        <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Manohar Kasagani" />
                                    </div>
                                    <div class="pt-3 px-2">
                                        <h6 class="text-violet text-center mb-1 font-weight-bold">Manohar Kasagani, Dallas</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    </div>
</section>

<!-- End of Convention Team -->

<!-- Standing Committee -->

<section class="container-fluid mt80">
    <div class="mx-auto main-heading">
        <div>
            <span>Convention committees</span>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Malla Reddy Karra</h6>
                                <div>AUDIT COMMITTEE - CHAIR</div>
                                <div>Boston</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Vishnu Maddu" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Vishnu Maddu</h6>
                                <div>AUDIT COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Vijaypal Reddy</h6>
                                <div>BYLAWS COMMITTE - CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Mohan Patalolla" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Mohan Patalolla</h6>
                                <div>BYLAWS COMMITTEE - CO-CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suresh Reddy Kokatam" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Suresh Reddy Kokatam</h6>
                                <div>ETHICS COMMITTEE - CHAIR</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venkat Nalluri Kokatam" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Venkat Nalluri</h6>
                                <div>ETHICS COMMITTEE - CO-CHAIR</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Bhasker Pinna" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Bhasker Pinna</h6>
                                <div>WEBSITE COMMITTEE - CHAIR</div>
                                <div>Pennsylvania</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Nidheesh Raju Baskaruni" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Nidheesh Raju Baskaruni</h6>
                                <div>WEBSITE COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venugopal Darur" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Venugopal Darur</h6>
                                <div>WEBSITE COMMITTEE - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Ashok Chintakunta" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Ashok Chintakunta</h6>
                                <div>CULTURAL COMMITTEE - CHAIR</div>
                                <div>New York</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suman Mudamba" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Suman Mudamba</h6>
                                <div>CULTURAL COMMITTEE - CO-CHAIR</div>
                                <div>Delaware</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="" />
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Deepthi Reddy</h6>
                                <div>CULTURAL COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 pt-3">
                        <div class="text-right">
                            <a href="standing_committe_members_details.php" class="text-decoration-none events-more-details-btn border-radius-20 fs16 btn-lg">View More <i class="fas fa-angle-right pl-2 see_more_arrow"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Standing Committee -->

<!-- Invitees -->

<section class="container-fluid mt80 invitees_bg">
    <div class="mx-auto main-heading">
        <span>Invitees</span>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-lg-10 offset-lg-1">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3 my-1">
                                <div class="invitees">
                                    <div>
                                        <img src="{{asset('images/suma.kanakala.jpg')}}" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                    </div>
                                    <div class="pt-3 px-2">
                                        <h6 class="text-violet text-center mb-1 font-weight-bold">Suma Kanakala</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3 my-1">
                                <div class="invitees">
                                    <div>
                                        <img src="{{asset('images/sp.sailaja.jpg')}}" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                    </div>
                                    <div class="pt-3 px-2">
                                        <h6 class="text-violet text-center mb-1 font-weight-bold">S.P. Sailaja</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3 my-1">
                                <div class="invitees">
                                    <div>
                                        <img src="{{asset('images/sp.charan.jpg')}}" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                    </div>
                                    <div class="pt-3 px-2">
                                        <h6 class="text-violet text-center mb-1 font-weight-bold">S.P. Charan</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Invitees -->

<!-- Donors -->

<section class="container-fluid mt80">
    <div class="mx-auto main-heading">
        <span>Convention Donors</span>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                        <div class="founding-member">
                            <div>
                                <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Harnath Policherla</h6>
                                <div class="text-center pb-2">Michigan</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                        <div class="founding-member">
                            <div>
                                <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr.Mohan Patalolla</h6>
                                <div class="text-center pb-2">New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                        <div class="founding-member">
                            <div>
                                <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Srinivas Anugu</h6>
                                <div class="text-center pb-2">New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                        <div class="founding-member">
                            <div>
                                <img src="{{asset('images/malepic.jpeg')}}" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Vikram Reddy Jangam</h6>
                                <div class="text-center pb-2">Dallas</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Donors -->

<!-- Events -->

<section class="container-fluid mt80">
    <div class="mx-auto main-heading">
        <span>Convention Program Schedule</span>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    @foreach($events as $event)
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="{{asset(config('conventions.event_display').$event->image_url)}}" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>{{$event->event_name}}</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i><!-- 27 May 2022 -->{{ \Carbon\Carbon::parse($event->from_date)->isoFormat('Do MMM YYYY')}}</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="{{url('event-schedule',$event->id)}}" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="{{asset('images/events/Business-and-womens_conference3.jpg')}}" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Business conference, Women’s Con...</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>28 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="{{asset('images/events/Laxmi-narsimhaswamy-kalyanam2.jpg')}}" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Sri Laxmi Narasimha Swamy Kalyanam</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>29 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="{{asset('images/events/Breakfast-and-matrimony.jpg')}}" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Breakfast and Matrimony</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>27 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="{{asset('images/events/Business-and-womens_conference3.jpg')}}" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Business conference, Women’s Con...</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>28 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="{{asset('images/events/Laxmi-narsimhaswamy-kalyanam2.jpg')}}" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Sri Laxmi Narasimha Swamy Kalyanam</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>29 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Events -->

<!-- Media Partners -->

<section class="container-fluid mt80">
    <div class="mx-auto main-heading">
        <span>Media partners</span>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                        <div class="">
                            <img src="{{asset('images/tv9.png')}}" class="img-fluid mx-auto d-block" />
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                        <div class="">
                            <img src="{{asset('images/ntv.png')}}" class="img-fluid mx-auto d-block" />
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                        <div class="">
                            <img src="{{asset('images/etv.png')}}" class="img-fluid mx-auto d-block" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- this part of is to clone not to show in page -- start--}}
    <div class="conv_team_template" style="display: none;">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div class="executive-comite">
                            <div>
                                <img src="{{asset('images/mallareddy.png')}}" class="img-fluid border-radius-15 mx-auto d-block conv_image" width="150" alt="Pailla Malla Reddy" />
                            </div>
                            <div class="pt-3 px-2">
                                <h6 class="text-violet text-center mb-1 font-weight-bold conv_name">Dr. Pailla Malla Reddy</h6>
                                <div class="text-center pb-2 conv_name_designation">FOUNDER</div>
                            </div>
                        </div>
                    </div>

    </div>



{{-- this part of is to clone not to show in page----  end --}}
<!-- End of Media Partners -->

<!-- End Of Content -->

    <!-- End of Events -->

    <!-- End Of Content -->
@section('javascript')
<script src="{{asset('js/common.js')}}"></script>
<script>
    $(".convention-team-nav-link").click(function () {
        //active_tab
        $(".convention-team_active_tab_btn").removeClass("convention-team-nav-link");
        $(".convention-team_active_tab_btn").removeClass("convention-team_active_tab_btn");
        $(this).addClass("convention-team-nav-link");
        $(this).addClass("convention-team_active_tab_btn");

        $(".convention-team_active_tab").hide();
        $(".convention-team_active_tab").removeClass("convention-team_active_tab");
        $($(this).attr("target-id")).addClass("convention-team_active_tab");
        $('.convention-add-div').show();
    });
</script>
<script>
    $(".convention-team-nav-link").click(function() {
        //alert();
        //active_tab
        //alert($(this).data('url'));
        ajaxCall($(this).data('url'), 'get', null, showDataInLeadership)
        $(".convention-team_active_tab_btn").removeClass("convention-team-nav-link");
        $(".convention-team_active_tab_btn").removeClass("convention-team_active_tab_btn");
        $(this).addClass("convention-team-nav-link");
        $(this).addClass("convention-team_active_tab_btn");
    });

    $(".invitees-nav-link").click(function() {
        //active_tab
        ajaxCall($(this).data('url'), 'get', null, showDataInInvitees)
        $(".invitees_active_tab_btn").removeClass("invitees-nav-link");
        $(".invitees_active_tab_btn").removeClass("invitees_active_tab_btn");
        $(this).addClass("invitees-nav-link");
        $(this).addClass("invitees_active_tab_btn");

    });

    $(".donors-nav-link").click(function() {
        ajaxCall($(this).data('url'), 'get', null, showDataInOurDonors)
        //active_tab
        $(".donors_active_tab_btn").removeClass("donors-nav-link");
        $(".donors_active_tab_btn").removeClass("donors_active_tab_btn");
        $(this).addClass("donors-nav-link");
        $(this).addClass("donors_active_tab_btn");
    });

    $(document).ready(function() {
        $('.first-date').addClass("donors_active_tab_btn");
        $('.first-convention-type').addClass("convention_active_tab_btn");
        ajaxCall($('.first-convention-type').data('url'), 'get', null, showDataInLeadership)
        ajaxCall($('.first-date').data('url'), 'get', null, showDataInOurDonors)
        ajaxCall($('.invitees_all_data').data('url'), 'get', null, showDataInInvitees)
    })

    function showDataInOurDonors(data) {
        $('.donors-add-div').empty()
        if (data.length == 0) {
            $('.donors-add-div').append('<center> <p>No Donors</p> </center>');
        } else {
            for (let i = 0; i < data.length; i++) {
                $(".donor-template").find('.main').first().clone()
                    .find(".donor-image").attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url).end()
                    .find(".donor-name").text(data[i].member.name).end()
                    .appendTo($('.donors-add-div'));
            }
            $(".see-more-template").find('.main').first().clone()
                .find(".see-more-url").attr('href', "{{url('more-donor-details')}}/" + data[0].sub_type_id).end()
                .appendTo($('.donors-add-div'));
        }
    }

    function showDataInInvitees(data) {
        $('.invitees-add-div').empty()
        if (data.length == 0) {
            $('invitees-add-div').append('<center> <p>No Data</p> </center>');
        } else {
            for (let i = 0; i < data.length; i++) {
                $(".invitees-template").find('.main').first().clone()
                    .find(".invitees-image").attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url).end()
                    .find(".invitees-name").text(data[i].member.name).end()
                    .find(".invitees-designation").text(data[i].designation.name).end()
                    .appendTo($('.invitees-add-div'));
            }

        }
    }

    function showDataInLeadership(data) {
        //alert(data.length);
        $('.conv_div_add').empty()
        if (data.length == 0) {
            $('.conv_div_add').append('<center> <p>No Data</p> </center>');
        } else {
            // var dynamic_cont = '<div class="row justify-content-center convention-team_active_tab" id="convention_advisory_committee">
            // <div class="col-12 col-md-10 col-lg-10">
            //     <div class="row py-3 ">';
            for (let i = 0; i < data.length; i++) {

                //dynamic_cont = '';

                //console.log(data);
                $(".conv_team_template").html();
                //attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url)
                $(".conv_team_template").find('.executive-comite').first().clone()
                    .find(".conv_image").attr('src', "{{asset(config('conventions.member_display'))}}/" + data[i].member.image_url).end()
                    .find(".conv_name").text(data[i].member.name).end()
                    .find(".conv_name_designation").text(data[i].designation.name).end()
                    .appendTo($('.conv_div_add'));
            }
            // $(".see-more-leadership-template").find('.main').first().clone()
            //     .find(".see-more-url").attr('href', "{{url('more-leadership-details')}}/" + data[0].sub_type_id).end()
            //     .appendTo($('.leadership-add-div'));
        }
    }
</script>
@endsection

@endsection