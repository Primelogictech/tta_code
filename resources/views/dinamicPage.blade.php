@extends('layouts.user.base')
@section('content')

<link href="{{ asset('css/gallery1.css') }}" rel="stylesheet" /> 

<section class="container-fluid px-2">
    @if($page_content!=null)
    {!! $page_content !!}
    @else
    <div class="container vertical-height pt-3">
    		<div class="row">
    			<div class="col-12 vertical-center">
					<div>
    					<img src="images/under-construction1.png" class="img-fluid mx-auto d-block" alt="">
    				</div>
    				<h3 class="text-center pt-4">This Page is Under Construction</h3>
    				<h5 class="text-center pt-1 text-muted">We're working on it</h5>
    			</div>
    		</div>
    	</div>
    @endif
    
</section>



@section('javascript')

<script src="js/gallery1.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none",
        });

        $(".zoom").hover(
            function () {
                $(this).addClass("transition");
            },
            function () {
                $(this).removeClass("transition");
            }
        );
    });
</script>

@endsection


@endsection