@extends('layouts.user.base')
@section('content')

<style>
.error{
    color:red
}

</style>
    <section class="container-fluid my-3 my-lg-5">
        <div class="container shadow-md-none bg-white shadow-small">
            <div class="row">
                <div class="col-12 px-0">
                    <div class="headingss-bg p-3">
                        <h4 class="mb-0">Convention Registration</h4>
                    </div>
                </div>
            </div>
            <div class="row border-top mt2">
                <div class="col-12 py-0 pt-1 px-1 pb-md-5">
                    <div class="row">
                        <div class="col-12 pb-5">
                            <div>
                                <img src="images/banners/convention-registration.jpg" class="img-fluid w-100" alt="">
                            </div>
                        </div>
                    </div>
            <div class="row px-4 px-lg-0">
                    <div class="col-12 col-lg-10 offset-lg-1  shadow-small px-sm-30 p40 p-md-4 mb-5">

                        <form id="form" action="{{url('bookticketstore')}}" method="post" enctype="multipart/form-data">
                            <div class="row">

                                @csrf
                                <!-- Personal Information -->

                                <div class="col-12 ">
                                    @foreach ($errors->all() as $error)
                                    <div class="error">{{ $error }}</div>

                                    @endforeach
                                     @if ($message = Session::get('success'))
                          <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                          @endif
                                    <h5 class="text-violet py-2">Personal Information</h5>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6" id="nriva_email" style="display: none;">
                                            <label>Mail id Regestred with Nirva</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" id="nriva_email_field" class="form-control" placeholder="mail" />
                                            </div>

                                            <div>
                                                <button id="btn_get_data">Get data</button>
                                            </div>
                                        </div>

                                    </div>
                                     <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Email Id:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="email" required name="email" id="email" value="{{  Auth::user()->email ?? '' }}" class="form-control" @if(Auth::user()) readonly="" @endif placeholder="Email Id"  />
                                                    <div class="error email_valid_msg"></div>
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Mobile (USA Number Only) :</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text"  required name="mobile" id="mobile" value="{{  Auth::user()->mobile ?? '' }}" class="form-control" placeholder="Mobile"  />

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>First Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="first_name" id="first_name" value="{{  Auth::user()->first_name ?? '' }}" class="form-control" required placeholder="First Name" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Last Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" required name="last_name" id="last_name" value="{{  Auth::user()->last_name ?? '' }}" class="form-control" placeholder="Last Name" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Spouse Full Name:</label>
                                            <div>
                                                <input type="text" name="spouse_full_name" value="{{  Auth::user()->spouse_full_name ?? '' }}" id="spouse_first_name" class="form-control" placeholder="Spouse full Name" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row mb15">
                                        <div class="col-12 col-md-3 col-lg-3 my-1 my-md-auto"><label class="mb-0">Children:</label>
                                        </div>
                                        <div class="col-12 col-md-3 col-lg-3 my-1 my-md-auto">
                                            <div>
                                                <label>below 6 Years</label>
                                            </div>
                                            <input type="text" value="{{ Auth::user()->children_count['below_6'] ?? ''}}" name="below_6" class="form-control" placeholder="" />
                                        </div>
                                        <div class="col-12 col-md-3 col-lg-3 my-1 my-md-auto">
                                            <div>
                                                <label>Age 7-15</label>
                                            </div>
                                            <input type="text" name="age_7_to_15"  value="{{ Auth::user()->children_count['age_7_to_15'] ?? ''}}" class="form-control" placeholder="" />
                                        </div>
                                        <div class="col-12 col-md-3 col-lg-3 my-1 my-md-auto">
                                            <div>
                                                <label>Youth 16-23</label>
                                            </div>
                                            <input type="text" value="{{ Auth::user()->children_count['age_16_to_23'] ?? ''}}"  name="age_16_to_23" class="form-control" placeholder="" />
                                        </div>
                                    </div>

                                    

                                </div>

                                <!-- End of Personal Information -->

                                <!-- Contact Information -->

                                <div class="col-12">
                                    <h5 class="py-2 text-violet">Contact Information</h5>
                                   

                                     <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Address line 1:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" required name="address" value="{{ Auth::user()->address ?? "" }}" class="form-control" placeholder="Address line 1" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Address line 2:</label>
                                            <div>
                                                <input type="text" name="address2" value="{{ Auth::user()->address2 ?? ""  }}"  class="form-control" placeholder="Address line 2" />
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>City:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" required name="city" value="{{ Auth::user()->city ?? "" }}" id="city" class="form-control" placeholder="City" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                              <label>State:</label><span class="text-red">*</span>
                                            <div>
                                                <select required name="state" class="form-control">
                                                    <option value="">Select State</option>
                                                    @foreach ($states as $state)
                                                    <option
                                                      @if(Auth::user())
                                                            @if(Auth::user()->state==$state->state)
                                                                selected
                                                            @endif
                                                      @endif  
                                                     value="{{ $state->state }}">{{ $state->state }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Country:</label><span class="text-red">*</span>
                                            <div>
                                                <select name="country" required class="form-control">

                                                    <option>Select Country</option>
                                                    <option selected>United States</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Zip Code:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" required  name="zip_code" value="{{ Auth::user()->zip_code ?? ""  }}" id="zip_code" class="form-control" placeholder="Zip Code" />
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>

                            <!-- end of Contact Information -->

                            <hr class="dashed-hr" />

                            <div class="row">
                                <div class="col-12 pt-2">
                                    <!-- <div class="row">
                                        <div class="col-12 col-md-6 col-lg-5">
                                            <h5 class="text-violet">Registration / Donation Category</h5>
                                        </div>
                                        <div class="col-12 col-md-6 col-lg-6 px-3 px-md-0 px-lg-3">
                                            @foreach ($SponsorCategoryTypes as $SponsorCategoryType)
                                            <input @if ($SponsorCategoryType->name=="Donor") checked @endif type="radio" required value="{{ $SponsorCategoryType->name }}" data-name='{{ $SponsorCategoryType->name }}' class="radio-btn reg-type" name="category" /><span class="fs16 ml-3 mr-5">{{ $SponsorCategoryType->display_name }}</span>
                                            @endforeach
                                        </div>
                                    </div> -->

                                    <!-- Family / Individual Table -->

                                    <div class="row my-3" id='family-or-individual'>
                                     <div class="col-12">  
                                        <h5 class="py-2 text-violet">Registration Packages</h5>
                                     </div>
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="datatable table table-bordered table-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Registration Category</th>
                                                            <th>Price</th>
                                                            <th style="width: 120px;">Count</th>
                                                            <th>Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($Individuals as $Individual)
                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>{{ $Individual->benefits[0] }}</td>
                                                            <td class="text-orange font-weight-bold">$ <span id="price_{{ $Individual->id }}">{{ $Individual->start_amount }} </span> </td>
                                                            <td>
                                                                <input type="text" class="form-control count" id="count_{{ $Individual->id }}" data-name="{{$Individual->benefits[0]}}"  data-amount="{{$Individual->start_amount}}"  name="count[{{$Individual->id}}]" />
                                                            </td>
                                                            <td> $ <span id="amount_{{ $Individual->id }}" class='Individual_registration_amount'> 0</span> </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of Family / Individual Table -->
                                    <!-- Donor's Table -->
                                    <div class="row my-3" id="donor">
                                    <div class="col-12">  
                                        <h5 class="py-2 text-violet">Donation Packages</h5>
                                     </div>
                                        <div class="col-12">
                                            <div class="table-responsive">
                                                <table class="datatable table table-bordered table-center mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th style="width: 200px;" >Sponsor Category</th>
                                                            <th style="min-width: 300px;">Benefits</th>
                                                            <th style="width: 170px;" >Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                        @foreach ($donors as $donor)
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <input type="radio" class="sponsor_category" id="sponsor_category_radio_{{$donor->id}}" name="sponsor_category" value="{{$donor->id}}" data-name="{{$donor->donortype->name}}" data-amount="{{$donor->amount  }}" />
                                                                </div>
                                                            </td>
                                                            <td> {{ $donor->donortype->name }}</td>
                                                           
                                                            <td>
                                                                <div class="row">

                                                                    <div class="col-12 col-md-12 col-lg-12">
                                                                         <ul class="flower-list fs16">
                                                                       @foreach ($donor->benfits as $benefit )
                                                                            <li>{{ $benefit->name }} {{ $benefit->pivot->count!=null ?   '('.$benefit->pivot->count .')' : ""}}
                                                                               <!--  @if($benefit->has_image)
                                                                                  <input type="file" id="benfit_{{$benefit->id}}" name="benfitimage[{{ $donor->id }}_{{ $benefit->id }}]" accept="image/*">
                                                                                @endif -->
                                                                            </li>
                                                                            @endforeach 
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                             <td class="text-orange font-weight-bold ">$ <span id="sponsor_category_start_amount_{{$donor->id}}" class="sponsor_category_start_amount"> {{ $donor->start_amount }} </span>  - $ <span id="sponsor_category_end_amount_{{$donor->id}}" class="sponsor_category_end_amount"> {{ $donor->end_amount }} </span>
                                                                <div class="amount_div_all" id="amount_div_{{$donor->id}}" style="display:none" >
                                                                   <div class="d-flex">
                                                                        <span class="pt5 pr-2" style="">$</span>
                                                                        <input type="text" class="form-control amount_form_field" placeholder="Enter Amount">
                                                                    </div>
                                                                    <div class="col-12 pt-2">
                                                                        <div class="donation_amount_error error" ></div>
                                                                    </div>
                                                              </div>
                                                             </td>
                                                        </tr>

                                                        @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- End of Donor's Table -->
                                    <div class="btn btn-sm btn-danger text-uppercase px-2 px-md-5 reset_sponsor_category fs-xs-9 fs-sm-11">Reset Selected Sponsor Category</div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <!--<h5 class="text-violet py-2">Additional DONOR Benefits</h5>-->
                                    {!! $RegistrationContent->additional_donor_benefits !!}
                                </div>
                            </div>
                            
                                
                                
                          <!--   <div class="row px-3">
                                <div class="col-12 col-sm-10 col-md-6 col-lg-5 shadow-small py-4">
                                    <h6 class="text-center">Upload Photo</h6>
                                    <div class="browse-button py-2">
                                        <input type="file" class="form-control browse" name="image">
                                    </div>
                                    <div class="pt-3 text-center fs15">( File Size could not be more than 5MB )</div>
                                </div>
                            </div> -->
                           {{--  <div class="row my-2 px-3">
                                <div class="col-12 shadow-small p-4">
                                    <div>
                                        <div class="text-orange fs16">Note:-</div>
                                        {!! $RegistrationContent->note !!}
                                    </div>
                                </div>
                            </div>
 --}}
                            <div id="benfit_image_uploade" class="row"></div>

                     <hr class="dashed-hr">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-6 pt-2">
                                    <!--  <h5>Security Code</h5> -->
                                    <div class="row">
                                        <div class="col-11">
                                                    </div>
                                        <div class="col-1 px-2 my-auto">
                                            <div>
                                                <!--   <button type="button" class="btn btn-danger" class="reload" id="reload">
                                                    &#x21bb;
                                                </button> -->
                                                <!--  <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt=""> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-12 pt-2">
                                    <div class="row my-1" id="paid_amount_div" style="display: none;">
                                        <div class="col-12 px-3 px-md-0">
                                            <div class="row">
                                                <div class="col-7 col-md-8 my-auto">
                                                    <label class="mb-0">Paid Amount</label>
                                                </div>
                                                <div class="col-1 my-auto">
                                                    <label class="mb-0">:</label>
                                                </div>
                                                <div class="col-3 my-auto">
                                                    <div class="payment ">$ <span class="previous_paid_amount"> </span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="row my-2">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-7 col-md-9 my-auto">
                                                    <label class="mb-0"><b>Selected Registration Packages</b> </label>
                                                </div>
                                                <div class="col-4 col-md-3 my-auto">
                                                   <div class="" >
                                                    <label class="mb-0"><b>Total</b></label>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="col-12" id="append_reg_data_div">
                                            <div class="row">
                                                <div class="col-7 col-md-9 my-auto">
                                                    <label class="mb-0"><i>No Packages Selected </i></label>
                                                </div>
                                                <div class="col-4 col-md-3 my-auto">
                                                   <div class="" >
                                                    <label class="mb-0"> $ 0</label>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
{{-- 
                                    <div class="row my-1">
                                        <div class="col-12 px-3 px-md-0">
                                            <div class="row">
                                                <div class="col-7 col-md-7 my-auto">
                                                    <label class="mb-0">Registration Amount</label>
                                                </div>
                                                <div class="col-1 my-auto">
                                                    <label class="mb-0">:</label>
                                                </div>
                                                <div class="col-3 my-auto">
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                                    <div class="payment " style="display:none">$ <span class="registration_amount">0</span></div>

                                            
                                     <div class="row my-2">
                                        <div class="col-12 ">
                                            <div class="row">
                                                <div class="col-7 col-md-8 my-auto">
                                                    <label class="mb-0"><b>Selected Donation Packages</b> </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12  border-bottom">
                                            <div class="row mb-2">
                                                <div class="col-7 col-md-9 my-auto">
                                                    <label class="mb-0 donation_amount_text"><i>No Packages Selected </i></label> <b class="text-orange"><span class="donotion_amount"></span></b>
                                                </div>
                                               {{--  <div class="col-1 my-auto">
                                                    <label class="mb-0">:</label>
                                                </div> --}}
                                               <div class="col-5 col-md-3 my-auto">
                                                    <div class="d-flex" ><span class="pt5 fs20 donation_amount_by_user_doller_sign" style="display:none">$</span>
                                                    <input style="display:none" type="hidden" name="donation_amount_hidden" disabled value="0" class="form-control donation_amount_by_user ml5" placeholder="Amount" />
                                                    <span id="donation_amount_lable" class="pt5 fs20"></span> 
                                                    </div>
                                                    <div class="payment"> </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    
                                    <div class="row my-1">
                                        <div class="col-12  px-3 px-md-0">
                                            <div class="row mb-2">
                                                <div class="col-7 col-md-9 my-auto">
                                                <div class="text-right ">
                                                <b>
                                                    <label class="mb-0 fs25 fs-sm-16">Total Amount &nbsp;&nbsp;&nbsp; : </label>
                                                </b>
                                                </div>
                                                      
                                                </div>
                                               
                                                <div class="col-5 col-md-3 my-auto">
                                                <b>
                                                    <div class="payment fs25 fs-sm-16">$ <span id="total_amount_to_be_paid">0</span> </div>
                                                </b>
                                                    <input type="hidden" name="total_amount_paid_amount" id="total_amount_paid_amount" value="0">
                                                   
                                                </div>
                                                
                                            </div>
                                                 <div class="error-if-amount-is-zero error fs20" ></div>
                                                 <div class="partial-amount-error error fs20" ></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 px-3 px-md-0">
                                            <div class="row">
                                        <div class="form-group col-7 col-md-9 my-auto">
                                         <div class="text-right">
                                           {{--  <input type="hidden" id="donation_amount_hidden" name="donation_amount_hidden"> --}}
                                            <input type="hidden" id="registration_amount_hidden" name="registration_amount_hidden">
                                            <input type="hidden" id="is_upgrade" name="is_upgrade">
                                            <input type="checkbox" class="input-checkbox" id="pay_partial_amount" name="pay_partial_amount"><span class="pl25 fs16">Pay Partial Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        </div>
                                        </div>

                                        <div class="form-group col-5 col-md-3 my-auto" id="paying_amount" style="display: none;">
                                            <div>
                                            <div class="d-flex">
                                                <span class="pt5 fs20 pr-2" style="">$</span>
                                                <input type="text" name="paying_amount" value="" class="form-control paying_amount_inputfield" placeholder="Paying Amount" />
                                            </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-12">
                                    <h5 class="text-violet py-2">Payment Types</h5>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        @foreach ($RegistrationContent->patment_types as $patment_type)
                                        <div class="col-6 col-md-3 col-lg-2">
                                        <span class="mr-1 mr-md-5 position-relative pr-3">
                                            <input @if ($RegistrationContent->paymenttype($patment_type)->name=='Paypal') checked @endif type="radio" required class="p-radio-btn payment-button" data-name="{{$RegistrationContent->paymenttype( $patment_type)->name }}" value="{{$RegistrationContent->paymenttype( $patment_type)->id }}" name="payment_type" /><span class="fs16 pl-4 pl-md-4">{{ucfirst($RegistrationContent->paymenttype( $patment_type)->name )}}</span></span>

                                        {{-- <span class="position-relative pr-3"><input type="radio" class="p-radio-btn" name="payment_type" /><span class="fs16 pl-4 pl-md-4">Cheque/Cash</span></span> --}}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <hr class="dashed-hr">
                            <div class="row">
                                <!-- paypal payment -->

                                <div class="col-12" id="paypal">
                                    <h5 class="text-violet mb-3">Payment by using Paypal</h5>
                                    <div class="paypal-note">
                                    </div>
                                    <div class="form-row">
                                    </div>
                                </div>

                                <!-- paypal payment end -->

                                <div class="col-12" id="check_payment" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using check</h5>
                                    <div class="check-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Cheque Number: </label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="cheque_number"  class="form-control cheque_form_field" placeholder="Cheque Number">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Cheque Date </label><span class="text-red">*</span>
                                            <div>
                                                <input type="date" name="cheque_date"  class="form-control cheque_form_field" placeholder="cheque Date">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Handed over to:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="more_info"  class="form-control cheque_form_field" placeholder="Handed over to">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- zelle -->
                                <div class="col-12" id="zelle" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using Zelle</h5>
                                    <div class="zelle-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Zelle Reference Number</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="Zelle_Reference_Number"  class="form-control zelle_form_field" placeholder="Zelle Reference Number">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- zelle end -->
                                <!-- othre Payments -->
                                <div class="col-12" id="other" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using other payments</h5>
                                    <div class="other-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>On Behalf Of :</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="Payment_made_through"  class="form-control other_payment_form_field" placeholder="On Behalf Of">
                                            </div>
                                        </div>
                                          <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Company Name</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="company_name"  class="form-control other_payment_form_field" placeholder="Company Name">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Payment Date </label><span class="text-red">*</span>
                                            <div>
                                                <input type="date" name="transaction_date"  class="form-control other_payment_form_field" placeholder="Transaction Date">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Payment Reference Number :</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="transaction_id"  class="form-control other_payment_form_field" placeholder="Payment Reference Number ">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- othre Payments end -->

                                <!-- First data Payments -->
                                <div class="col-12" id="first-data" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using First data payments</h5>
                                    <div class="other-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>On Behalf Of :</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="Payment_made_through"  class="form-control other_payment_form_field" placeholder="On Behalf Of">
                                            </div>
                                        </div>
                                          <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Company Name</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="company_name"  class="form-control other_payment_form_field" placeholder="Company Name">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Payment Date </label><span class="text-red">*</span>
                                            <div>
                                                <input type="date" name="transaction_date"  class="form-control other_payment_form_field" placeholder="Transaction Date">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Payment Reference Number :</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="transaction_id"  class="form-control other_payment_form_field" placeholder="Payment Reference Number ">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- first data Payments end -->
                            </div>
                            <hr class="dashed-hr">
                            
                           
                            <div class="row py-2">
                                <div class="col-12 col-md-12 my-auto">
                                    <div>
                                        <input type="checkbox" class="input-checkbox agree-checkbox" required name="agrement">
                                        <div class="pl40 fs15 py-1 word-break-break-word">I DECLARE that I have read and understand the package information, I AGREE all donations are final and no refunds / Exchanges / Cancellations will be accepted.</div>
                                       <div class="pl40 fs15 py-1">I authorize the above charge and No refunds will be provided for any type of Registrations. I abide by TTA standards, rules and regulations.</div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 my-2 my-md-auto">
                                    <div class="text-center text-sm-right">
                                      
                                        <input type="submit" @if(!Auth::user()) disabled @endif  class="btn btn-lg btn-danger text-uppercase px-5 submit-button" value="Check out" name="">
                                        
                                        
                                        <!-- <div class="btn btn-lg btn-danger text-uppercase px-5">ASDFAD</div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="row pt-3 pb-2">
                                <div class="col-12 fs16">
                                    <!--  <span class="text-uppercase font-weight-bold">MAKE CHECKS PAYABLE TO :</span> -->
                                    {!! $RegistrationContent->check_payable_to !!}

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="row  image_Uploade_template px-3" style="display:none">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4  main mt-2">
       <div class="shadow-small py-4">
        <h6 class="text-center benfit_name text-danger">Upload Photo</h6>
        <div class="browse-button py-2">
        <div class="text-center">
            <input type="file" class="form-control text-center pb-3 browse benfit_input" onchange="ValidateFile(this)" name="benfitimage[]">
        </div>
        </div>
        <div class="file_name text-center"></div>

        </div>
        <div class="pt-3 text-center fs15">(Allowed file types: JPG, JPEG, PNG. File Max limit 5MB)</div>
    </div>
</div>


 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title " style="float: left;">Registration</h4>
        </div>

        <div class="modal-body">
            <div class="alert alert-success">Please enter OTP received in email. There might be delay up to 30 seconds to receive email and check spam/junk folder</div>
            <form class="" action="{{url('new_registertion')}}" method="post">
                 @csrf
                <input type="hidden" name="email" id="new_email">
             <div class="form-row">
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Code :</label><span class="text-red">*</span>
                        <div>
                            <input type="number" required name="otp" id="otp" value="" class="form-control" placeholder="Verification Code"  />

                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6 col-lg-6">
                        <label>Create Password (Optional) :</label>
                        <div>
                            <input type="password" name="password" id="password"  class="form-control" placeholder="Password"  />
                        </div>
                    </div>
                </div>
                 <button type="submit" class="btn btn-primary">Submit</button>
            </form>

          
        </div>
         <div class="modal-footer">
        <a href="{{url('/')}}" class="btn btn-danger" >Close</a>
      </div>
        
      </div>
      
    </div>
  </div>



@section('javascript')
<script src="https://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
<script>

    
       var _validFileExtensions = [".jpg", ".jpeg",  ".png"];    


        function ValidateFile(oInput) {
            
            $(oInput).parent().parent().parent().find('.file_name ').text(' ')
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            filename=sFileName.split('\\')[(sFileName.split('\\').length)-1]
                            $(oInput).parent().parent().parent().find('.file_name ').text(filename)
                            break;
                        }
                    }
                    
                    if (!blnValid) {
                        
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }

    $(document).ready(function() {

        registrationtype = JSON.parse('{!!json_encode(1) !!}')
        Individuals = JSON.parse('{!!json_encode($Individuals) !!}')
        paymenttypes = ({!!html_entity_decode($paymenttype) !!})
        donors = ({!!html_entity_decode($donors) !!})
        user = JSON.parse('{!!json_encode($user) !!}')
        if (registrationtype) {
            //$('#paid_amount_div').show()
            $('#is_upgrade').val('upgrade')
            $('.previous_paid_amount').text(user.amount_paid)
            $('[data-name="' + registrationtype.name + '"]').trigger("click");
            changeRegType($('[data-name="' + registrationtype.name + '"]'))
            //$('.reg-type').attr('disabled', 'disabled')

            $('.sponsor_category_start_amount').each(function() {
                if ((parseInt($(this).text()) <= parseInt(user.donor_amount))) {
                    id = $(this).attr('id').split("_")
                    $('#sponsor_category_radio_' + id[4]).attr('disabled', 'disabled')
                }
            })

        }

        $('.reset_sponsor_category').click(function () {
            $('.donation_amount_by_user').val('')
            $('#donation_amount_lable').text('')
            $('.donotion_amount').empty()
            $('.donation_amount_by_user').hide()
            $('.donation_amount_by_user_doller_sign').hide()
            $('.donation_amount_text').html('<i>No Packages Selected</i>')
            $('.donation_amount_error').empty()
            $('.donation_amount_by_user').val(0)
            $('.donation_amount_by_user').attr('disabled',true)
            $("input:radio[name=sponsor_category]:checked")[0].checked = false;
            $('#benfit_image_uploade').empty()

            $('.amount_div_all').hide()
            $('.showing_amount_div').find('.form-control').val('')
            $('.showing_amount_div').removeClass('showing_amount_div')
            
             reg_amount=parseInt(parseInt($('.registration_amount').text()))
             $('#total_amount_to_be_paid').text( reg_amount );
             $('#total_amount_paid_amount').val( reg_amount );
             if(reg_amount>0){
                $('.submit-button').attr('disabled',false)
             }else{
                $('.submit-button').attr('disabled',true)
             }
        })

          $('.submit-button').click(function(e){
           
            if($('#total_amount_to_be_paid').text()==0){
                $('.error-if-amount-is-zero').text('Please select any of the above packages')
                if ($('.reg-type').prop("checked")) {
                    if($('.reg-type').val()=='Family / Individual'){
                        $('.error-if-amount-is-zero').text('Please Enter Count')
                       }
                } 
                e.preventDefault()
            }else{
                $('.error-if-amount-is-zero').empty()
            }

             if ($('#pay_partial_amount').prop("checked")) {
                 Total=parseInt($('#total_amount_to_be_paid').text())
                    payment=$('.paying_amount_inputfield').val()
                    if(payment>=Total){
                        $('.partial-amount-error').text("Partial Amount Should be less then total amount")
                        e.preventDefault()
                    }else{
                        $('.partial-amount-error').empty()
                    }
            }

        })
        
        
         $.validator.addMethod('filesize', function (value, element, arg) {
                if(element.files[0]!=undefined){
                    if(element.files[0].size<=arg){ 
                        return true; 
                    }else { 
                        return false; 
                    } 
                }else{
                    return true; 
                }
         }); 

         $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });

        jQuery.validator.addClassRules('count', {
              digits: true,
              min: 1
         });

         jQuery.validator.addClassRules('amount_form_field', {
              digits: true,
              min: 1
         });

         

        $("#form").validate({
            
            rules: {
                 "benfitimage[]": { 
                     //   filesize : 1,
                    },
                    required:true,
                first_name: "alpha",
                last_name: "alpha",
                spouse_full_name: "alpha",
                mobile :{
                    maxlength: 14,
                    digits: true
                },
                below_6 :{
                    min: 1,
                    digits: true
                },

                 age_7_to_15 :{
                    min: 1,
                    digits: true
                },

                 age_16_to_23 :{
                    min: 1,
                    digits: true
                },
                'paying_amount': {
                     min: 1,
                     digits: true
                },
                'zip_code': {
                     digits: true
                },
            },
             messages: {
                "benfitimage[]": {
                    required: "Please upload file.",
                    filesize: "Max file size allowed is 5MB",
                },
                 "first_name": {
                    alpha: "First name should not contain numbers.",
                },
                 "last_name": {
                    alpha: "Last name should not contain numbers.",
                },
                 "spouse_full_name": {
                    alpha: "Spouse name should not contain numbers.",
                },
                 "mobile": {
                    maxlength: "Max length is  14.",
                }
            },
        });

           function displayImageUploadeField (benfit,doner_id){
                 $(".image_Uploade_template").find('.main').first().clone()
                 .find(".benfit_name").text(benfit.name).end()
                 .find(".benfit_input").addClass('benfit_input_real').end()
                 .find(".benfit_input").attr('name', 'benfitimage['+doner_id+'_'+benfit.id+']').end()
                    .appendTo($('#benfit_image_uploade'));
        }

        function showImageUploadeFieldsIfexist(doner_id){
            $('#benfit_image_uploade').empty()
            donors.forEach(donor => {
                if(doner_id==donor.id){
                    donor.benfits.forEach(benfit => {
                        if(benfit.has_image==1){
                            displayImageUploadeField(benfit,doner_id)
                        }
                    });
                }
            });
        }


        $(".nriva_member_check_box").change(function() {
            if (IsCheckBoxChecked(this)) {
                $('#nriva_email').show()
            } else {
                $('#nriva_email').hide()
            }
        })

          $('.submit-button').click(function(e){
              $('.benfit_input_real').each(function() {
                    $(this).rules("add", 
                        {
                            filesize : 5242880,
                            required: true,
                            messages: {
                            filesize: "Max file size allowed is 5MB"
                        }
                    })
            });
            
               
         /*    if($('#total_amount_to_be_paid').text()==0){
                $('.error-if-amount-is-zero').text('Please select any of the above packages')
                if ($('.reg-type').prop("checked")) {
                    if($('.reg-type').val()=='Family / Individual'){
                        $('.error-if-amount-is-zero').text('Please Enter Count')
                       }
                } 
                e.preventDefault()
            }else{
                $('.error-if-amount-is-zero').empty()
            } */
        });

          @if(!Auth::user())
$('#email').on('change',function(){
        $('.email_valid_msg').text('')
        var email = $('#email').val();
        var thiss =$(this);
          if(email !=""){
            if(email.search('@') >0 ){
            }else{
                $('.email_valid_msg').text('Please enter a valid email')
            }
        $.ajax({
            type: 'GET',
            url: "{{url('email_verification')}}?email="+email,
            success:function(data){
              if(data=="success"){
                $('#new_email').val(email);
                $('#myModal').modal({backdrop: 'static', keyboard: false});
                $('#myModal').modal('show');
              }else{
                window.location.href="{{url('login')}}?error=exit&email="+email;
              }
             
            }
        });
      }else{
          $('.email_valid_msg').text('Please enter email')
        //alert('Please enter email');
      }

    });
@endif


        $(".sponsor_category").click(function(e) {

            $('.amount_div_all').hide()
            $('#amount_div_'+$(this).val()).show()
            $('.showing_amount_div').find('.form-control').val('')
            $('.showing_amount_div').removeClass('showing_amount_div')
            $('#amount_div_'+$(this).val()).addClass('showing_amount_div')
            $('.donation_amount_by_user').val('')
            $('#donation_amount_lable').text(0)
            $('.donation_amount_by_user_doller_sign').show()
            $('.donation_amount_error').text('')
            $('.donation_amount_by_user').val(0)
              $('.donation_amount_text').text( '1.' +$(this).data('name'))
            $('.donotion_amount').text( "( $"+ $('#sponsor_category_start_amount_'+$(this).val()).text()+ ' -' + '$' + $('#sponsor_category_end_amount_'+$(this).val()).text() + ' )' )
            $('#donation_amount_hidden').val($(this).data('amount'))
           // $('.registration_amount').text(0)
            $('#total_amount_to_be_paid').text(parseInt(parseInt($('.registration_amount').text())));
            $('#total_amount_paid_amount').val($(this).data('amount') - user.amount_paid+parseInt($('.registration_amount').text()));
            $('.donation_amount_by_user').attr('disabled',false)
            //$('.submit-button').hide()
            $('.submit-button').attr('disabled',true)
             showImageUploadeFieldsIfexist($(this).val())
           
        });

        $('#reload').click(function() {
            $.ajax({
                type: 'GET',
                url: 'reload-captcha',
                success: function(data) {
                    $(".captcha span").html(data.captcha);
                }
            });
        });

        $('#pay_partial_amount').click(function() {
            if ($(this).prop("checked")) {
                $('#paying_amount').show()
                $('.paying_amount_inputfield').attr('required',true)
            } else {
                $('#paying_amount').hide()
                $('.paying_amount_inputfield').attr('required',false)
            }
        })
        $(".payment-button").click(function(e) {
            alert()
            if ($(this).prop("checked")) {
                if ($(this).data('name') == "{{ config('conventions.paypal_name_db') }}") {
                    $('#paypal').show();
                    $('#check_payment').hide();
                    $('#zelle').hide();
                    $('#other').hide();
                    paymenttypes.forEach(data => {
                        if (data.name == "{{ config('conventions.paypal_name_db') }}") {
                            $('.paypal-note').html(data.note)
                        }
                    });
                    $('.cheque_form_field').prop("required", false);
                    $('.zelle_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", false);

                }

                if ($(this).data('name') == "{{ config('conventions.check_name_db') }}") {
                    $('#check_payment').show();
                    $('#paypal').hide();
                    $('#zelle').hide();
                    $('#other').hide();

                    paymenttypes.forEach(data => {
                        if (data.name == "Check") {
                            $('.check-note').html(data.note)
                        }
                    });
                    $('.cheque_form_field').prop("required", true);
                    $('.zelle_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", false);
                }

                if ($(this).data('name') ==  "{{ config('conventions.zelle_name_db') }}") {
                    $('#zelle').show();
                    $('#paypal').hide();
                    $('#check_payment').hide();
                    $('#other').hide();
                    paymenttypes.forEach(data => {
                        if (data.name ==  "{{ config('conventions.zelle_name_db') }}") {
                            $('.zelle-note').html(data.note)
                        }

                    });
                    $('.zelle_form_field').prop("required", true);
                    $('.cheque_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", false);

                }
                if ($(this).data('name') == "{{ config('conventions.other_name_db') }}") {

                    $('#other').show();
                    $('#zelle').hide();
                    $('#paypal').hide();
                    $('#check_payment').hide();
                    paymenttypes.forEach(data => {
                        if (data.name == "{{ config('conventions.other_name_db') }}") {
                            $('.other-note').html(data.note)
                        }
                    });

                    $('.zelle_form_field').prop("required", false);
                    $('.cheque_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", true);

                }
            alert($(this).data('name'))
                 if ($(this).data('name') == "{{ config('conventions.first_data_name_db') }}") {
                     alert()
                  

                }
                //zelle
            }
        });

        $("#btn_get_data").click(function(e) {
            e.preventDefault();
            ajaxCall('{{config("conventions.APP_URL") }}/api/get-details?email=' + $('#nriva_email_field').val(), 'get', null, poplateInpFields)
        });

        function poplateInpFields(data) {
            const obj = JSON.parse(data);
            if (obj.length == 0) {
                alert('no data fount')
            } else {
                $('#first_name').val(obj.first_name)
                $('#last_name').val(obj.last_name)

                $('#email').val(obj.email)
                $('#mobile').val(obj.mobile)

                $('#city').val(obj.member.city)
                $('#zip_code').val(obj.member.zipcode)

            }
        }


          function isGreaterThenTodatDate(date) {
            today = new Date("{{date('m/d/Y')}}")
            date = new Date(date)
            if (today >= date) {
                return false
            } else {
                return true
            }
        }

        // $(".donation_amount_by_user").keyup(function() {
         $(".amount_form_field").keyup(function() {
             $('#donation_amount_lable').text($(this).val())
             $(".donation_amount_by_user").val($(this).val())
             input_value=$(this).val()
             sponsor_category_id=$(".sponsor_category:checked").val()
             start= parseInt($('#sponsor_category_start_amount_'+sponsor_category_id).text());
             registration_amount = parseInt($('.registration_amount').text());
             end= parseInt($('#sponsor_category_end_amount_'+sponsor_category_id).text())
            if ( !((input_value >= start) && (input_value <= end)) ) {
                $('.donation_amount_error').text('Amount should be in the selected package price range')
                //$('.submit-button').hide()
                $('.submit-button').attr('disabled',true)

            }else{
                $('.donation_amount_error').empty()
               // $('.submit-button').show()
                $('.submit-button').attr('disabled',false)
            }
            if (registrationtype) {
                $('#total_amount_to_be_paid').text( $(this).val() - $('.previous_paid_amount').text()+registration_amount);
                $('#total_amount_paid_amount').val($(this).val() - $('.previous_paid_amount').text()+registration_amount);
            }else{
               // alert($('#total_amount_to_be_paid').text());
                $('#total_amount_to_be_paid').text($(this).val()+registration_amount);
                $('#total_amount_paid_amount').val($(this).val()+registration_amount);
            }
             
         });


         $(".count").keyup(function() {
            sum = 0;
            id = $(this).attr('id').split("_")
            count = $(this).val();
            $('.submit-button').attr('disabled',false)
            var donation_amount = $('.donation_amount_by_user').val();
            $('#amount_' + id[1]).text($('#price_' + id[1]).text() * count)
            $('.Individual_registration_amount').each(function() {
                sum = parseInt(sum) + parseInt($(this).text())
                $('.registration_amount').text(sum)
                $('#registration_amount_hidden').val(sum)
                $('#registration_amount_hidden').val(sum)
                 $('#total_amount_to_be_paid').text(sum+parseInt(donation_amount));
                 $('#total_amount_paid_amount').val(sum+parseInt(donation_amount));
            });

            $('#append_reg_data_div').empty()
            var sno= 0;
            $('.count').each(function() {
               if($(this).val().length>=1){
                   sno= sno+1;
               $('#append_reg_data_div').append('<div class="row my-1"><div class="col-12"><div class="row"><div class="col-7 col-md-9 my-auto">'+
                '<label class="mb-0">'+sno+'. '+ $(this).data('name') +' ( count - '+ $(this).val() +') </label></div><div class="col-4 col-md-3 my-auto"><div  ><label class="mb-0"><b> $ '+ $(this).val() * $(this).data('amount')   +'</b></label></div></div></div</di</div>')
                  // a.push($(this).data('name'));
               }
            });

            if(sno==0){
                 $('#append_reg_data_div').append('<div class="row my-1"><div class="col-12"><div class="row"><div class="col-7 col-md-9 my-auto">'+
                '<label class="mb-0"><i>No Packages Selected</i></label></div><div class="col-4 col-md-3 my-auto"><div  ><label class="mb-0"> $ 0 </label></div></div></div</di</div>')
                  // a.push($(this).data('name'));
            }
            
            

        })

        $('.reg-type').click(function() {
            changeRegType(this)
        })

        function changeRegType(thisVal) {
            if ($(thisVal).prop("checked")) {
                if ($(thisVal).data('name') == 'Donor') {
                    $('#donor').show();
                    $('#family-or-individual').hide();
                    $('.sponsor_category').attr('required', 'required')
                    $('.count').val(0);
                    $( ".count" ).trigger( "keyup" );
                }

                if ($(thisVal).data('name') == 'Family / Individual') {
                    $('#donor').hide();
                    $('#family-or-individual').show();
                    $('.sponsor_category').removeAttr('required', 'required')
                    $('#total_amount_to_be_paid').text('0');
                    $('.sponsor_category').prop('checked', false);
                    $('.donotion_amount').text('0')
                    $('.donation_amount_by_user').val(0)
                    $('.donation_amount_by_user').attr('disabled',true)
                    $('.donation_amount_error').empty()
                   // $('.submit-button').show()
                   $('.submit-button').attr('disabled',false)
                    
                }
            }
        }
    });
</script>

@endsection


@endsection
