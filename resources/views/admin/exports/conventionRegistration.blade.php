 <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>Sl NO.</th>
                                <th>Member ID</th>
                                <th>Registration ID</th>
                                <th>Member Details</th>
                                <th>Spouse Name</th>
                                <th>Sponsorship Type</th>
                                <th>Sponsorship Category</th>
                                <th>Amount Paid</th>
                                <th>Amount Details</th>
                                <th>Payment Status</th>
                                <th>Ticket Issued</th>
                                <th>Notes</th>
                                <th>Transactions </th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($users as $user)
                            <?php
                             $payments = \App\Models\Payment::where('user_id', $user->id)->with('paymentmethord')->get();
                             ?>
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td> {{ $user->member_id }}</td>
                                <td> {{ $user->registration_id }}</td>
                                <td>
                                    <div class="py-1">
                                        <span>Name:</span><span> {{ $user->first_name }} {{ $user->last_name }}</span><br>
                                    </div>
                                    <div class="py-1">
                                        <span>Email:</span><span>{{ $user->email }}</span><br>
                                    </div>
                                    <div class="py-1">
                                        <span>Mobile:</span><span>{{ $user->phone_code }} {{ $user->mobile }}</span><br>
                                    </div>
                                </td>
                                <td>
                                        {{ $user->spouse_full_name }}    
                                </td>
                                <td>
                                    @if(!empty($user->sponsorship_category_id)){{  "Donor" }}
                                    @endif
                                    <br>
                                    @if(!empty($user->registration_amount)){{  "Family / Individual" }}
                                    @endif
                                </td>
                                <td> {{ $user->categorydetails->donortype->name ?? ""}} {{ isset($user->categorydetails->start_amount) ? ( '($'. $user->categorydetails->start_amount . '- $'. $user->categorydetails->end_amount . ")")  : ""  }}
                                    <br>
                                    @if(!empty($user->sponsorship_category_id))
                                    Donation Amount : $ {{number_format($user->donor_amount) }}
                                    @endif
                                    <br>
                                        @if(is_array($user->individual_registration))
                                        @foreach($user->individual_registration as $key => $value)
                                        @if($value > 0)
                                        <?php 
                                        $Individual = \App\Models\Admin\SponsorCategory::where('status', 1)->where('id',$key)->first();
                                         ?>
                                         @if(isset($Individual->benefits[0]))
                                       {{$Individual->benefits[0]}} - {{$value}};
                                       @endif

                                       <br>
                                         @endif
                                        @endforeach
                                       Registration Amount : $ {{number_format($user->registration_amount) }}
                                       @endif

                                 </td>
                                <td> {{ isset($user->amount_paid) ? '$'. number_format($user->amount_paid)  : ""  }}</td>
                                 <td>
                                    <b>Total Amount :</b> ${{$user->total_amount??"0"}}<br>
                                    <b>Discount Amount :</b> ${{$user->discount_amount??0 }}<br>
                                    <b>Discount Code :</b> {{$user->discount_code??"" }}<br>
                                    <b>Paid Amount :</b> ${{$user->amount_paid??"0"}}<br>
                                    
                                    <b>Due Amount :</b> ${{$user->total_amount-$user->amount_paid-$user->discount_amount??"0"}}<br>
                                </td>
                                <td>{{ ucfirst($user->payment_status) }}</td>
                                <td>@if($user->ticket_issue==1) Ticket Issued @else No @endif</td>
                                <td>{{ ucfirst($user->notes) }}</td>
                                <td>
                                    <br>
                                    @foreach($payments as $payment)
                                  
                                    <b>Transaction Id :</b>
                                    {{ $payment->unique_id_for_payment }}<br>
                                    <b>Transaction Date :</b>
                                    {{ $payment->created_at??'' }}<br>
                                    
                               <b>Payment Methods :</b> {{ ucfirst($payment->paymentmethord->name)  }}<br>
                                <b>Amount :</b> $ {{ $payment->payment_amount }} <br>
                                <b>Payment Status :</b> {{ ucfirst($payment->payment_status)}}<br>
                                =================================================
                                <br>
                                    @endforeach
                                </td>
                            </tr>
                            @empty

                            @endforelse


                        </tbody>
                    </table>