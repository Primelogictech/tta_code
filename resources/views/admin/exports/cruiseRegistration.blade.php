  <table class="table table-hover table-center mb-0">
    <thead>
                        <tr>

                            <th>Sl NO.</th>
                            <th>First name</th>
                            <th>Last Name</th>
                            <th>Phone Number</th>
                            <th>Email Id</th>
                            <th>DOB</th>
                            <th>Amount</th>
                            <th>Payment Details</th>
                            <th >Payment Status</th>

                        </tr>

                        </thead>

                        <tbody>
                             
                        @foreach ($registrations as $registration)

                            <tr>

                                <td>{{ $loop->iteration }}</td>

                                <td>{{ $registration->firstName }}</td>

                                <td> {{ $registration->lastName }}</td>
                                <td>{{ $registration->phone }}</td>
                                <td>{{ $registration->email }}</td>
                                <td>{{ $registration->dob }}</td>
                                <td>${{ $registration->amount }}</td>
                                <td>
                                   @if($registration->payment_type==2)
                                   Paypal



                                   @elseif($registration->payment_type==3)
                                   Zelle
                                   @elseif($registration->payment_type==4)
                                   Check
                                   @elseif($registration->payment_type==5)
                                   Other
                                   @elseif($registration->payment_type==6)
                                   Credit / Debit Card
                                   @endif
                                   <br>
                                   {{$registration->payment_details}}


                                    
                                </td>
                                 <td>
                                    {{ $registration->payment_status }}
                                  
                                 </td>
                               

                            </tr>

                            @endforeach
                          

                        </tbody>




                    </table>