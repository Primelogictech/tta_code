 <table class="table table-hover table-center mb-0">
    <thead>
                        <tr>
                            <th>Sl NO.</th>
                            <th>Organization name</th>
                            <th>Full Name</th>
                            <th>Phone Number</th>
                            <th>Email Id</th>
                            <th>Tax ID</th>
                            <th>Payment Details</th>
                            <th width="200px">Payment Status</th>
                           
                            <th>Package Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $registration)


                            <tr>
                                <td>{{ $loop->iteration }}</td>

                                <td>{{ $registration->company_name }}</td>

                                <td>{{ $registration->firstname }} {{ $registration->lastname }}</td>
                                <td>{{ $registration->phone_number }}</td>
                                <td>{{ $registration->email }}</td>
                                <td>{{ $registration->package_id }}</td>
                                <td>
                                    <b>Payment Type:</b> {{ $registration->paymentDetails->paymentmethord->name??"" }}<br>
                                    <b>Transaction Id:</b> {{ $registration->paymentDetails->unique_id_for_payment??"" }}<br>
                                    <b>Payment Date:</b> {{ $registration->paymentDetails->paymentmethord->updated_at??"" }}<br>
                                @if(isset($registration->paymentDetails->paymentmethord->name))
                                     @if($registration->paymentDetails->paymentmethord->name=="Other")
                                    <br>
                                        <b>On Behalf Of : </b>{{$registration->paymentDetails->more_info['Payment_made_through']??""}}
                                    <br>
                                        <b>Company Name : </b>{{$registration->paymentDetails->more_info['company_name']??""}}
                                    @endif


                                     @if($registration->paymentDetails->paymentmethord->name=="Check")
                                    <br>
                                        <b>Handed over to : </b>{{$registration->paymentDetails->more_info['handed_over_to']??""}}
                                    @endif

                                @endif

                                    
                                </td>
                                     <td>
                                  {{ ucfirst($registration->paymentDetails->payment_status??"")}}
                                </td>
                                <?php
                                $packages=\App\Models\PackageList::where('package_id',$registration->package_id)->get();
                                //dd($packages);
                                ?>
                                <td>
                                @foreach ($packages as $packag)
                                            
                                    <b>Package Code: </b>{{$packag->package_code??''}}<br>
                                        <b>Exhibitor Type/ Booth Size: </b>{{$packag->exhibit_type??''}}<br>
                                        <b>Quantity: </b>{{$packag->number_booths??''}}<br>
                                        <b>Package Amount: </b>{{$packag->amount??''}}
                                 @endforeach
                                </td>
                            </tr>
                            @endforeach
                       </tbody>
</table>