@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Audit Logs</h5>
        </div>
        
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>

                            <tr>
                                <th>Sl NO.</th>
                                <th>Registration Type</th>
                                <th>Details</th>
                                <th>User Details</th>
                                <th>Old Status</th>
                                <th>New Status  </th>
                                <th>statusChanged_by</th>
                                <th>Created At</th>
                               
                               
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($audit as $voucher)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$voucher->reg_type}}</td>
                                <td>{{$voucher->more_info}}</td>
                                <td>
                                    Convention Id : {{$voucher->convention_id}}<br>
                                    User Name : {{$voucher->user_name}}<br>
                                    User Email : {{$voucher->user_email}}<br>
                                    Id: {{$voucher->registration_id}}</td>
                                <td>@if($voucher->old_status=="1")
                                    Issued
                                    @elseif($voucher->old_status=="0")
                                    Not Issued
                                    @else
                                    {{$voucher->old_status}}
                                    @endif
                                </td>
                                <td>@if($voucher->new_status=="1")
                                    Issued
                                    @elseif($voucher->new_status=="0")
                                    Not Issued
                                    @else
                                    {{$voucher->new_status}}
                                    @endif</td>
                                <td>{{$voucher->statusChanged_by}}</td>
                                <td>{{date('Y-m-d H:i',strtotime($voucher->created_at))}}</td>
                                
                                
                            </tr>
                            @empty
                            <p>No audit to Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
