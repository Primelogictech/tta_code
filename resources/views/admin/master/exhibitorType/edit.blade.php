@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Edit Exhibitor Type</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('exhibitor-type.index')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('exhibitor-type.update', $exhibitorType->id )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Exhibitor Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" value="{{$exhibitorType->name}}" name="name" placeholder="exhibitorType Name">
                        </div>
                    </div>

                    <div class="size-price-template">
                        <div class="form-group row">
                            <label class="col-form-label col-md-2 chaild_of_main">Size & Price<span class="mandatory">*</span></label>

                            <div class="col-12 col-md-2 my-auto">
                                <input type="text" id="size_0" name="size[]" value="" class="form-control size" placeholder="Size" required="">
                            </div>
                            <div class="col-10 col-md-2 my-auto">
                                <input type="number" id="price_before_0" name="price_before[]" value="" class="form-control price_before" placeholder="Price Before" required="">

                            </div>
                            <div class="col-10 col-md-2 my-auto">
                                <input type="number" id="price_after_0" name="price_after[]" value="" class="form-control price_after" placeholder="Price After" required="">

                            </div>
                            <div class="col-10 col-md-3 my-auto">
                                <input type="date" id="till_date_0" name="till_date[]" value="" class="form-control till_date" placeholder="Enter Till Date" required="">


                            </div>
                            <div class="col-2 col-md-1 my-auto">
                                <div class="button-div">
                                    <span data-count="0" class="plus_n_minus_icons add-size-price"><i class="fas fa-plus"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="size-price-add-div"></div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" name="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')

<script>
    $(document).ready(function() {
        exhibitorType = JSON.parse('{!! $exhibitorType !!}')

        for (let i = 0; i < exhibitorType.size_price.length; i++) {
                count=$('.add-size-price').attr('data-count')
                $('#size_'+count).val(exhibitorType.size_price[i].size)
                $('#price_before_'+count).val(exhibitorType.size_price[i].price_before)
                $('#price_after_'+count).val(exhibitorType.size_price[i].price_after)
                $('#till_date_'+count).val(exhibitorType.size_price[i].till_date)
                addSizePrize($('.add-size-price'),exhibitorType.size_price[i])
        }

        $('body').on('click', '.add-size-price', function() {
            addSizePrize(this)
        })

        $(document).on("click", ".delete-item", function() {
             id = $(this).attr('id').split('_')
             $('#delete_size-price_' + id[3]).parent().remove();

        })

        function addSizePrize(thisval,data=null) {
             count = $(thisval).data('count')
             $(thisval).attr('data-count', parseInt($(thisval).attr('data-count')) + 1)
             $(".size-price-template").find('.row').first().clone()
             .find(".chaild_of_main").attr('id', 'delete_size-price_' + $(thisval).attr('data-count')).end()
             .find(".size").val('').end()
             .find(".size").attr('id', 'size_' + $(thisval).attr('data-count')).end()

             .find(".price_before").val('').end()
             .find(".price_before").attr('id', 'price_before_' + $(thisval).attr('data-count')).end()

             .find(".price_after").val('').end()
             .find(".price_after").attr('id', 'price_after_' + $(thisval).attr('data-count')).end()

              .find(".till_date").val('').end()
              .find(".till_date").attr('id', 'till_date_' + $(thisval).attr('data-count')).end()


             .find(".button-div").empty().end()
             .find(".button-div").append('<span class="plus_n_minus_icons delete-item" id="btn_delete_size-price_' + $(thisval).attr('data-count') + '"><i class="fas fa-minus"></i>').end()
                 .appendTo($('.size-price-add-div'));
        }
    })

</script>


@endsection

@endsection
