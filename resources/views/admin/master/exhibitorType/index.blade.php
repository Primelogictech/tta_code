@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Exhibitor Type</h5>

        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('exhibitor-type.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>

            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>

                            <tr>
                                <th>Sl NO.</th>
                                <th>Exhibitor Type Name</th>
                                <th>Size, Price Before, Price After, Till Date</th>

                                <th style="width:300px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($exhibitorTypes as $exhibitorType)


                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$exhibitorType->name}}</td>
                                <td>
                                    @foreach ($exhibitorType->size_price as $size)
                                    {{ $size['size'] }}, ${{ $size['price_before'] }}, ${{ $size['price_after'] }}, {{ $size['till_date'] }}
                                    <br>
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{route('exhibitor-type.edit', $exhibitorType->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button href="#" data-id={{$exhibitorType->id}} data-url='{{ env("APP_URL") }}/admin/exhibitor-type/{{$exhibitorType->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</button>
                                    <button href="#" data-url="{{ env("APP_URL") }}/admin/exhibitor-type-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$exhibitorType->id}}>{{ $exhibitorType->status=='1' ?  'Active': 'InActive' }}</button>
                                </td>
                            </tr>
                            @empty
                            <p>No ExhibitorType to Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
