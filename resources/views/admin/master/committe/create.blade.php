@extends('layouts.admin.base')
@section('content')


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Add Committe Type</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('committe.index')}}" data-toggle="tooltip" title="Back" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('committe.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Committe Name<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="text" class="form-control" name="name" placeholder="Committe Name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Display Order</label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" name="display_order" placeholder="Display Order">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Committe Mail<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="email" class="form-control" name="committe_email" placeholder="Committe Email">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Upload Image <span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="file" class="form-control" name="image">
                            <b>Image should be in 1:1 ration eg: 200X200</b>
                        </div>
                    </div>



                    <!--    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Has Count Type<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="checkbox" value="1" class="count-checkbox" name="has_count">
                        </div>
                    </div> -->



                    <div class="form-group row count-field" style="display:none">
                        <label class="col-form-label col-md-4 my-auto">Total count<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <input type="number" class="form-control" name="count" placeholder="count">
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script>
    $(".count-checkbox").change(function() {
        if (IsCheckBoxChecked(this)) {
            $('.count-field').show()
        } else {
            $('.count-field').hide()
        }
    })
</script>


@endsection


@endsection
