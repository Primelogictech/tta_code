@extends('layouts.admin.base')
@section('content')


<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Committe Type</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('committe.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>

                            <tr>
                                <th>Sl NO.</th>
                                <th>Committes Name</th>
                                <th>Diplay Order</th>
                                <th>Email</th>
                                <th>Image</th>
                                <th style="width:300px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($committes as $committe)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$committe->name}}</td>
                                <td>{{$committe->display_order}}</td>
                                <td>{{$committe->committe_email}}</td>
                                <td>
                                    <div>
                                        <img src="{{asset(config('conventions.committe_display').$committe->image_url)}}" style="background-color: #8c8c8c;" alt="{{$committe->name}}" width="50px" height="50px" class="img-fluid">
                                    </div>
                                </td>
                                <!--     <td>{{$committe->count}}</td> -->

                                <td>
                                    <a href="{{route('committe.edit', $committe->id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <button href="#" data-id={{$committe->id}} data-url='{{ env("APP_URL") }}/admin/committe/{{$committe->id}}' class="btn btn-sm btn-success my-1 mx-1  delete-btn">Delete</button>
                                    <button href="#" data-url="{{ env("APP_URL") }}/admin/committe-status-update" class="btn btn-sm btn-success text-white my-1 mx-1 status-btn" data-id={{$committe->id}}>{{ $committe->status=='1' ?  'Active': 'InActive' }}</button>
                                </td>
                            </tr>
                            @empty
                            <p>No committes to Show</p>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
