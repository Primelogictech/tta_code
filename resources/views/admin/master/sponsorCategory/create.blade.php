@extends('layouts.admin.base')
@section('content')

<style>
    .checkbox-input {
        position: relative;
        top: 30px;
        width: 16px;
        height: 16px;
    }
</style>


<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Add Sponsor Category Type</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('sponsor-category.create')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('sponsor-category.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Registration / Sponsor Category <span class="mandatory">*</span></label>
                        @foreach ($SponsorCategoryTypes as $SponsorCategoryType)
                        <div class="col-md-4 my-auto">
                            <input type="radio" data-name='{{ $SponsorCategoryType->name }}' value="{{ $SponsorCategoryType->name }}" class="radio-btn" name="type_id">
                            <span class="pl25 ">{{ $SponsorCategoryType->display_name }}</span>
                        </div>
                        @endforeach
                    </div>

                    <div id='donor' style="display: none;">
                        <div class="form-group row">
                            <label class="col-form-label col-md-4 my-auto">Select Category Type <span class="mandatory">*</span></label>
                            <div class="col-md-8 my-auto">
                                <select class="form-control" name="donor_type_id">
                                    <option selected disabled>-- Select --</option>
                                    @foreach($Donortypes as $Donortype)
                                    <option value="{{$Donortype->id}}">{{$Donortype->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-md-4">Start Amount<span class="mandatory">*</span></label>
                            <div class="col-12 col-md-8">
                                <input type="text" name="start_amount" value="" class="form-control" placeholder="Start Amount">
                            </div>

                              <label class="col-form-label col-md-4">End Amount<span class="mandatory">*</span></label>
                            <div class="col-12 col-md-8">
                                <input type="text" name="end_amount" value="" class="form-control" placeholder="End Amount">
                            </div>

                        </div>

                        <div class="form-group row mb-2">
                            <label class="col-form-label col-md-4 my-auto">Benifits<span class="mandatory">*</span></label>
                            <div class="col-12 col-md-8 my-auto">
                                <div class="donor-template donor-add-div">
                                    <div class="row mb-2 main ">
                                        @foreach ($benfits as $benfit)
                                        <label class="col-form-label col-md-4 my-auto">{{ $benfit->name }} {{ $benfit->has_input ? ', has input' : ""}} {{ $benfit->has_count ? "total-". $benfit->count : "" }} <span class="mandatory">*</span></label>
                                        <div class="col-5 col-md-5 my-auto chaild-of-main">
                                                <input type="checkbox" id="" name="benifits[]" value="{{ $benfit->id }}" class="checkbox-input" placeholder="">
                                                <input type="text" id="" name="count[{{ $benfit->id }}]" value="" class="form-control ml-4" placeholder="Enter Note / Count if Applicable">
                                                <input type="number" id="" name="display_order[{{ $benfit->id }}]" value="" class="form-control ml-4" placeholder="Enter Display Order">
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id='family-or-individual' style="display: none;">

                        <div class="family-or-individual-template">
                            <div class="form-group row main ">
                                <label class="col-form-label col-md-4 chaild-of-main">Enter Registration Category Name<span class="mandatory">*</span></label>
                                <div class="col-12 col-md-4 my-auto">
                                    <input type="text" id="" name="category[]" value="" class="form-control category-value" placeholder="">
                                </div>
                                <div class="col-10 col-md-3 my-auto">
                                    <input type="number" id="" name="amount[]" value="" class="form-control amount-value" placeholder="Enter Price">
                                </div>
                                <div class="col-2 col-md-1 my-auto">
                                    <div class="button-div">
                                        <span class="plus_n_minus_icons add-category-name" data-count="0"><i class="fas fa-plus"></i>
                                        </span>
                                    </div>
                                    <div class="col-md-4"></div>

                                </div>
                            </div>
                            <div class="family-or-individual-add-div"></div>
                        </div>
                    </div>

                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')


<script>
    $(document).ready(function() {

        $('.radio-btn').click(function() {

            if ($(this).prop("checked")) {
                if ($(this).data('name') == 'Donor') {
                    $('#donor').show();
                    $('#family-or-individual').hide();
                }

                if ($(this).data('name') == 'Family / Individual') {
                    $('#donor').hide();
                    $('#family-or-individual').show();
                }
            }
        })

        $('body').on('click', '.add-category-name', function() {
            count = $(this).data('count')
            $(this).attr('data-count', parseInt($(this).attr('data-count')) + 1)
            $(".family-or-individual-template").find('.main').first().clone()
                .find(".chaild-of-main").attr('id', 'delete_individual_' + $(this).attr('data-count')).end()
                .find(".category-value").val('').end()
                .find(".amount-value").val('').end()
                .find(".button-div").empty().end()
                .find(".button-div").append('<span class="plus_n_minus_icons delete-item" id="btn_delete_individual_' + $(this).attr('data-count') + '" ><i class="fas fa-minus"></i>').end()
                .appendTo($('.family-or-individual-add-div'));
        })

        $('body').on('click', '.add-category-donor', function() {
            count = $(this).data('count')
            $(this).attr('data-count', parseInt($(this).attr('data-count')) + 1)
            $(".donor-template").find('.main').first().clone()
                .find(".chaild-of-main").attr('id', 'delete_donor_' + $(this).attr('data-count')).end()
                .find(".benifit-value").val('').end()
                .find(".button-div").empty().end()
                .find(".button-div").append('<span class="plus_n_minus_icons delete-item" id="btn_delete_donor_' + $(this).attr('data-count') + '" ><i class="fas fa-minus"></i>').end()
                .appendTo($('.donor-add-div'));
        })

        $(document).on("click", ".delete-item", function() {
            id = $(this).attr('id').split('_')
            $('#delete_' + id[2] + '_' + id[3]).parent().remove();
        })

    })
</script>

@endsection


@endsection
