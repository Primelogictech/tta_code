@extends('layouts.admin.base')

@section('content')

<div class="page-header">

    <div class="row">

        <div class="col-9 col-sm-6 my-auto">

            <h5 class="page-title mb-0">Exhibit Registrations</h5>

        </div>

        <div class="col-3 col-sm-6 col-md-6 my-auto">



        </div>

    </div>

</div>



<!-- /Page Header -->



<div class="row">

   <div class="col-12">

                                <div class="my-2 text-center">

                                    <h5 class="d-inline-block text-skyblue">Exhibits Package Details:</h5>

                                </div>

                                <div class="table-responsive">

                                    <table class="datatable table table-bordered table-center mb-0">

                                        <thead>

                                            <tr>

                                                <th >Package Code</th>

                                                <th >Exhibitor Type/ Booth Size</th>

                                                <th >Quantity</th>
                                                <th >Package Amount</th>

                                            </tr>

                                        </thead>

                                        <tbody>
                                         
                        @foreach ($packages as $packag)
                                            <tr>

                                                <td>{{$packag->package_code}}</td>

                                                <td>{{$packag->exhibit_type}}</td>
                                                <td>{{$packag->number_booths}}</td>
                                                <td>{{$packag->amount}}</td>

                                            </tr>
                                            @endforeach

                                        </tbody>

                                    </table>

                                </div>

                            </div>

</div>





@endsection

