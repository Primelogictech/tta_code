@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                @if(Cookie::get('conventionCookie'))
                 <a href="{{url('private_convention_members')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
                @else
                <a href="{{url('admin\registrations')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>S No</th>
                                <th>Transaction details</th>
                                <th>Payment Methord</th>
                                <th>Amount</th>
                                <th>Payment Status</th>
                                <th>Account Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payments as $payment)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                     <b>Date</b>
                                       @if($payment->paymentmethord->name=="Other")
                                            {{$payment->more_info['transaction_date']}}
                                        @elseif($payment->paymentmethord->name=="Check")
                                            {{$payment->more_info['cheque_date']}}
                                        @else
                                            {{ $payment->created_at }}
                                        @endif
                                    <br>
                                    <b>Transaction Id </b>
                                    {{ $payment->unique_id_for_payment }}
                                     @if($payment->paymentmethord->name=="Other")
                                    <br>
                                        <b>On Behalf Of : </b>{{$payment->more_info['Payment_made_through']??""}}
                                    <br>
                                        <b>Company Name : </b>{{$payment->more_info['company_name']??""}}
                                    @endif

                                    @if($payment->paymentmethord->name=="Check")
                                    <br>
                                        <b>Handed over to : </b>{{$payment->more_info['handed_over_to']??""}}
                                    @endif
                                  
                                  
                                </td>
                                <td>{{ ucfirst($payment->paymentmethord->name)  }}</td>
                                <td> $ {{ $payment->payment_amount }} </td>
                                <td>
                                  <select class="form-control payment_status" name="payment_status" data-id="{{ $payment->id }}">
                                    @foreach(paymentStatus() as $payment_status_name)
                                    <option value="{{$payment_status_name}}" 
                                    {{ ucfirst($payment->payment_status)==$payment_status_name? "selected" :"" }} >{{$payment_status_name}}</option>
                                    @endforeach
                                </select>
                                 </td>
                                <td> {{ ucfirst($payment->account_status) }} </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@section('javascript')
<script>
 $(document).ready(function() {
    $('.payment_status').change(function () {
        
         data = {
                 "_token": $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                status: $(this).val()
            }
       ajaxCall('{{ url('admin/update-payment-status') }}/'+$(this).data('id'), 'put', data , afterStatusUpdate)
    })

    function afterStatusUpdate(data){
        if(data==1){
                swal({
                        title: "success!",
                        text: "Stauts updated!",
                        icon: "success",
                    });
        }else{
             swal({
                    title: "Oops...!",
                    text: "Something went wrong!!",
                    icon: "error",
                });
        }
    }

  })
</script>
@endsection



@endsection
