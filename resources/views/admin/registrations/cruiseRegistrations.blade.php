@extends('layouts.admin.base')

@section('content')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<div class="page-header">

    <div class="row">

        <div class="col-9 col-sm-6 my-auto">

            <h5 class="page-title mb-0">Cruise Registrations</h5>

        </div>

         <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">

                <a href="{{url('cruise-registration-export')}}" title="" class="add-new-btn btn" data-original-title="Export">Export</a>
            </div>
        </div> 

    </div>

</div>



<!-- /Page Header -->



<div class="row">

    <div class="col-md-12">

        <div class="card">

            <div class="card-body">

                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <tr>

                            <th>Sl NO.</th>
                            <th>First name</th>
                            <th>Last Name</th>
                            <th>Phone Number</th>
                            <th>Email Id</th>
                            <th>DOB</th>
                            <th>Amount</th>
                            <th>Payment Details</th>
                            <th width="200px">Payment Status</th>

                            <th>Action</th>

                        </tr>

                        </thead>

                        <tbody>
                             
                        @foreach ($registrations as $registration)

                            <tr>

                                <td>{{ $loop->iteration }}</td>

                                <td>{{ $registration->firstName }}</td>

                                <td> {{ $registration->lastName }}</td>
                                <td>{{ $registration->phone }}</td>
                                <td>{{ $registration->email }}</td>
                                <td>{{ $registration->dob }}</td>
                                <td>${{ $registration->amount }}</td>
                                <td>
                                   @if($registration->payment_type==2)
                                   Paypal



                                   @elseif($registration->payment_type==3)
                                   Zelle
                                   @elseif($registration->payment_type==4)
                                   Check
                                   @elseif($registration->payment_type==5)
                                   Other
                                   @elseif($registration->payment_type==6)
                                   Credit / Debit Card
                                   @endif
                                   <br>
                                   {{$registration->payment_details}}


                                    
                                </td>
                                 <td>
                                    {{ $registration->payment_status }}
                                  
                                 </td>
                                <td>
                                    <select class="form-control payment_status" name="payment_status" data-id="{{ $registration->id??"" }}" style="width:100px">
                                   <option value="Pending" @if($registration->payment_status=="Pending") selected @endif>Pending</option>
                                    <option value="Inprocess" @if($registration->payment_status=="Inprocess") selected @endif>Inprocess</option>
                                     <option value="Paid" @if($registration->payment_status=="Paid") selected @endif>Paid</option>
                                </select> 

                                </td>

                            </tr>

                            @endforeach
                          

                        </tbody>



                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>

@section('javascript')
<script>
 $(document).ready(function() {
    $('.payment_status').change(function () {
        
         data = {
                 "_token": $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                status: $(this).val()
            }
       ajaxCall('{{ url('admin/update-cruisepayment-status') }}/'+$(this).data('id'), 'put', data , afterStatusUpdate)
    })

    function afterStatusUpdate(data){
        if(data==1){
                swal({
                        title: "success!",
                        text: "Stauts updated!",
                        icon: "success",
                    });
        }else{
             swal({
                    title: "Oops...!",
                    text: "Something went wrong!!",
                    icon: "error",
                });
        }
    }

  })
</script>
@endsection

@if(!Auth::user())
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" style="opacity:initial;background-color: grey;padding-top: 150px;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Authentication</h4>
        </div>
        <div class="modal-body">
          <input type="password" name="password" id="password" placeholder="Enter Password" class="form-control">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="password_check">Submit</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var cookieValue = getCookie('Exbhitlogin');

        if(cookieValue != 'Exbhit'){
        $('#myModal').modal('show');
        $('#password_check').click(function(){
            var pass=$('#password').val();
            if(pass=="Exbhit@2022"){
                setCookie('Exbhitlogin','Exbhit','1'); 
                $('#myModal').modal('hide');
                window.location.href=window.location.href;
                }
            });
        }
    });
     function setCookie(key, value, expiry) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    }

    function getCookie(key) {
        var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
        return keyValue ? keyValue[2] : null;
    }

    function eraseCookie(key) {
        var keyValue = getCookie(key);
        setCookie(key, keyValue, '-1');
    }
</script>
@endif




@endsection

