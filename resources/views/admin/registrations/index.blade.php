@extends('layouts.admin.base')
@section('content')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">

                <a href="{{url('convention-registration-export')}}" title="" class="add-new-btn btn" data-original-title="Export">Export</a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->
<form method="post" enctype="multipart/form-data" id="donorImport" action="{{ url('admin/donorImport') }}">
    {{ csrf_field() }}
    <div class="form-group">
     <table class="table">
      <tr>
       <td width="40%" align="right"><label>Select File for Import</label></td>
       <td width="30">
        <input type="file" name="select_file" />
       </td>
       <td width="30%" align="left">
        <input type="submit" name="upload" class="btn btn-primary" value="Import">
       </td>
      </tr>
      <tr>
       <td width="40%" align="right"></td>
       <td width="30"><span class="text-muted">.xls, .xslx</span></td>
       <td width="30%" align="left"></td>
      </tr>
     </table>
    </div>
   </form>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>Sl NO.</th>
                                <th>Member ID</th>
                                <th>Registration ID</th>
                                <th>Member Details</th>
                                <th>Children Details</th>
                                <th> Partisipation in Yadadri Narasimha Swamy Kalyanam.</th>
                                <th>Sponsorship Type</th>
                                <th>Sponsorship Category</th>
                                <th>Amount status</th>
                                <th>Payment Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            
                            

                            @if(Auth::user() || Cookie::get('conventionCookie'))
                            @forelse ($users as $user)


                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td> {{ $user->member_id }}</td>
                                <td> {{ $user->registration_id }}</td>
                                <td>
                                    <div class="py-1">
                                        <span>Name:</span><span> {{ $user->first_name }} {{ $user->last_name }}</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Email:</span><span>{{ $user->email }}</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Mobile:</span><span>{{ $user->phone_code }} {{ $user->mobile }}</span>
                                    </div>
                                </td>
                                <td>
                                <b>Age Below 6</b>: {{ $user->children_count['below_6']?? "0" }}
                                    <br>
                                    <b>Age 7 to 15 </b>: {{ $user->children_count['age_7_to_15']?? "0" }}
                                    <br>
                                    <b>Age 16 to 23 </b>: {{ $user->children_count['age_16_to_23']?? "0" }}
                                    
                                </td>
                                <td>
                                    @if ($user->participate_in_YNSK==0)
                                    Not willing 
                                    @else
                                    Willing 
                                    @endif
                                </td>
                                <td>@if(!empty($user->sponsorship_category_id)){{  "Donor" }}
                                    @endif
                                    <br>
                                    @if(!empty($user->registration_amount)){{  "Family / Individual" }}
                                    @endif

                                </td>


                                <td> {{ $user->categorydetails->donortype->name ?? ""}} {{ isset($user->categorydetails->start_amount) ? ( '($'. $user->categorydetails->start_amount . '- $'. $user->categorydetails->end_amount . ")")  : ""  }}
                                    <br>
                                    @if(!empty($user->sponsorship_category_id))
                                    Donation Amount : $ {{$user->donor_amount }}
                                    @endif
                                
                                    <br>


                                        @if(is_array($user->individual_registration))
                                        @foreach($user->individual_registration as $key => $value)
                                        @if($value > 0)
                                        <?php $Individual = \App\Models\Admin\SponsorCategory::where('status', 1)->where('id',$key)->first();

                                        if(isset($Individual->benefits)){
                                        
                                        echo $Individual->benefits[0]." - ".$value;
                                        echo "</br>";
                                        }

                                         ?>
                                         @endif

                                        @endforeach
                                       Registration Amount : $ {{$user->registration_amount }}

                                       @endif

                                 </td>
                                <td>
                                    <b>Total Amount :</b> ${{$user->total_amount??"0"}}<br>
                                    <b>Discount Amount :</b> ${{$user->discount_amount??0 }}<br>
                                    
                                    <b>Discount Code :</b> {{$user->discount_code??"" }}<br>
                                    <b>Paid Amount :</b> ${{$user->amount_paid??"0"}}<br>
                                    <b>Due Amount :</b> ${{$user->total_amount-$user->amount_paid-$user->discount_amount??"0"}}<br>
                                </td>
                                <td>{{ ucfirst($user->payment_status) }}</td>
                                <td>
                                   <!--  <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a> -->
                                

                                        @if($user->sponsorship_category_id!=0)
                                        <a href="{{ url('admin/assigne-features',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1">Assign Features</a>
                                        @endif


                                @if(!empty($user->sponsorship_category_id) || !empty($user->registration_amount))

                                        <a href="{{ url('admin/add-note',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1">Add Note</a>
                                        <a href="{{ url('admin/payment-history',$user->id) }}" class="btn btn-sm btn-success my-1 mx-1">Payment History</a>
                                @endif

                                @if($user->is_imported_record == 1)
                                        <a href="{{ url('admin/importMemberMail') }}?email={{$user->email}}" class="btn btn-sm btn-success my-1 mx-1">Send Mail</a>
                                        @endif
                                        @if($user->mail_sent == 1)
                                        {{"Mail sent Successfully"}}
                                         @endif
                                         <div>
                                <input type="checkbox" name="ticket_issue" class="ticket_issue" data-id="{{$user->id}}" @if($user->ticket_status==1) checked @endif > Ticket Issued
                            </div>
                                <div>
                                    <textarea class="notes form-control" data-id="{{$user->id}}" placeholder="Notes / Comments">{{$user->notes}}</textarea>
                                </div>

                                </td>
                            </tr>
                            @empty

                            @endforelse
                            @endif
                           


                            <!--  <tr>
                                <td>2</td>
                                <td>23456</td>
                                <td>
                                    <div class="py-1">
                                        <span>Name:</span><span> Dinesh</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Email:</span><span> dinesh@gmail.com</span>
                                    </div>
                                    <div class="py-1">
                                        <span>Mobile:</span><span> 9230447709</span>
                                    </div>
                                </td>
                                <td>Donor</td>
                                <td>Diamond ($5000)</td>
                                <td>$5000</td>
                                <td>Full amount paid</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <a href="assign_features.php" class="btn btn-sm btn-success my-1 mx-1">Assign Features</a>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@if(!Auth::user())
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" style="opacity:initial;background-color: grey;padding-top: 150px;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Authentication</h4>
        </div>
        <div class="modal-body">
            <input type="email" name="private_email" id="private_email" placeholder="Enter Email" class="form-control">
            <br>
          <input type="password" name="password" id="password" placeholder="Enter Password" class="form-control">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="password_check">Submit</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var cookieValue = getCookie('conventionCookie');

        if(cookieValue != 1){
        $('#myModal').modal('show');
        $('#password_check').click(function(){
            var myarray =["madhu@gmail.com","madhukar@gmail.com","a@nriva.org"];
            var email = $('#private_email').val();
            if(jQuery.inArray(email, myarray) !== -1){
                var pass=$('#password').val();
                if(pass=="Convention@2022"){
                    setCookie('conventionCookie','1','1'); 
                    setCookie('ConloginUserEmail',email,'1'); 
                    $('#myModal').modal('hide');
                    window.location.href=window.location.href;
                    }else{
                        alert('Password id Invalid');
                    }
                }else{
                    alert('Email id Invalid');
                }
            });
            
        }
    });
    
</script>
@endif
<script type="text/javascript">
 function setCookie(key, value, expiry) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    }

    function getCookie(key) {
        var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
        return keyValue ? keyValue[2] : null;
    }

    function eraseCookie(key) {
        var keyValue = getCookie(key);
        setCookie(key, keyValue, '-1');
    }
    $(document).ready(function(){
        $('.notes').change(function(){
            var user_id = $(this).data('id');
            var notes = $(this).val();
                $.ajax({
                type: 'GET',
                url: "{{url('admin/update_notes')}}?user_id="+user_id+"&notes="+notes,
                success:function(data){
                  
                 
                }
            });
        });

        $('.ticket_issue').change(function(){
            var user_id = $(this).data('id');
            if($(this).is(':checked')){
                var status =1;
            }else{
                var status =0;
            }
            $.ajax({
            type: 'GET',
            url: "{{url('admin/update_tickets')}}?user_id="+user_id+"&status="+status,
            success:function(data){
              
            }
        });

        });
    });
</script>

@endsection
