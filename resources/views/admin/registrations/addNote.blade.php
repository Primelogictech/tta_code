@extends('layouts.admin.base')
@section('content')


<style type="text/css">
    input.form-control {
        height: 30px;
        width: 300px;
    }
</style>

<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Note</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                @if(Cookie::get('conventionCookie'))
                 <a href="{{url('private_convention_members')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
                @else
                <a href="{{url('admin\registrations')}}" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('registration.addnote')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <div class="form-group row">
                        <label class="col-form-label col-md-4 my-auto">Note<span class="mandatory">*</span></label>
                        <div class="col-md-8 my-auto">
                            <textarea name="registration_note">{{ $user->registration_note }}</textarea>
                        </div>
                    </div>
                    <div class="text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@section('javascript')
<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace('registration_note');
</script>
@endsection


@endsection
