@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Exhibit Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">

        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <tr>
                            <th>Sl NO.</th>
                            <th>Full Name</th>
                            <th>Comapany Name</th>
                            <th>Category</th>
                            <th>Tax ID</th>
                            <th>Mailing Address</th>
                            <th>Phone No</th>
                            <th>Email Id</th>
                            <th>Fax</th>
                            <th>Name of your Website</th>
                            <th>Booth Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($registrations as $registration)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $registration->full_name }}</td>
                                <td>{{ $registration->company_name }}</td>
                                <td>{{ $registration->category }}</td>
                                <td>{{ $registration->tax_ID }}</td>
                                <td>{{ $registration->mailing_address }}</td>
                                <td>{{ $registration->phone_no }}</td>
                                <td>{{ $registration->email }}</td>
                                <td>{{ $registration->fax }}</td>
                                <td>{{ $registration->website_url }}</td>
                                <td>{{ $registration->booth_type }}</td>
                                <td>
                                  <!--   <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    <a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
                                    <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
                                    <a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a> -->
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
