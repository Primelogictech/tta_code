@extends('layouts.admin.base')

@section('content')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<div class="page-header">

    <div class="row">

        <div class="col-9 col-sm-6 my-auto">

            <h5 class="page-title mb-0">Exhibit Registrations</h5>

        </div>

        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">

                <a href="{{url('exhibits-registration-export')}}" title="" class="add-new-btn btn" data-original-title="Export">Export</a>
            </div>
        </div>

    </div>

</div>



<!-- /Page Header -->



<div class="row">

    <div class="col-md-12">

        <div class="card">

            <div class="card-body">

                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <tr>

                            <th>Sl NO.</th>

                            <th>Organization name</th>
                            <th>Full Name</th>
                            <th>Phone Number</th>
                            <th>Email Id</th>
                            <th>Tax ID</th>
                            <th>Payment Details</th>
                            <th width="200px">Payment Status</th>

                            <th>Action</th>

                        </tr>

                        </thead>

                        <tbody>
                             @if(Auth::user() || Cookie::get('conventionCookie'))
                        @foreach ($registrations as $registration)

                            <tr>

                                <td>{{ $loop->iteration }}</td>

                                <td>{{ $registration->company_name }}</td>

                                <td>{{ $registration->firstname }} {{ $registration->lastname }}</td>
                                <td>{{ $registration->phone_number }}</td>
                                <td>{{ $registration->email }}</td>
                                <td>{{ $registration->package_id }}</td>
                                <td>
                                    <b>Payment Type:</b> {{ $registration->paymentDetails->paymentmethord->name??"" }}<br>
                                    <b>Transaction Id:</b> {{ $registration->paymentDetails->unique_id_for_payment??"" }}<br>
                                    <b>Payment Date:</b> {{ $registration->paymentDetails->paymentmethord->updated_at??"" }}<br>
                                @if(isset($registration->paymentDetails->paymentmethord->name))
                                     @if($registration->paymentDetails->paymentmethord->name=="Other")
                                    <br>
                                        <b>On Behalf Of : </b>{{$registration->paymentDetails->more_info['Payment_made_through']??""}}
                                    <br>
                                        <b>Company Name : </b>{{$registration->paymentDetails->more_info['company_name']??""}}
                                    @endif


                                     @if($registration->paymentDetails->paymentmethord->name=="Check")
                                    <br>
                                        <b>Handed over to : </b>{{$registration->paymentDetails->more_info['handed_over_to']??""}}
                                    @endif

                                @endif

                                    
                                </td>
                                 <td>
                                  <select class="form-control payment_status" name="payment_status" data-id="{{ $registration->paymentDetails->id??"" }}">
                                    @foreach(paymentStatus() as $payment_status_name)
                                    <option value="{{$payment_status_name}}" 
                                    {{ ucfirst($registration->paymentDetails->payment_status??"")==$payment_status_name? "selected" :"" }} >{{$payment_status_name}}</option>
                                    @endforeach
                                </select>
                                 </td>
                                <td>

                                    <a href="{{url('selected-packages/'.$registration->package_id)}}" class="btn btn-sm btn-success text-white my-1 mx-1">Package Details</a>

                                    <!--<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>

                                    <a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>

                                    <a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a> -->

                                </td>

                            </tr>

                            @endforeach
                            @endif

                        </tbody>



                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>

@section('javascript')
<script>
 $(document).ready(function() {
    $('.payment_status').change(function () {
        
         data = {
                 "_token": $('meta[name=csrf-token]').attr('content'),
                id: $(this).data('id'),
                status: $(this).val()
            }
       ajaxCall('{{ url('admin/update-payment-status') }}/'+$(this).data('id'), 'put', data , afterStatusUpdate)
    })

    function afterStatusUpdate(data){
        if(data==1){
                swal({
                        title: "success!",
                        text: "Stauts updated!",
                        icon: "success",
                    });
        }else{
             swal({
                    title: "Oops...!",
                    text: "Something went wrong!!",
                    icon: "error",
                });
        }
    }

  })
</script>
@endsection

@if(!Auth::user())
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" style="opacity:initial;background-color: grey;padding-top: 150px;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Authentication</h4>
        </div>
        <div class="modal-body">
             <input type="email" name="private_email" id="private_email" placeholder="Enter Email" class="form-control">
            <br>
          <input type="password" name="password" id="password" placeholder="Enter Password" class="form-control">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="password_check">Submit</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var cookieValue = getCookie('Exbhitlogin');

        if(cookieValue != 'Exbhit'){
        $('#myModal').modal('show');
        $('#password_check').click(function(){
            var myarray =["madhu@gmail.com","madhukar@gmail.com","a@nriva.org"];
            var email = $('#private_email').val();
            if(jQuery.inArray(email, myarray) !== -1){
            var pass=$('#password').val();

            if(pass=="Exbhit@2022"){
                setCookie('Exbhitlogin','Exbhit','1'); 
                setCookie('ExbhitloginUserEmail',email,'1'); 
                $('#myModal').modal('hide');
                window.location.href=window.location.href;
                }else{
                        alert('Password id Invalid');
                    }
                }else{
                    alert('Email id Invalid');
                }
            });
        }
    });
     function setCookie(key, value, expiry) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    }

    function getCookie(key) {
        var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
        return keyValue ? keyValue[2] : null;
    }

    function eraseCookie(key) {
        var keyValue = getCookie(key);
        setCookie(key, keyValue, '-1');
    }
</script>
@endif




@endsection

