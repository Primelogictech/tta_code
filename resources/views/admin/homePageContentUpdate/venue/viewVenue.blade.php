@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Venue Date & Time</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="{{route('venue.create')}}" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>Sl NO.</th>
                                <th>Start Date </th>
                                <th>End Date </th>
                                <th>Place</th>
                                <th style="width: 300px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    @if(isset($venue->Event_date_time))
                                    {{ \Carbon\Carbon::parse($venue->Event_date_time)->isoFormat('Do MMM YYYY') ?? " " }}
                                    @endif
                                    <!-- {{$venue->Event_date_time ?? " " }} -->
                                </td>
                                <td>
                                    @if(isset($venue->end_date))
                                    {{ \Carbon\Carbon::parse($venue->end_date)->isoFormat('Do MMM YYYY') ?? " " }}
                                    @endif
                                </td>
                                <td>{{$venue->location ?? "" }}</td>
                                <td>
                                    @if($venue!= null)
                                    <a href="{{route('venue.edit',$venue->id ?? '' )}}" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
                                    @endif
                                    <!-- <a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a> -->
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
