@extends('layouts.admin.base')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="page-title">Welcome Admin!</h3>
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Dashboard</li>
            </ul>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-xl-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="dash-widget-header">
                    <span>
                        <img src="admin_asserts/images/users-icon.png" class="img-fluid users-icon" alt="" />
                    </span>
                    <div class="dash-count">
                        <h3>{{ $count['reg_users'] }}</h3>
                    </div>
                </div>
                <div class="dash-widget-info">
                    <h6 class="text-muted">Convention registrations count</h6>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-primary w-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="dash-widget-header">
                    <span>
                        <img src="admin_asserts/images/sellers-icon.png" class="img-fluid sellers-icon" alt="" />
                    </span>
                    <div class="dash-count">
                        <h3>{{ $count['exbhit_count'] }}</h3>
                    </div>
                </div>
                <div class="dash-widget-info">
                    <h6 class="text-muted">Exbithit registration count</h6>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-success w-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="dash-widget-header">
                    <span>
                        <img src="admin_asserts/images/property-icon.png" class="img-fluid properties-icon" alt="" />
                    </span>
                    <div class="dash-count">
                        <h3>{{ $count['donotion_amount'] }}</h3>
                    </div>
                </div>
                <div class="dash-widget-info">
                    <h6 class="text-muted">Total amount received</h6>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-danger w-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="dash-widget-header">
                    <span>
                        <img src="admin_asserts/images/service-seller-icon.png" class="img-fluid service-seller-icon" alt="" />
                    </span>
                    <div class="dash-count">
                        <h3>$ {{ $count['total_amount']-$count['donotion_amount'] }}</h3>
                    </div>
                </div>
                <div class="dash-widget-info">
                    <h6 class="text-muted">Total Amoun pending</h6>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-warning w-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
