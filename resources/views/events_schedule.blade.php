@extends('layouts.user.base')
@section('content')

<style type="text/css">
    .fade:not(.show) {
        opacity: 1;
    }
	.tabs{
		padding-left: 1.5rem !important;		
		background-image: linear-gradient(178deg, #ea6900, #35bbdc80);
		color: #000000;
		border-radius: 2px;
	}
	#container{
		box-shadow: 1px 1px 1px 1px grey;
		background-image: linear-gradient(178deg, #ea0000f7, #783939de);
		text-align: left;
		border-radius: 2px;
		overflow: hidden;
		margin: 2em auto;
		height: 200px;
		width: 425px;
	}
	.product-details {
		position: relative;
		text-align: left;
		overflow: hidden;
		padding: 30px;
		height: 100%;
		float: left;
	}
	
	.seperator {
	  background: #ccf;
	  padding: 1px;
	  list-style: none;
	}
	
	#container .product-details h1{
		font-family: arial;
		display: inline-block;
		position: relative;
		text-align: left;
		font-size: 23px;
		color: #ffffff;
		margin: 0;
		
	}
	
	#container .product-details > p {
		
		text-align: left;
		font-size: 15px;
		color: #dfcea7;
	}
</style>

<section class="container-fluid mt-3 mb300">
    <div class="container shadow-small">
        <div class="row">
            <div class="col-12 px-0">
                <div class="headingss-bg py-2 px-3">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-6 my-auto">
                            <div>
                                <h5 class="mb-0">Events Schedule</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 border-top mt2 px-sm-20 pt-2 pb-3 px-md-3 p20">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <ul class="nav nav-tabs sc-tabs fs16 horizontal-scroll" role="tablist">
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 tabs border active" href="#twenty-seventh-may" role="tab" data-toggle="tab">27&nbsp;May,&nbsp;2022</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 tabs border " href="#twenty-eighth-may" role="tab" data-toggle="tab">28&nbsp;May,&nbsp;2022</a>
                                </li>
                                <li class="nav-item d-inline-flex">
                                    <a class="nav-link px-4 tabs border" href="#twenty-ninth-may" role="tab" data-toggle="tab">29&nbsp;May,&nbsp;2022</a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
							<p>** Event timings are subject to change.</p>
                            <div role="tabpanel" class="tab-pane fade in active es_active_tab" id="twenty-seventh-may">
								
                                <div class="col-12 col-md-10 col-lg-12">
									
									<div class="row">
										<div id="container">	
											<div class="product-details">
												<h1>TTA Youth - Dinner & Music in NYC Cruise</h1>
												<p class="information">**Cruise Registration Required</p>
												<p class="information">Time:  4:00 PM Youth Meet for Cruise at Sheraton</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>Grand Banquet Night</h1>
												<p class="information">Cultural Programs, Souvenir Release, Donors Recognition</p>
												<p class="information">Time:  @ 5:30 PM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>TTA Awards for Excellence</h1>
												<p class="information">Presentation of TTA Awards</p>
												<p class="information">Time:  @ 7:00 PM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>Live Musical Concert</h1>
												<p class="information">Legendary Music Director Koti & Usha - Live concert with Mehar Band</p>
												<p class="information">Time:  @ 9:00 PM</p>
											</div>
										</div>										
									</div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="twenty-eighth-may">
								<div class="col-12 col-md-10 col-lg-12">
									<div class="row justify-content-center">
										<div id="container">	
											<div class="product-details">
												<h1>Morning Session</h1>
												<p class="information">Procession from Outside to Stage, Welcome Guests and Inaugural Speeches</p>
												<p class="information">Time: 9:00 AM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>TTA Matrimonial</h1>
												<p class="information">Parents Meet & Parichaya Vedika</p>
												<p class="information">Time: 10:00 AM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>Cultural Programs</h1>
												<p class="information">Telangana and Telugu foot tapping performances by Local and International Celebrity performers</p>
												<p class="information">Time: 11:00 AM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>All Party Political Debate</h1>
												<p class="information">With Political Leaders across the parties from India and USA</p>
												<p class="information">Time: 1:30 PM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>TTA Star Semi-Finals</h1>
												<p class="information">TTA Star Competition Semi-Finals</p>
												<p class="information">Time: 1:30 PM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>Business Forum</h1>
												<p class="information">Topic: How to be successful in business to become as successful businessman</p>
												<p class="information">Time: 1:30 PM</p>
											</div>
										</div>	
										<div id="container">	
											<div class="product-details">
												<h1>Women's Forum</h1>
												<p class="information">Mehandi, Muggula Poti, Dharma Sandehalu & Women's Bash</p>
												<p class="information">Time: 1:30 PM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>CME Sessions</h1>
												<p class="information"> </p>
												<p class="information">Time: 1:30 PM to 2:00 PM @ Edison Sheraton</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>CPR for Citizens</h1>
												<p class="information"> </p>
												<p class="information">Time: 2:00 PM to 6:00 PM @ Edison Sheraton</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>Immigration Forum</h1>
												<p class="information">Discussion on Recent Immigration Updates, Effect of DUI, Family Law and more…</p>
												<p class="information">Time: 5:00 PM</p>
											</div>
										</div>									
										<div id="container">	
											<div class="product-details">
												<h1>Telangana Folk Music</h1>
												<p class="information">Foot tapping Telangana Folk by Rasamai Balakrishna and Team</p>
												<p class="information">Time: 6:00 PM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>TTA Youth Events</h1>
												<p class="information">Youth Fashion Show & Dance Medley by TTA Youth</p>
												<p class="information">Time: 7:00 PM</p>
											</div>
										</div>										
										<div id="container">	
											<div class="product-details">
												<h1>Jabardasth Comedy Show</h1>
												<p class="information">Telugu Comedy Show By Adhire Abhi and Rocket Raghava</p>
												<p class="information">Time: 7:30 PM</p>
											</div>
										</div>										
										<div id="container">	
											<div class="product-details">
												<h1>Sunitha Live In Concert with Band Capricio</h1>
												<p class="information">Musical Show by Singer Sunitha & Capriccio Band</p>
												<p class="information">Time: 9:00 PM</p>
											</div>
										</div>
									</div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="twenty-ninth-may">
								<div class="col-12 col-md-10 col-lg-12">
									<div class="row justify-content-center">
										<div id="container">	
											<div class="product-details">
												<h1>Yadadri Lakshmi Narasimha Swamy Kalyanam</h1>
												<p class="information">Archakulu and Veda Panditulu from Yadadri Devasthanam with deities from Yadagirigutta.</p>
												<p class="information">Time: 8:00 AM to 11:00 AM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>TTA Matrimonial</h1>
												<p class="information">Singles Meet</p>
												<p class="information">Time: 10:00 AM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>TTA Star Finals</h1>
												<p class="information">Finals for TTA Star Competition</p>
												<p class="information">Time: 11:00 AM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>Cultural Programs</h1>
												<p class="information">Telangana and Telugu foot tapping performances by Local and International Celebrity performers</p>
												<p class="information">Time: 11:00 AM Onwards</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>Political Forum</h1>
												<p class="information">Meet and Greets</p>
												<p class="information">Time: 11:00 AM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>Women's Forum</h1>
												<p class="information">Women's Bash & Social Media Stars</p>
												<p class="information">Time: 4:00 PM</p>
											</div>
										</div>										
										<div id="container">	
											<div class="product-details">
												<h1>TTA Youth Networking Events</h1>
												<p class="information">Youth Networking and Career Guidance sessions</p>
												<p class="information">Time: 5:00 PM</p>
											</div>
										</div>
										<div id="container">	
											<div class="product-details">
												<h1>Rockstar DSP & Team Grand Musical Concert</h1>
												<p class="information">Grand Finale show by Rockstar Devi Sri Prasad and Musical Troupe (Sagar, Geetha Madhuri, Rita, Ranjit..)</p>
												<p class="information">Time: 9:00 PM</p>
											</div>
										</div>																			
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('javascript')

@endsection

@endsection
