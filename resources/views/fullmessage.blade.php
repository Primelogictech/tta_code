@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small px-sm-30 py-4 p-md-4 p40">
                <div class="text-center float-sm-none float-md-right px-2">
                    <img src="{{asset(config('conventions.message_display').$message->image_url)}}" class="tta-member-image" alt="Nagender_Aytha" />
                    <div class="py-2">
                        <div class="text-danger fs16 text-center">{{ $message->name }}</div>
                        <div class="text-danger fs12 text-center">- {{ $message->designation->name }}</div>
                    </div>
                </div>
                @if($message!=null)

                    {!! $message->message !!}

                @else
                <h4 class="my-5 text-center text-skyblue">
                    Coming Soon ...
                </h4>
                @endif
            </div>
        </div>
    </div>
</section>


@section('javascript')

@endsection


@endsection
