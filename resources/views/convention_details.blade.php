@extends('layouts.user.base')
@section('content')
<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 bg-white shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/convention.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div class="row">
                            <div class="col-12 col-sm-5 col-md-4 col-lg-6 my-1 my-md-auto">
                                <h5 class="d-inline-block mb-0">Registration Package:</h5>
                            </div>
                            <div class="col-12 col-sm-7 col-md-8 col-lg-6 my-1 my-md-auto">
                                <div class="text-center text-sm-right">
                                    <a href="{{url('bookticket')}}" class="btn btn-lg btn-danger text-uppercase fs-xs-15 fs-xss-11 px-3 px-md-4 fs-sm-15">Click Here For Registration </a>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card-body px-0">
                                    <div class="table-responsive">
                                        <table class="datatable table-striped table-bordered table table-hover table-center mb-0">
                                            <tbody class="fs15">
                                                <tr>
                                                    <th class="text-center" width="9%">S.No</th>
                                                    <th class="text-left" width="52%">Registration Package</th>
                                                    <th class="text-center" width="20%">Amount</th>
                                                </tr>
                                                <tr>
                                                    <td align="center">1</td>
                                                    <td>One Person (Age 7 Years and Above) (May 27 & May 29,2022)</td>
                                                    <td align="center">$100 (2 Days)</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">2</td>
                                                    <td>One Person ( May 28 or May 29,2022)</td>
                                                    <td align="center">$50 (1 Day)</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">3</td>
                                                    <td>Banquette Night ( May 27,2022)</td>
                                                    <td align="center">$75</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">4</td>
                                                    <td>CME</td>
                                                    <td align="center">$125</td>
                                                </tr>
                                                <!-- <tr>
                                                    <td align="center">5</td>
                                                    <td>Single (Admission Only)</td>
                                                    <td align="center">$ 75.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">6</td>
                                                    <td>Business Conference (Per Person)</td>
                                                    <td align="center">$ 150.00</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">7</td>
                                                    <td>Womens Conference (Per Person)</td>
                                                    <td align="center">$ 50.00</td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 my-2">
                                <h5 class="pb-2 d-inline-block">Donation Package:</h5>
                                <div class="card-body px-0">
                                    <div class="table-responsive">
                                        <table class="datatable table table-bordered table-center mb-0">
                                            <thead>
                                                <tr>
                                                    <th style="width: 180px;">Donation Package</th>
                                                    <th style="width: 150px;">Amount</th>
                                                    <th style="min-width: 300px;">Benefits</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Chakravarti Poshakulu</td>
                                                    <td class="text-orange font-weight-bold">$100,000+</td>
                                                    <td>
                                                        <div class="col-12 col-md-12 col-lg-12">
                                                            <!-- <ul class="flower-list mb-0">
                                                                <li>Contact Us</li>
                                                            </ul> -->
                                                            <div class="fs15">
                                                                Please Email to registration@ttaconvention.org (or) Contact Registration Committee to formalize this Package
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Maharaja Poshakulu</td>
                                                    <td class="text-orange font-weight-bold">$50,000 - $99,999</td>
                                                    <td>
                                                        <div class="col-12 col-md-12 col-lg-12">
                                                            <!-- <ul class="flower-list mb-0">
                                                                <li>Contact Us</li>
                                                            </ul> -->
                                                            <div class="fs15">
                                                                Please Email to registration@ttaconvention.org (or) Contact Registration Committee to formalize this Package
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Raja Poshakulu</td>
                                                    <td class="text-orange font-weight-bold">$25,000 - $49,999</td>
                                                    <td>
                                                        <div class="col-12 col-md-12 col-lg-12">
                                                            <ul class="flower-list mb-0">
                                                                <li>16 Number of Banquet Tickets</li>
                                                                <li>16 Number of Event Tickets</li>
                                                                <li>5 Hotel Rooms</li>
                                                                <li>Color Picture in the Website</li>
                                                                <li>Color Picture in the Souvenir</li>
                                                                <li>Stage Recognition</li>
                                                                <li>CME Tickets</li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Mukhya Atithi</td>
                                                    <td class="text-orange font-weight-bold">$10,000 - $24,999</td>
                                                    <td>
                                                        <div class="col-12 col-md-12 col-lg-12">
                                                            <ul class="flower-list mb-0">
                                                                <li>8 Number of Banquet Tickets</li>
                                                                <li>10 Number of Event Tickets</li>
                                                                <li>3 Hotel Rooms</li>
                                                                <li>Color Picture in the Website</li>
                                                                <li>Color Picture in the Souvenir</li>
                                                                <li>Stage Recognition</li>
                                                                <li>CME Tickets</li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Visishta Atithi</td>
                                                    <td class="text-orange font-weight-bold">$5,000 - $9,999</td>
                                                    <td>
                                                        <div class="col-12">
                                                            <ul class="flower-list mb-0">
                                                                <li>6 Number of Banquet Tickets</li>
                                                                <li>8 Number of Event Tickets</li>
                                                                <li>2 Hotel Rooms</li>
                                                                <li>Color Picture in the Website</li>
                                                                <li>Color Picture in the Souvenir</li>
                                                                <li>CME Tickets</li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Pratyeka Atithi</td>
                                                    <td class="text-orange font-weight-bold">$2,500 - $4,999</td>
                                                    <td>
                                                        <div class="col-12">
                                                            <ul class="flower-list mb-0">
                                                                <li>4 Number of Banquet Tickets</li>
                                                                <li>6 Number of Event Tickets</li>
                                                                <li>1 Hotel Room</li>
                                                                <li>Color Picture in the Website</li>
                                                                <li>CME Tickets</li>
                                                            </ul>   
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Gourava Atithi</td>
                                                    <td class="text-orange font-weight-bold">$1,000 - $2,499</td>
                                                    <td>
                                                        <div class="col-12">
                                                            <ul class="flower-list mb-0">
                                                                <li>2 Number of Banquet Tickets</li>
                                                                <li>4 Number of Event Tickets</li>
                                                                <li>Color Picture in the Website</li>
                                                                <li>CME Tickets</li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Others</td>
                                                    <td class="text-orange font-weight-bold"><$1,000</td>
                                                        <td>
                                                            <!-- <div class="col-12">
                                                                <ul class="flower-list">
                                                                    <li>Admission Tickets (4)</li>
                                                                    <li>Banquet Tickets (2)</li>
                                                                    <li>Kalyanam Tickets (1)</li>
                                                                    <li>1 Hotel Rooms (2 nights)</li>
                                                                    <li>Listing in Souvenir</li>
                                                                </ul>
                                                            </div> -->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 fs15 pt-2">
                                    <div class="">
                                        <span class="text-uppercase font-weight-bold">MAKE CHECKS PAYABLE TO :</span>
                                        <span>Telangana American Telugu Association</span>
                                    </div>
                                    <div>Cheque Mailing Address:-</div>
                                    <div>5 Independence Way,</div>
                                    <div>Suite #300,</div>
                                    <div>Princeton, New Jersey 08540</div>
                                    <div></div>
                                    <div>
                                        <span><b>For online registration</b></span>
                                        <span>and Convention programs & updates, please visit</span>
                                        <a href="https://ttaconvention.org/" target="_blank" class="text-orange">www.ttaconvention.org</a>
                                    </div>
                                    <div>
                                        <span><b>For Registration Information, email,</b></span>
                                        <a href="" class="text-orange">registration@ttaconvention.org</a>
                                        <span>or reach</span>
                                        <a href="#" class="text-orange">Registration Committee.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endsection
