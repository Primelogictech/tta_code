
         <div class="row mt-2">
             <div class="col-12">
                 <h5 class="text-violet py-2">Payment Types</h5>
             </div>
             <div class="col-12">
                 <div class="row">
                        @foreach ($RegistrationContent->patment_types as $patment_type)
                    <div class="col-12 col-sm-6 col-md-4 col-lg-20">
                        <span class="position-relative" id="{{$RegistrationContent->paymenttype( $patment_type)->name}}_payment_div">

                         <input type="radio" id="payment_{{$RegistrationContent->paymenttype( $patment_type)->name }}" class="p-radio-btn payment-button" data-name="{{$RegistrationContent->paymenttype( $patment_type)->name }}" value="{{$RegistrationContent->paymenttype( $patment_type)->id }}" name="payment_type" /><span class="fs16 pl-4 pl-md-4">{{ucfirst($RegistrationContent->paymenttype( $patment_type)->name )}}</span>
                         </span>
                     </div>


                     {{-- <span class="position-relative pr-3"><input type="radio" class="p-radio-btn" name="payment_type" /><span class="fs16 pl-4 pl-md-4">Cheque/Cash</span></span> --}}
                     @endforeach
                     <div class="payment-button error"></div>
                 </div>
             </div>
         </div>

         <hr class="dashed-hr">
         <div class="row">
             <!-- paypal payment -->

             <div class="col-12 payments_field_div" id="paypal" style="display: none;">
                 <h5 class="text-violet mb-3 Paypal_text">Payment by using Paypal</h5>
                 <div class="paypal-note">
                 </div>
                 <div class="form-row">
                 </div>
             </div>

             <!-- paypal payment end -->

             <div class="col-12 payments_field_div" id="check_payment" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using check</h5>
                 <div class="check-note">
                 </div>
                 <div class="form-row">
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Cheque Number: </label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="cheque_number" id="" class="form-control cheque_form_field payment_fields" placeholder="Cheque Number">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Cheque Date </label><span class="text-red">*</span>
                         <div>
                             <input type="date" name="cheque_date" id="" class="form-control cheque_form_field payment_fields" placeholder="cheque Date">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Handed over to:</label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="more_info" id="" class="form-control cheque_form_field payment_fields" placeholder="Handed over to">
                         </div>
                     </div>
                 </div>
             </div>

             <!-- zelle -->
             <div class="col-12 payments_field_div" id="zelle" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using Zelle</h5>
                 <div class="zelle-note">
                 </div>
                 <div class="form-row">
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Zelle Reference Number</label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="Zelle_Reference_Number" id="" class="form-control zelle_form_field payment_fields" placeholder="Zelle Reference Number">
                         </div>
                     </div>
                 </div>
             </div>
             <!-- zelle end -->
             <!-- othre Payments -->
             <div class="col-12 payments_field_div" id="other" style="display: none;">
                 <h5 class="text-violet mb-3">Payment by using other payments</h5>
                 <div class="other-note">
                 </div>
                 <div class="form-row">
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Payment Made through </label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="Payment_made_through" id="" class="form-control other_payment_form_field payment_fields" placeholder="Payment made through">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Company Name</label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="company_name" id="" class="form-control other_payment_form_field payment_fields" placeholder="Company Name">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Transaction Date </label><span class="text-red">*</span>
                         <div>
                             <input type="date" name="transaction_date" id="" class="form-control other_payment_form_field payment_fields" placeholder="Transaction Date">
                         </div>
                     </div>
                     <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                         <label>Transaction Id </label><span class="text-red">*</span>
                         <div>
                             <input type="text" name="transaction_id" id="" class="form-control other_payment_form_field payment_fields" placeholder="Transaction id">
                         </div>
                     </div>
                 </div>
             </div>
             </div>
             <!-- othre Payments end -->

                 <!-- First data Payments -->
                        <div class="col-12 payments_field_div" id="first_data" style="display: none;">
                           <div class="first-data-note">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                    <label>Card type :</label><span class="text-red">*</span>
                                    <div>
                                        <select name="type"  class="form-control payment_fields firstdata_form_field payment_fields">
                                            <option>Select card type</option>
                                            <option value="visa">Visa</option>
                                            <option value="mastercard">MasterCard </option>
                                            <option value="Discover">Discover</option>
                                            <option value="American Express">American Express</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                    <label>Card Hold Name</label><span class="text-red">*</span>
                                    <div>
                                        <input type="text" name="cardholder_name"  class="form-control firstdata_form_field payment_fields"  placeholder="Card Hold Name">
                                    </div>
                                </div>
                                <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                    <label>Card Number </label><span class="text-red">*</span>
                                    <div>
                                        <input type="text" name="card_number"  class="form-control firstdata_form_field payment_fields" placeholder="card number">
                                    </div>
                                </div>
                                <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                    <label>CVV :</label><span class="text-red">*</span>
                                    <div>
                                        <input type="number" name="cvv"  class="form-control firstdata_form_field payment_fields" placeholder="CVV">
                                    </div>
                                </div>

                                <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                    <label>Expiry Date(MMYY) :</label><span class="text-red">*</span>
                                    <div>
                                        <input type="number" name="exp_date"  class="form-control firstdata_form_field payment_fields" placeholder="Expiry Date ">
                                    </div>
                                </div>

                            </div>
                        </div>
                    <!-- first data Payments end -->

             @section('eaxtra-javascript')
             <script>
                 paymenttypes = ({!!html_entity_decode($paymenttypes) !!})
                 $(".payment-button").click(function(e) {

                    $('.payments_field_div').hide();
                    $('.payment_fields').prop("required", false);

                     if ($(this).prop("checked")) {
                         if ($(this).data('name') == "{{ config('conventions.paypal_name_db') }}") {
                             $('#paypal').show();
                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.paypal_name_db') }}") {
                                     $('.paypal-note').html(data.note)
                                 }
                             });
                         }

                         if ($(this).data('name') == "{{ config('conventions.check_name_db') }}") {
                             $('#check_payment').show();
                             $('.cheque_form_field').prop("required", true);
                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.check_name_db') }}") {
                                     $('.check-note').html(data.note)
                                 }
                             });
                         }

                         if ($(this).data('name') == "{{ config('conventions.zelle_name_db') }}") {
                            $('#zelle').show();
                            $('.zelle_form_field').prop("required", true);
                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.zelle_name_db') }}") {
                                     $('.zelle-note').html(data.note)
                                 }
                             });
                            
                         }
                         if ($(this).data('name') == "{{ config('conventions.other_name_db') }}") {
                             $('#other').show();
                             $('.other_payment_form_field').prop("required", true);
                             paymenttypes.forEach(data => {
                                 if (data.name == "{{ config('conventions.other_name_db') }}") {
                                     $('.other-note').html(data.note)
                                 }
                             });
                         }
                         //zelle

                         if ($(this).data('name') == "{{ config('conventions.first_data_name_db') }}") {
                                $('#first_data').show();
                                $('.firstdata_form_field').prop("required", true);
                                paymenttypes.forEach(data => {
                                    if (data.name == "{{ config('conventions.first_data_name_db') }}") {
                                        $('.first-data-note').html(data.note)
                                    }
                                });
                            }
                     }
                 });

             </script>
             @endsection
