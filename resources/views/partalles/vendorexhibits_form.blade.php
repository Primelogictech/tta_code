<style type="text/css">
    input[type="checkbox"],
    input[type="radio"] {
        box-sizing: border-box;
        padding: 0;
        position: relative;
        top: 2px;
    }

    .table th {
        vertical-align: middle !important;
    }

    .p-radio-btn {
        position: absolute !important;
        top: 0px !important;
    } 

    .input-checkbox {
        position: absolute;
        top: 6px;
    }

    .agree-checkbox {
        position: relative;
        top: 21px !important;
    }

    .sub-input {
        border: 0;
    }

    .subtotal-input {

        border: 0;

    }

    .sub-input-two {
        border: 0;
    }

    .error{
        color:red
    }

    input:focus{
        outline: none;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Firefox */
    input[type=number] {
      -moz-appearance: textfield;
    }

</style>



<section class="container-fluid px-0 px-lg-3 my-3 my-lg-5">
    <div class="container shadow-md-none bg-white shadow-small">
        
        <div class="row border-top mt2">
            <div class="col-12 py-0 pt-1 px-1 pb-md-5">
                

                <form method="post" id="form" action="{{url('exhibits-reservation')}}">

                    @csrf

                    <div class="row mx-1 mx-lg-5">

                        <div class="col-12 col-md-12 col-lg-12 shadow-small p-3 py-md-5 px-md-4 my-3">

                            <div class="row">
                                 @foreach ($errors->all() as $error)
                                        <div class="error">{{ $error }}</div>
                                    @endforeach

                                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                                <div class="col-12">
                                    @foreach ($errors->all() as $error)
					                    <div class="error">{{ $error }}</div>
					                @endforeach

                                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                                    <div class="form-row">

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>Exhibitor Name:</label><span class="text-red"> *</span>

                                            <div>

                                                <input type="text" name="name" id="" class="form-control" placeholder="Exhibitor Name" required />

                                            </div>

                                        </div>

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>Organization Name:</label><span class="text-red"> *</span>

                                            <div>

                                                <input type="text" name="company-name" id="" class="form-control" placeholder="Organization Name" required />

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-row">

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>First Name:</label><span class="text-red"> *</span>

                                            <div>

                                                <input type="text" name="first-name" id="first_name" class="form-control" placeholder="First Name" required />

                                            </div>

                                        </div>

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>Last Name:</label><span class="text-red"> *</span>

                                            <div>

                                                <input type="text" name="last-name" id="last_name" class="form-control" placeholder="Last Name" required />

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-row">

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>Address Line 1:</label><span class="text-red"> *</span>

                                            <div>

                                                <input type="text" name="address-1" id="address_one" class="form-control" placeholder="Address" required />

                                            </div>

                                        </div>

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>Address Line 2:</label>

                                            <div>

                                                <input type="text" name="address-2" id="address_two" class="form-control" placeholder="Address" />

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-row">

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>City:</label><span class="text-red">*</span>

                                            <div>

                                                <input type="text" name="city" id="city" class="form-control" placeholder="City" required />

                                            </div>

                                        </div>

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>State:</label><span class="text-red">*</span>

                                            <div>

                                                <select class="form-control" name="state" id="state" required>

                                                    <option value="">Select State</option>

                                                    @foreach ($states as $state)

                                                    <option value="{{ $state->state }}">{{ $state->state }}</option>

                                                    @endforeach

                                                </select>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-row">

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>Zip Code:</label><span class="text-red">*</span>

                                            <div>

                                                <input type="text" name="zipcode" id="zipcode" class="form-control" placeholder="Zip Code" required />

                                            </div>

                                        </div>

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>Country:</label><span class="text-red">*</span>

                                            <div>

                                                <select class="form-control" name="country">

                                                    <option>USA</option>

                                                </select>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-row">

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>Phone:</label><span class="text-red">*</span>

                                            <div>

                                                <input type="text" name="phone" id="" class="form-control" placeholder="Phone" required />

                                            </div>

                                        </div>

                                        <div class="form-group col-12 col-md-6 col-lg-6">

                                            <label>Email:</label><span class="text-red">*</span>

                                            <div>

                                                <input type="text" name="email" id="" class="form-control" placeholder="Email" required />

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>



                            <div class="row py-5">

                                <div class="col-12">

                                    <h5 class="text-violet text-center mb-3">Exhibits Selection</h5>

                                </div>

                                <div class="col-12">

                                    <div class="table-responsive">

                                        <!-- <table class="datatable table table-bordered table-center mb-0">

                                            <thead>

                                                <tr>

                                                    <th>Package</th>

                                                    <th>Exhibitor Type</th>

                                                    <th width="130px">No. of booths</th>

                                                    <th>Amount</th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <tr>

                                                    <td>EXE01</td>

                                                    <td>Jewelry-Gold & Diamond (10’X20’)</td>

                                                    <td>

                                                        <input type="text" class="form-control" name="" />

                                                    </td>

                                                    <td></td>

                                                </tr>

                                                <tr>

                                                    <td>EXE02</td>

                                                    <td>Real Estate & Fin. Services (10’X10’)</td>

                                                    <td>

                                                        <input type="text" class="form-control" name="" />

                                                    </td>

                                                    <td></td>

                                                </tr>

                                                <tr>

                                                    <td>EXE03</td>

                                                    <td>Semi-Precious Jewelry, Sarees & Boutique, Arts & Crafts (10’X10’)</td>

                                                    <td>

                                                        <input type="text" class="form-control" name="" />

                                                    </td>

                                                    <td></td>

                                                </tr>

                                                <tr>

                                                    <td>EXE04</td>

                                                    <td>All other Types (10’X10’)</td>

                                                    <td>

                                                        <input type="text" class="form-control" name="" />

                                                    </td>

                                                    <td></td>

                                                </tr>

                                                <tr>

                                                    <td>EX05</td>

                                                    <td>Non- Profit (10’X10’)</td>

                                                    <td>

                                                        <input type="text" class="form-control" name="" />

                                                    </td>

                                                    <td></td>

                                                </tr>

                                                <tr>

                                                    <td colspan="3" class="text-right">Total Amount: $</td>

                                                    <td></td>

                                                </tr>

                                            </tbody>

                                        </table> -->

                                        <table class=" table table-bordered table-center mb-0">

                                            <thead>

                                                <tr>

                                                    <th rowspan="2">Package Code</th>

                                                    <th rowspan="2">Exhibitor Type/ Booth Size</th>

                                                    <th class="text-center" colspan="2">Package Amount</th>

                                                    <th width="130px" rowspan="2">No. of booths</th>

                                                    <th rowspan="2">Amount</th>

                                                </tr>

                                                <tr>

                                                    <th>Before 04/30/2022</th>

                                                    <th>After 04/30/2022</th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <tr>

                                                    <td>EXE01</td>

                                                    <td>Jewelry Booths - (10’X20’)</td>

                                                    <td>$12,000</td>

                                                    <td>$14,000</td>

                                                    <td>



                                                        <input type="hidden" name="package_code_1" value="EXE01">

                                                        <input type="hidden" name="package_trype_1" value="Jewelry Booths - (10’X20’)">

                                                        <input type="text" min="1" class="form-control exhibit-qnt" data-id="1" data-amount="14000" name="exe-qnt-1" />

                                                    </td>

                                                    <td>$<input type="text" id="exe-sub-total-1" class="sub-input" readonly value="0" name="exe-qnt-1-sub"></td>

                                                </tr>

                                                <tr>

                                                    <td>EXE02</td>

                                                    <td>Jewelry Booths (10’X10’)</td>

                                                    <td>$8,000</td>

                                                    <td>$10,000</td>

                                                    <td>



                                                        <input type="hidden" name="package_code_2" value="EXE02">

                                                        <input type="hidden" name="package_trype_2" value="Jewelry Booths - (10’X10’)">

                                                        <input type="text" min="0" class="form-control exhibit-qnt" data-id="2" data-amount="10000" name="exe-qnt-2" />

                                                    </td>

                                                    <td>$<input type="text" id="exe-sub-total-2" class="sub-input" name="exe-qnt-2-sub" readonly value="0"></td>

                                                </tr>

                                                <tr>

                                                    <td>EXE03</td>

                                                    <td>Corporations / Real Estate (10’X20’)</td>

                                                    <td>$10,000</td>

                                                    <td>$12,000</td>

                                                    <td>



                                                        <input type="hidden" name="package_code_3" value="EXE03">

                                                        <input type="hidden" name="package_trype_3" value="Corporations / Real Estate (10’X20’)">

                                                        <input type="text" min="0" class="form-control exhibit-qnt" data-id="3" data-amount="12000" name="exe-qnt-3" />

                                                    </td>

                                                    <td>$<input type="text" id="exe-sub-total-3" class="sub-input" readonly value="0" name="exe-qnt-3-sub"></td>

                                                </tr>

                                                <tr>

                                                    <td>EXE04</td>

                                                    <td>Corporations / Real Estate (10’X10’)</td>

                                                    <td>$6,000</td>

                                                    <td>$7,000</td>

                                                    <td>

                                                        <input type="hidden" name="package_code_4" value="EXE04">

                                                        <input type="hidden" name="package_trype_4" value="Corporations / Real Estate (10’X10’)">

                                                        <input type="text" min="0" class="form-control exhibit-qnt" data-id="4" data-amount="7000" name="exe-qnt-4" />

                                                    </td>

                                                    <td> $<input type="text" id="exe-sub-total-4" class="sub-input" readonly value="0" name="exe-qnt-4-sub"></td>

                                                </tr>

                                                <tr>

                                                    <td>EXE05</td>

                                                    <td>Semi-Precious/Pearls/Gems ( 10’X10’)</td>

                                                    <td>$2,000</td>

                                                    <td>$3,000</td>

                                                    <td>



                                                        <input type="hidden" name="package_code_5" value="EXE05">

                                                        <input type="hidden" name="package_trype_5" value="Semi-Precious/Pearls/Gems ( 10’X10’)">

                                                        <input type="text" min="0" class="form-control exhibit-qnt"  data-id="5" data-amount="3000" name="exe-qnt-5" />

                                                    </td>

                                                    <td>$<input type="text" id="exe-sub-total-5" class="sub-input" readonly value="0" name="exe-qnt-5-sub"></td>

                                                </tr>

                                                <tr>

                                                    <td>EXE06</td>

                                                    <td>Sarees / Boutique (10’X10’)</td>

                                                    <td>$2,000</td>

                                                    <td>$3,000</td>

                                                    <td>

                                                        <input type="hidden" name="package_code_6" value="EXE06">

                                                        <input type="hidden" name="package_trype_6" value="Sarees / Boutique (10’X10’)">



                                                        <input type="text" min="0" class="form-control exhibit-qnt" data-id="6" data-amount="3000" name="exe-qnt-6" />

                                                    </td>

                                                    <td>
                                                        <div class="d-flex"><span>$</span><input type="text" id="exe-sub-total-6" class="sub-input" readonly value="0" name="exe-qnt-6-sub">
                                                        </div>
                                                    </td>

                                                </tr>

                                                <tr>

                                                    <td>EXE07</td>

                                                    <td>Miscellaneous (10’X10’)</td>

                                                    <td>$2,000</td>

                                                    <td>$3,000</td>

                                                    <td>

                                                        <input type="hidden" name="package_code_7" value="EXE07">

                                                        <input type="hidden" name="package_trype_7" value="Miscellaneous (10’X10’)">



                                                        <input type="text" min="0" class="form-control exhibit-qnt" data-id="7" data-amount="3000" name="exe-qnt-7" />

                                                    </td>

                                                    <td>$<input type="text" id="exe-sub-total-7" class="sub-input" readonly value="0" name="exe-qnt-7-sub"></td>

                                                </tr>

                                                 <tr>

                                                    <td>EXE08</td>

                                                    <td>Non-Profit Groups (10’X10’)</td>

                                                    <td>$1,000</td>

                                                    <td>$1,000</td>

                                                    <td>

                                                        <input type="hidden" name="package_code_12" value="EXE08">

                                                        <input type="hidden" name="package_trype_12" value="Miscellaneous (10’X10’)">



                                                        <input type="text" min="0" class="form-control exhibit-qnt" data-id="12" data-amount="1000" name="exe-qnt-12" />

                                                    </td>

                                                    <td>$<input type="text" id="exe-sub-total-12" class="sub-input" readonly value="0" name="exe-qnt-12-sub"></td>

                                                </tr>

                                                <tr>

                                                    <td colspan="5" class="text-right">Total Amount: $</td>

                                                    <td><input type="text" id="sub-total-one" class="subtotal-input" readonly value="0"></td>

                                                </tr>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>

                            </div>



                            <div class="row pb-4">

                                <div class="col-12">

                                    <h5 class="text-violet text-center mb-3">Digital/Web/Souvenir Selection</h5>

                                </div>

                                <div class="col-12">

                                    <div class="table-responsive">

                                        <table class="datatable table table-bordered table-center mb-0">

                                            <thead>

                                                <tr>

                                                    <th>Package</th>

                                                    <th>Description</th>

                                                    <th>Package Price</th>

                                                    <th width="100px">Qty</th>

                                                    <th width="200px" >Amount</th>

                                                </tr>

                                            </thead>

                                            <tbody>

                                                <tr>

                                                    <td>DIG01</td>

                                                    <td>Digital Ads (10 - 20 sec)</td>

                                                    <td>$2,000 - $5000</td>

                                                    <td><input type="hidden" name="package_code_8" value="DIG01">

                                                        <input type="hidden" name="package_trype_8" value="Digital Ads (10 - 20 sec)">


                                                        
                                                        <input type="text" min="0" class="form-control exhibit-two" id="digi_qnt" name="exe-qnt-8" data-amount="0" data-id="8" />

                                                    </td>

                                                    <td>
                                                       <div id="hint_msg" style="display:none" >Enter Amount</div>
                                                       <div class="d-flex">
                                                           <span style="padding-top:8px">$</span>
                                                           <input style="background-color : #ffffff; " type="number" id="exe-sub-total-8" readonly class="form-control sub-input-two exhibit-two minmax" value="0" placeholder="Enter Amount" name="exe-qnt-8-sub">
                                                       </div>
                                                       <div class="error" id="DIG01_error_msg"></div>
                                                   </td>
                                               </tr>

                                               <tr>

                                                <td>SOU01</td>

                                                <td>Souvenir Ad Full Page (Color)</td>

                                                <td>$1,000</td>

                                                <td>

                                                    <input type="hidden" name="package_code_9" value="SOU01">

                                                    <input type="hidden" name="package_trype_9" value="Souvenir Ad Full Page (Color)">

                                                    <input type="text" min="0" class="form-control exhibit-two" data-id="9" data-amount="1000" name="exe-qnt-9" />

                                                </td>

                                                <td>
                                                    <div class="d-flex"><span>$</span><input type="text" id="exe-sub-total-9" class="sub-input-two" readonly value="0" name="exe-qnt-9-sub">
                                                    </td>

                                                </tr>

                                                <tr>

                                                    <td>SOU02</td>

                                                    <td>Souvenir Ad Half Page (Color)</td>

                                                    <td>$500</td>

                                                    <td>



                                                        <input type="hidden" name="package_code_10" value="SOU02">

                                                        <input type="hidden" name="package_trype_10" value="Souvenir Ad Half Page (Color)">

                                                        <input type="text" min="0" class="form-control exhibit-two" data-id="10" data-amount="500" name="exe-qnt-10" />

                                                    </td>

                                                    <td>$<input type="text" id="exe-sub-total-10" class="sub-input-two" readonly value="0" name="exe-qnt-10-sub"></td>

                                                </tr>

                                                <tr>

                                                    <td>SOU03</td>

                                                    <td>Souvenir Ad Quarter Page (Color)</td>

                                                    <td>$250</td>

                                                    <td>



                                                        <input type="hidden" name="package_code_11" value="SOU03">

                                                        <input type="hidden" name="package_trype_11" value="Souvenir Ad Quarter Page (Color)">

                                                        <input type="text" min="0" class="form-control exhibit-two" data-id="11" data-amount="250" name="exe-qnt-11" />

                                                    </td>

                                                    <td>$<input type="text" id="exe-sub-total-11" class="sub-input-two" readonly value="0" name="exe-qnt-11-sub"></td>

                                                </tr>

                                                <tr>

                                                    <td colspan="4" class="text-right">Total Amount: $</td>

                                                    <td><input type="text" id="sub-total-two" class="subtotal-input" readonly value="0"></td>

                                                </tr>

                                                <tr>

                                                    <td class="text-right" colspan="3">Grand Total (Exhibits + Banner + Souvenir Ads): </td>

                                                    <td colspan="2">$<b> <span id="total-show"></span> </b></td>

                                                </tr>

                                            </tbody>

                                        </table>

                                    </div>

                                </div>

                            </div>

                            <div class="row my-2 px-3">
                                <div class="col-12 shadow-small p-2 p-md-4">
                                    <div>
                                        <div class="text-orange fs16">Note:-</div>
                                        <p class="overflow-wrap_break-word">For more information, Please email to <b>vendorexhibits@ttaconvention.org </b> (or) <b>naresh.chintala2012@gmail.com </b> </p>
                                        <p>Mobile : 732-763-9650</p>
                                        

                                    </div>
                                </div>
                            </div>

                            @include('partalles.paymentForm') 
                            <div class="row mt-4 mb-2" id="paypal">
                        <input type="text" style="display:none" class=" pl10 pr10 text-right box-shadow-none form-control donatein border-0" name="amount" id="total-amount" required="" readonly="">

                    </div>
                    <!-- <input type="checkbox" name="billing_same" value="0" id="address_check">Please check this box if your billing address is same as current address <br><br> -->
                    <div class="col-12 pt-4 bhide billing_address_div px-0">


                            <div class="row">

                                <div class="col-12">

                                    <input type="checkbox" class="agree-checkbox" name="" required>

                                    <div class="pl40 fs15">I DECLARE that I have read and understand the package information, Terms and conditions provided on page 2 of this form.

                                        I authorize the above charges. No refunds will be provided for any type of Registrations.

                                    </div>

                                </div>

                                <div class="col-12 py-4">
                                <div class="text-center" style="color:red">
                                    <h4>All vendor Booth spaces are sold out </h4>
                                    </div>

                                    <div class="text-center text-sm-right" style="color:red">
                                        <button type="submit" class="btn btn-lg btn-danger text-uppercase px-4 px-sm-5" id="check_out1" disabled>Check out</button>
                                    </div>

                                </div>

                            </div>



                            <!-- Terms And Conditions -->



                            <div class="row">

                                <div class="col-12">

                                    <h5 class="text-violet">Terms & Conditions</h5>

                                    <ul class="fs15 pl-4 pl-md-5">

                                        <li class="py-1 pl-2 pl-md-3">Exhibit space, sizes, timings, floor plans, banner locations, additional terms & Conditions are available on the convention website.</li>

                                        <li class="py-1 pl-2 pl-md-3">Each exhibit space will be provided with a booth (10' x 10' in Size, 8' rear draping and 3'inside draping), one 6'X30" skirted table and two chairs. Each Exhibitor (10’X10’) gets 2 exhibitor passes.</li>

                                        <li class="py-1 pl-2 pl-md-3">Allocation of booth is based on first come first served basis. Booths and Banner spaces will be allotted upon full payment only.</li>

                                        <li class="py-1 pl-2 pl-md-3">Amount once paid cannot be refunded and not transferable to other vendors.</li>

                                        <li class="py-1 pl-2 pl-md-3">Convention committee has all rights to change the floor plan at any point of time based on circumstances.</li>

                                        <li class="py-1 pl-2 pl-md-3">TTA or Convention center is not responsible for any kind of theft, loss or damage of your goods. No Insurance will be provided by TTA or Convention Center.</li>

                                        <li class="py-1 pl-2 pl-md-3">Exhibitor is responsible for obtaining the necessary business licenses, liability insurances and collecting local and state taxes during the convention.</li>

                                        <li class="py-1 pl-2 pl-md-3">TTA or Convention center is not responsible for any kind of Taxes or permits. Exhibitor must comply with city, state and federal laws and regulations for Exhibiting. TTA is not responsible for any kind of international taxes or permits.</li>

                                        <li class="py-1 pl-2 pl-md-3">Exhibitors cannot sub-lease their rented booth. Booth dimensions should not modify in any form and should not obstruct the view of others.</li>

                                        <li class="py-1 pl-2 pl-md-3">Exhibit owners will have full responsibility over any kind of damage that caused to the convention center’s property.</li>

                                        <li class="py-1 pl-2 pl-md-3">Exhibitors are not allowed to sell/display any products other than mentioned category packages in the application.</li>

                                        <li class="py-1 pl-2 pl-md-3">No open flames are allowed. All exhibitors must adhere to the local fire department regulations. All electrical wiring must meet appropriate safety specifications.</li>

                                        <li class="py-1 pl-2 pl-md-3">Food/Beverages will be sold only by TTA or TTA designated vendors only.</li>

                                        <li class="py-1 pl-2 pl-md-3">Exhibitors and sponsors responsibility to provide their banner as per the TTA specifications.</li>

                                    </ul>

                                </div>

                            </div>







                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

</section>









@section('javascript')

<script src="https://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>


<script type="text/javascript">

    $(document).ready(function() {
        paymenttypes = ({!!html_entity_decode($paymenttypes) !!})

        $('#Other_payment_div').hide()

        $('.Paypal_text').text('Paypal / Credit, Debit Card through Paypal')
        $("[data-name=Paypal]").prop("checked", true).trigger("click");

        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });

        jQuery.validator.addClassRules('exhibit-qnt', {
            digits: true,
            min: 1,
        });

        jQuery.validator.addClassRules('exhibit-two', {
            digits: true,
            min: 0,
        });
        
        $("#form").validate({
            rules: {
                name: "alpha",
                'first-name': "alpha",
                'last-name': "alpha",
                'email': "email",
                phone :{
                    maxlength: 14,
                    digits: true
                },

            },
            messages: {
               "name": {
                alpha: "Name should not contain numbers.",
            }, 
            "first-name": {
                alpha: "First name should not contain numbers.",
            },
            "last-name": {
                alpha: "Last name should not contain numbers.",
            },

        },
    });

    });
    
    $(document).on('click', '#exe-sub-total-8', function() {

       IsReadonly = $('#exe-sub-total-8').prop( "readonly" )

       if (($('#exe-sub-total-8').val()==0) && (!IsReadonly)  ) {

        $('#exe-sub-total-8').val('')

    }

})  



    $(document).on('change keyup', '.exhibit-qnt', function() {

        var qnt = parseInt($(this).val()) || 0;

        var qid = $(this).attr('data-id');

        var qamt = parseInt($(this).attr('data-amount'));



        var qntsub = parseInt(qnt * qamt);

        $('#exe-sub-total-' + qid).val(qntsub);



        var sum = 0;

        $(".sub-input").each(function() {

            sum += +$(this).val();

        });

        $('#sub-total-one').val(sum);



        var subtone = parseInt($('#sub-total-one').val());

        var subttwo = parseInt($('#sub-total-two').val());

        $('#total-amount').val(subtone + subttwo);

        //$('#check_out').show();
        $('#check_out').attr('disabled', false)
        $('#total-show').empty();

        $('#total-show').append(subtone + subttwo);



    });


    $(document).on('change keyup', '.minmax', function() {

       $('#DIG01_error_msg').text(' ')
       var digi_amt = parseInt($('#digi_qnt').val()) || 0;
       if (digi_amt > 0) {

        var amt = $('.minmax').val();

        if (amt < 2000) {

                    //alert('Please enter value between 2000 - 5000');
                    $('#DIG01_error_msg').text('Please enter value between 2000 - 5000')
                // $('.minmax').val('0');

            }

            if (amt > 5000) {

                // alert('Please enter value between 2000 - 5000');
                $('#DIG01_error_msg').text('Please enter value between 2000 - 5000')
                //  $('.minmax').val('0');

            }

        }

    });

    $(document).on('keyup', '#digi_qnt', function() {
       if($('#digi_qnt').val().length>0){
        $('#hint_msg').show()
        $('#exe-sub-total-8').attr('readonly',false)
    }else{
        $('#hint_msg').hide()
        $('#DIG01_error_msg').text('')
        $('#exe-sub-total-8').attr('readonly',true)
    }
})

    $(document).on('click', '#check_out', function() {

        var digi_amt = parseInt($('#digi_qnt').val()) || 0;
        if (digi_amt > 0) {
            var amt = $('.minmax').val();



            if (amt < 2000) {

               // alert('Please enter value between 2000 - 5000');
               $('#DIG01_error_msg').text('Please enter value between 2000 - 5000')
               event.preventDefault();
              //  $('.minmax').val('0');

          }

          if (amt > 5000) {

                //alert('Please enter value between 2000 - 5000');
                $('#DIG01_error_msg').text('Please enter value between 2000 - 5000')
                event.preventDefault();
               // $('.minmax').val('0');

           }

       }

   });

    $(document).on('change keyup', '.exhibit-two', function() {

        var qnt = parseInt($(this).val()) || 0;

        var qid = $(this).attr('data-id');

        var qamt = parseInt($(this).attr('data-amount'));



        var qntsub = parseInt(qnt * qamt);

        $('#exe-sub-total-' + qid).val(qntsub);



        var sum = 0;

        $(".sub-input-two").each(function() {

            sum += +$(this).val();

        });

        $('#sub-total-two').val(sum);



        var subtone = parseInt($('#sub-total-one').val());

        var subttwo = parseInt($('#sub-total-two').val());

        $('#total-amount').val(subtone + subttwo);

       // $('#check_out').show();
       $('#check_out').attr('disabled', false)
       $('#total-show').empty();

       $('#total-show').append(subtone + subttwo);

   });

    $(document).on('click change keyp', '#address_check', function() {

        if ($(this).prop("checked") == true) {
            var fname = $('#first_name').val();

            var lname = $('#last_name').val();
            $('#bname').val('');
            $('#bname').val(fname + ' ' + lname);
            // alert(fname+lname);
            $('#address_check').val('1');

            var adone = $('#address_one').val();
            $('#baddress_one').val('');
            $('#baddress_one').val(adone);
            var adtwo = $('#address_two').val();
            $('#baddress_two').val('');
            $('#baddress_two').val(adtwo);
            var city = $('#city').val();
            $('#bcity').val('')
            $('#bcity').val(city)
            var state = $('#state').val();
            $('#bstate').val('')
            $('#bstate').val(state)
            var zipcode = $('#zipcode').val();
            $('#bzipcode').val('')
            $('#bzipcode').val(zipcode)
        } else if ($(this).prop("checked") == false) {
            $('#address_check').val('0');
            $('.rmval').val('');
        }
    });

</script>



@endsection




