@extends('layouts.user.base')

@section('content')

<!-- <section class="container-fluid mt-3 mb300">
    <div class="container pr-0">
		<div class="row">
			<div class="col-12">
	            <div class="text-right">
	                <a href="{{ url('registration') }}?page=regpage" class="btn btn-danger">Donate / Register Now </a>
	            </div>
	        </div>
		</div>
	</div>
</section> -->
<style type="text/css">
	.inner-page-donors-bg {
        background: #92278f;
        background-image: url(images/body-bg9.png), url(images/convention-team-bg.png);
        width: 100%;
        padding: 0px 0px 20px 0px;
        background-repeat: repeat-y, repeat;
        background-position: left top, left top;
        background-size: contain, auto;
        text-align: center;
    }
    .donors-nav-item a {
        cursor: pointer;
        font-family: "Eurostar";
        font-size: 20px;
        color: #e1e1e1;
    }
    @media (min-width: 1200px) {
        .container,
        .container-lg,
        .container-md,
        .container-sm,
        .container-xl {
            max-width: 1260px;
        }
    }
</style>

<section class="container mt35 inner-page-donors-bg">
    <div class="mx-auto main-heading">
        <span>Donors</span>
    </div>
    <div class="container">
        <div class="row px-4 px-md-4 px-lg-2">
            <div class="col-12 px-3 px-md-3 px-lg-5">
                <div>
                    <ul class="list-unstyled text-center donors-horizontal-scroll">
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none donors_active_tab_btn" target-id="#chakravarti-poshakulu">
                                <span>Chakravarti Poshakulu</span>
                                <h6>$100,000+</h6>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#maharaja-poshakulu">
                                <span>Maharaja Poshakulu</span>
                                <h6>$50,000+</h6>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#raja-poshakulu">
                                <span>Raja Poshakulu</span>
                                <h6>$25,000+</h6>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#mukhya-atithi">
                                <span>Mukhya Atithi</span>
                                <h6>$10,000+</h6>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#visishta-atithi">
                                <span>Visishta Atithi</span>
                                <h6>$5,000+</h6>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#pratyeka-atithi">
                                <span>Pratyeka Atithi</span>
                                <h6>$2,500+</h6>
                            </a>
                        </li>
                        <li class="donors-nav-item">
                            <a class="donors-nav-link text-decoration-none" target-id="#gourava-atithi">
                                <span>Gourava Atithi</span>
                                <h6>$1,000+</h6>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row justify-content-center donors_active_tab" id="chakravarti-poshakulu">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/mallareddy.png" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Malla Reddy Pailla" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Dr. Malla Reddy Pailla & Sadhana</h6>
                                <div class="donors-amount">$100,000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center" id="maharaja-poshakulu" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Mohan Patalolla.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Mohan Patalolla" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Dr. Mohan Patalolla</h6>
                                <div class="donors-amount">$50,000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center" id="raja-poshakulu" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/haranath_policherla.png" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Haranath Policherla" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Dr. Haranath Policherla</h6>
                                <div class="donors-amount">$25,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Vijaypal Reddy.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="VijayPal Reddy" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Dr. VijayPal Reddy</h6>
                                <div class="donors-amount">$25,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Srinivas_Ganagoni.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Srinivas Ganagoni" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Srinivas Ganagoni</h6>
                                <div class="donors-amount">$25,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Vamshi Reddy.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Vamshi Reddy" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Vamshi Reddy</h6>
                                <div class="donors-amount">$25,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Bharath Madadi.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Bharath Madadi" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Bharath Madadi</h6>
                                <div class="donors-amount">$25,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Dr. Sudhakar Vidiyala & Geetha" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Dr. Sudhakar Vidiyala & Geetha</h6>
                                <div class="donors-amount">$25,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Ramana Reddy" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Ramana Reddy</h6>
                                <div class="donors-amount">$25,000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center" id="mukhya-atithi" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Sekhar Vemparala" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Sekhar Vemparala</h6>
                                <div class="donors-amount">$20,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/sharath-vemuganti.png" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Sharath Vemuganti" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Sharath Vemuganti</h6>
                                <div class="donors-amount">$12,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Amith Reddy" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Amith Reddy</h6>
                                <div class="donors-amount">$10,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Gangadhar_Vuppala.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Gangadhar Vuppala" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Gangadhar Vuppala</h6>
                                <div class="donors-amount">$10,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Suresh_venkannagari_3.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Suresh Reddy Venkannagari" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Suresh Reddy Venkannagari</h6>
                                <div class="donors-amount">$10,000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center" id="visishta-atithi" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Anna Reddy" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Anna Reddy</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Rajeshwar Reddy Gangasani" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Rajeshwar Reddy Gangasani</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Divakar Jandyam" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Divakar Jandyam</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Kiran Duddagi.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Kiran Duddagi" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Kiran Duddagi</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/narasimha_reddy_ln.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Lakshmi Narasimha Reddy Donthireddy" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Lakshmi Narasimha Reddy Donthireddy</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Pavan Ravva.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Pavan Ravva" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Pavan Ravva</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Ram Mohan Chinnala" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Ram Mohan Chinnala</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Shiva Reddy  Kolla.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Shiva Reddy Kolla" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Shiva Reddy Kolla</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Srinivas Anugu" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Srinivas Anugu</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Venkat Gaddam.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Venkat Gaddam" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Venkat Gaddam</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Vijay Bhaskar Kalal.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Vijay Bhaskar Kalal" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Vijay Bhaskar Kalal</h6>
                                <div class="donors-amount">$5,000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center" id="pratyeka-atithi" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Anjan Karnati" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Anjan Karnati</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Ashok Chintakunta.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Ashok Chintakunta & Madhavi Soleti" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Ashok Chintakunta & Madhavi Soleti</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Bhaskar Pinna.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Bhaskar Pinna" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Bhaskar Pinna</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Hari Pyreddy" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Hari Pyreddy</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Kavitha Reddy.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Kavitha Reddy" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Kavitha Reddy</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Kiran Guduru.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Kiran Guduru" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Kiran Guduru</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Mahendar Narala.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Mahender Narala" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Mahender Narala</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Narsimha Peruka.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Narasihma Peruka" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Narasihma Peruka</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Prasad Kunarapu.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Prasad Kunurapu" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Prasad Kunurapu</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Ramana Kotha" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Ramana Kotha</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Ranjeeth Kyatham" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Ranjeeth Kyatham</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Roopak Kalluri.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Roopak Kalluri" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Roopak Kalluri</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Sahodhar-Peddireddy.png" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Sahodhar Peddireddy" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Sahodhar Peddireddy</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/satish-mekala.png" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Satish Mekala" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Satish Mekala</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Srinivas-Guduru.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Srinivas Guduru" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Srinivas Guduru</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Usha Reddy Mannem.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Usha Mannem" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Usha Mannem</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Vamshi Gullapalli" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Vamshi Gullapalli</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Venkat Sunki Reddy" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Venkat Sunki Reddy</h6>
                                <div class="donors-amount">$2,500</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Narendar Reddy Yarava.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Narender Yarava" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Narender Yarava</h6>
                                <div class="donors-amount">$2,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Srikanth Routhu" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Srikanth Routhu</h6>
                                <div class="donors-amount">$1,250</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Aruna Sridhar Kancharla" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Aruna Sridhar Kancharla</h6>
                                <div class="donors-amount">$1,200</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" id="gourava-atithi" style="display: none;">
            <div class="col-12">
                <div class="row px-0 px-sm-4 px-md-5 px-lg-5 py-3">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Naveen Goli.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Naveen Goli" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Naveen Goli</h6>
                                <div class="donors-amount">$1,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Nishanth Sirikonda.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Nishanth Singirikonda" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Nishanth Singirikonda</h6>
                                <div class="donors-amount">$1,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/Raghu Madupoju.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Raghu Mudupolu" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Raghu Mudupolu</h6>
                                <div class="donors-amount">$1,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Varun" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Varun</h6>
                                <div class="donors-amount">$1,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="donors-team">
                            <div>
                                <img src="images/no-image1.jpg" class="img-fluid border-radius-15 mx-auto d-block tta-member-image" alt="Yogi & Rama Vanama" />
                            </div>
                            <div class="pt-1">
                                <h6 class="donors-name">Mr. Yogi & Rama Vanama</h6>
                                <div class="donors-amount">$1,000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@endsection

