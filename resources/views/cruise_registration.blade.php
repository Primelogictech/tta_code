@extends('layouts.user.base')
@section('content')

<style>
.error{
    color:red
}
.input-checkbox {
    position: absolute;
    top: 8px;
}
.p-radio-btn {
    position: absolute;
    top: 1px;
}
</style>
    <section class="container-fluid my-3 my-lg-5">
        <div class="container shadow-md-none bg-white shadow-small">
            <div class="row">
                <div class="col-12 px-0">
                    <div class="headingss-bg p-3">
                        <h4 class="mb-0">Cruise Registration</h4>
                    </div>
                </div>
            </div>
            <div class="row border-top mt2">
                <div class="col-12 py-0 pt-1 px-1 pb-md-5">
                   
            <div class="row px-4 px-lg-0">
                    <div class="col-12 col-lg-10 offset-lg-1  shadow-small px-sm-30 p40 p-md-4 mb-5">

                        <form id="form" action="{{url('cruise_registration')}}" method="post" enctype="multipart/form-data">
                            <div class="row">

                                @csrf
                                <!-- Personal Information -->

                                <div class="col-12 ">
                                    @foreach ($errors->all() as $error)
                                    <div class="error">{{ $error }}</div>

                                    @endforeach
                                     @if ($message = Session::get('success'))
                          <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                          Download the Youth Cruise 
                          <a class="btn btn-sm btn-success" href="{{asset('Youth_Cruise_Waiver.pdf')}}" download>Waiver Form</a>
                          @endif
                                    <h5 class="text-violet py-2">Personal Information</h5>
                                    
                                    

                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>First Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="first_name" id="first_name" value="{{  Auth::user()->first_name ?? '' }}" class="form-control" required placeholder="First Name" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Last Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" required name="last_name" id="last_name" value="{{  Auth::user()->last_name ?? '' }}" class="form-control" placeholder="Last Name" />

                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Email Id:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="email" required name="email" id="email" value="{{  Auth::user()->email ?? '' }}" class="form-control" @if(Auth::user()) readonly="" @endif placeholder="Email Id"  />
                                                    <div class="error email_valid_msg"></div>
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Mobile (USA Number Only) :</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text"  required name="mobile" id="mobile" value="{{  Auth::user()->mobile ?? '' }}" class="form-control" placeholder="Mobile"  />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>DOB:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="dob" id="datepicker" class="form-control" placeholder="Date of birth" required/>
                                            </div>
                                        </div>

                                        <div class="form-group col-12 col-md-6 col-lg-6" style="display:none;">
                                            <label>Waiver Attachment:</label>
                                            <div>
                                                <input type="file" name="waiver_attachment" value="" id="waiver" class="form-control" />
                                            </div>
                                        </div>
                                    </div>

                                    
                                    
                                    <div class="row my-1">
                                        <div class="col-12  px-3 px-md-0">
                                            <div class="row mb-2">
                                                <div class="col-7 col-md-9 my-auto">
                                                <div class="text-right ">
                                                <b>
                                                    <label class="mb-0 fs25 fs-sm-16">Total Amount &nbsp;&nbsp;&nbsp; : </label>
                                                </b>
                                                </div>
                                                      
                                                </div>
                                               
                                                <div class="col-5 col-md-3 my-auto">
                                                <b>
                                                    <div class="payment fs25 fs-sm-16">$ <span id="total_amount_to_be_paid">50</span> </div>
                                                </b>
                                                    
                                                   
                                                </div>
                                                
                                            </div>
                                                 
                                        </div>
                                    </div>
                                    

                            <div class="row mt-2">
                                <div class="col-12">
                                    <h5 class="text-violet py-2">Payment Types</h5>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        @foreach ($RegistrationContent->patment_types as $patment_type)
                                        <div class="col-6 col-md-3 col-lg-20 px-2">
                                        <span class="position-relative" id="p_{{$RegistrationContent->paymenttype( $patment_type)->name }}">
                                            <input @if ($RegistrationContent->paymenttype($patment_type)->name=='Paypal') checked @endif type="radio" required class="p-radio-btn payment-button" data-name="{{$RegistrationContent->paymenttype( $patment_type)->name }}" value="{{$RegistrationContent->paymenttype( $patment_type)->id }}" name="payment_type" /><span class="fs16 pl-4 pl-md-4">{{ucfirst($RegistrationContent->paymenttype( $patment_type)->name )}}</span></span>

                                      
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <hr class="dashed-hr">
                            <div class="row">
                                <!-- paypal payment -->

                                <div class="col-12" id="paypal">
                                    <h5 class="text-violet mb-3">Payment by using Paypal</h5>
                                    <div class="paypal-note">
                                    </div>
                                    <div class="form-row">
                                    </div>
                                </div>

                                <!-- paypal payment end -->

                                <div class="col-12" id="check_payment" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using check</h5>
                                    <div class="check-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Cheque Number: </label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="cheque_number"  class="form-control cheque_form_field" placeholder="Cheque Number">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Cheque Date </label><span class="text-red">*</span>
                                            <div>
                                                <input type="date" name="cheque_date"  class="form-control cheque_form_field" placeholder="cheque Date">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Handed over to:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="more_info"  class="form-control cheque_form_field" placeholder="Handed over to">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- zelle -->
                                <div class="col-12" id="zelle" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using Zelle</h5>
                                    <div class="zelle-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Zelle Reference Number</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="Zelle_Reference_Number"  class="form-control zelle_form_field" placeholder="Zelle Reference Number">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- zelle end -->
                                <!-- othre Payments -->
                                <div class="col-12" id="other" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using other payments</h5>
                                    <div class="other-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>On Behalf Of :</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="Payment_made_through"  class="form-control other_payment_form_field" placeholder="On Behalf Of">
                                            </div>
                                        </div>
                                          <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Company Name</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="company_name"  class="form-control other_payment_form_field" placeholder="Company Name">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Payment Date </label><span class="text-red">*</span>
                                            <div>
                                                <input type="date" name="transaction_date"  class="form-control other_payment_form_field" placeholder="Transaction Date">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Payment Reference Number :</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="transaction_id"  class="form-control other_payment_form_field" placeholder="Payment Reference Number ">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- othre Payments end -->

                                <!-- First data Payments -->
                                 <div class="col-12 payments_field_div" id="first_data" style="display: none;">
                                    <h5 class="text-violet mb-3">Payment by using Credit / Debit Card</h5>
                                    <div class="first-data-note">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Card type :</label><span class="text-red">*</span>
                                            <div>
                                                <select name="type"  class="form-control payment_fields firstdata_form_field">
                                                    <option>Select card type</option>
                                                    <option value="visa">Visa</option>
                                                    <option value="mastercard">MasterCard </option>
                                                    <option value="Discover">Discover</option>
                                                    <option value="American Express">American Express</option>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Card Hold Name</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="cardholder_name"  class="form-control firstdata_form_field payment_fields"  placeholder="Card Hold Name">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Card Number </label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="card_number"  class="form-control firstdata_form_field payment_fields" placeholder="card number">
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>CVV :</label><span class="text-red">*</span>
                                            <div>
                                                <input type="number" name="cvv"  class="form-control firstdata_form_field payment_fields" placeholder="CVV">
                                            </div>
                                        </div>

                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Expiry Date(MMYY) :</label><span class="text-red">*</span>
                                            <div>
                                                <input type="number" name="exp_date"  class="form-control firstdata_form_field payment_fields" placeholder="Expiry Date ">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- first data Payments end -->
                            </div>
                            <hr class="dashed-hr">
                            <div class="row py-2">
                                <div class="col-12 col-md-12 my-auto">
                                    <div>
                                        <input type="checkbox" class="input-checkbox agree-checkbox" required="" name="agrement">
                                        <div class="pl40 fs15 py-1 word-break-break-word">I DECLARE that I have read and understand the Registration information, I AGREE all Registrations are final and no refunds / Exchanges / Cancellations will be accepted.</div>
                                        <div class="pl40 fs15 py-1">I authorize the above charge and No refunds will be provided for any type of Registrations. I abide by TTA standards, rules and regulations.</div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 my-2 my-md-auto">
                                    <div class="text-center text-sm-right">
                                        <input type="submit" class="btn btn-lg btn-danger text-uppercase px-5 submit-button" value="Check out" name="" />
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row pt-4 pb-2">
                                <div class="col-12 col-md-12 my-auto">
                                    <h5 class="text-violet mb-3">Terms and Conditions:</h5>
                                    <div>
                                        <ul class="fs15 pl-4 pl-md-5">
                                            <li class="py-1 pl-2 pl-md-3">All the fields marked with *, in this form are mandatory and must be filled for processing an application.</li>
                                            <li class="py-1 pl-2 pl-md-3">All Legal Guardians should be aware that alcoholic beverages will be available to youth members ages 21+.</li>

                                            <li class="py-1 pl-2 pl-md-3">Refunds will not be issued in the event that the waiver form is not completed &amp; attendees are refused onboard</li>
                                            <li class="py-1 pl-2 pl-md-3">
                                                Valid ID proof is required to onboard Cruise. Drivers License is preferred, those who do not have a valid drivers license should bring a copy of a passport or any government issued ID that shows proper DOB.
                                            </li>
                                            <li class="py-1 pl-2 pl-md-3">After the process of application is completed, an email will be sent with a confirmation number to the Registrant with a waiver form.</li>
                                            <li class="py-1 pl-2 pl-md-3">Please bring signed waiver form (20 below ONLY) on the day of the event & hand to the check-in team </li>
                                            <li class="py-1 pl-2 pl-md-3">No Refunds / Exchanges / Cancellations are accepted for any type of Registrations.</li>
                                            <li class="py-1 pl-2 pl-md-3">The TTA organization &amp; TTA Youth Team will not be responsible for members who decide to consume said beverages.</li>
                                            <li class="py-1 pl-2 pl-md-3">ID Bands will be handed to all members attending the cruise to differentiate the set age groups.</li>

                                            <li class="py-1 pl-2 pl-md-3">All attendees will be responsible for their transportation to and from the shuttle pick-up point at the Edison Sheraton Hotel.</li>

                                            <li class="py-1 pl-2 pl-md-3">Shuttles will depart promptly at 6:00pm to Hoboken Pier 14. Shuttles will return all attendees to Edison Sheraton after the cruise concludes at 11:00pm.</li>

                                            <li class="py-1 pl-2 pl-md-3">Please arrange proper transportation back to your homes/hotel rooms.</li>

                                            <li class="py-1 pl-2 pl-md-3">We appreciate your cooperation in this matter, and hope that all members enjoy this event.</li>

                                            <li class="py-1 pl-2 pl-md-3">Safety is our top priority! We want to ensure that everyone reaches their destination safely.</li>
                                            <li class="py-1 pl-2 pl-md-3">All the details of the program and information regarding the convention can be found in www.ttaconvention.org</li>
                                        </ul>
                                    </div>
                                </div>
                                
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




@section('javascript')
<script src="https://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/additional-methods.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
<script>
    $(document).ready(function() {
        paymenttypes = ({!!html_entity_decode($paymenttype) !!})
            
            $('#p_Zelle').hide();
            $('#p_Check').hide();
            $('#p_Other').hide();
         $(".payment-button").click(function(e) {
           
            if ($(this).prop("checked")) {
                if ($(this).data('name') == "{{ config('conventions.paypal_name_db') }}") {
                    $('#paypal').show();
                    $('#check_payment').hide();
                    $('#zelle').hide();
                    $('#other').hide();
                     $('#first_data').hide();
                    paymenttypes.forEach(data => {
                        if (data.name == "{{ config('conventions.paypal_name_db') }}") {
                            $('.paypal-note').html(data.note)
                        }
                    });
                    $('.cheque_form_field').prop("required", false);
                    $('.zelle_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", false);
                    $('.firstdata_form_field ').prop("required", false);

                }

                if ($(this).data('name') == "{{ config('conventions.check_name_db') }}") {
                    $('#check_payment').show();
                    $('#paypal').hide();
                    $('#zelle').hide();
                    $('#other').hide();
                    $('#first_data').hide();
                    paymenttypes.forEach(data => {
                        if (data.name == "Check") {
                            $('.check-note').html(data.note)
                        }
                    });
                    $('.cheque_form_field').prop("required", true);
                    $('.zelle_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", false);
                    $('.firstdata_form_field ').prop("required", false);
                }

                if ($(this).data('name') ==  "{{ config('conventions.zelle_name_db') }}") {
                    $('#zelle').show();
                    $('#paypal').hide();
                    $('#check_payment').hide();
                    $('#other').hide();
                     $('#first_data').hide();
                    paymenttypes.forEach(data => {
                        if (data.name ==  "{{ config('conventions.zelle_name_db') }}") {
                            $('.zelle-note').html(data.note)
                        }

                    });
                    $('.zelle_form_field').prop("required", true);
                    $('.cheque_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", false);
                    $('.firstdata_form_field ').prop("required", false);

                }
                if ($(this).data('name') == "{{ config('conventions.other_name_db') }}") {

                    $('#other').show();
                    $('#zelle').hide();
                    $('#paypal').hide();
                    $('#check_payment').hide();
                    $('#first_data').hide();
                    paymenttypes.forEach(data => {
                        if (data.name == "{{ config('conventions.other_name_db') }}") {
                            $('.other-note').html(data.note)
                        }
                    });

                    $('.zelle_form_field').prop("required", false);
                    $('.cheque_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", true);
                    $('.firstdata_form_field ').prop("required", false);


                }
           
                 if ($(this).data('name') == "{{ config('conventions.first_data_name_db') }}") {
                     
                  $('#other').hide();
                    $('#zelle').hide();
                    $('#paypal').hide();
                    $('#check_payment').hide();
                    $('#first_data').show();
                     $('.zelle_form_field').prop("required", false);
                    $('.cheque_form_field').prop("required", false);
                    $('.other_payment_form_field').prop("required", false);
                    $('.firstdata_form_field ').prop("required", true);

                }
                //zelle
            }
        });

         $.validator.addMethod('filesize', function (value, element, arg) {
                if(element.files[0]!=undefined){
                    if(element.files[0].size<=arg){ 
                        return true; 
                    }else { 
                        return false; 
                    } 
                }else{
                    return true; 
                }
         }); 

         $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        });

        jQuery.validator.addClassRules('count', {
              digits: true,
              min: 1
         });

         jQuery.validator.addClassRules('amount_form_field', {
              digits: true,
              min: 1
         });

         

        $("#form").validate({
            
            rules: {
                
                first_name: "alpha",
                last_name: "alpha",
                dob: "required",
                mobile :{
                    maxlength: 14,
                    digits: true
                },
                
            },
             messages: {
                
                 "first_name": {
                    alpha: "First name should not contain numbers.",
                },
                 "last_name": {
                    alpha: "Last name should not contain numbers.",
                },
                 
                 "mobile": {
                    maxlength: "Max length is  14.",
                }
            },
        });

          
    });
</script>

@endsection


@endsection
