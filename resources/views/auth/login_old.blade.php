@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small bg-white px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 px-0 px-md-2 py-2 ">

                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
                @endforeach
                @endif
               @if(app('request')->input('error'))
                <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>Your email already exists. Please login here to continue.</strong>
                          </div>
                          @endif
              
                 @if ($message = Session::get('success'))
                          <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                          @endif
                        @if ($message = Session::get('error'))
                          <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                          @endif

                @yield('content')

                <div class="shadow-small shadow-small-sm-none border-radius-5 px-0 py-4 py-md-4 px-md-4">
                    <h4 class="text-center mb-0">Login</h4>
                  <!--   <div class="text-center fs16">Please use your NRIVA login details</div> -->

                    <form method="POST" action="{{ route('login') }}" class="col-12 px-0 pt-3">
                        @csrf
                        <div class="col-12 my-2">
                            <label>Email:</label><span class="text-red">*</span>
                            <div>
                                <input type="text" id="" type="email" name="email" value="{{app('request')->input('email')??''}}" required autofocus class="form-control" placeholder="Enter Your Email" />
                            </div>
                        </div>

                        <div class="col-12 my-2">
                            <div class="form-group">
                                <label>Password</label><span class="text-red">*</span>
                                <div>
                                    <input type="password" type="password" name="password" required autocomplete="current-password" class="form-control" placeholder="Password" />
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div>
                                <input type="checkbox" class="input-checkbox" name=""><span class="pl25 fs16">Keep me logged in</span>
                            </div>
                        </div>
                        <div class="col-12 pt-3">
                            <div>
                                <button class="btn btn-violet w-100 border-radius-5 signin-btn"> {{ __('Log in') }}</button>
                            </div>
                        </div>
                        
                        
                    </form>

                </div>
                  <div class="text-center pt-3 fs16">Don't know your password ? <a href="{{ url('forgotpassword') }}" class="text-violet font-weight-bold">Forgot Password</a></div> 
                        
            </div>
        </div>
    </div>
    </div>
    </div>
</section>


@section('javascript')

@endsection


@endsection
