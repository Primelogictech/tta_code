@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small bg-white px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 py-2 ">

                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
                @endforeach
                @endif

                
                <div class="alert alert-success alert-block" id="successmsg" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>Email verification Code sent to your email</strong>
                          </div>
                
                   
              
              
                 @if ($message = Session::get('success'))
                          <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                          @endif
                        @if ($message = Session::get('error'))
                          <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                          @endif

                @yield('content')

                <div class="shadow-small  border-radius-5 p-4">
                    <h4 class="text-center">Forgot Password</h4>

        <form method="POST" action="{{ url('update_password') }}">
            @csrf

            <!-- Email Address -->
           
            <div class="col-12">
                            <label>Email:</label><span class="text-red">*</span>
                            <div>
                                <input id="email" class="form-control" type="email" name="email" :value="old('email')" required autofocus />
                            </div>
                            <br>
                            <div>
                                <button type="button"  class="btn btn-sm btn-violet send_otp">Send Code</button>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Code:</label><span class="text-red">*</span>
                            <div>
                                <input id="otp" class="form-control" type="number" name="otp" :value="old('otp')" required autofocus />
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Password:</label><span class="text-red">*</span>
                            <div>
                                <input id="password" class="form-control" type="password" name="password" :value="old('password')" required autofocus />
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Confirm Password:</label><span class="text-red">*</span>
                            <div>
                                <input type="password"  name="password_confirmation" required autocomplete="current-password" class="form-control" placeholder="Password" />
                            </div>
                        </div>

            <div class="col-12 pt-3">
                            <div>
                                <button class="btn btn-violet w-100 border-radius-5 signin-btn"> {{ __('Submit') }}</button>
                            </div>
                        </div>
        </form>
    </div>
                 
                        
            </div>
        </div>
    </div>
    </div>
    </div>
</section>


@section('javascript')



<script type="text/javascript">
    $('.send_otp').click(function(){

        var email = $('#email').val();
        var thiss =$(this);
          if(email !=""){
        $.ajax({
            type: 'GET',
            url: "{{url('otp_send')}}?email="+email,
            success:function(data){
              if(data=="success"){
                 thiss.hide();
                 $('#successmsg').show();
                $('.otp_class').show();
              }else{
                $('#successmsg').hide();
                alert('Email invalid');
              }
             
            }
        });
      }else{
        alert('Please enter email');
      }

    });
</script>
@endsection


@endsection
