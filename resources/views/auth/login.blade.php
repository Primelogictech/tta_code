@extends('layouts.user.base')
@section('content')
<style>
.error{
    color:red;
}
.sucess{
    color:green;
}
</style>

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small bg-white px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 px-0 px-md-2 py-2 ">

                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
                @endforeach
                @endif
                @if(app('request')->input('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>Your email already exists. Please login here to continue.</strong>
                </div>
                @endif

                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif

                @yield('content')

                <div class="shadow-small shadow-small-sm-none border-radius-5 px-0 py-4 py-md-4 px-md-4">
                    <h4 class="text-center mb-0">Login</h4>
                    <!--   <div class="text-center fs16">Please use your NRIVA login details</div> -->

                    <form method="POST" action="{{ route('login') }}" class="col-12 px-0 pt-3">
                        @csrf
                        <div class="col-12 my-2">
                            <label>Email:</label><span class="text-red">*</span>
                            <div>
                                <input type="text" id="email" type="email" name="email" value="{{app('request')->input('email')??''}}" required autofocus class="form-control" placeholder="Enter Your Email" />
                                <div class="email_valid_msg error fs20" > </div>
                                <div class="email_sucess sucess fs20" > </div>
                            </div>
                        </div>

                          <div class="col-12 my-2">
                            <label>Login with:</label><span class="text-red">*</span>
                            <div class="d-flex">
                            <br>
                                <input type="radio" class="login-radio-btn" id="with_otp" name="login_with" required value="with_otp">
                                <label for="with_password" class="pl-2" > Email Verification Code</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" class="login-radio-btn" id="with_password" name="login_with" required value="with_password">
                                <label for="age2" class="pl-2">I Have Password</label>
                            </div>
                        </div>

                        <div class="col-12 my-2" id="otp_div" style="display:none">
                            <div>
                                    <button id="get_opt_btn" class='login-get-otp-btn btn btn-violet w-100 border-radius-5'>Click Here to Get Verification Code to your email.</button>
                            </div>
                            <br>
                            <br>
                        <div id="otp_input_div" style="display:none">
                        
                            <label  >Email Verifiaction code Sent to your email. </label>
                            <label  >Please enter it below </label>
                            <span class="text-red"></span>
                            <div class="d-flex my-auto">
                                <div>
                                    <input type="text" type="text" data="login_otp"  name="login_otp" value=""  autofocus class="col-12  form-control" placeholder="Enter Your OTP" />
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="col-12 my-2" id="password_div" style="display:none">
                            <div class="form-group">
                                <label>Password</label><span class="text-red">*</span>
                                <div>
                                    <input type="password" type="password" name="password"  autocomplete="current-password" class="form-control" placeholder="Password" />
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                           {{--  <div>
                                <input type="checkbox" class="input-checkbox" ><span class="pl25 fs16">Keep me logged in</span>
                            </div> --}}
                        </div>
                        <div class="col-12 pt-3">
                            <div>
                                <button class="btn btn-violet w-100 border-radius-5 signin-btn"> {{ __('Log in') }}</button>
                            </div>
                        </div>


                    </form>

                </div>
                <div class="text-center pt-3 fs16">Don't know your password ? <a href="{{ url('forgotpassword') }}" class="text-violet font-weight-bold">Forgot Password</a></div>

            </div>
        </div>
    </div>
    </div>
    </div>
</section>


@section('javascript')

<script>

       

 $('#get_opt_btn').on('click', function(e) {
        e.preventDefault()
     
        $('.email_valid_msg').text('')
        $('.email_sucess').text('')
        var email = $('#email').val();
        var thiss = $(this);
        if (email != "") {
            if (email.search('@') > 0) {} else {
                $('.email_valid_msg').text('Please enter a valid email')
                return 0;
            }
             $('.email_valid_msg').text('Please wait ...')
            $.ajax({
                type: 'GET'
                , url: "{{url('send-otp-for-login')}}?email=" + email
                , success: function(data) {
                    if (data == "success") {
                         $('.email_valid_msg').text('')
                       // $('.email_sucess').text('Pleace check Your Mail For OTP')
                        $('#otp_input_div').show()
                      
                    } else {
                        $('.email_valid_msg').html('<p> We did not Find your details, please register <a href="{{url("bookticket")}}">here</a> </p>')
                        //window.location.href = "{{url('login')}}?error=exit&email=" + email;
                    }
                        $('#get_opt_btn').text('Click Here to Re-send Verification Code to your email.')

                }
            });
        } else {
            $('.email_valid_msg').text('Please enter email')
            //alert('Please enter email');
        }

    }); 

    $("input[name='login_with']").change(function(){
        if($(this).val()=='with_password') {
            $('#password_div').show()
            $('#otp_div').hide()
            $('password').attr('required',true)
            $('login_otp').attr('required',true)
        }else{
            $('#password_div').hide()
            $('#otp_div').show()
            $('password').attr('required',false)
            $('login_otp').attr('required',true)
        }
    });
</script>

@endsection


@endsection
