
@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small bg-white px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 col-lg-10 offset-lg-1 py-2 ">

                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
                @endforeach
                @endif

                 @if ($message = Session::get('success'))
                          <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                          @endif
                        @if ($message = Session::get('error'))
                          <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                          @endif

                @yield('content')

                <div class="shadow-small  border-radius-5 p-4">
                    <h4 class="text-center">Register</h4>
                  <!--   <div class="text-center fs16">Please use your NRIVA login details</div> -->

                    <form method="POST" action="{{ route('register') }}" class="col-12 pt-3">
                        @csrf
                        <div class="form-group row">
                        <div class="col-12 col-md-6 col-lg-6">
                            <label>First Name:</label><span class="text-red">*</span>
                            <div>
                                <input type="text" name="firstname" :value="old('firstname')" required autofocus class="form-control" placeholder="Enter Your Firstname" />
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-6">
                            <label>Last Name:</label><span class="text-red">*</span>
                            <div>
                                <input type="text"   name="lastname" :value="old('lastname')" required  class="form-control" placeholder="Enter Your Lastname" />
                            </div>
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-12 col-md-6 col-lg-6">
                            <label>Email:</label><span class="text-red">*</span>
                            <div>
                                <input  id="email" type="email" name="email" :value="old('email')" required  class="form-control" placeholder="Enter Your Email" />
                            </div>
                            <div>
                                <button type="button"   class="btn btn-sm btn-violet send_otp">Send OTP</button>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-6 otp_class" style="display: none;">
                            <label>OTP:</label><span class="text-red">*</span>
                            <div>
                                <input  id="" type="number" name="otp" :value="old('otp')" required  class="form-control" placeholder="Enter Your OTP" />
                            </div>
                            <div>
                                <button type="button" class="btn btn-sm btn-violet send_otp">Resend OTP</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-md-6 col-lg-6">
                       
                            <label>Mobile:</label><span class="text-red">*</span>
                            <div>
                                <input type="text"   name="mobile" :value="old('mobile')" required  class="form-control" placeholder="Enter Your Mobile Number" />
                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label>Password</label><span class="text-red">*</span>
                                <div>
                                    <input type="password"  name="password" required autocomplete="current-password" class="form-control" placeholder="Password" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label>Confirm Password</label><span class="text-red">*</span>
                                <div>
                                    <input type="password"  name="password_confirmation" required autocomplete="current-password" class="form-control" placeholder="Password" />
                                </div>
                            </div>
                        </div>
                    </div>
                        
                        
                        <div class="col-12  col-md-6 offset-md-3 pt-3">
                            <div>
                                <button class="btn btn-violet w-100 border-radius-5 signin-btn"> {{ __('Register') }}</button>
                            </div>
                        </div>
                        
                        
                    </form>

                </div>
              
                        
            </div>
        </div>
    </div>
    </div>
    </div>
</section>


@section('javascript')

<script type="text/javascript">
    $('.send_otp').click(function(){

        var email = $('#email').val();
        var thiss =$(this);
          if(email !=""){
        $.ajax({
            type: 'GET',
            url: "{{url('otp_send')}}?email="+email,
            success:function(data){
              if(data=="success"){
                 thiss.hide();
                $('.otp_class').show();
              }else{
                alert('Email invalid');
              }
             
            }
        });
      }else{
        alert('Please enter email');
      }

    });
</script>

@endsection


@endsection

