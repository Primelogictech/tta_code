@extends('layouts.user.base')
@section('content')

<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small bg-white px-sm-20 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 py-2 ">

                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
                @endforeach
                @endif
              
              
                 @if ($message = Session::get('success'))
                          <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                          @endif
                        @if ($message = Session::get('error'))
                          <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                  <strong>{{ $message }}</strong>
                          </div>
                          @endif

                @yield('content')

                <div class="shadow-small  border-radius-5 p-4">
                    <h4 class="text-center">Change Password</h4>

        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <!-- Password Reset Token -->
            <input type="hidden" name="token" value="{{ $request->route('token') }}">

            <!-- Email Address -->
           

<div class="col-12">
                            <label>Email:</label><span class="text-red">*</span>
                            <div>
                                <input id="email" class="form-control" type="email" name="email" value="{{old('email', $request->email)}}" required autofocus readonly="" />
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Password:</label><span class="text-red">*</span>
                            <div>
                                <input id="password" class="form-control" type="password" name="password" :value="old('password', $request->email)" required autofocus />
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Confirm Password:</label><span class="text-red">*</span>
                            <div>
                                <input id="password_confirmation" class="form-control" type="password" name="password_confirmation" :value="old('password_confirmation', $request->password_confirmation)" required autofocus />
                            </div>
                        </div>

          
           <!--  <div class="btn btn-violet w-100 border-radius-5 signin-btn">
                <x-button>
                    {{ __('Reset Password') }}
                </x-button>
            </div> -->
            <div class="col-12 pt-3">
                            <div>
                                <button class="btn btn-violet w-100 border-radius-5 signin-btn"> {{ __('Reset Password') }}</button>
                            </div>
                        </div>
        </form>
   </div>
                 
                        
            </div>
        </div>
    </div>
    </div>
    </div>
</section>


@section('javascript')

@endsection


@endsection

