<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Paymenttype;
use Illuminate\Http\Request;

class PaymenttypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paymenttypes = Paymenttype::get();
        return view('admin.master.paymenttype.index', compact('paymenttypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.paymenttype.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' =>['required', 'string', 'max:50']
        ];
        $request->validate($rules);
        $Paymenttype = Paymenttype::create($request->all());
        return redirect()
        ->route('paymenttype.index')
        ->with('message-suc', $request->name . ' Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Paymenttype  $Paymenttype
     * @return \Illuminate\Http\Response
     */
    public function show(Paymenttype $Paymenttype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Paymenttype  $Paymenttype
     * @return \Illuminate\Http\Response
     */
    public function edit(Paymenttype $paymenttype)
    {
        return view('admin.master.paymenttype.edit', compact('paymenttype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Paymenttype  $Paymenttype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paymenttype $paymenttype)
    {
        $rules = [
            'name' => [ 'string', 'max:50']
        ];
        $request->validate($rules);
        $paymenttype->update($request->all());

        $request->session()->flash('paymenttype.id', $paymenttype->id);

        return redirect()->route('paymenttype.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Paymenttype  $Paymenttype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paymenttype $Paymenttype)
    {
        return   $Paymenttype->delete();
    }

    public function updateStatus(Request $request)
    {
        return Paymenttype::where('id', $request->id)->update(['status' => $request->status]);
    }
}
