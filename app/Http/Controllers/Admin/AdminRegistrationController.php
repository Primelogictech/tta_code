<?php



namespace App\Http\Controllers\Admin;



use App\Http\Controllers\Controller;

use App\Models\Registration;
use App\Models\Reservation;
use App\Models\Cruise;
use App\Models\PackageList;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Payment as ModelPayment;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExhibitRegistrationsExport;
use App\Exports\ConventionRegistrationsExport;
use App\Exports\SatamanamBhavatiRegistrationsExport;
use App\Exports\CruiseRegistrationsExport;


class AdminRegistrationController extends Controller

{

    /**

     * @param \Illuminate\Http\Request $request

     * @return \Illuminate\Http\Response

     */

    public function selected_packages($id)

    {
//dd($id);PackageList
        $packages=PackageList::where('package_id',$id)->get();

        return view('admin.registrations.package-details', compact('packages') );
    }

    public function private_convention_members($value='')
    {
        $users=User::with('registrationtype')->where('email','!=',env('admin_email'))->orderBy('created_at', 'desc')->get();
        return view('admin.registrations.index', compact('users'));
    }

    public function private_exhibits_members($value='')
    {
        $registrations=Reservation::with('paymentDetails','paymentDetails.paymentmethord')->orderBy('created_at', 'desc')->get();
        return view('admin.registrations.exhibitRegistrations', compact('registrations') );
    }

     public function cruiseRegistrations()

    {

        $registrations=Cruise::orderBy('created_at', 'desc')->get();
        return view('admin.registrations.cruiseRegistrations', compact('registrations') );

    }
    public function exhibitRegistrations()

    {

        $registrations=Reservation::with('paymentDetails','paymentDetails.paymentmethord')->orderBy('created_at', 'desc')->get();
        return view('admin.registrations.exhibitRegistrations', compact('registrations') );

    }

     public function youthActivitiesRegistrations()

    {

        $registrations=Registration::where('registration_type_name','YouthActivities')->get();

        return view('admin.registrations.exhibitRegistrations', compact('registrations') );

    }

    /*Excel  export functions */
    public function ExhibitRegistrationsExport() 
    {
        return Excel::download(new ExhibitRegistrationsExport, 'ExhibitRegistrations.xlsx');
    }

    public function ConventionRegistrationsExport() 
    {
        return Excel::download(new ConventionRegistrationsExport, 'ConventionRegistrations.xlsx');
    }

    public function satamanamBhavatiRegistrationsExport() 
    {
        return Excel::download(new SatamanamBhavatiRegistrationsExport, 'SatamanamBhavatiRegistration.xlsx');
    }

     public function CruiseRegistrationsExport() 
    {
        return Excel::download(new CruiseRegistrationsExport, 'CruiseRegistration.xlsx');
    }






}

