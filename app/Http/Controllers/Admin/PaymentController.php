<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\SponsorCategory;
use App\Models\Admin\SponsorCategoryType;
use Illuminate\Http\Request;
use App\Models\Admin\Paymenttype;
use App\Models\User;
use App\Models\Cruise;
use App\Models\AuditLog_Model;
//require  base_path() . '/vendor/paypal/sdk/src/Twilio/autoload.php';

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Carbon\Carbon;
use URL;
use Session;
use Config;
use Auth;
use App\Models\Admin\RegistrationContent;
use App\Models\Payment as ModelPayment;
use Mail;
use App\Mail\NewConventionRegistration;
use App\Jobs\NewConventionRegistrationJob;
use App\Mail\NewExhibitRegistration;
use App\Jobs\NewExhibitRegistrationJob;
use App\Mail\DonationPackageUpgradeNotification;
use App\Jobs\DonationPackageUpgradeNotificationJob;
use App\Models\Reservation;
use App\Models\PackageList;
use App\Mail\PaidPendingAmountNotification;
use App\Jobs\PaidPendingAmountNotificationJob;
use App\Jobs\NewCruiseRegistrationJob;

class PaymentController extends Controller
{
     public function __construct()
    {

        $settings = Config::get('paypal');

        $this->_api_context = new ApiContext(new OAuthTokenCredential(config('conventions.PAYPAL_SANDBOX_CLIENT_ID'), config('conventions.PAYPAL_SANDBOX_CLIENT_SECRET')));
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
    }



    public function PayWithPayPal($amount,$name, $payment_id, $return_route= 'payment.status')
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $userCurrency = 'USD';
        $pay_amount = $amount;
        $item_1->setName($name)
        ->setCurrency($userCurrency)
            ->setQuantity(1)
            ->setPrice($pay_amount);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($userCurrency)
            ->setTotal($pay_amount);
        $timestamp = Carbon::now()->toDateTimeString();

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setInvoiceNumber($timestamp . '_' . $payment_id)
            ->setItemList($item_list)
            ->setDescription('Transaction Details');


        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route($return_route))
        ->setCancelUrl(URL::route($return_route));
        $payment = new Payment();
        $payment->setIntent('Sale')
        ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            dd($ex);
            if (\Config::get('app.debug')) {
                return redirect()->back()->withError('Connection timeout');
            } else {
                return redirect()->back()->withError('Some error occur, sorry for inconvenient');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('payment_id', $payment_id);
        Session::put('resirect_rs', $name);

        if (isset($redirect_url)) {
            return redirect()->to($redirect_url);
        }
    }

    public function paypalStatus(Request $request)
    {
        $token = $request->get('token');
        $payerId = $request->get('PayerID');
        $payment_id = Session::get('paypal_payment_id');
        $ModelPayment = ModelPayment::find(Session::get('payment_id'));

        if (empty($payerId) || empty($token)) {
            $result = ['state' => 'User Cancelled'];
            $updateData = [
                'more_info' =>  json_encode($result),
                'payment_status' => "User Cancelled",
                'status'=> 0
            ];
            $ModelPayment->fill($updateData)->save();
            return redirect(url('/exhibits-reservation/'))->withErrors('Payment failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        // dd($result);exit;
        $donation = SponsorCategoryType::find($request->session()->get('sponsor_category'));
        // print_r($donation_id);
        Session::forget('paypal_payment_id');

        //echo('<pre>');print($p_order);exit;

        if ($result->getState() == 'approved' && $ModelPayment) {

            $transactions = $result->getTransactions();
           
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

            $updateData = [
               
                'unique_id_for_payment' => $order->getId(),
                'more_info' => $result->toarray(),
                
                'payment_status' => "Paid",
                'status' => 1
                //'order_date' => date("Y-m-d"),
            ];
            $ModelPayment->fill($updateData)->save();
            

          if(Session::get('resirect_rs') == "Exhibit Regestration"){
            
            $reserv=Reservation::find($ModelPayment->user_id);
            $Paymenttype=Paymenttype::find($ModelPayment->payment_methord);
            $PackageList=PackageList::where('package_id',$reserv->package_id)->get();
            //Mail::to($reserv->email)->bcc(['vendorsexhibits@ttaconvention.org','naresh.chintala2012@gmail.com'] )->send(new NewExhibitRegistration($reserv,$Paymenttype,$PackageList));

            $details = array('email'=>$reserv->email,'user'=>$reserv,'payment'=>$Paymenttype,'cc'=>['vendorexhibits@ttaconvention.org','naresh.chintala2012@gmail.com'],'packagesList'=>$PackageList);
                NewExhibitRegistrationJob::dispatchNow($details);
            
            return redirect(url('registration-success')."?id=".$ModelPayment->id);

          }else{
            $id = \Auth::user()->id;
            $user = User::find($id);
            return redirect(url('myaccount'));
          }

            
        } else {
            $updateData = [
                'more_info' => $result,
                'status' => 0
            ];

            $donation->fill($updateData)->save();
            return redirect(url('/exhibits-reservation/'))->withErrors("Payment failed");
        }

    }

    public function cruisestatus(Request $request)
    {
        $token = $request->get('token');
        $payerId = $request->get('PayerID');
        $payment_id = Session::get('paypal_payment_id');
        $cruise_id = Session::get('cruise_id');
        
        if (empty($payerId) || empty($token)) {
            $result = ['state' => 'User Cancelled'];
            Cruise::where('id',$cruise_id)->update(['payment_details'=>json_encode($result),
                'payment_status'=>"User Cancelled","status"=>0]);
            return redirect(url('/cruise_registration/'))->withErrors('Payment failed');
        }


            Cruise::where('id',$cruise_id)->update(['payment_details'=>$request->toarray(),
                'payment_status'=>"Paid","status"=>1]);
            $cruise = Cruise::where('id',$cruise_id)->first();
            //dd($cruise);
            $details = array('email'=>$cruise->email,"user_id"=>$cruise_id);
                NewCruiseRegistrationJob::dispatchNow($details);
            return redirect(url('cruise_registration'))->withSuccess('Registration Successful');

    }


    public function index()
    {
        $payer = new Payer();

        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $userCurrency = 'USD';
        $pay_amount = 1221;
        $item_1->setName('NRIVA Convention test')
        ->setCurrency($userCurrency)
            ->setQuantity(1)
            ->setPrice($pay_amount);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($userCurrency)
            ->setTotal($pay_amount);
        $timestamp = Carbon::now()->toDateTimeString();

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setInvoiceNumber($timestamp . '_' . '$donation->id')
            ->setItemList($item_list)
            ->setDescription('Transaction Details');


        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status'))
        ->setCancelUrl(URL::route('payment.status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

            if (\Config::get('app.debug')) {
                return redirect()->back()->withError('Connection timeout');
            } else {
                return redirect()->back()->withError('Some error occur, sorry for inconvenient');
            }

            // dd($input);

        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            return redirect()->to($redirect_url);
        }
    }


    public function status(Request $request)
    {
        ///donationform/status?paymentId=PAYID-L2GJHIQ52M352183J769814J&token=EC-8LY65310JP231322J&PayerID=MY6L2NR5JMRE6
        $token = $request->get('token');
        $payerId = $request->get('PayerID');
        $payment_id = Session::get('paypal_payment_id');
        $is_upgrade = Session::get('is_upgrade');
        $ModelPayment = ModelPayment::find(Session::get('payment_id'));
        
        if (empty($payerId) || empty($token)) {
            $result = ['state' => 'User Cancelled'];
            /* $updateData = [
                'response_message' => json_encode($result),
               // 'status' => 0
            ]; */
            $updateData = [
                //  'transaction_id' => $order->getId(),
                'more_info' =>  json_encode($result),
                'payment_status' => "User Cancelled",
                'status' => 0
                //'order_date' => date("Y-m-d"),
            ];
            $ModelPayment->fill($updateData)->save();
            return redirect(url('registration')."?page=regpage")->withErrors('Payment failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        // dd($result);exit;
        $donation = SponsorCategoryType::find($request->session()->get('sponsor_category'));
        // print_r($donation_id);
         Session::forget('paypal_payment_id');

        //echo('<pre>');print($p_order);exit;

        if ($result->getState() == 'approved' && $ModelPayment) {

            $transactions = $result->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

             $updateData = [
              //  'transaction_id' => $order->getId(),
               'unique_id_for_payment' => $order->getId(),
               'more_info' => $result->toarray(),
                'payment_status' => "Paid",
                'status' => 1
                //'order_date' => date("Y-m-d"),
            ];
            $ModelPayment->fill($updateData)->save();
            $id = \Auth::user()->id;
            $user = User::find($id);

            $SponsorCategory = SponsorCategory::where('id', session::get('sponsor_category'))->first();
            $SponsorCategoryType = SponsorCategoryType::where('id', session::get('category_id'))->first();

            
                $individual_registration = session::get('individual_registration');
                $total_amount = session::get('Individual_amount');
                $user->total_amount =$total_amount;
                $user->individual_registration =$individual_registration;
                $user->registration_amount =session::get('registration_amount');
                $user->donor_amount =session::get('donor_amount');
                $user->discount_amount =session::get('discount_amount');
             
           
                $user->sponsorship_category_id = $SponsorCategory->id??0;
                

           
                if( ($user->amount_paid+ $ModelPayment->payment_amount+session::get('discount_amount')) == $user->total_amount ){
                    $user->payment_status = 'Paid';
                }else{
                    $user->payment_status = 'Partial Paid';
                }
            $user->amount_paid = $user->amount_paid + $ModelPayment->payment_amount;
            $user->registration_type_id = $SponsorCategoryType->id??0;
            $user->save();
            $Paymenttype=Paymenttype::find($ModelPayment->payment_methord);
            //send reg suc mail
            
            if(!$is_upgrade){
                $user=User::where('email',$user->email)->first();
                //Mail::to($user->email)->bcc(['registration@ttaconvention.org','kirandrk@gmail.com'])->send(new NewConventionRegistration($user,$Paymenttype));
                $details = array('email'=>$user->email,'user'=>$user,'payment'=>$Paymenttype,'cc'=>['registration@ttaconvention.org','kirandrk@gmail.com']);
                NewConventionRegistrationJob::dispatchNow($details);

                return redirect(url('/myaccount'))->with('message-suc', 'Registration Completed Successfully');
            }else{
                $user=User::where('email',$user->email)->first();
               // Mail::to($user->email)->cc('registration@ttaconvention.org')->send(new DonationPackageUpgradeNotification($user,$Paymenttype));
                $details = array('email'=>$user->email,'user'=>$user,'payment'=>$Paymenttype);
            DonationPackageUpgradeNotificationJob::dispatchNow($details);
                return redirect(url('/myaccount'))->with('message-suc', 'Upgrade Successfully');   
            }

            
        } else {
            $updateData = [
                'more_info' => $result,
                'status' => 0
            ];

            $donation->fill($updateData)->save();
            return redirect(url('/bookticket/'))->withErrors("Payment failed");
        }

    }

    public function payPendingAmount()
    {
        $RegistrationContent = RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status', 1)->get();
        return view('pendingAmountPay', compact('RegistrationContent','paymenttypes'));
    }

    public function storePayPendingAmount(Request $request)
    {
        $Paymenttype = Paymenttype::find($request->payment_type);
        $user = User::where('email', $request->email)->first();
        $paying_amount =$request->pending_amount;

        $user = \Auth::user();
        // call payment methord
        
        if ($Paymenttype->name == Config('conventions.paypal_name_db')) {
            return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id, true);
        } else {
            // PayWithFirstDate($user_id, $paying_amount, $Paymenttype_id, $payment_made_towards, $request)

            if ($Paymenttype->name == Config('conventions.first_data_name_db')) {
                $payment= App(PaymentController::Class)->PayWithFirstDate($user->id, $paying_amount, $Paymenttype->id, 'Partial payment to full payment', $request);
    
                if($payment->payment_status=='pending'){
                    $msg_fin=null;
                    foreach($payment->more_info['Error']['messages'] as $msg){
                        if($msg_fin!=null){
                            $msg_fin=$msg_fin.', '.$msg['description'];
                        }else{
                            $msg_fin=$msg['description'];
                        }
                    }
                    return redirect()->back()->withErrors($msg_fin);  
                }
    
            }

            $this->Dopayment($request,  $user,  $paying_amount);
            $user->payment_status = 'Paid';
            $user->amount_paid = $user->amount_paid + $paying_amount;
            $user->save();
            
           // Mail::to($user->email)->send(new PaidPendingAmountNotification($user,$Paymenttype));

             $details = array('email'=>$user->email,'user'=>$user,'payment'=>$Paymenttype);
            PaidPendingAmountNotificationJob::dispatchNow($details);

            return redirect('myaccount');
        }

    }

    public function PaypalPayment($amount, $user_id, $request,$payment_type_id,$payingPendingAmount=false)
    {
        $settings = Config::get('paypal');

        $this->_api_context = new ApiContext(new OAuthTokenCredential(config('conventions.PAYPAL_SANDBOX_CLIENT_ID'), config('conventions.PAYPAL_SANDBOX_CLIENT_SECRET')));

        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
        $payer = new Payer();

        if($payingPendingAmount){
            $redrict_url_status= 'pendingpayment.status';
        }else{
            $redrict_url_status='payment.status';
        }

        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $userCurrency = 'USD';
        $pay_amount = $amount;
        $item_1->setName('NRIVA Convention test')
            ->setCurrency($userCurrency)
            ->setQuantity(1)
            ->setPrice($pay_amount);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($userCurrency)
            ->setTotal($pay_amount);
        $timestamp = Carbon::now()->toDateTimeString();

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setInvoiceNumber($timestamp )
            ->setItemList($item_list)
            ->setDescription('Transaction Details');


        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route($redrict_url_status))
            ->setCancelUrl(URL::route($redrict_url_status));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
            $temp = [
                'user_id' => $user_id,
                'payment_amount' => $pay_amount,
                'payment_methord' => $payment_type_id,
                'unique_id_for_payment' =>  $payment->id,
                'more_info' => $request->more_info,
                'payment_status' => 'Pending',
                'account_status' => 'Partial payment to full payment',
                'payment_made_towards' => "Convention Registration"
                
            ];
            $ModelPayment = ModelPayment::create($temp);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

            if (\Config::get('app.debug')) {
                return redirect()->back()->withError('Connection timeout');
            } else {
                return redirect()->back()->withError('Some error occur, sorry for inconvenient');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if($request->has('sponsor_category')){
            Session::put('sponsor_category', $request->sponsor_category);
        }
        Session::put('payment_id', $ModelPayment->id);
        if (isset($redirect_url)) {
            return redirect($redirect_url);
        }
    }


    public function Dopayment($request, $user,  $paying_amount)
    {
        $Paymenttype = Paymenttype::find($request->payment_type);

        if ($Paymenttype->name == Config('conventions.check_name_db')) {
            $more_info = [
                'cheque_date' => $request->cheque_date,
                'handed_over_to' => $request->more_info,
            ];
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->cheque_number,
                'more_info' => $more_info,
                'payment_status' => 'Pending',
                'account_status' => 'Partial payment to full payment',
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);
        }
        if ($Paymenttype->name == Config('conventions.zelle_name_db')) {
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->Zelle_Reference_Number,
                'more_info' => $request->more_info,
                'payment_status' => 'Inprocess',
                'account_status' => 'Partial payment to full payment',
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);
        }

        if ($Paymenttype->name == Config('conventions.other_name_db')) {
            $more_info = [
                'transaction_date' => $request->transaction_date,
                'Payment_made_through' => $request->Payment_made_through,
                'company_name' => $request->company_name,
            ];
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->transaction_id,
                'more_info' => $more_info,
                'payment_status' => 'Inprocess',
                'account_status' => 'Partial payment to full payment',
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);
        }
    }

    public function pendingstatus(Request $request)
    {
        ///donationform/status?paymentId=PAYID-L2GJHIQ52M352183J769814J&token=EC-8LY65310JP231322J&PayerID=MY6L2NR5JMRE6
        $input = $request->input();
        $paymentId = $request->get('paymentId');
        $token = $request->get('token');
        $payerId = $request->get('PayerID');

        $payment_id = Session::get('paypal_payment_id');
        $ModelPayment = ModelPayment::find(Session::get('payment_id'));
        if (empty($payerId) || empty($token)) {
            $result = ['state' => 'User Cancelled'];
            /*    $updateData = [
                'response_message' => json_encode($result),
                'status' => 0
            ]; */
            $updateData = [
                //  'transaction_id' => $order->getId(),
                
                'more_info' =>  json_encode($result),
                'payment_status' => "User Cancelled",
                //'order_date' => date("Y-m-d"),
            ];
            $ModelPayment->fill($updateData)->save();
            //$donation->fill($updateData)->save();
            return redirect(url('/bookticket/'))->withErrors('Payment failed');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        // dd($result);exit;
        $donation = SponsorCategoryType::find($request->session()->get('sponsor_category'));
        // print_r($donation_id);
        Session::forget('paypal_payment_id');

        //echo('<pre>');print($p_order);exit;
        if ($result->getState() == 'approved' && $ModelPayment) {

            $transactions = $result->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $order = $relatedResource->getSale();

            $updateData = [
                //  'transaction_id' => $order->getId(),
                'unique_id_for_payment' => $order->getId(),
                'more_info' =>   $result->toarray(),
                 'payment_status' => "Paid",
                 'status' => 1,
                //'order_date' => date("Y-m-d"),
            ];
            $ModelPayment->fill($updateData)->save();

            $id = \Auth::user()->id;
            $user = User::find($id);

            $SponsorCategory = SponsorCategory::where('id', session::get('sponsor_category'))->first();
            $SponsorCategoryType = SponsorCategoryType::where('id', session::get('category_id'))->first();
            if (($user->amount_paid + $ModelPayment->payment_amount) == $user->total_amount) {
                $user->payment_status = 'Paid';
            } else {
                $user->payment_status = 'Partial Paid';
            }
            $user->amount_paid = $user->amount_paid + $ModelPayment->payment_amount;
            $user->save();

            $Paymenttype=Paymenttype::find($ModelPayment->payment_methord);
            //Mail::to($user->email)->send(new PaidPendingAmountNotification($user,$Paymenttype));
            $details = array('email'=>$user->email,'user'=>$user,'payment'=>$Paymenttype);
            PaidPendingAmountNotificationJob::dispatchNow($details);

            return redirect(url('myaccount'));
        } else {
            $updateData = [
                'response_message' => $result,
                'status' => 0
            ];

            $donation->fill($updateData)->save();
            return redirect(url('/bookticket/'))->withErrors("Payment failed");
        }

    }


    public function PayWithCheck($user_id, $paying_amount, $Paymenttype_id, $payment_made_towards, $request)
    {
        $more_info = [
            'cheque_date' => $request->cheque_date,
            'handed_over_to' => $request->more_info,
        ];
            $temp = [
                'user_id' => $user_id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype_id,
                'unique_id_for_payment' => $request->cheque_number,
                'more_info' => $more_info,
                'payment_status' => 'Pending',
                'payment_made_towards' => $payment_made_towards,
               // 'account_status' => $account_status
            ];
            return ModelPayment::create($temp);
    }

    public function PayWithZelle($user_id, $paying_amount, $Paymenttype_id, $payment_made_towards, $request)
    {
        $temp = [
            'user_id' => $user_id,
            'payment_amount' => $paying_amount,
            'payment_methord' => $Paymenttype_id,
            'unique_id_for_payment' => $request->Zelle_Reference_Number,
            'payment_status' => 'Inprocess',
            'payment_made_towards' => $payment_made_towards,
        ];
        return ModelPayment::create($temp);
    }

    public function PayWithOther($user_id, $paying_amount, $Paymenttype_id, $payment_made_towards, $request)
    {

        $more_info = [
            'transaction_date' => $request->transaction_date,
            'Payment_made_through' => $request->Payment_made_through,
            'company_name' => $request->company_name,
        ];
        $temp = [
            'user_id' => $user_id,
            'payment_amount' => $paying_amount,
            'payment_methord' => $Paymenttype_id,
            'unique_id_for_payment' => $request->transaction_id,
            'more_info' => $more_info,
            'payment_status' => 'Inprocess',
            'payment_made_towards' => $payment_made_towards,
        ];
        return ModelPayment::create($temp);
         
    }

    
    public function PayWithFirstDate($user_id, $paying_amount, $Paymenttype_id, $payment_made_towards, $request,$account_status=null,$type=false)
    {

        $client = new \Payeezy_Client();
        $client->setApiKey(config('conventions.FIRSTDATA_CLIENT_ID'));
        $client->setApiSecret(config('conventions.FIRSTDATA_CLIENT_SECRET'));
        $client->setMerchantToken(config('conventions.FIRSTDATA_MERCHANT_TOKEN'));
        $client->setUrl(config('conventions.FIRSTDATA_SITE_URL'));

        $card_transaction = new \Payeezy_CreditCard($client);

        $response = $card_transaction->purchase([
        "merchant_ref" => $payment_made_towards.' - user_id-'.$user_id,
        "transaction_type"=>'purchase',
        "amount" => $paying_amount*100,
        "currency_code" => "USD",
        "credit_card" => array(
            "type" => $request->type,
            "cardholder_name" => $request->cardholder_name,
            "card_number" => $request->card_number,
            "exp_date" => $request->exp_date,
            "cvv" => $request->cvv
        ),
        "billing_address"=>array(
            "city" => $request->city,
            "country" => $request->country,
            "email" => $request->email,
            "phone" => array(
            "type" => "+1",
            "number" => $request->mobile
                ),
            "street" => $request->address,
            "state_province" => $request->state,
            "zip_postal_code" => $request->zip_code,
        )
        ]);
     
        if(isset($response->transaction_status) && $response->transaction_status=="approved"){
            $transaction_id=$response->transaction_id;
            $payment_status='paid';
            $status=1;
        }else{
            $transaction_id=null;
            $status=0;
            $payment_status='pending';
        }
        if($type){

            if(isset($response->transaction_status) && $response->transaction_status=="approved"){
            Cruise::where('id',$user_id)->update(['payment_details'=>json_encode($response),
                'payment_status'=>"Paid","status"=>1]);
            }else{
                
                Cruise::where('id',$user_id)->update(['payment_details'=>json_encode($response),
                'payment_status'=>"Pending","status"=>0]);
            }

            $temp = [
            'payment_amount' => $paying_amount,
            'payment_methord' => $Paymenttype_id,
            'unique_id_for_payment' =>  $transaction_id,
            'more_info' => $response,
            'status' => $status,
            'payment_status' => $payment_status,
            'account_status' => $response->transaction_status??'',
            'validation_status'=>$response->validation_status??'',
            'payment_made_towards' => $payment_made_towards
            
        ];

        return $temp;

        }else{
        $temp = [
            'user_id' => $user_id,
            'payment_amount' => $paying_amount,
            'payment_methord' => $Paymenttype_id,
            'unique_id_for_payment' =>  $transaction_id,
            'more_info' => $response,
            'status' => $status,
            'payment_status' => $payment_status,
            'account_status' => $response->transaction_status??'',
            'validation_status'=>$response->validation_status??'',
            'payment_made_towards' => $payment_made_towards
            
        ];
           return ModelPayment::create($temp);
       }

    }
    public function first_data_transactions($value='')
    {
        // code...
    }

    
    public function updatePaymentStatus(Request $request, $payment_id)
    {
        $result=0;
        $payment=ModelPayment::find($payment_id);
       
            if($payment->payment_made_towards=='Convention Registration'){
                 $statusChangeEmail=\Cookie::get('ConloginUserEmail');
                 $reg_type="Convention Registration";

            }else{
                $statusChangeEmail=\Cookie::get('ExbhitloginUserEmail');
                $reg_type="Exbhit Registration";
            }
            if(Auth::user()){
                $statusChangeEmail=Auth::user()->email;
            }
            //dd($payment);
            AuditLog_Model::create([
                'registration_id'=>$payment->id,
                'convention_id'=>$payment->userDetails->registration_id??'',
                'user_name'=>$payment->userDetails->name??'',
                'user_email'=>$payment->userDetails->email??'',
                'old_status'=>$payment->payment_status,
                'new_status'=>$request->status,
                'reg_type'=>$reg_type,
                'more_info'=>"Payment Status Change",
                'statusChanged_by'=>$statusChangeEmail
            ]); 

        $payment_main=$payment;
        if($request->status=='Paid'){
            $payment->payment_status='Paid';
            $payment->status=1;
            $payment->save();
            $result=1;
        }else{
            $payment->payment_status=$request->status;
            $payment->status=0;
            $payment->save();
            $result=1;
        }

        

        //updating payment status in user table if all payments are paid
        $allPayments=ModelPayment::where('payment_made_towards','Convention Registration')->where('user_id',$payment_main->user_id)->get();
        $status='Update';
        foreach ($allPayments as $allPayment ) {
            if($allPayment->status==0){
                $status='do not Update';
                break;
            }
        }
       
       
        if(($status=='Update') && (count($allPayments)>0)){
            User::where('id',$payment_main->user_id)->update([
                'payment_status'=>'Paid'
            ]);
        }

        if(count($allPayments)==0){
            User::where('id',$payment_main->user_id)->update([
                'payment_status'=>'Not Initiated'
            ]); 
        }


        if((count($allPayments)>0)  &&  ($status=='do not Update') ){
            User::where('id',$payment_main->user_id)->update([
                'payment_status'=>'Pending'
            ]); 
        }


        return $result;

    }
    public function updatecruisepaymentStatus(Request $request, $id)
    {
        return Cruise::where('id',$id)->update(['payment_status'=>$request->status]);
    }

    

}
