<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommitteStoreRequest;
use App\Http\Requests\CommitteUpdateRequest;
use App\Models\Admin\Committe;
use Illuminate\Http\Request;

class CommitteController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $committes = Committe::all();

        return view('admin.master.committe.index', compact('committes'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.master.committe.create');
    }

    /**
     * @param \App\Http\Requests\CommitteStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommitteStoreRequest $request)
    {
        $committe = Committe::create($request->validated());
        $file_name = 'committe' . '_' . $committe->id . '.' . $request->image->getClientOriginalExtension();
        UploadFile(config('conventions.committe_upload'), $request->file('image'), $file_name);
        $committe->image_url=$file_name;
        $committe->save();

        $request->session()->flash('committe.id', $committe->id);

        return redirect()->route('committe.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Committe $committe
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Committe $committe)
    {
        return view('admin.master.committe.show', compact('committe'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Committe $committe
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Committe $committe)
    {
        return view('admin.master.committe.edit', compact('committe'));
    }

    /**
     * @param \App\Http\Requests\CommitteUpdateRequest $request
     * @param \App\Models\Admin\Committe $committe
     * @return \Illuminate\Http\Response
     */
    public function update(CommitteUpdateRequest $request, Committe $committe)
    {
         $committe->update($request->validated());

        if ($request->hasfile('image')) {
            $file_name = 'committe' . '_' . $committe->id . '.' . $request->image->getClientOriginalExtension();
            $request->file('image')->storeAs(config('conventions.committe_upload'), $file_name);
            Committe::where('id', $committe->id)->update(['image_url' => $file_name]);
        }

        $request->session()->flash('committe.id', $committe->id);

        return redirect()->route('committe.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Committe $committe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Committe $committe)
    {
        return $committe->delete();

        return redirect()->route('committe.index');
    }

    public function updateStatus(Request $request)
    {
        return Committe::where('id', $request->id)->update(['status' => $request->status]);
    }
}
