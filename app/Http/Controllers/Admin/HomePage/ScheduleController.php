<?php

namespace App\Http\Controllers\Admin\HomePage;

use App\Http\Controllers\Controller;
use App\Http\Requests\ScheduleStoreRequest;
use App\Http\Requests\ScheduleUpdateRequest;
use App\Models\Admin\Event;
use App\Models\Admin\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScheduleController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /* $schedules= Event::with(['schedules' =>function($q){
                $q->groupBy('date');
        }])->get(); */

        $schedules = Schedule::select('tta_2022_schedules.*', DB::raw('count(*) as total'))
        ->with('event')
        ->groupby('event_id', 'date')
        ->get();
        return view('admin.homePageContentUpdate.schedule.index', compact('schedules'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $events = Event::all();
        return view('admin.homePageContentUpdate.schedule.create', compact('events'));
    }

    /**
     * @param \App\Http\Requests\ScheduleStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScheduleStoreRequest $request)
    {
        foreach($request->from_time as $key => $value) {
            $temp= [
                'event_id'=> $request->event_id,
                'date'=> $request->date,
                'from_time'=> $request->from_time[$key],
                'to_time'=> $request->to_time[$key],
                'program_name'=> $request->program_name[$key],
                'room_no'=> $request->room_no[$key],
             ];
            $schedule = Schedule::create($temp);
        }
        $request->session()->flash('schedule.id', $schedule->id);
        return redirect()->route('schedule.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Schedule $schedule)
    {
        return view('admin.homePageContentUpdate.schedule.show', compact('schedule'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Schedule $schedule)
    {
        $events = Event::all();
        $schedules = Schedule::where('event_id', $schedule->event_id)
        ->where('date', $schedule->date)
        ->get();
        return view('admin.homePageContentUpdate.schedule.edit', compact('schedules', 'events'));
    }

    /**
     * @param \App\Http\Requests\ScheduleUpdateRequest $request
     * @param \App\schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(ScheduleUpdateRequest $request, Schedule $schedule)
    {

        foreach ($request->schedule_id as $key => $value) {
            $temp = [
                'event_id' => $request->event_id,
                'date' => $request->date,
                'from_time' => $request->from_time[$key],
                'to_time' => $request->to_time[$key],
                'program_name' => $request->program_name[$key],
                'room_no' => $request->room_no[$key],
            ];
            $schedule = Schedule::find($request->schedule_id[$key]);
            $schedule->update($temp);
        }


        $request->session()->flash('schedule.id', $schedule->id);

        return redirect()->route('schedule.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Schedule $schedule)
    {
        return  $schedule->delete();
    }

    public function getScheduleDatails($id)
    {
        $temp=Schedule::find($id);
        return Schedule::where('date', $temp->date)->where('event_id', $temp->event_id)->get();
    }
}
