<?php

namespace App\Http\Controllers\Admin\HomePage;

use App\Http\Controllers\Controller;
use App\Http\Requests\DonorStoreRequest;
use App\Http\Requests\DonorUpdateRequest;
use App\Models\Admin\Donor;
use App\Models\Admin\Donortype;
use Illuminate\Http\Request;

class DonorController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $donors = Donor::all();

        return view('admin.homePageContentUpdate.donor.index', compact('donors'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $donorTypes = Donortype::where('status',1)->get();
        return view('admin.homePageContentUpdate.donor.create', compact('donorTypes'));
    }

    /**
     * @param \App\Http\Requests\DonorStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonorStoreRequest $request)
    {
        $donor = Donor::create($request->validated());

        if ($request->hasFile('image')) {
            $file_name = 'donor' . '_' . $donor->id . '.' . $request->image->getClientOriginalExtension();
            $request->file('image')->storeAs(config('conventions.donor_upload'), $file_name);
        } else {
            $file_name = null;
        }

        $donor->image_url = $file_name;
        $donor->save();

        $request->session()->flash('donor.id', $donor->id);

        return redirect()->route('donor.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Donor $donor
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Donor $donor)
    {

        return view('admin.homePageContentUpdate.donor.show', compact('donor'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Donor $donor
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Donor $donor)
    {
        $donorTypes = Donortype::where('status', 1)->get();
        return view('admin.homePageContentUpdate.donor.edit', compact('donor', 'donorTypes'));
    }

    /**
     * @param \App\Http\Requests\DonorUpdateRequest $request
     * @param \App\Models\Admin\Donor $donor
     * @return \Illuminate\Http\Response
     */
    public function update(DonorUpdateRequest $request, Donor $donor)
    {
        $donor->update($request->validated());

        if ($request->hasFile('image')) {
            $file_name = 'donor' . '_' . $donor->id . '.' . $request->image->getClientOriginalExtension();
            $request->file('image')->storeAs(config('conventions.donor_upload'), $file_name);
        } else {
            $file_name = null;
        }

        $donor->image_url = $file_name;
        $donor->save();

        $request->session()->flash('donor.id', $donor->id);

        return redirect()->route('donor.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Donor $donor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Donor $donor)
    {
        if (file_exists(config('conventions.donor_display') . $donor->image_url)) {
            unlink(config('conventions.donor_display') . $donor->image_url);
        }
        return  $donor->delete();
    }

    public function updateStatus(Request $request)
    {
        return Donor::where('id', $request->id)->update(['status' => $request->status]);
    }

    public function getDonorsWithTypeId($id)
    {
        return Donor::where('donortype_id',$id)->take(8)->get();
    }
}
