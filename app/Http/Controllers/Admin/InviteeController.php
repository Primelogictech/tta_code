<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\InviteeStoreRequest;
use App\Http\Requests\InviteeUpdateRequest;
use App\Models\Admin\Invitee;
use Illuminate\Http\Request;

class InviteeController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $invitees = Invitee::all();

        return view('admin.master.invitee.index', compact('invitees'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.master.invitee.create');
    }

    /**
     * @param \App\Http\Requests\InviteeStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(InviteeStoreRequest $request)
    {
        $invitee = Invitee::create($request->validated());

        $request->session()->flash('invitee.id', $invitee->id);

        return redirect()->route('invitee.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Invitee $invitee
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Invitee $invitee)
    {
        return view('admin.master.invitee.show', compact('invitee'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Invitee $invitee
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Invitee $invitee)
    {
        return view('admin.master.invitee.edit', compact('invitee'));
    }

    /**
     * @param \App\Http\Requests\InviteeUpdateRequest $request
     * @param \App\Models\Admin\Invitee $invitee
     * @return \Illuminate\Http\Response
     */
    public function update(InviteeUpdateRequest $request, Invitee $invitee)
    {
        $invitee->update($request->validated());

        $request->session()->flash('invitee.id', $invitee->id);

        return redirect()->route('invitee.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Admin\Invitee $invitee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Invitee $invitee)
    {
        return $invitee->delete();

    }

    public function updateStatus(Request $request)
    {
        return Invitee::where('id', $request->id)->update(['status' => $request->status]);
    }
}
