<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Otp_model;
use App\Models\Registration;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mail;
use DB;
use App\Mail\SouvenirRegistrationNotification;

class SouvenirRegistrationController extends Controller
{
    public function souvenirRegestration()
    {
        $states=DB::table('tta_2022_states')->get();
        $user=Auth::user();
        if($user){
            $user = Registration::where('email',$user->email)->first();
        }
        return view('registration.showSouvenirRegistrationForm', compact('states','user'));
    }

    public function email_verification(Request $request)
    {

        $user = User::where("email",$request->email)->first();

        if(empty($user)){
            $otp = rand(10000,99999);
            
        Mail::raw("Hi, your email verification code for Souvenir registration is : $otp", function ($message) use ($request) {
          $message->to($request->email)
            ->subject("Email Verification code from Souvenir");
        });
         Otp_model::updateOrCreate(['user'=>$request->email],['otp'=>$otp, 'status'=>1]);
            return "success";
        }else{
            return "failed";
        }
    }


    public function new_registertion(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255|unique:users',
            'otp'=>'required|exists:tta_2022_otp_verification,otp'
        ]);

        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

       $exist = \DB::table('tata_users')->where('email_id',$request->email)->first();
        $user->member_id = $user->id;
        $user->first_name = $exist->first_name??'';
        $user->last_name = $exist->last_name??'';
        $user->address = $exist->street??'';
        $user->city = $exist->city??'';
        $user->state = $exist->state??'';
        $user->country = $exist->country??'';
        $user->spouse_full_name = $exist->spouse_first_name??'';
        $user->zip_code = $exist->zipcode??'';
        $user->mobile = $exist->mobile_number??'';
       
        $user->registration_id = "CON".sprintf("%04d", $user->id);
        $user->save();

        event(new Registered($user));

        Auth::login($user);
        return redirect('souvenir-reservation')->with('success','You have Authenticated successfully. Please continue to registration');
    }

    public function storesouvenirRegestrationForm(Request $request)
    {
        $user=User::where('email',$request->email)->first();
        $Registration=Registration::where('email',$request->email)->first();
        
        if ($request->hasfile('article')) {
            $image=$request->article;
            $article =  $request->first_name.'_'.$user->id . '.' . $image->getClientOriginalExtension();
            $image->storeAs(config('conventions.souvenir_artical_upload'), $article);
        }else{
            $article =   $Registration->extra_data['article']??"";
        }
        
        if($request->hasfile('passport')) {
            $image=$request->passport;
            $passport =  $request->first_name.'_'.$user->id . '.' . $image->getClientOriginalExtension();
            $image->storeAs(config('conventions.souvenir_passport_upload'), $passport);
        }else{
            $passport =  $Registration->extra_data['passport']??"";
        }

        if($request->sponsor_type=='Other'){
                $TypeofArticle=$request->other_text;
            }else{
                $TypeofArticle=$request->sponsor_type;
        }

        $extra_data= [
            'latest_published_date'=> $request->latest_published_date,
            'latest_published_platform'=> $request->latest_published_platform,
            'your_biography'=> $request->your_biography,

            'first_name'=> $request->first_name,
            'last_name'=> $request->last_name,
            'pen_name'=> $request->pen_name,
            'passport'=> $passport,
            'sponsor_type'=> $TypeofArticle,
            'article'=> $article??"",
            'city'=> $request->city,
            'state'=> $request->state,
            'zipcode'=> $request->zipcode,
            'country'=> $request->country,
        ];

       

        $reg=Registration::updateOrCreate(['email'=>$request->email],[
            'full_name' =>  $request->first_name .' ' .$request->last_name,
            'user_id' =>  $user->id,
            'phone_no' => $request->phone,
            'extra_data' => $extra_data,
            'registration_type_name' => 'Souvenir',
        ]);
        if($reg->wasRecentlyCreated){
            Mail::to($request->email)->send(new SouvenirRegistrationNotification($reg->full_name));
        }


        return redirect('myaccount')->with('success','You have successfully Registered / Updated for Souvenir.');

    }
}
