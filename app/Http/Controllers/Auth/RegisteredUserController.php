<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use App\Models\Otp_model;
use Mail;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'mobile' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'otp'=>'required|exists:tta_2022_otp_verification,otp',
            'password' => ['required', 'confirmed'],
        ]);

        $user = User::create([
            'first_name' => $request->firstname,
            'last_name' => $request->lastname,
            'name' => $request->firstname.' '.$request->lastname,
            'mobile' => $request->mobile,
            'email' => $request->email,
            
            'password' => Hash::make($request->password),
        ]);

      
        $user->member_id = $user->id;
         $user->registration_id = "CON".sprintf("%04d", $user->id);
        $user->save();

        event(new Registered($user));

        Auth::login($user);

        return redirect('bookticket');
    }
    public function send_verify_email(Request $request)
    {

        $otp = rand(10000,99999);
        try{
        Mail::raw("Hi, welcome user! Your Otp is $otp", function ($message) use ($request) {
          $message->to($request->email)
            ->subject("Otp Verification from TTA");
        });
         Otp_model::updateOrCreate(['user'=>$request->email],['otp'=>$otp, 'status'=>1]);
        return "success";
        }catch(Exception $e){
             return $e->message();
        }
    }

    public function new_registertion(Request $request)
    {
        $request->validate([
           'email' => 'required|string|email|max:255|unique:users',
            'otp'=>'required|exists:tta_2022_otp_verification,otp']);

        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

       $exist = \DB::table('tata_users')->where('email_id',$request->email)->first();


      
        $user->member_id = $user->id;
        $user->first_name = $exist->first_name??'';
        $user->last_name = $exist->last_name??'';
        $user->address = $exist->street??'';
        $user->city = $exist->city??'';
        $user->state = $exist->state??'';
        $user->country = $exist->country??'';
        $user->spouse_full_name = $exist->spouse_first_name??'';
        $user->zip_code = $exist->zipcode??'';
        $user->mobile = $exist->mobile_number??'';
       
         $user->registration_id = "CON".sprintf("%04d", $user->id);
        $user->save();

        event(new Registered($user));

        Auth::login($user);

        return redirect('registration?page=regpage')->with('success','You have Authenticated successfully. Please continue to convention registration');
    }

    public function update_password(Request $request)
    {
        $request->validate([
            
            'email' => 'required|string|email|max:255',
            'otp'=>'required|exists:tta_2022_otp_verification,otp',
            'password' => ['required', 'confirmed'],
        ]);

         User::where('email',$request->email)->update(['password' => Hash::make($request->password)
        ]);
        $user =User::where('email',$request->email)->first();
         Auth::login($user);
         return redirect('/');

    }
}
