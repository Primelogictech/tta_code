<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use App\Models\User;
use Session;
use Illuminate\Support\Facades\Validator;


class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //https://nriva.apttechsol.com/public/api/check-credientials
      //  $response = Http::get(env('NRIVA_URL') . '/api/get-check-credientials?email=' . $request->email);
      // $response = Http::post(env('NRIVA_URL') . '/api/check-credientials', [
      //     'email' =>  $request->email,
      //     'password' => $request->password,
      //   ]);
      //   $is_newUser = false;
      //   $body = (string) $response->getBody();
      //   $res=preg_replace("/\r|\n/", "", $body);
      //    if($res== 'success'){
      //       $response = Http::get(env('NRIVA_URL') . '/api/get-details?email=' . $request->email);
      //       $response=$response->json();
      //       $user= User::where('email', $response['email'])->first();
      //           if(!isset($user)){
      //               $this->createUser($response);
      //               $is_newUser=true;
      //               $user = User::where('email', $response['email'])->first();
      //               $user->member_id = $response['member_id'];
      //                $user->registration_id = "CON".sprintf("%04d", $user->id);
      //               $user->save();
      //           }
            //Session::put('user', $user);

          $this->validate($request, [
        'email'   => 'required|email',
         'password' => 'required_without:login_otp',
         'login_otp' => 'required_without:password',
      ]);
      if( (!empty($request->login_otp))  && ($request->login_with=='with_otp')  ){
         $user=User::where('email',$request->email)
                ->where('login_otp',$request->login_otp)->first();
        
        if($user){
            Auth::login($user);
            $request->session()->regenerate();
            $user->login_otp=null;
            $user->save();
            if($user->email== 'admin@ttaconvention.org'){
                return redirect('admin/registrations');
            }else{
                 return redirect()->route('myaccount');
            }
        }else{
            return redirect()->route('login')->with('error','Invalid Login Details'); 
        }
      }elseif(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();

            Auth::login($user);
            $request->session()->regenerate();

           
                    if($user->email== 'admin@ttaconvention.org'){
                        return redirect('admin/registrations');
                    }else{
                         return redirect()->route('myaccount');
                    }
        }else {
           return redirect()->route('login')->with('error','Invalid Login Details');
        }



    }

    public function createUser($response)
    {
        $data = [
            'email' => $response['email'],
            'first_name' => $response['first_name'],
            'last_name' => $response['last_name'],
            'spouse_full_name' => '',
            'phone_code' => $response['phone_code'],
            'mobile' => $response['mobile'],
            'country' => $response['member']['country'],
            'state' => $response['member']['state'],
            'city' => $response['member']['city'],
            'zip_code' => $response['member']['zipcode'],
            'address' => $response['member']['address'],
            'address2' => $response['member']['address2'],
        ];
        $user = User::create($data);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
