<?php

namespace App\Http\Controllers;

use App\Models\Admin\SponsorCategory;
use App\Models\Admin\SponsorCategoryType;
use App\Models\Admin\RegistrationContent;
use App\Http\Controllers\Admin\PaymentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Str;

use App\Models\Admin\ExhibitorType;
use App\Models\Admin\Donortype;
use App\Http\Controllers\Controller;
use App\Models\Admin\Paymenttype;
use App\Models\Admin\BannerImages;
use App\Models\Admin\Vouchers;
use App\Models\User;
use App\Models\Cruise;
use App\Models\Reservation;
use App\Models\PackageList;
use App\Models\Registration;
use App\Models\AuditLog_Model;
use App\Models\Payment as ModelPayment;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use App\Models\Admin\Venue;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Carbon\Carbon;
use URL;
use Session;
use Config;
use DB;
use Excel;
use App\Models\Otp_model;
use Mail;
use App\Mail\NewConventionRegistration;
use App\Mail\NewExhibitRegistration;
use App\Mail\DonationPackageUpgradeNotification;
use App\Jobs\OTPEmailJob;
use App\Jobs\NewConventionRegistrationJob;
use App\Jobs\NewCruiseRegistrationJob;
use App\Jobs\NewExhibitRegistrationJob;
use App\Jobs\DonationPackageUpgradeNotificationJob;
use App\Imports\UsersImport;
use File;
class RegistrationController extends Controller
{

    public function __construct()
    {
        $this->_api_context = "";
    }


    public function ShowRegistrationForm()
    {
        session::forget('payment_id');
        session::forget('sponsor_category');
        session::forget('category_id');
        session::forget('Individual_amount');
        session::forget('is_upgrade');
        if(Auth::user()){
        $user=User::find(Auth::user()->id);

         if(!empty($user->sponsorship_category_id) || !empty($user->individual_registration)){
                return redirect('myaccount');
            }
        }
        else{
            $user=array();
        }
        $SponsorCategoryTypes= SponsorCategoryType::where('status',1)->get();
        $Individuals= SponsorCategory::where('status',1)->where('donor_type_id', 0)->get();
        $donors= SponsorCategory::with('donortype')->where('status',1)->where('donor_type_id','!=' ,0)->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttype = Paymenttype::where('status',1)->orderBy('created_at',"asc")->get();
        $states=DB::table('tta_2022_states')->get();
       // dd($donors[0]->benfits[0]->name);
        // return view('registration',compact("SponsorCategoryTypes", 'Individuals', 'donors', 'RegistrationContent', 'user', 'paymenttype', 'states'));
        return view('committees.registration_committee',compact("SponsorCategoryTypes", 'Individuals', 'donors', 'RegistrationContent', 'user', 'paymenttype', 'states'));
    }
//
    public function RegesterUserStore(Request $request)
    {   

        if(!Auth::user()){

            $user = user::where('email',$request->email)->first();
            if(!$user){

            $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

       $exist = \DB::table('tata_users')->where('email_id',$request->email)->first();
        $user->member_id = $user->id;
        $user->first_name = $exist->first_name??'';
        $user->last_name = $exist->last_name??'';
        $user->address = $exist->street??'';
        $user->city = $exist->city??'';
        $user->state = $exist->state??'';
        $user->country = $exist->country??'';
        $user->spouse_full_name = $exist->spouse_first_name??'';
        $user->zip_code = $exist->zipcode??'';
        $user->mobile = $exist->mobile_number??'';
       
        $user->registration_id = "CON".sprintf("%04d", $user->id);
        $user->save();

         }
         Auth::login($user);

    }

        if ($request->hasfile('benfitimage')) {
                $rules= [
                    'benfitimage.*' => ['image','mimes:jpg,png','max:5120',
                ],
              ];
              $request->validate($rules);
          }
        if($request->has('participate_in_Yadadri_Narasimha_Swamy_Kalyanam')){
            $request->merge(['participate_in_YNSK' => 1]);
        }else{
            $request->merge(['participate_in_YNSK' => 0]);
        }

        if($request->sponsor_category){
            $SponsorCategory=SponsorCategory::find($request->sponsor_category);
            $benfits_Ids= $SponsorCategory->benfits->pluck('id')->toArray();
        }

        $user = user::where('email',$request->email)->first();
        $vouchers = Vouchers::where('name',$request->discount_code)->first();
        $discount_amount=0;
        if($vouchers){
            $discount_amount= ($request->registration_amountabove25dollers/100)*$vouchers->discount;
        }
        //dd($discount_amount);
        if($request->is_upgrade== 'upgrade'){
            $is_upgrade = false;
            //$request->category= 'Donor';
        }else {
            $is_upgrade = false;
        }
        if($request->has('pay_partial_amount')){
            $paying_amount=$request->paying_amount-$discount_amount;
        }else{
            if($request->category == 'Donor'){
                $paying_amount= $request->total_amount_paid_amount-$discount_amount;
                if($is_upgrade){
                    $paying_amount = $paying_amount- $user->amount_paid-$discount_amount;
                }
            }else{
                $paying_amount= $request->total_amount_paid_amount-$discount_amount;
            }
        }
       
        $Paymenttype = Paymenttype::find($request->payment_type);

        $user = User::where('email', $request->email)->first();
       // dd($user);
        $user->spouse_full_name = $request->spouse_full_name??'';
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->address = $request->address;
        $user->address2 = $request->address2;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->zip_code = $request->zip_code;
        $user->mobile = $request->mobile;
        $user->participate_in_YNSK = $request->participate_in_YNSK;
        $user->name = $request->first_name." ".$request->last_name;
        $user->discount_code = $request->discount_code;

        $user->children_count = [
                            'below_6'=>$request->below_6,
                            'age_7_to_15'=>$request->age_7_to_15,
                            'age_16_to_23'=>$request->age_16_to_23
                        ];
        $user->save();

        if (!isset($user)) {
            $user = User::create($request->all());
            if ($request->hasfile('image')) {
                $file_name = 'user' . '_' . $user->id . '.' . $request->image->getClientOriginalExtension();
                $request->file('image')->storeAs(config('conventions.user_upload'), $file_name);
                user::where('id', $user->id)->update(['image_url' => $file_name]);
            }
        }

       
            $category=SponsorCategoryType::where('name', 'Donor')->first();
            $SponsorCategory  = SponsorCategory::find($request->sponsor_category);
            $user = User::where('email', $request->email)->first();

             if ($request->hasfile('benfitimage')) {
              foreach ($request->benfitimage as $key => $image) {
                  $key=explode("_",$key);
                    if($key[0]==$request->sponsor_category){
                        $key=$key[1];
                            $file_name = 'benfit_id' . '_' .$key."_". $user->id . '.' . $image->getClientOriginalExtension();
                            $image->storeAs(config('conventions.benfit_image_upload'), $file_name);
                                $temp = [
                                    'user_id' => $user->id,
                                    'benfit_id' => $key,
                                    'image_url' => $file_name,
                                ];
                            $BannerImages = BannerImages::create($temp);
                    }
                }
            }

        
            $category = SponsorCategoryType::where('name', 'Family / Individual')->first();
            $SponsorCategory  = SponsorCategory::find($request->sponsor_category);
            $individual_registration=[];
            $total_amount = 0;
            foreach ($request->count as $key => $value) {

                $Individual = SponsorCategory::where('status', 1)->where('id',$key)->first();
                $a=[
                    $key => $value
                ];
                $individual_registration = $a+ $individual_registration;
            }

            $total_amount = $request->total_amount_paid_amount;
            $user = User::where('email', $request->email)->first();
            // call payment methord

            if ($Paymenttype->name == Config('conventions.paypal_name_db')) {
                Session::put('individual_registration', $individual_registration);
                Session::put('Individual_amount', $total_amount);
                Session::put('registration_amount', $request->registration_amount_hidden);
                Session::put('donor_amount', $request->donation_amount_hidden);
                Session::put('discount_amount', $discount_amount);
                //dd('test');
                return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id);
            } else{

                if ($Paymenttype->name == Config('conventions.first_data_name_db')) {
                    $payment = App(PaymentController::Class)->PayWithFirstDate($user->id, $paying_amount, $Paymenttype->id, "Convention Registration", $request,'Initial Payment');
                        if($payment->payment_status=='pending'){
                            $msg_fin=null;
                            
                            if(isset($payment->more_info['Error'])){
                           
                            foreach($payment->more_info['Error']['messages'] as $msg){
                                if($msg_fin!=null){
                                    $msg_fin=$msg_fin.', '.$msg['description'];
                                }else{
                                    $msg_fin=$msg['description'];
                                }
                            }
                        }else{
                            $msg_fin="Transaction Failed. Please Check Your Card Details";
                        }
                            //dd($payment->more_info);
                            return redirect()->back()->withErrors($msg_fin);  
                        }
                }else{
                    $this->Dopayment($request,  $user,  $paying_amount);
                }

                $user->individual_registration =  $individual_registration;
                $user->sponsorship_category_id = $SponsorCategory->id??0;
                if($request->has('pay_partial_amount')){
                    $user->payment_status = 'Partial Paid';
                }else{
                    $user->payment_status = 'Paid';
                }
                $user->total_amount = $total_amount;
                $user->amount_paid = $paying_amount;
                $user->registration_amount = $request->registration_amount_hidden;
                $user->donor_amount = $request->donation_amount_hidden;
                $user->discount_amount = $discount_amount;
                $user->registration_type_id = $category->id??0;
                $user->save();
                //myaccount.blade
               
                $user=User::where('email',$request->email)->first();

                $details = array('email'=>$request->email,'user'=>$user,'payment'=>$Paymenttype,'cc'=>['registration@ttaconvention.org','kirandrk@gmail.com']);
                NewConventionRegistrationJob::dispatchNow($details);
               // Mail::to($request->email)->cc('registration@ttaconvention.org','kirandrk@gmail.com')->send(new NewConventionRegistration($user,$Paymenttype));

                //return view('registration.youthActivitiesRegistration')
                return redirect('myaccount')->with('message-suc', "Registration Completed Successfully!"); //;
            }
       // }
      // return  $this->ShowRegistrationForm();
       // return redirect(RouteServiceProvider::HOME);
    }

    public function PaypalPayment($amount, $user_id, $request,$payment_id, $is_upgrade=false)
    {
        $settings = Config::get('paypal');

        $this->_api_context = new ApiContext(new OAuthTokenCredential(config('conventions.PAYPAL_SANDBOX_CLIENT_ID'), config('conventions.PAYPAL_SANDBOX_CLIENT_SECRET') ));
        /** setup PayPal api context **/
        $this->_api_context->setConfig($settings);
        $payer = new Payer();

        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $userCurrency = 'USD';
        $pay_amount = $amount;
        $item_1->setName('TTA Convention')
        ->setCurrency($userCurrency)
            ->setQuantity(1)
            ->setPrice($pay_amount);


        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency($userCurrency)
            ->setTotal($pay_amount);
        $timestamp = Carbon::now()->toDateTimeString();

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setInvoiceNumber($timestamp . '_' . '$donation->id')
            ->setItemList($item_list)
            ->setDescription('Transaction Details');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status'))
        ->setCancelUrl(URL::route('payment.status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
        ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));


        try {
            $payment->create($this->_api_context);
            if($is_upgrade){
                $account_status= 'Plan Upgrade';
            }else{
                $account_status= 'Initial Payment';
            }
            $temp = [
                'user_id' => $user_id,
                'payment_amount' => $pay_amount,
                'payment_methord' => $payment_id,
                'unique_id_for_payment' => $payment->id,
                'more_info' => $request->more_info,
                'payment_status' => 'pending',
                'account_status' => $account_status,
                'payment_made_towards' => "Convention Registration"
            ];
            $Modelpayment = ModelPayment::create($temp);

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            if (\Config::get('app.debug')) {
                dd($ex);
                return redirect()->back()->withError('Connection timeout');
            } else {
                return redirect()->back()->withError('Some error occured, sorry for inconvenient');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('payment_id', $Modelpayment->id);
        Session::put('sponsor_category', $request->sponsor_category);
       
        $SponsorCategoryType=SponsorCategoryType::where('name', $request->category)->first();
        Session::put('category_id', $SponsorCategoryType->id??'');

        if (isset($redirect_url)) {
            return redirect($redirect_url);
        }

    }

    public function Dopayment($request, $user,  $paying_amount,$is_upgrade=false)
    {

        if ($is_upgrade) {
            $account_status = 'Plan Upgrade';
        } else {
            $account_status = 'Initial Payment';
        }
        $Paymenttype=Paymenttype::find($request->payment_type);

        if($Paymenttype->name== Config('conventions.check_name_db')){
            $more_info=[
                'cheque_date'=> $request->cheque_date,
                'handed_over_to'=> $request->more_info,
            ];
            $temp=[
                'user_id'=> $user->id,
                'payment_amount'=> $paying_amount,
                'payment_methord'=> $Paymenttype->id,
                'unique_id_for_payment'=>$request->cheque_number,
                'more_info'=> $more_info,
                'payment_status' => 'Pending',
                'account_status' => $account_status,
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);

        }
        if($Paymenttype->name== Config('conventions.zelle_name_db')){
            $more_info = [
                'cheque_date' => $request->cheque_date,
                'more_info' =>  $request->more_info
            ];
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->Zelle_Reference_Number,
                'more_info' =>   $more_info,
                'payment_status' => 'Inprocess',
                'account_status' => $account_status,
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);
        }

        if ($Paymenttype->name == Config('conventions.other_name_db')) {
            $more_info=[
                'transaction_date'=> $request->transaction_date,
                'Payment_made_through'=> $request->Payment_made_through,
                'company_name'=> $request->company_name,
            ];
            $temp = [
                'user_id' => $user->id,
                'payment_amount' => $paying_amount,
                'payment_methord' => $Paymenttype->id,
                'unique_id_for_payment' => $request->transaction_id,
                'more_info' => $more_info,
                'payment_status' => 'Inprocess',
                'account_status' => $account_status,
                'payment_made_towards' => "Convention Registration"
            ];
            $payment = ModelPayment::create($temp);
        }



    }

    public function RegistrationPageContentedit()
    {
        $RegistrationContent=RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status', 1)->get();
        return view('admin.ContentMangement.Registration.contentupdate', compact('RegistrationContent', 'paymenttypes'));
    }

    public function RegistrationPageContentUpdate(Request $request)
    {
        $RegistrationContent = RegistrationContent::first();
        $RegistrationContent->update($request->all());
        foreach ($request->payment_note as $id => $note) {
            Paymenttype::where('id', $id)->update(['note' => $note]);
        }
        $paymenttypes = Paymenttype::where('status', 1)->get();
          return redirect('registration-page-content-update');
      //  return view('admin.ContentMangement.Registration.contentupdate', compact('RegistrationContent', 'paymenttypes'));
    }

    public function getdetailsfromNriva(Request $request)
    {
     //   $response = Http::get('http://localhost/nriva/public/api/get-details?email='.$request->email);
        $response = Http::get(env('NRIVA_URL').'/api/get-details?email='.$request->email);
        return $response;
    }


    public function showRegistrations()
    {
        $users=User::with('registrationtype')->where('email','!=',env('admin_email'))->orderBy('created_at', 'desc')->get();
        return view('admin.registrations.index', compact('users'));

    }

    public function showAssigneFeatures($id)
    {
        $user=User::FindOrFail($id);
        $user_benfits= $user->benfits;
        $bannerimages= $user->bannerimages;
        return view('admin.registrations.assigneFeatures', compact('user','user_benfits','bannerimages'));
    }

    public function updateAssigneFeatures(Request $request)
    {
        $user=User::find($request->user_id);
        $ben=$user->benfits()->where('benfit_id', $request->id)->first();
        if(isset($ben)){
            return   $user->benfits()->updateExistingPivot($request->id,['content' => $request->content]);
        }else{
            return   $user->benfits()->attach($request->id,['content' => $request->content]);
        }
    }

    public function myaccount()
    {
        $id=Auth::user()->id;
        $registrations= Registration::where('user_id',$id)->get();
        $first_payment=ModelPayment::where('payment_made_towards','Convention Registration')->where('user_id',$id)->first();
        return view('myaccount', compact('registrations','first_payment'));
    }


    public function PaymentHistory()
    {
            $payments=ModelPayment::where('user_id', Auth::user()->id)->with('paymentmethord')->get();
            return view('PaymentHistory',compact('payments'));
    }

    public function adminSidePaymentHistory($user_id)
    {
        $payments = ModelPayment::where('user_id', $user_id)->with('paymentmethord')->get();
        return view('admin.registrations.adminSidePymentHistory', compact('payments'));
    }

    public function printTicket()
    {
        $venue = Venue::first();

 //       $payments = ModelPayment::where('user_id', Auth::user()->id)->with('paymentmethord')->get();
        return view('printticket', compact('venue'));
    }

    public function viewfeatures()
    {
        $user= Auth::user();
        $categorydetails= $user->categorydetails;
        $benfits=$user->benfits;
        $bannerimages=$user->bannerimages;
        return view('viewfeatures', compact('categorydetails', 'benfits','bannerimages'));
    }

    public function showAddNoteForm($id)
    {
        $user =User::findOrFail($id);
        return view('admin.registrations.addNote', compact('user'));
    }

    public function storeAddNoteForm(Request $request)
    {
        //registration_note
        User::where('id', $request->user_id)->update([
            'registration_note'=>$request->registration_note
        ]);
        return $this->showAddNoteForm($request->user_id);
    }

   public function registrationsuccess(Request $request){
        $payment = ModelPayment::where("id","$request->id")->first();
       return view('registration.registration-success',compact('payment'));
    }

    public function exibitRegestration()
    {
        $exhibitsTypes= ExhibitorType::where('status',1)->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttypes = Paymenttype::where('status', 1)->latest()->get();
           $states=DB::table('tta_2022_states')->get();
        $Venue = Venue::first();

        // return view('registration.showexhibitsRegistrationForm', compact('exhibitsTypes', 'RegistrationContent', 'paymenttypes', 'Venue','states'));
        return view('committees.vender_n_exhibits_committee', compact('exhibitsTypes', 'RegistrationContent', 'paymenttypes', 'Venue','states'));
    }


    public function youthActivitiesRegestration()
    {
        return view('registration.youthActivitiesRegistration');
    }

    public function storeYouthActivitiesRegestration(Request $request)
    {
           $data=array_merge($request->all(),
        [
            'user_id' => Auth::user()->id,
            'registration_type_name' => "YouthActivities"
            ]
        );
          Registration::create($data);
        return view('registration.youthActivitiesRegistration')->with('message-suc', "Youth Activities registered successfully");;
    }

    public function stoteExibitRegestrationForm(Request $request )
    {
        
        $trnid= Str::random(6);

       $reserv = new Reservation;
        $reserv->name = $request['name'];
        $reserv->company_name = $request['company-name'];
        $reserv->firstname = $request['first-name'];
        $reserv->lastname = $request['last-name'];
        $reserv->address1 = $request['address-1'];
        $reserv->address2 = $request['address-2'];
        $reserv->city = $request['city'];
        $reserv->state = $request['state'];
        $reserv->zipcode = $request['zipcode'];
        $reserv->country = $request['country'];
        $reserv->package_id = $trnid;
        $reserv->phone_number = $request['phone'];
        $reserv->email = $request['email'];
        $reserv->save();
        for($i=1;$i<=12;$i++){
            if($request['exe-qnt-'.$i] != ''){
          $package = new PackageList;
          $package->package_code = $request['package_code_'.$i];
          $package->exhibit_type = $request['package_trype_'.$i];
          $package->number_booths = $request['exe-qnt-'.$i];
          $package->amount = $request['exe-qnt-'.$i.'-sub'];
          $package->package_id = $trnid;
          $package->save();
      }
        }
       
        $data=array_merge($request->all(),
        [
            'user_id' => $reserv->id,
            'registration_type_name' => "Exhibit"
            ]
        );
          Registration::create($data);
        $payment_made_towards = "Paid Towards Exhibit registration";
        $paymenttype= Paymenttype::find($request->payment_type);
        
        
        if($paymenttype->name== Config('conventions.check_name_db')){
           $payment= App(PaymentController::Class)->PayWithCheck($reserv->id, $request->amount, $paymenttype->id, $payment_made_towards, $request );
        }

        if ($paymenttype->name == Config('conventions.zelle_name_db')) {
            $payment= App(PaymentController::Class)->PayWithZelle($reserv->id, $request->amount, $paymenttype->id, $payment_made_towards, $request);
        }

        if ($paymenttype->name == Config('conventions.other_name_db')) {
            $payment= App(PaymentController::Class)->PayWithOther($reserv->id, $request->amount, $paymenttype->id, $payment_made_towards, $request);
        }
        if ($paymenttype->name == Config('conventions.first_data_name_db')) {
            $payment= App(PaymentController::Class)->PayWithFirstDate($reserv->id, $request->amount, $paymenttype->id, $payment_made_towards, $request);

            if($payment->payment_status=='pending'){
                $msg_fin=null;
                if(isset($payment->more_info['Error'])){
                           
                            foreach($payment->more_info['Error']['messages'] as $msg){
                                if($msg_fin!=null){
                                    $msg_fin=$msg_fin.', '.$msg['description'];
                                }else{
                                    $msg_fin=$msg['description'];
                                }
                            }
                        }else{
                            $msg_fin="Transaction Failed. Please Check Your Card Details Once";
                        }
                return redirect()->back()->withErrors($msg_fin);  
            }

        }

        if ($paymenttype->name == Config('conventions.paypal_name_db')) {
                $temp = [
                    'user_id' => $reserv->id,
                    'payment_amount' => $request->amount,
                    'payment_methord' => $paymenttype->id,
                    'payment_status' => 'Pending'
                ];
                $payment = ModelPayment::create($temp);

            return  App(PaymentController::Class)
                ->PayWithPayPal($request->amount, 'Exhibit Regestration',  $payment->id, 'paypal.status');
        }

     

        $PackageList=PackageList::where('package_id',$reserv->package_id)->get();

        //Mail::to($reserv->email)->bcc(['vendorsexhibits@ttaconvention.org','naresh.chintala2012@gmail.com'])->send(new NewExhibitRegistration($reserv,$paymenttype,$PackageList));

         $details = array('email'=>$reserv->email,'user'=>$reserv,'payment'=>$paymenttype,'cc'=>['vendorsexhibits@ttaconvention.org','naresh.chintala2012@gmail.com'],'packagesList'=>$PackageList);
                NewExhibitRegistrationJob::dispatchNow($details);

        return redirect(url('registration-success')."?id=$payment->id")->with('message-suc', "Exhibit registered successfully");
    }

    public function showRegistrationDetails($id)
    {
        $Registration=Registration::where('id',$id)->where('user_id', auth::user()->id)->firstOrFail();
        return view('registration.showRegistrationDetails',compact('Registration'));
    }


    public function download($id) {
        $BannerImages = BannerImages::findorFail($id);
        return \Storage::download(  config('conventions.benfit_image_upload') . '/'. $BannerImages->image_url);
    }


    public function updateBannerImage(Request $request)
    {
      
        if ($request->hasfile('benfitimage')) {
            $key=$request->benfit_id;
            $image=$request->benfitimage[$key];
            $user_id =Auth::user()->id;

             $BannerImage=BannerImages::where('user_id', $user_id)
                ->where('benfit_id',$request->benfit_id)->first();
               if($BannerImage){
                if(file_exists(config('conventions.benfit_image_display').$BannerImage->image_url)){
                    unlink(config('conventions.benfit_image_display').$BannerImage->image_url);  
                }
               }

                $file_name = 'benfit_id' . '_' .$key."_". $user_id . '.' . $image->getClientOriginalExtension();
               
                $image->storeAs(config('conventions.benfit_image_upload'), $file_name);

                $BannerImages = BannerImages::updateOrCreate([
                        'user_id' => $user_id,
                        'benfit_id' => $key,
                    ],[
                        'image_url' => $file_name,
                    ]);
                
            }
         return redirect('viewfeatuees');
    }

    public function email_verification(Request $request)
    {
        $user = User::where("email",$request->email)->first();

        if(empty($user)){
            $otp = rand(10000,99999);
         $details=array('msg'=>"Hi, your email verification code for TTA convention registration is : $otp", 'subject'=>"Email Verification code from TTA Convention", 'to'=>$request->email);
        OTPEmailJob::dispatchNow($details);
         Otp_model::updateOrCreate(['user'=>$request->email],['otp'=>$otp, 'status'=>1]);
            return "success";
        }else{
            return "failed";
        }
    }

    
    public function sendOtpForLogin(Request $request)
    {
        $user = User::where("email",$request->email)->first();
        if(!empty($user)){
            $otp = rand(10000,99999);

            $details=array('msg'=>"Hi, your OTP code for TTA convention Login is : $otp",
                            'subject'=>"TTA Convention Login OTP",
                            'to'=>$request->email);
        OTPEmailJob::dispatchNow($details);
        $user->login_otp=$otp;
        $user->save();
            return "success";
        }else{
            return "failed";
        }
    }
    


    public function showUpgradeForm()
    {
        session::forget('payment_id');
        session::forget('sponsor_category');
        session::forget('category_id');
        session::forget('Individual_amount');
        session::forget('is_upgrade');

        $SponsorCategoryTypes= SponsorCategoryType::where('status',1)->get();
        $Individuals= SponsorCategory::where('status',1)->where('donor_type_id', 0)->get();
        $donors= SponsorCategory::with('donortype')->where('status',1)->where('donor_type_id','!=' ,0)->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttype = Paymenttype::where('status',1)->orderBy('created_at',"asc")->get();
        $states=DB::table('tta_2022_states')->get();
        $user=auth::user();
        return view('showUpgradeForm',compact("SponsorCategoryTypes", 'Individuals', 'donors', 'RegistrationContent', 'user', 'paymenttype', 'states'));
    }

    public function showRegPkgUpgradeForm()
    {
        session::forget('payment_id');
        session::forget('sponsor_category');
        session::forget('category_id');
        session::forget('Individual_amount');
        session::forget('is_upgrade');
        
        $SponsorCategoryTypes= SponsorCategoryType::where('status',1)->get();
        $Individuals= SponsorCategory::where('status',1)->where('donor_type_id', 0)->get();
        $donors= SponsorCategory::with('donortype')->where('status',1)->where('donor_type_id','!=' ,0)->get();
        $RegistrationContent = RegistrationContent::first();
        $paymenttypes=$paymenttype = Paymenttype::where('status',1)->orderBy('created_at',"asc")->get();
        $states=DB::table('tta_2022_states')->get();
        $user=auth::user();
        return view('showRegPkgUpgrade',compact("SponsorCategoryTypes", 'Individuals', 'donors', 'RegistrationContent', 'user', 'paymenttype','paymenttypes', 'states'));
    }

    public function storeRegPkgUpgradeForm(Request $request)
    {
        //$SponsorCategory=SponsorCategory::find($request->sponsor_category);
        //dd($SponsorCategory);
        //$benfits_Ids= $SponsorCategory->benfits->pluck('id')->toArray();
        $user = user::where('email',$request->email)->first();
        $Paymenttype = Paymenttype::find($request->payment_type);
        $paying_amount = 0;

        if($request->has('pay_partial_amount')){
            $paying_amount = $request->paying_amount;
        }else{
            $paying_amount =  $request->total_amount_paid_amount ;
        }

        $user= $this->updateUserDate($request);

        $individual_registration=[];
        $total_amount = 0;
        foreach ($request->count as $key => $value) {

            $Individual = SponsorCategory::where('status', 1)->where('id',$key)->first();
            $a=[
                $key => $value
            ];
            $individual_registration = $a+ $individual_registration;
        }
        $total_amount = $user->donor_amount + $request->registration_amount_hidden;

        if ($Paymenttype->name == Config('conventions.paypal_name_db')) {
            Session::put('individual_registration', $individual_registration);
            Session::put('Individual_amount', $total_amount);
            Session::put('registration_amount', $request->registration_amount_hidden);
            Session::put('donor_amount', $user->donor_amount);
            Session::put('is_upgrade', true);
            return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id,true);
        } else {
            if ($Paymenttype->name == Config('conventions.first_data_name_db')) {
                $payment = App(PaymentController::Class)->PayWithFirstDate($user->id, $paying_amount, $Paymenttype->id, "Convention Registration", $request,'Plan Upgrade');
                    if($payment->payment_status=='pending'){
                        $msg_fin=null;
                        if(isset($payment->more_info['Error'])){
                           
                            foreach($payment->more_info['Error']['messages'] as $msg){
                                if($msg_fin!=null){
                                    $msg_fin=$msg_fin.', '.$msg['description'];
                                }else{
                                    $msg_fin=$msg['description'];
                                }
                            }
                        }else{
                            $msg_fin="Transaction Failed. Please Check Your Card Details Once";
                        }
                        return redirect()->back()->withErrors($msg_fin);  
                    }
            }else{
                $this->Dopayment($request,  $user,  $paying_amount,true);
            }
            $user->individual_registration =  $individual_registration;
            if($request->has('pay_partial_amount')){
                $user->payment_status = 'Partial Paid';
            }else{
                $user->payment_status = 'Paid';
            }
            $user->total_amount = $total_amount;
            $user->amount_paid = $user->amount_paid + $paying_amount;
            $user->registration_amount = $request->registration_amount_hidden;
            $user->save();
           
            $user=User::where('email',$request->email)->first();
            $details = array('email'=>$request->email,'user'=>$user,'payment'=>$Paymenttype,'cc'=>['registration@ttaconvention.org','kirandrk@gmail.com']);
                NewConventionRegistrationJob::dispatchNow($details);
            return redirect('myaccount'); //;
        }


    }

    public function StoreUpgradeForm(Request $request)
    {
        //valadate
        if ($request->hasfile('benfitimage')) {
            $rules= [
                'benfitimage.*' => ['image','mimes:jpg,png','max:5120',
            ],
          ];
          $request->validate($rules);
      }
      
            $SponsorCategory=SponsorCategory::find($request->sponsor_category);
         //   dd($request->donation_amount_hidden);
            $user = user::where('email',$request->email)->first();
            $Paymenttype = Paymenttype::find($request->payment_type);
            $category=SponsorCategoryType::where('name', 'Donor')->first();
            $paying_amount = 0;
            if($request->has('pay_partial_amount')){
                $paying_amount = $request->paying_amount;
            }else{
                $paying_amount =  $request->total_amount_paid_amount ;
            }
            
            $total_amount = $user->registration_amount + $request->donation_amount_hidden;

        //update user data
         $user= $this->updateUserDate($request);
        
         //payment
            $this->uploadImage($request,$user);
            
           // $total_amount = $request->total_amount_paid_amount;

            if ($Paymenttype->name == Config('conventions.paypal_name_db')) {
                Session::put('individual_registration', $user->individual_registration);
                Session::put('Individual_amount', $total_amount);
                Session::put('registration_amount', $user->registration_amount);
                Session::put('is_upgrade', true);
                Session::put('donor_amount', $request->donation_amount_hidden);
                return  $this->PaypalPayment($paying_amount, $user->id, $request, $Paymenttype->id,true);
            } else {
                $this->Dopayment($request,  $user,  $paying_amount,true);
                //update package
                $user->sponsorship_category_id = $SponsorCategory->id??0;
                if($request->has('pay_partial_amount')){
                    $user->payment_status = 'Partial Paid';
                }else{
                    $user->payment_status = 'Paid';
                }
                $user->total_amount = $total_amount;
                $user->amount_paid = $user->amount_paid + $paying_amount;
                $user->donor_amount = $request->donation_amount_hidden;
                $user->save();
            }
        //send mail
        $user=User::where('email',$request->email)->first();
        //Mail::to($request->email)->send(new DonationPackageUpgradeNotification($user,$Paymenttype));
         $details = array('email'=>$request->email,'user'=>$user,'payment'=>$Paymenttype);
            DonationPackageUpgradeNotificationJob::dispatchNow($details);
        return redirect('myaccount'); //;
    }


    public function uploadImage($request,$user)
    {
        if ($request->hasfile('benfitimage')) {
            foreach ($request->benfitimage as $key => $image) {
                $key=explode("_",$key);
                  if($key[0]==$request->sponsor_category){
                      $key=$key[1];
                          $file_name = 'benfit_id' . '_' .$key."_". $user->id . '.' . $image->getClientOriginalExtension();
                          $image->storeAs(config('conventions.benfit_image_upload'), $file_name);
                              $temp = [
                                  'user_id' => $user->id,
                                  'benfit_id' => $key,
                                  'image_url' => $file_name,
                              ];
                          $BannerImages = BannerImages::updateOrCreate([
                            'user_id' => $user->id,
                            'benfit_id' => $key,
                            ],[
                              'image_url' => $file_name,
                          ]  );
                  }
              }
          }
    }


    public function updateUserDate($request)
    {
        $user = User::where('email', $request->email)->first();

        $user->spouse_full_name = $request->spouse_full_name;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->address = $request->address;
        $user->address2 = $request->address2;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->zip_code = $request->zip_code;
        $user->mobile = $request->mobile;
        $user->name = $request->first_name." ".$request->last_name;

        $user->children_count = [
                            'below_6'=>$request->below_6,
                            'age_7_to_15'=>$request->age_7_to_15,
                            'age_16_to_23'=>$request->age_16_to_23
                        ];
        $user->save();

        return $user;
    }

    public function voucher_verification(Request $request)
    {
        $vouchers = Vouchers::where('name',$request->name)
                                ->where('status',1)
                                ->first();
        if($vouchers){
            return $vouchers->discount??0;
        }else{
            return "failed";
        }
    }

    public function cruise_registration($value='')
    {
        $RegistrationContent = RegistrationContent::first();
        $paymenttype = Paymenttype::where('status',1)->orderBy('created_at',"asc")->get();
        return view('cruise_registration',compact('RegistrationContent','paymenttype'));
    }

     public function submit_cruise_registration(Request $request)
    {
        $file_name=null;
         if ($request->hasfile('waiver_attachment')) {
              $image = $request->waiver_attachment;
                $file_name = time().'.'. $image->getClientOriginalExtension();
                $image->storeAs(config('conventions.user_upload'), $file_name);     
            }
            $cruise = new Cruise();
            $cruise->firstName=$request->first_name;
            $cruise->lastName=$request->last_name;
            $cruise->email=$request->email;
            $cruise->phone=$request->mobile;
            $cruise->dob=$request->dob;
            $cruise->amount=50;
            $cruise->payment_type=$request->payment_type;
            $cruise->payment_status="Inprocess";
            $cruise->waiver_attachment=$file_name;
            $cruise->save();
            $payment_made_towards= "cruise registration";
            $paymenttype= Paymenttype::find($request->payment_type);
        
        $payment_details=[];
        if($paymenttype->name== Config('conventions.check_name_db')){
          $payment_details=['cheque_number'=>$request->cheque_number,
          "cheque_date"=>$request->cheque_date,"more_info"=>$request->more_info];
        }

        if ($paymenttype->name == Config('conventions.zelle_name_db')) {
           $payment_details=['Zelle_Reference_Number'=>$request->Zelle_Reference_Number];
        }

        if ($paymenttype->name == Config('conventions.other_name_db')) {
           $payment_details=['Payment_made_through'=>$request->Payment_made_through,
            "company_name"=>$request->company_name,
            "transaction_date"=>$request->transaction_date,
            "transaction_id"=>$request->transaction_id];
        }
       
        if ($paymenttype->name == Config('conventions.first_data_name_db')) {
            
            $payment = $payment_details= App(PaymentController::Class)->PayWithFirstDate($cruise->id, 50, $paymenttype->id, $payment_made_towards, $request,null,true);
            //dd($payment);
            if($payment['payment_status']=='pending'){
                $msg_fin=null;
                if(isset($payment->more_info['Error'])){
                           
                            foreach($payment['more_info']['Error']['messages'] as $msg){
                                if($msg_fin!=null){
                                    $msg_fin=$msg_fin.', '.$msg['description'];
                                }else{
                                    $msg_fin=$msg['description'];
                                }
                            }
                        }else{
                            $msg_fin="Transaction Failed. Please Check Your Card Details Once";
                        }
                return redirect()->back()->withErrors($msg_fin);  
            }

        }
        Cruise::where('id',$cruise->id)->update(['payment_details'=>$payment_details]);

        if ($paymenttype->name == Config('conventions.paypal_name_db')) {
            Session::put('cruise_id', $cruise->id);
                 return  App(PaymentController::Class)
                ->PayWithPayPal(50, 'cruise Regestration',  $cruise->id, 'paypal.cruisestatus');
        }
         $details = array('email'=>$request->email,"user_id"=>$cruise->id);
                NewCruiseRegistrationJob::dispatchNow($details);
         return redirect(url('cruise_registration'))->withSuccess('Registration Successful');

    }
     public function ImportRegesterUserStore(Request $request)
    {   
        

        $this->validate($request, [
          'select_file'  => 'required|mimes:xls,xlsx'
         ]);

         $path = $request->file('select_file')->getRealPath();
         $uploaded_filename = $request->file('select_file')->getClientOriginalName();

         $data = Excel::toArray(new UsersImport,$request->file('select_file'));
         //dd($data[0]);
         $invalid_email=$email_missing=$email_exists=$sponsor_missing=$success_cnt=0;
         $invalid_records_byemail=$missing_records_byemail=$missing_records_bysponser=array();
         if(count($data[0]) > 0)
         {
          foreach($data[0] as $key => $value)
          { 
            //dd($value);

            $email=$value['email'];
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $invalid_records_byemail[]=$value['email'];
                $invalid_email++;
                continue;
            }
            if(!$email){
                $missing_records_byemail[]=$value['donor_name'];
                $email_missing++;
                continue;
            }
            $donation_amount=$value['donor_package_amount'];
            $donation_amount=floatval(preg_replace("/[^-0-9\.]/","",$donation_amount));
            $paying_amount=0;
           if(!empty($value['amount_paid'])){
            $paying_amount = floatval(preg_replace("/[^-0-9\.]/","",$value['amount_paid']));
           }
            if(!empty($value['donor_package'])){
                $donors= Donortype::where('name' ,$value['donor_package'])->first();
                if(!$donors){
                    $missing_records_bysponser[]=$value['donor_name'];
                    $sponsor_missing++;
                    continue;
                }
                $SponsorCategory=SponsorCategory::where('donor_type_id',$donors->id)->first();
                if(!$SponsorCategory){
                    $missing_records_bysponser[]=$value['donor_name'];
                    $sponsor_missing++;
                    continue;
                }
                $SponsorCategory=SponsorCategory::find($SponsorCategory->id);
                $benfits_Ids= $SponsorCategory->benfits->pluck('id')->toArray();
                
                
            }
            $kids_details=null;
            $user = user::where('email',$email)->first();
            if(isset($user)){
                $missing_records_byemail[]=$value['donor_name'];
                $email_exists++;
                continue;
            }

           
           

            $user = User::where('email', $email)->first();
            if (!isset($user)) {
                $uname=explode('&',$value['donor_name']);
                if(count($uname)>1){
                    $uname=explode(' ',trim($uname[1]));
                }
                else{
                    $uname=explode(' ',$uname[0]);
                }
                $lname=$fname='';
                $fname = $uname[0];
                if(isset($uname[1])){
                    $lname = $uname[1];
                }
                #added for wife&husband names

                //dd($value);
                $fname=$value['donor_name'];
                $lname='';
                $user_temp=[
            'first_name'  => $fname,
            'last_name'  => $lname,
            'email' => $email,
            'mobile' => $value['mobile_number'],
            "spouse_full_name" => $value['spouse_full_name'],
            "children_count"=> [
                            'below_6'=>$value['below_6_years'],
                            'age_7_to_15'=>$value['age_7_15'],
                            'age_16_to_23'=>$value['youth_16_23']
                        ],
            "is_imported_record"=>1,
            "address"=>$value['address_line_1'],
            "address2"=>$value['address_line_2'],
            "zip_code"=>$value['zip'],
            "country"=>$value['country'],
            "state"=>$value['state'],
            
            ];
            //'chapter' => $row['chapter'],
            //'leadership' => $row['leadership'],
            
                $user = User::create($user_temp);

                $mainsite_user = DB::table('users')->where('email',$user->email)->first();
                if($mainsite_user){
                    $user->member_id=$mainsite_user->member_id;
                }
                $user->registration_id = "CON".sprintf("%04d", $user->id);
                $user->save();
            }
            $user = User::where('email', $email)->first();

            $SponsorCategory  = SponsorCategory::where('donor_type_id',$donors->id)->first();
            
            $individual_registration=null;
            $total_amount = $donation_amount;
            $user = User::where('email', $email)->first();
            // call payment methord

            $package_details=array();
            $all_details=array();
            if($value['payment_method']=="Paypal"){
                $Paymenttype = Paymenttype::find(2);

            }elseif($value['payment_method']=="Other"){
                $Paymenttype = Paymenttype::find(5);

            }elseif($value['payment_method']=="Credit Card"){
                $Paymenttype = Paymenttype::find(6);
                
            }elseif($value['payment_method']=="Zelle"){
                $Paymenttype = Paymenttype::find(3);
                
            }elseif($value['payment_method']=="Check"){
                $Paymenttype = Paymenttype::find(4);
                
            }else{
                $Paymenttype = Paymenttype::find(5);
            }
            

            array_push($package_details, array('package_name'=> "Convention Registration"));
            
            array_push($all_details, array($SponsorCategory->donortype->name??''));
            
            array_push($package_details, array('package_details'=> $all_details));

              
                    $more_info=[
                        'transaction_date'=> $value['payment_date'],
                        'payee_name'=> $value['donor_name'],
                        'company_name'=> 'Offline Registration',
                        'Payment_made_through'=>$value['payment_method'],
                    ];
                    
                    $temp = [
                        'user_id' => $user->id,
                        'payment_amount' => $paying_amount,
                        'payment_methord' => $Paymenttype->id??4,
                        'unique_id_for_payment' => $value['transaction_id'],
                        'more_info' => $more_info,
                        'payment_status' => $value['payment_status'],
                        'status' => 1,
                        'account_status' => 'Initial Payment',
                        'package_details' =>$package_details,
                        'payment_made_towards' => "Convention Registration"
                    ];
                    $payment = ModelPayment::create($temp);
               
                $user->sponsorship_category_id = $SponsorCategory->id??0;
                $user->individual_registration =  $individual_registration;
                $user->payment_status = $value['payment_status'];
                $user->total_amount = $total_amount;
                $user->amount_paid = $paying_amount;
                $user->registration_amount = 0;
                $user->donor_amount = $total_amount;
                // $user->free_exbhit_registrations = 0;
                // if($user->sponsorship_category_id>0){
                //     foreach($SponsorCategory->benfits as $benfits){
                //     if($benfits->name=='Vendor booth spaces at the event'){

                //         $user->free_exbhit_registrations = (int)$benfits->pivot->count??0;
                //         $user->vendor_booth_space_count = (int)$benfits->pivot->count??0;
                //         }
                //     }
                // }
                //dd($user->free_exbhit_registrations);
                $user->save();
                $success_cnt++;
                echo 'Successfully : '.$email.'<br>';
                
                //myaccount.blade
                // Mail::to($email)
                //  ->cc('registration@convention.nriva.org')
                //  ->send(new NewConventionRegistration($user,$Paymenttype));
                /*if($key==2){
                    return redirect('admin/registrations')->with('message-suc', "Convention Registration Completed Successfully. Total created: ".$success_cnt);
                }*/
                //return redirect('myaccount')->with('message-suc', "Convention Registration Completed Successfully");
          } // foreach end 
        }  //count if condition end
        ##log file creation code --start
        $data="\n========Start at ".date('Y-m-d_H-i-s')."================";
        $data .="\nFile Name: ".$uploaded_filename."  \nTotal created: ".$success_cnt."\nDuplicate Email: ".$email_exists."\n".implode("\n",$missing_records_byemail)."\nSponsor missing: ".$sponsor_missing."\n".implode("\n",$missing_records_bysponser)."\nInvalid Emails: ".$sponsor_missing."\n".implode("\n",$invalid_records_byemail);
        $data.="\n========End at ".date('Y-m-d_H-i-s')."================";
         $file = date('Y-m-d'). '_file.txt';
      $destinationPath=public_path()."/upload/";
      if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
      File::append($destinationPath.$file,$data);
      ##log file creation code --end
        return redirect('admin/registrations')->with('message-suc', "Convention Registration Completed Successfully. Total created: ".$success_cnt." Duplicate Email: ".$email_exists." Sponsor missing: ".$sponsor_missing." Invalid Emails: ".$invalid_email);
        echo 'Successfully Imported';exit;

            
        }

        public function importMemberMail(Request $request)
        {
            $user = User::where('email', $request->email)->first();

            $Paymenttype = Paymenttype::find(6);
            $details = array('email'=>$request->email,'user'=>$user,'payment'=>$Paymenttype,'cc'=>['registration@ttaconvention.org','kirandrk@gmail.com']);
                NewConventionRegistrationJob::dispatchNow($details);
                $user->mail_sent=1;
                $user->save();
                return redirect('admin/registrations')->with('message-suc', "Email Sent Successfully");
        }
        public function update_tickets(Request $request)
        {
         
            $statusChangeEmail=\Cookie::get('ConloginUserEmail');
            if(Auth::user()){
                $statusChangeEmail=Auth::user()->email;
            }
            $user = User::where('id', $request->user_id)->first();  

            AuditLog_Model::create([
                'registration_id'=>$user->id,
                'convention_id'=>$user->registration_id,
                'user_name'=>$user->name,
                'user_email'=>$user->email,
                'old_status'=>$user->ticket_status,
                'new_status'=>$request->status,
                'reg_type'=>"Convention Registration",
                'more_info'=>"Ticket Status Change",
                'statusChanged_by'=>$statusChangeEmail
            ]); 
            $user->ticket_status=$request->status;
            $user->save();
            return "success";
        }
        public function update_notes(Request $request)
        {
            $statusChangeEmail=\Cookie::get('ConloginUserEmail');
            if(Auth::user()){
                $statusChangeEmail=Auth::user()->email;
            }
            $user = User::where('id', $request->user_id)->first();
            AuditLog_Model::create([
                'registration_id'=>$user->id,
                'convention_id'=>$user->registration_id,
                'user_name'=>$user->name,
                'user_email'=>$user->email,
                'old_status'=>$user->notes,
                'new_status'=>$request->notes,
                'reg_type'=>"Convention Registration",
                'more_info'=>"Notes Update in convention",
                'statusChanged_by'=>$statusChangeEmail
            ]); 
            $user->notes=$request->notes;
            $user->save();
            return "success";
        }

}
