<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProgramStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'chair_name' => ['required', 'string', 'max:100'],
            'mobile_number' => ['required', 'string', 'max:20'],
            'co_chair_name' => ['required', 'string', 'max:100'],
            'program_name' => ['required', 'string', 'max:200'],
            'Location' => ['required', 'string', 'max:200'],
            'date' => ['required'],
            'page_content' => ['required', 'string'],
            'image' => ['required', 'image', 'mimes:jpg,bmp,png', 'max:1024', 'dimensions:ratio=4/3']

        ];
    }
}
