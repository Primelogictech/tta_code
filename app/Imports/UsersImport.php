<?php

/*namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {   
        return new User([
            'name'  => $row['mrsmr_name'],
            'email' => $row['email'],
            'mobile' => $row['phone'],
            'donor_amount' => $row['donation_amount'],
            'sponsor_level' => $row['sponsor_level'],
            'chapter' => $row['chapter'],
            'leadership' => $row['leadership'],
            "spouse_first_name" => $row['wife_first_name'],
            "spouse_last_name" => $row['last_name'],

        ]);
    }
}*/


namespace App\Imports;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToArray, WithHeadingRow

{
    use Importable;

    public function array($file): array
    {
        return [
            'name'  => $file['mrsmr_name'],
            'email' => $file['email'],
            'mobile' => $file['phone'],
            'donor_amount' => $file['donation_amount'],
            'sponsor_level' => $file['sponsor_level'],
            'chapter' => $file['chapter'],
            'leadership' => $file['leadership'],
            "spouse_first_name" => $file['wife_first_name'],
            "spouse_last_name" => $file['last_name'],
        ];
    }
}
