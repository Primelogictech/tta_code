<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Cruise;

class NewCruiseRegistration extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $PaymentType;

    public function __construct($user)
    {
        $this->user_id=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = Cruise::where('id',$this->user_id)->first();
      // dd($user);
        return $this->subject('TTA Cruise Registration')->view('mails.NewCruiseRegistration')->with([
            'user_details' => $user ]);
    }
}
