<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cruise extends Model
{
	 use HasFactory;
    protected $table = "tta_2022_cruise_registration";
    protected $fillable = ['firstName', 'lastName', 'email', 'phone', 'dob', 'waiver_attachment', 'payment_type', 'payment_status', 'payment_details', 'status', 'created_at', 'updated_at','amount'
    ];
   
}