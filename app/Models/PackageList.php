<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageList extends Model
{
   protected $table = 'tta_2022_packages_list';
   public $primarykey = 'id';
   public $timestamp = true;
}
 