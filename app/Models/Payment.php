<?php

namespace App\Models;

use App\Models\Admin\Paymenttype;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $table = 'tta_2022_payments';

    protected $fillable = [
        'user_id',
        'payment_amount',
        'payment_made_towards',
        'status',
        'payment_methord',
        'unique_id_for_payment',
        'payment_status',
        'account_status',
        'more_info',
        'validation_status'
    ];

    protected $casts = [
        'more_info' => 'array',
    ];

    public function paymentmethord()
    {
        return $this->belongsTo(Paymenttype::class, 'payment_methord');
    }

     public function userDetails()
    {
        return $this->HasOne(User::class, 'id', 'user_id');
    }

}
