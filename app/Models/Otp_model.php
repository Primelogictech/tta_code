<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Otp_model extends Model
{
	 use HasFactory;
    protected $table = "tta_2022_otp_verification";
    protected $fillable = ['user', 'otp', 'status', 'created_at', 'updated_at'
    ];
   
}