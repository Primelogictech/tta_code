<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
   protected $table = 'tta_2022_registration_packages_user';
   public $primarykey = 'id';
   public $timestamp = true;

   public function paymentDetails()
   {
       return $this->BelongsTo(Payment::class, 'id', 'user_id');
   }


}
 