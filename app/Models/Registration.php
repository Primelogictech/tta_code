<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\ExhibitorType;

class Registration extends Model
{
    use HasFactory;
    protected $table = 'tta_2022_registrations';

    protected $fillable = [
        'user_id',
        'registration_type_name',
        'full_name',
        'company_name',
        'category',
        'tax_ID',
        'mailing_address',
        'phone_no',
        'email',
        'fax',
        'website_url',
        'booth_type',
    ];

    public function exhibitorTypes()
    {
        return $this->HasOne(ExhibitorType::class, 'id', 'booth_type');
    }

    
    public function paymentdata()
    {
        return $this->HasOne(Payment::class, 'id', 'payment_id');
    }


}
