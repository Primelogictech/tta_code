<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class SponsorCategory extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = "tta_2022_sponsor_categories";
    protected $fillable = [
        'category_type_id',
        'donor_type_id',
        'amount',
        'benefits',
        'start_amount',
        'end_amount',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'benefits' => 'array',
        'price_change_date' => 'datetime',

        'start_amount' => 'integer',
        'end_amount' => 'integer',

    ];

    public function sponsorcategorytype()
    {
        return $this->belongsTo(SponsorCategoryType::class, 'category_type_id', 'id');
    }

    public function donortype()
    {
        return $this->belongsTo(Donortype::class, 'donor_type_id', 'id');
    }

    public function benfits()
    {
        return $this->belongsToMany(Benefittype::class, 'tta_2022_benfit_sponsor_category', 'sponsor_category_id', 'benfit_id')
        ->withPivot('count','display_order')
        ->orderBy(DB::raw('ISNULL(display_order), display_order'));
    }

}
