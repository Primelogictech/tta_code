<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
      protected $table = "tta_2022_donors";
    protected $fillable = [
        'donortype_id',
        'donor_name',
        'description',
        'image_url',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'donortype_id' => 'integer',
    ];


    public function donortype()
    {
        return $this->belongsTo(\App\Models\Admin\Donortype::class);
    }
}
