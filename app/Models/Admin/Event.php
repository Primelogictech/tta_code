<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "tta_2022_events";
    protected $fillable = [
        'event_name',
        'from_date',
        'to_date',
        'description',
        'image_url',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'from_date' => 'date:Y-m-d' ,
        'to_date' => 'date:Y-m-d',
    ];



    public function schedules()
    {
        return $this->hasMany(\App\Models\Admin\Schedule::class);
    }
}
