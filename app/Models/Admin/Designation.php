<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    use HasFactory;
    protected $table = "tta_2022_designations";
    protected $fillable = ['name', 'status'];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

}
