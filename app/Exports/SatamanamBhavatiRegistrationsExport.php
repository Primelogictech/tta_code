<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\Registration;

class SatamanamBhavatiRegistrationsExport implements FromView
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $users=Registration::where('registration_type_name','ShathamanamBhavathi')->with('paymentdata')->get();
        return view('admin.exports.satamanamBhavatiRegistration', [
            'users' => $users
        ]);
    }
}
