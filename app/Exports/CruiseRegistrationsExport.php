<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\Cruise;

class CruiseRegistrationsExport implements FromView
{
    use Exportable;
   
    public function view(): View
    {
        $registrations=Cruise::orderBy('created_at', 'desc')->get();
        
        return view('admin.exports.cruiseRegistration', [
            'registrations' => $registrations
        ]);
    }
}
