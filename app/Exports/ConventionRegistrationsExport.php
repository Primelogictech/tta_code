<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\User;

class ConventionRegistrationsExport implements FromView
{
    use Exportable;
   
    public function view(): View
    {
        $users=User::get();
        return view('admin.exports.conventionRegistration', [
            'users' => $users
        ]);
    }
}
