<?php

namespace App\Exports;
use App\Models\Reservation;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class ExhibitRegistrationsExport implements FromView
{
    use Exportable;
   

    public function view(): View
    {
         $registrations=Reservation::with('paymentDetails','paymentDetails.paymentmethord')->orderBy('created_at', 'desc')->get();
        return view('admin.exports.exhibitRegistration', [
            'users' => $registrations
        ]);
    }
}

