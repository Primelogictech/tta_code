<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Admin\Venue;
use App\Models\TrackUser;
use App\Models\Admin\Menu;
use App\Models\Admin\Logo;
use DB;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('layouts.user.header', function ($view) {
            $data['venue'] = Venue::first();
           

           $data['menu'] = Menu::with( ['submenus' => function($query)
           {
                $query->where('status', 1)->orderBy(DB::raw('ISNULL(display_order), display_order'));
           }])
               ->where('parent_id', 0)->where('status', 1)->orderBy(DB::raw('ISNULL(display_order), display_order'))
               ->get();

            $data['left_logo'] = Logo::where('type', 'left')->first();
            $data['right_logo'] = Logo::where('type', 'right')->first();
            $view->with('data', $data);
        });

        view()->composer('layouts.user.footer', function ($view) {
            $data['visitors_count'] = DB::table('tta_2022_track_users')->count();
            $view->with('data', $data);
        });




        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
