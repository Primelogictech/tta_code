-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2021 at 05:07 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nriva_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state`, `country_id`, `state2`) VALUES
(1, 'Alabama', 231, 'AL'),
(2, 'Alaska', 231, 'AK'),
(3, 'American Somoa', 231, 'AS'),
(4, 'Arizona', 231, 'AZ'),
(5, 'Arkansas', 231, 'AR'),
(6, 'California', 231, 'CA'),
(7, 'Colorado', 231, 'CO'),
(8, 'Connecticut', 231, 'CT'),
(9, 'Delaware', 231, 'DE'),
(10, 'District Of Columbia', 231, 'DC'),
(11, 'Federated States Of Micronesia', 231, 'FM'),
(12, 'Florida', 231, 'FL'),
(13, 'Georgia', 231, 'GA'),
(14, 'Guam', 231, 'GU'),
(15, 'Hawaii', 231, 'HI'),
(16, 'Idaho', 231, 'ID'),
(17, 'Illinois', 231, 'IL'),
(18, 'Indiana', 231, 'IN'),
(19, 'Iowa', 231, 'IA'),
(20, 'Kansas', 231, 'KS'),
(21, 'Kentucky', 231, 'KY'),
(22, 'Louisiana', 231, 'LA'),
(23, 'Maine', 231, 'ME'),
(24, 'Marshall Islands', 231, 'MH'),
(25, 'Maryland', 231, 'MD'),
(26, 'Massachusetts', 231, 'MA'),
(27, 'Michigan', 231, 'MI'),
(28, 'Minnesota', 231, 'MN'),
(29, 'Mississippi', 231, 'MS'),
(30, 'Missouri', 231, 'MO'),
(31, 'Montana', 231, 'MT'),
(32, 'Nebraska', 231, 'NE'),
(33, 'Nevada', 231, 'NV'),
(34, 'New Hampshire', 231, 'NH'),
(35, 'New Jersey', 231, 'NJ'),
(36, 'New Mexico', 231, 'NM'),
(37, 'New York', 231, 'NY'),
(38, 'North Carolina', 231, 'NC'),
(39, 'North Dakota', 231, 'ND'),
(40, 'Northern Mariana Islands', 231, 'MP'),
(41, 'Ohio', 231, 'OH'),
(42, 'Oklahoma', 231, 'OK'),
(43, 'Oregon', 231, 'OR'),
(44, 'Palau', 231, 'PW'),
(45, 'Pennsylvania', 231, 'PA'),
(46, 'Puerto Rico', 231, 'PR'),
(47, 'Rhode Island', 231, 'RI'),
(48, 'South Carolina', 231, 'SC'),
(49, 'South Dakota', 231, 'SD'),
(50, 'Tennessee', 231, 'TN'),
(51, 'Texas', 231, 'TX'),
(52, 'Utah', 231, 'UT'),
(53, 'Vermont', 231, 'VT'),
(54, 'Virginia', 231, 'VA'),
(55, 'Virgin Islands', 231, 'VI'),
(56, 'Washington', 231, 'WA'),
(57, 'West Virginia', 231, 'WV'),
(58, 'Wisconsin', 231, 'WI'),
(59, 'Wyoming', 231, 'WY'),
(60, 'Other', 231, 'Other');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
