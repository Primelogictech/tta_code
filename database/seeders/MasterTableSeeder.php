<?php

namespace Database\Seeders;

use App\Models\Admin\Designation;
use App\Models\Admin\SponsorCategoryType;
use App\Models\Admin\Paymenttype;
use Illuminate\Database\Seeder;

class MasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 4; $i++) {
            Designation::create([
                'name' => "d" . $i,
            ]);
        }

        SponsorCategoryType::create([
            'name' => "Family / Individual",
        ]);
        SponsorCategoryType::create([
            'name' => "Donor",
        ]);

        Paymenttype::create([
            'name' => "Paypal",
        ]);


        Paymenttype::create([
            'name' => "Zelle",
        ]);


        Paymenttype::create([
            'name' => "Check",
        ]);


        Paymenttype::create([
            'name' => "Other",
        ]);
    }
}
