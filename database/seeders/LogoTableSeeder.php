<?php

namespace Database\Seeders;

use App\Models\Admin\Logo;
use Illuminate\Database\Seeder;

class LogoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 2; $i++) {
            Logo::create([
                'image_url' => "logo_" . $i,
            ]);
        }
    }
}
