<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->id();
            $table->string('chair_name', 100);
            $table->string('mobile_number', 20);
            $table->string('co_chair_name', 100);
            $table->string('program_name', 200);
            $table->string('Location', 200);
            $table->string('image_url', 200)->nullable();
            $table->date('date');
            $table->longText('page_content');
            $table->boolean('status')->default(1)->comment();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
