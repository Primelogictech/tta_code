function convertToSlug(Text) {
    return Text
        .toLowerCase()
        .replace(/ +/g, '-')
       .replace(/[^\w-]+/g, '')
}


$(document).ready(function () {
    $('.datatable').DataTable();

    $(".speakers-more-details").click(function () {
        $(this).parent().parent().parent().find(".speakers-content").toggleClass("speakers-details-height");
        if ($(this).text() == "Show More +") {
            $(this).text("Show Less - ");
        } else {
            $(this).text("Show More +");
        }
    });

    $('[name=title], [name=name]').on('keyup', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);

    });


    $('[name=title], [name=name]').on('blur', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);

    });


    $('[name=slug]').on('keyup', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);

    });

    $('[name=slug]').on('blur', function () {
        var title = $(this).val();
        var slug = convertToSlug(title);
        $('[name=slug]').val(slug);
    });
       
})


function IsCheckBoxChecked(thisVal) {
    if (thisVal.checked) {
           return true
        } else {
            return false
        }
}


function ajaxCall(url, method, data = null, callBack = null) {
    $.ajax({
        url: url,
        type: method,
        data: data,
        success: function (response) {
            if (callBack != null) {
                callBack(response);
            }
        }
    });
}



$('.status-btn').click(function() {
        if ($(this).text() == 'Active') {
        $(this).text('InActive')
            status = 0
        } else {
        $(this).text('Active')
            status = 1
        }
    data = {
        "_token": $('meta[name=csrf-token]').attr('content'),
    id: $(this).attr('data-id'),
    status: status
        }
        ajaxCall($(this).data('url'), 'put', data, afterStatusUpdate)

    function afterStatusUpdate(data) {

    }
    })

$('.delete-btn').click(function () {

    swal({
        title: "Are you sure ?",
        text: "Do you want to delete ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if(willDelete){
            data = {
                        "_token": $('meta[name=csrf-token]').attr('content'),
                        id: $(this).attr('data-id'),
                    }
                ajaxCall($(this).data('url'), 'delete', data, afterdelete)


            }
        });

})

function afterdelete(data) {
    location.reload();
}


$('.dropdown-element').change(function () {
    ajaxCall($(this).data('url') + '/' + $(this).val(), 'get', null , putDataToDropdown)
})


$('.dropdown-element-edit').change(function () {
    ajaxCall($(this).data('url') + '/' + $(this).val()+'/'+$(this).data('id'), 'get', null, putDataToDropdown)
})



function putDataToDropdown(data) {
    $('.dropdown-target').empty()
    $('.dropdown-target').append('<option selected disabled value="">Select</option>')
    for (let i = 0; i < data.length; i++) {
        $('.dropdown-target').append('<option  value="'+ data[i].id+'">'+data[i].name+'</option>')
    }
}


$(".invitees-nav-link").click(function () {



    //active_tab

    $(".invitees_active_tab_btn").removeClass("invitees-nav-link nav-item");
    $(".invitees_active_tab_btn").removeClass("invitees_active_tab_btn");
    $(this).addClass("invitees-nav-link nav-item");
    $(this).addClass("invitees_active_tab_btn");

    $(".invitees_active_tab").hide();
    $(".invitees_active_tab").removeClass("invitees_active_tab");
    $($(this).attr("target-id")).addClass("invitees_active_tab");
    $($(this).attr("target-id")).show();
});

$(".donors-nav-link").click(function () {
    //active_tab

    $(".donors_active_tab_btn").removeClass("donors-nav-link nav-item");
    $(".donors_active_tab_btn").removeClass("donors_active_tab_btn");
    $(this).addClass("donors-nav-link nav-item");
    $(this).addClass("donors_active_tab_btn");

    $(".donors_active_tab").hide();
    $(".donors_active_tab").removeClass("donors_active_tab");
    $($(this).attr("target-id")).addClass("donors_active_tab");
    $($(this).attr("target-id")).show();
});




