<?php

namespace Tests\Feature\Http\Controllers\HomePage;

use App\Models\Admin\Schedule;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\HomePage\ScheduleController
 */
class ScheduleControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $schedules = Schedule::factory()->count(3)->create();

        $response = $this->get(route('schedule.index'));

        $response->assertOk();
        $response->assertViewIs('schedule.index');
        $response->assertViewHas('schedules');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('schedule.create'));

        $response->assertOk();
        $response->assertViewIs('schedule.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\ScheduleController::class,
            'store',
            \App\Http\Requests\ScheduleStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $response = $this->post(route('schedule.store'));

        $response->assertRedirect(route('schedule.index'));
        $response->assertSessionHas('schedule.id', $schedule->id);

        $this->assertDatabaseHas(schedules, [ /* ... */ ]);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $schedule = Schedule::factory()->create();

        $response = $this->get(route('schedule.show', $schedule));

        $response->assertOk();
        $response->assertViewIs('schedule.show');
        $response->assertViewHas('schedule');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $schedule = Schedule::factory()->create();

        $response = $this->get(route('schedule.edit', $schedule));

        $response->assertOk();
        $response->assertViewIs('schedule.edit');
        $response->assertViewHas('schedule');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\HomePage\ScheduleController::class,
            'update',
            \App\Http\Requests\ScheduleUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $schedule = Schedule::factory()->create();

        $response = $this->put(route('schedule.update', $schedule));

        $schedule->refresh();

        $response->assertRedirect(route('schedule.index'));
        $response->assertSessionHas('schedule.id', $schedule->id);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $schedule = Schedule::factory()->create();

        $response = $this->delete(route('schedule.destroy', $schedule));

        $response->assertRedirect(route('schedule.index'));

        $this->assertDeleted($schedule);
    }
}
