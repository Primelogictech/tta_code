<?php

namespace Tests\Feature\Http\Controllers\Admin;

use App\Models\Admin\Invitee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;

use Tests\TestCase;

/**
 * @see \App\Http\Controllers\Admin\InviteeController
 */
class InviteeControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $invitees = Invitee::factory()->count(3)->create();

        $response = $this->get(route('invitee.index'));

        $response->assertOk();
        $response->assertViewIs('invitee.index');
        $response->assertViewHas('invitees');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('invitee.create'));

        $response->assertOk();
        $response->assertViewIs('invitee.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\Admin\InviteeController::class,
            'store',
            \App\Http\Requests\InviteeStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $name = $this->faker->name;
        $status = $this->faker->boolean;

        $response = $this->post(route('invitee.store'), [
            'name' => $name,
            'status' => $status,
        ]);

        $invitees = Invitee::query()
            ->where('name', $name)
            ->where('status', $status)
            ->get();
        $this->assertCount(1, $invitees);
        $invitee = $invitees->first();

        $response->assertRedirect(route('invitee.index'));
        $response->assertSessionHas('invitee.id', $invitee->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $invitee = Invitee::factory()->create();

        $response = $this->get(route('invitee.show', $invitee));

        $response->assertOk();
        $response->assertViewIs('invitee.show');
        $response->assertViewHas('invitee');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $invitee = Invitee::factory()->create();

        $response = $this->get(route('invitee.edit', $invitee));

        $response->assertOk();
        $response->assertViewIs('invitee.edit');
        $response->assertViewHas('invitee');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\Admin\InviteeController::class,
            'update',
            \App\Http\Requests\InviteeUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $invitee = Invitee::factory()->create();
        $name = $this->faker->name;
        $status = $this->faker->boolean;

        $response = $this->put(route('invitee.update', $invitee), [
            'name' => $name,
            'status' => $status,
        ]);

        $invitee->refresh();

        $response->assertRedirect(route('invitee.index'));
        $response->assertSessionHas('invitee.id', $invitee->id);

        $this->assertEquals($name, $invitee->name);
        $this->assertEquals($status, $invitee->status);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $invitee = Invitee::factory()->create();

        $response = $this->delete(route('invitee.destroy', $invitee));

        $response->assertRedirect(route('invitee.index'));

        $this->assertDeleted($invitee);
    }
}
