<?php

namespace Tests\Feature\Http\Controllers\Admin;

use App\Models\Admin\Logo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\Admin\LogoController
 */
class LogoControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $logos = Logo::factory()->count(3)->create();

        $response = $this->get(route('logo.index'));

        $response->assertOk();
        $response->assertViewIs('logo.index');
        $response->assertViewHas('logos');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('logo.create'));

        $response->assertOk();
        $response->assertViewIs('logo.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\Admin\LogoController::class,
            'store',
            \App\Http\Requests\LogoStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $image_url = $this->faker->word;
        $status = $this->faker->boolean;

        $response = $this->post(route('logo.store'), [
            'image_url' => $image_url,
            'status' => $status,
        ]);

        $logos = Logo::query()
            ->where('image_url', $image_url)
            ->where('status', $status)
            ->get();
        $this->assertCount(1, $logos);
        $logo = $logos->first();

        $response->assertRedirect(route('logo.index'));
        $response->assertSessionHas('logo.id', $logo->id);
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $logo = Logo::factory()->create();

        $response = $this->get(route('logo.show', $logo));

        $response->assertOk();
        $response->assertViewIs('logo.show');
        $response->assertViewHas('logo');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $logo = Logo::factory()->create();

        $response = $this->get(route('logo.edit', $logo));

        $response->assertOk();
        $response->assertViewIs('logo.edit');
        $response->assertViewHas('logo');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\Admin\LogoController::class,
            'update',
            \App\Http\Requests\LogoUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $logo = Logo::factory()->create();
        $image_url = $this->faker->word;
        $status = $this->faker->boolean;

        $response = $this->put(route('logo.update', $logo), [
            'image_url' => $image_url,
            'status' => $status,
        ]);

        $logo->refresh();

        $response->assertRedirect(route('logo.index'));
        $response->assertSessionHas('logo.id', $logo->id);

        $this->assertEquals($image_url, $logo->image_url);
        $this->assertEquals($status, $logo->status);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $logo = Logo::factory()->create();

        $response = $this->delete(route('logo.destroy', $logo));

        $response->assertRedirect(route('logo.index'));

        $this->assertDeleted($logo);
    }
}
